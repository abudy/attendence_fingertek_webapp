﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using HRWebAPI.DataAccess;
using HRWebAPI.Models.Repository.UnitOfWork;
using Microsoft.AspNet.Identity;


namespace HRWebAPI.Controllers
{
    [Authorize]
    public class vacationsmodelsController : ApiController
    {
        private attendenceschemaEntities db = new attendenceschemaEntities();

        // GET: api/vacationsmodels
        public IQueryable<object> Getvacationsmodels()
        {
            var userId = User.Identity.GetUserId();
            using (var unitOfWork = new UnitOfWork(new attendenceschemaEntities()))
            {
                var bId = db.aspnetusers.Find(userId).BranchesModelID;
                if (User.IsInRole("مدير النظام") || User.IsInRole("الموارد البشرية"))
                {
                    var list = unitOfWork.TimeOffs.GetVactionsAllWithInclude()
                        .Where(a => a.approvalstate == 0 && (a.aspnetuser.BranchesModelID.HasValue ? a.aspnetuser.BranchesModelID.Value == bId : false))
                        .Select(a => new
                        {
                            EventDescription =  a.message,
                            userName = a.aspnetuser.EmployeeName,
                            id = a.ID,
                            EventTitle = a.vacationtypemodel.vacationName,
                            Image = "https://www.1plusx.com/app/mu-plugins/all-in-one-seo-pack-pro/images/default-user-image.png",
                            When = a.date_requested.ToString("MMMM dd, yyyy")
                        });
                }


                return unitOfWork.TimeOffs.GetTimeOffForUser(User.Identity.GetUserId()).Where(a=> a.approvalstate == 0 ).OrderByDescending(aa => aa.date_requested).Take(8).ToList()
                    .Select(a => new
                    {
                        EventDescription =  a.message,
                        userName = a.aspnetuser.EmployeeName,
                        id = a.ID,
                        EventTitle = a.vacationtypemodel.vacationName,
                        Image = "https://www.1plusx.com/app/mu-plugins/all-in-one-seo-pack-pro/images/default-user-image.png",
                        When = a.date_requested.ToString("MMMM dd, yyyy")
                    }).AsQueryable();

            }

        }

        // GET: api/vacationsmodels/5
        [ResponseType(typeof(bool))]
        [Route("SetVacationsmodelState")]
        public bool SetVacationsmodelState(int id, int approved)
        {
            return true;

        }

        // GET: api/vacationsmodels/5
        [ResponseType(typeof(Models.ViewModel.TimeOffStatus))]
        public IHttpActionResult Getvacationsmodel(int id)
        {
            var userId = User.Identity.GetUserId();
            using (var unitOfWork = new UnitOfWork(new attendenceschemaEntities()))
            {
                var res = new Models.ViewModel.TimeOffStatus
                {

                    HoursAbsents = 0,
                    TimeOffApproved = unitOfWork.TimeOffs.GetVactionsAllWithInclude().Where(a => a.ApplicationUserId == userId && a.approvalstate == 1).Count(),
                    TimeOffWaiting = unitOfWork.TimeOffs.GetVactionsAllWithInclude().Where(a => a.ApplicationUserId == userId && a.approvalstate == 0).Count(),
                    TimeOffRejected = unitOfWork.TimeOffs.GetVactionsAllWithInclude().Where(a => a.ApplicationUserId == userId && a.approvalstate == 3).Count()
                };

                return Ok(res);
            }
        }

        // PUT: api/vacationsmodels/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putvacationsmodel(int id, vacationsmodel vacationsmodel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vacationsmodel.ID)
            {
                return BadRequest();
            }

            db.Entry(vacationsmodel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!vacationsmodelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/vacationsmodels
        [ResponseType(typeof(vacationsmodel))]
        public IHttpActionResult Postvacationsmodel(vacationsmodel vacationsmodel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.vacationsmodels.Add(vacationsmodel);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = vacationsmodel.ID }, vacationsmodel);
        }

        // DELETE: api/vacationsmodels/5
        [ResponseType(typeof(vacationsmodel))]
        public IHttpActionResult Deletevacationsmodel(int id)
        {
            vacationsmodel vacationsmodel = db.vacationsmodels.Find(id);
            if (vacationsmodel == null)
            {
                return NotFound();
            }

            db.vacationsmodels.Remove(vacationsmodel);
            db.SaveChanges();

            return Ok(vacationsmodel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool vacationsmodelExists(int id)
        {
            return db.vacationsmodels.Count(e => e.ID == id) > 0;
        }
    }
}