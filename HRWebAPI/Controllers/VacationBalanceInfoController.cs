﻿using HRWebAPI.DataAccess;
using HRWebAPI.Models.Repository.UnitOfWork;
using HRWebAPI.Models.ViewModel.TimeOffBalanceInfo;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HRWebAPI.Controllers
{

    public class VacationBalanceInfoController : ApiController
    {
        // GET: api/VacationBalanceInfo
        public List<TimeOffBalanceInfoViewModel> Getvacationsmodels()
        {
            var userId = User.Identity.GetUserId();
            using (var unitOfWork = new UnitOfWork(new attendenceschemaEntities()))
            {
                return unitOfWork.TimeOffs.GetTimeOffBalanceForUser(User.Identity.GetUserId());
            }
        }

    }
}
