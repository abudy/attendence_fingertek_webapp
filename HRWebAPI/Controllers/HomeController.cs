﻿using HRWebAPI.DataAccess;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HRWebAPI.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        // GET: Home/SetVacationsmodelState
        [Authorize]
        public JsonResult SetVacationsmodelState(int id, int approved)
        {
            var userId = User.Identity.GetUserId();
            using (var unitOfWork = new attendenceschemaEntities())
            {
                try
                {
                    var vacation = unitOfWork.vacationsmodels.Find(id);
                    vacation.approvalstate = approved == 1 ? 1 : 3;

                    unitOfWork.Entry(vacation).State = System.Data.Entity.EntityState.Modified;
                    unitOfWork.SaveChanges();

                    return Json(true,JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json(false, JsonRequestBehavior.DenyGet);
                }
            }
        }
    }
}
