﻿using HRWebAPI.DataAccess;
using HRWebAPI.Models.ViewModel.EmployeeInfo;
using HRWebAPI.Models.ViewModel.Personnal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRWebAPI.Models.Repository.Users
{
    public class UserRepository : Repository.Repository<aspnetuser>, IUserRepository
    {
        public UserRepository(attendenceschemaEntities context)
            : base(context)
        {
        }

        public attendenceschemaEntities ApplicationDbContext
        {
            get
            {
                return Context as attendenceschemaEntities;
            }
        }

        public int? GetUserBranch(string Id)
        {
            var user = ApplicationDbContext.aspnetusers.Find(Id);
            return user.BranchesModelID;
        }
        public void UpdateUser(string Id)
        {
            var user = ApplicationDbContext.aspnetusers.Find(Id);
            ApplicationDbContext.Entry(user).State = System.Data.Entity.EntityState.Modified;
        }
        public void UpdateUserEmployeeInformations(EmployeeInfoViewModel empInfo)
        {
            var user = ApplicationDbContext.aspnetusers.Find(empInfo.userId);
            user.TeamsModelID = empInfo.TeamsModelID;
            user.TitleModelID = empInfo.TitleModelID;
            user.perssonalEmail = empInfo.workEmail;
            user.PositionModelID = empInfo.PositionModelID;
            user.ApplicationUserId = empInfo.ApplicationUserId;
            user.dateEnrollDay = user.dateEnrollDay.HasValue ? empInfo.dateEnrollDay.Value.AddDays(empInfo.dateEnrollDay.Value.Subtract(user.dateEnrollDay.Value).TotalDays) : empInfo.dateEnrollDay;
            user.skype = empInfo.skype;
            user.linkedln = empInfo.linkedln;
            user.drivingLicense = empInfo.drivingLicense;
            user.firstName = empInfo.firstName;
            user.lastName = empInfo.lastName;
            user.EmploymentStateModelID = empInfo.EmploymentStateModelID;

            ApplicationDbContext.Entry(user).State = System.Data.Entity.EntityState.Modified;
        }

        public EmployeeInfoViewModel UserToEmployeeInfo(string userId)
        {
            var user = ApplicationDbContext.aspnetusers.Find(userId);
            return new EmployeeInfoViewModel()
            {
                TeamsModelID = user.TeamsModelID,
                ApplicationUserId = user.ApplicationUserId,
                dateEnrollDay = user.dateEnrollDay,
                drivingLicense = user.drivingLicense,
                EmploymentStateModelID = user.EmploymentStateModelID,
                PositionModelID = user.PositionModelID,
                firstName = user.firstName,
                lastName = user.lastName,
                linkedln = user.linkedln,
                skype = user.skype,
                TitleModelID = user.TitleModelID,
                userId = user.Id,
                workEmail = user.perssonalEmail
            };
        }
        public PersonnalViewModel UserToPersonnalInfo(string userId)
        {
            var user = ApplicationDbContext.aspnetusers.Find(userId);

            return new PersonnalViewModel()
            {
                city = user.city,
                street_2 = user.street_2,
                street_1 = user.street_1,
                CountryModelID = user.CountryModelID,
                dateOfBirth = user.empBirthDay,
                homePhone = user.homePhone,
                workPhone = user.workPhone,
                MaritalStatusModelID = user.MaritalStatusModelID,
                NationalityModelID = user.NationalityModelID,
                postCode = user.postCode,
                state = user.state,
                userId = userId,
                firstName = String.IsNullOrEmpty(user.firstName) ? user.EmployeeName : user.firstName,
                lastName = user.lastName
            };
        }

        public IQueryable<aspnetuser> GetAllUsersWithInclude()
        {
            return ApplicationDbContext.aspnetusers.Include("positionmodel");
        }
    }
}