﻿using HRWebAPI.DataAccess;
using HRWebAPI.Models.ViewModel.EmployeeInfo;
using HRWebAPI.Models.ViewModel.Personnal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRWebAPI.Models.Repository.Users
{
    public interface IUserRepository : IRepository<aspnetuser>
    {
        int? GetUserBranch(string Id);
        void UpdateUser(string Id);

        void UpdateUserEmployeeInformations(EmployeeInfoViewModel empInfo);
        EmployeeInfoViewModel UserToEmployeeInfo(string userId);

        PersonnalViewModel UserToPersonnalInfo(string userId);

        IQueryable<aspnetuser> GetAllUsersWithInclude();
    }
}
