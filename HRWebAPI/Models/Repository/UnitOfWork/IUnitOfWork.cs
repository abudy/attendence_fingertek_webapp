﻿using HRWebAPI.Models.Repository.TimeOff;
using HRWebAPI.Models.Repository.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRWebAPI.Models.Repository.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        ITimeOffRepository TimeOffs { get;  }
        IUserRepository Users { get; }
        int Complete(bool sendNotify, string optionalMsg);
    }
}
