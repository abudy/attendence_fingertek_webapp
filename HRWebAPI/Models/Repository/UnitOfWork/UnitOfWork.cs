﻿using HRWebAPI.Models.Repository.TimeOff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HRWebAPI.DataAccess;
using HRWebAPI.Models.Repository.Users;

namespace HRWebAPI.Models.Repository.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        protected readonly attendenceschemaEntities  _context;

        public UnitOfWork(attendenceschemaEntities context)
        {
            _context = context;
            TimeOffs = new TimeOffRepository(_context);
            Users = new UserRepository(_context);
        }

        public ITimeOffRepository TimeOffs { get; private set; }
        public IUserRepository Users { get; private set; }

        public int Complete(bool sendNotify = true, string optionalMsg = "Action Completed Successfully")
        {
            try
            {
                int res = _context.SaveChanges();
                //if (sendNotify)
                //    Startup.MiscellaneousMethod.ActionCompletionNotify(optionalMsg);

                return res;
            }
            catch (Exception ll)
            {
             //   Startup.MiscellaneousMethod.ActionCompletionNotify("Operation Failed: " + ll.Message);
                return 0;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

    }
}