﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRWebAPI.DataAccess;
using HRWebAPI.Models.ViewModel.TimeOffBalanceInfo;

namespace HRWebAPI.Models.Repository.TimeOff
{
    public interface ITimeOffRepository
    {
        IQueryable<vacationsmodel> GetTimeOffForBranch(int brId);
        IQueryable<vacationsmodel> GetTimeOffForUser(string userId);
        void UpdateTimeOff(vacationsmodel model);
        IQueryable<vacationsmodel> GetVactionsAllWithInclude();
        List<TimeOffBalanceInfoViewModel> GetTimeOffBalanceForUser(string userId);
        void ChangeTimeOffStatus(int id, int state);
    }
}
