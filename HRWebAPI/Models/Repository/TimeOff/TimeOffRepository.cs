﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HRWebAPI.DataAccess;
using System.Data.Entity;
using HRWebAPI.Models.ViewModel.TimeOffBalanceInfo;
using System.Globalization;

namespace HRWebAPI.Models.Repository.TimeOff
{
    static class DateTimeExtensions
    {
        static GregorianCalendar _gc = new GregorianCalendar();
        public static int GetWeekOfMonth(this DateTime time)
        {
            DateTime first = new DateTime(time.Year, time.Month, 1);
            return time.GetWeekOfYear() - first.GetWeekOfYear() + 1;
        }

        static int GetWeekOfYear(this DateTime time)
        {
            return _gc.GetWeekOfYear(time, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
        }
    }

    public class TimeOffRepository : Repository<vacationsmodel>, ITimeOffRepository
    {
        public TimeOffRepository(attendenceschemaEntities context)
            : base(context)
        {
        }

        public attendenceschemaEntities ApplicationDbContext
        {
            get
            {
                return Context as attendenceschemaEntities;
            }
        }

        public void ChangeTimeOffStatus(int id, int state)
        {
            var vac = Get(id);
            vac.approvalstate = (int)state;

            UpdateTimeOff(vac);
        }

        public IQueryable<vacationsmodel> GetTimeOffForBranch(int brId)
        {
            return ApplicationDbContext.vacationsmodels.Include(aa => aa.aspnetuser)
                .Where(a => a.aspnetuser.BranchesModelID.HasValue ? a.aspnetuser.BranchesModelID == brId : false);
        }

        public IQueryable<vacationsmodel> GetTimeOffForUser(string userId)
        {
            return ApplicationDbContext.vacationsmodels.Include(aa => aa.vacationtypemodel).Include(aa=>aa.aspnetuser)
                .Where(a => a.ApplicationUserId == userId);
        }

        public List<TimeOffBalanceInfoViewModel> GetTimeOffBalanceForUser(string userId)
        {
            List<TimeOffBalanceInfoViewModel> res = new List<TimeOffBalanceInfoViewModel>();
            int currentYear = System.DateTime.Now.Year;
            int currentMonth = System.DateTime.Now.Month;
            int weekNumber = System.DateTime.Now.GetWeekOfMonth();

            // get timeOff values
            foreach (var timeOfftype in ApplicationDbContext.vacationtypemodels.ToList())
            {
                // get all balances
                var balance = ApplicationDbContext.vacationsmodels.Where(aa => aa.approvalstate == 1 && aa.ApplicationUserId == userId
                                        && aa.VacationTypeModelID == timeOfftype.ID);

                // this will get the available balance for each type of policy
                switch (timeOfftype.policyResetEvery)
                {
                    case 1:
                        {
                            balance = balance.Where(a => a.fromDate.Year == currentYear);
                            break;
                        }
                    case 2:
                        {
                            balance = balance.Where(a => a.fromDate.Year == currentYear && a.fromDate.Month == currentMonth);
                            break;
                        }
                    case 3:
                        {
                            balance = balance.Where(a => a.fromDate.Year == currentYear && a.fromDate.Month == currentMonth && a.fromDate.GetWeekOfMonth() == weekNumber);
                            break;
                        }
                }

                TimeOffBalanceInfoViewModel bala = new TimeOffBalanceInfoViewModel()
                {
                    BackgroundColor = "A",
                    Count = balance.Where(aa => aa.VacationTypeModelID == timeOfftype.ID).Count(),
                    Icon = "" + timeOfftype.policyResetEvery,
                    Name = timeOfftype.vacationName,
                    balanceDescription= timeOfftype.balance.HasValue ? (timeOfftype.balance.Value - balance.Count()).ToString() + " Of " + timeOfftype.balance.Value + " Used"  : "" + balance.Count()
                };

                res.Add(bala);
            }

            return res;
        }

        public void UpdateTimeOff(vacationsmodel model)
        {
            ApplicationDbContext.Entry(model).State = EntityState.Modified;
        }

        public IQueryable<vacationsmodel> GetVactionsAllWithInclude()
        {
            return ApplicationDbContext.vacationsmodels.Include(aa=>aa.branchesmodel).Include(aa=>aa.aspnetuser).Include(aa=>aa.vacationtypemodel);
        }
    }
}