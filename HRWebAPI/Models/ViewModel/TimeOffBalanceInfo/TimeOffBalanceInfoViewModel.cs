﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace HRWebAPI.Models.ViewModel.TimeOffBalanceInfo
{
    public class TimeOffBalanceInfoViewModel
    {
        public string Name { get; set; }

        public int Count { get; set; }

        public string Icon { get; set; }

        public string BackgroundColor { get; set; }

        public string balanceDescription { get; set; }
    }
}