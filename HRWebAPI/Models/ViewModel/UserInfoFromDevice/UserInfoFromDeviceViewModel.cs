﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.UserInfoFromDevice
{
    public class UserInfoFromDeviceViewModel
    {
        public int enrollNo { get; set; }
        public string name { get; set; }
        public string pwd { get; set; }
        public int level { get; set; }
        public bool status { get; set; }

        public int branchID { get; set; }
    }
}