﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.Branch
{
    public class BranchViewModel
    {
        public int DT_RowId { get; set; }
        public int branch_id { get; set; }
        public string branch_name { get; set; }
        public string branch_ip { get; set; }
        public int branch_port { get; set; }
        public string branch_key { get; set; }
        public string branch_location { get; set; }
        public int branch_status { get; set; }
    }

    public class BranchEditViewModel
    {
        public int BranchID { get; set; }
        public string BranchName
        {
            get; set;
        }
        public string BranchIP { get; set; }
        public int BranchPort { get; set; }
        public string BranchKey { get; set; }
        public string BranchLocation { get; set; }
    }
}