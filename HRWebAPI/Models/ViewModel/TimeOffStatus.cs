﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRWebAPI.Models.ViewModel
{
    public class TimeOffStatus
    {
        public string UserName { get; set; }
        public string Id { get; set; }
        public string JobDescription { get; set; }
        public int HoursAbsents { get; set; }
        public int TimeOffApproved { get; set; }
        public int TimeOffWaiting { get; set; }
        public int TimeOffRejected { get; set; }
    }
}