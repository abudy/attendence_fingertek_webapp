﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.UserNotification
{
    public class UserNotificationViewModel
    {
        public int id;
        public DateTime eventDate;
        public string description;
        public string url;
        public string madeBy;
        public bool shown;
        public int countNotshown;
    }
}