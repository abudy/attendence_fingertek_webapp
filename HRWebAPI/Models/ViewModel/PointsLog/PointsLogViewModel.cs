﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.PointsLog
{
    public class PointsLogViewModel
    {
        public int DT_RowId { get; set; }

        public int CheckID { get; set; }

        public string CheckEmpID { get; set; }

        public string EmployeeAName { get; set; }

        public string CheckDate { get; set; }

        public string CheckDay { get; set; }

        public string CheckTimeIn { get; set; }

        public string CheckName { get; set; }

        public string CheckTimeOut { get; set; }

        public int CheckType { get; set; }

        public int CheckBranch { get; set; }

        public int CheckRLength { get; set; }

        public string CheckRBranch { get; set; }

        public string EmployeeID { get; set; }

        public string EmployeeBranch { get; set; }

        public string CheckRType { get; set; }
    }

    public class PointsViewModel
    {

    }
}