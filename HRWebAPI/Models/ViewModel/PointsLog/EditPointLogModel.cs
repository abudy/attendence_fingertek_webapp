﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.PointsLog
{
    public class EditPointLogModel
    {
        public int? Branch { get; set; }
        public int? PointID { get; set; }
        public string PointFID { get; set; }
        public DateTime? PointDate { get; set; }
        public string PointIn { get; set; }
        public string PointOut { get; set; }
        public int? PointType { get; set; }
    }
}