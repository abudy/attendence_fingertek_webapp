﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.PointsLog
{
    public class CreatePointLogModel
    {
        public string PointsEmp { get; set; }
        public int PointsType { get; set; }
        public DateTime? PointsDate { get; set; }
        public string PointsStart { get; set; }
        public string PointsEnd { get; set; }
        public int BranchPoint { get; set; }
    }
}