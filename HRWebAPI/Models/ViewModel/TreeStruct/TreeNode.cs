﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.TreeStruct
{
    [JsonConverter(typeof(TreeNodeConverter))]
    public class TreeNode
    {
        public readonly Dictionary<string, TreeNode> children = new Dictionary<string, TreeNode>();

        public readonly string id;
        public string text;
        public TreeNode Parent { get; private set; }
        public bool selectedForRoles;
        public bool selectedForUsers;

        public TreeNode(string text, string id, bool selectedForRoles = false, bool selectedForUsers = false)
        {
            this.id = id;
            this.text = text;
            this.selectedForRoles = selectedForRoles;
            this.selectedForUsers = selectedForUsers;
        }

        public TreeNode GetChild(string id)
        {
            return this.children[id];
        }

        public bool ContainsChild(string id)
        {
            return this.children.ContainsKey(id) ? true : false;
        }

        public void Add(TreeNode item)
        {
            if (item.Parent != null)
            {
                item.Parent.children.Remove(item.id);
            }

            item.Parent = this;
            this.children.Add(item.id, item);
        }

        public IEnumerator<TreeNode> GetEnumerator()
        {
            return this.children.Values.GetEnumerator();
        }


        public List<TreeNode> GetNodes()
        {
            return this.children.Values.ToList();
        }

        public int Count
        {
            get { return this.children.Count; }
        }
    }

    class TreeNodeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            // we can serialize everything that is a TreeNode
            return typeof(TreeNode).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            // we currently support only writing of JSON
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            // we serialize a node by just serializing the _children dictionary
            var node = value as TreeNode;

            serializer.Serialize(writer, node.children);
        }
    }
}