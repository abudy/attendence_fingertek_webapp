﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HRWebAPI.Models.ViewModel.User
{
    public class UserViewModel
    {
        public string DT_RowId { get; set; }

        public string EmployeeEName { get; set; }

        public int EmployeeDepart { get; set; }

        public string Id { get; set; }

        public int branch_id { get; set; }

        public string EmployeeID { get; set; }

        public string EmployeeAName { get; set; }

        public string depart_name { get; set; }

        public int EmployeeGender { get; set; }

        public string EmployeeEmail { get; set; }

        public string branch_name { get; set; }

        public string EmployeeBirthDate { get; set; }

        public string EmployeeLastLogin { get; set; }

        public string schedule_name { get; set; }

        public string EmployeeEnable { get; set; }

        public string EmployeeIsManager { get; set; }

        public int EmployeeShow { get; set; }

        public string EmpcakeHRID { get; set; }

        public string roleId { get; set; }
        public string roleName { get; set; }
    }

    public class EditUserViewModel
    {
        public string EmpID { get; set; }
        public int Branch { get; set; }
        public string EmpArName { get; set; }
        public string EmpEnName { get; set; }
        public int EmpDepart { get; set; }
        public string EmpEmail { get; set; }
        public int EmpFP { get; set; }
        public int EmpGender { get; set; }
        public DateTime EmpBirth { get; set; }
        public int EmpSchedule { get; set; }
        public int EmpEnable { get; set; }
        public int EmployeeShow { get; set; }
        public string EmpcakeHRID { get; set; }
        public string password { get; set; }
        public string confirmPassword { get; set; }
        public string role { get; set; }
    }
}
