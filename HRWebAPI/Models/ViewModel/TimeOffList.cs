﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRWebAPI.Models.ViewModel
{
    public class TimeOffList
    {
        public string EventTitle { get; set; }
        public string EventDescription { get; set; }
        public string Image { get; set; }
        public string When { get; set; }
    }
}