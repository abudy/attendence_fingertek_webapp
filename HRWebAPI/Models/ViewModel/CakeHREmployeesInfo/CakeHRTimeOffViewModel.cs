﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.CakeHREmployeesInfo
{
    public class CakeHRTimeOffViewModel
    {
        public int id { get; set; }
        public string day_part { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public string hours { get; set; }
        public string approval { get; set; }
        public string message { get; set; }
        public string date_requested { get; set; }
        public int timeoff_id { get; set; }
        public ApplicationUser user { get; set; }
        public string userId { get; set; }

    }

    public class User
    {
        public int id { get; set; }
        public string first { get; set; }
        public string last { get; set; }
        public string userId { get; set; }
    }
}