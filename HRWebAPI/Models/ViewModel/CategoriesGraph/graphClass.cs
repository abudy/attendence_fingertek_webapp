﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.CategoriesGraph
{
    public class CategoriesGraphViewModel
    {
        public List<CategoriesList> list { get; set; }
        public List<GraphConnections> connection { get; set; }
    }

    public class CategoriesList
    {
        public string key { get; set; }
        public string question { get; set; }
        public List<DetailedValues> actions { get; set; }
        public string category { get; set; }
        public string text { get; set; }
    }

    public class DetailedValues
    {
        public string text { get; set; }
        public string figure { get; set; }
        public string fill { get; set; }
    }

    public class GraphConnections
    {
        public string from { get; set; }
        public string to { get; set; }
        public int answer { get; set; }
    }
}