﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.ActivityLog
{
    public class ActivityLogViewModel
    {
        public int DT_RowId { get; set; }

        public int sys_id { get; set; }

        public string manager_name { get; set; }

        public string sys_action { get; set; }

        public string sys_date { get; set; }
    }
}