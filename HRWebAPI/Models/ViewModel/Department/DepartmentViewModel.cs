﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.Department
{
    public class DepartmentViewModel
    {
        public int DT_RowId { get; set; }
        public int depart_id { get; set; }
        public int depart_branch { get; set; }
        public string depart_name { get; set; }
        public string depart_manager { get; set; }
        public string EmployeeAName { get; set; }
        public string branch_name { get; set; }
    }

    public class EditDepartmentViewModel
    {
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int DepartmentBranch { get; set; }
        public string DepartmentManager { get; set; }
    }
}