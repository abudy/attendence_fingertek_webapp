﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.DevicePoint
{
    public class DevicePointViewModel
    {
        public string EnrollNo { get; set; }
        public int IsInValid { get; set; }
        public int AttState { get; set; }
        public int VerifyMode { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }
        public int Second { get; set; }
        public int WorkCode { get; set; }
        public int branchID { get; set; }
    }
}