﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.TimeOffBalance
{
    public class TimeOffBalanceViewModel
    {
        public string timeOffTypeName { get; set; }

        public string typelimit { get; set; }

        public string balance { get; set; }

        public string message { get; set; }

        public string Id { get; set; }
    }
}