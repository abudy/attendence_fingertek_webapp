﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.Schedule
{
    public class SchedulesGeneralInfo
    {
        public int ID { get; set; }

        public int numberOfWorkDaysPerWeek { get; set; }

        public int numOfWorkMinutesPerWeek { get; set; }

        public double numOfMinWorkPerSelectedDate { get; set; }
        
        public int numberOfWorkDaysPerSelectedDate { get; set; }
    }
}