﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.Schedule
{
    public class ScheduleViewModel
    {
        public int schedule_id { get; set; }

        public string schedule_name { get; set; }

        public string schedule_allowtime { get; set; }

        public string schedule_saturday { get; set; }

        public string schedule_saturday2 { get; set; }

        public string schedule_sunday { get; set; }

        public string schedule_sunday2 { get; set; }

        public string schedule_monday { get; set; }

        public string schedule_monday2 { get; set; }

        public string schedule_tuesday { get; set; }

        public string schedule_tuesday2 { get; set; }

        public string schedule_wednesday { get; set; }

        public string schedule_wednesday2 { get; set; }

        public string schedule_thursday { get; set; }

        public string schedule_thursday2 { get; set; }

        public string schedule_friday { get; set; }

        public string schedule_friday2 { get; set; }

        public string DT_RowId { get; set; }
    }

    public class ScheduleViewModelPosting
    {
        public int ScheduleID { get; set; }
        public string ScheduleName { get; set; }
        public int ScheduleAllowTime { get; set; }
        public string ScheduleSaturday { get; set; }
        public string ScheduleSaturday2 { get; set; }
        public string ScheduleSunday { get; set; }
        public string ScheduleSunday2 { get; set; }
        public string ScheduleMonday { get; set; }
        public string ScheduleMonday2 { get; set; }
        public string ScheduleTuesday { get; set; }
        public string ScheduleTuesday2 { get; set; }
        public string ScheduleWednesday { get; set; }
        public string ScheduleWednesday2 { get; set; }
        public string ScheduleThursday { get; set; }
        public string ScheduleThursday2 { get; set; }
        public string ScheduleFriday { get; set; }
        public string ScheduleFriday2 { get; set; }
    }
}