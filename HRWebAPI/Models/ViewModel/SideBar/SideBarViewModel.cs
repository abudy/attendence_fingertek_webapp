﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.SideBar
{
    public class SideBarViewModel
    {
        [DisplayName("المتحكم المطلوب ")]
        public string ControllerName
        { get; set; }

        [DisplayName("الدالة المطلوبة ")]
        public string ActionMethodName
        { get; set; }

        [DisplayName("الاسم بالعربي")]
        public string Description
        { get; set; }

        [DisplayName("الاسم بالانكليزي")]
        public string DescriptionEng
        { get; set; }

        [DisplayName("صلاحيات من (اتركة فراغا اذا كان للجميع)")]
        public string AccessedByRoles { get; set; }

        [DisplayName("اختر الايقونة: ")]
        public string selectedIcon { get; set; }

    }
}