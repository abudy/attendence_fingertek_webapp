﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
 
namespace HRWebAPI.Models.ViewModel.EmployeeInfo
{
    public class EmployeeInfoViewModel
    {
        public string userId { get; set; }

        [Display(Name = "Title")]
        public int? TitleModelID { get; set; }

        [Display(Name = "First Name")]
        public string firstName { get; set; }

        [Display(Name = "Last Name")]
        public string lastName { get; set; }

        [Display(Name = "Position")]
        public int? PositionModelID { get; set; }

        [Display(Name = "Joined")]
        public DateTime? dateEnrollDay { get; set; }

        [Display(Name ="Reports To")]
        public string ApplicationUserId { get; set; }

        [Display(Name = "Team")]
        public int? TeamsModelID { get; set; }

        [Display(Name = "Employment Status")]
        public int? EmploymentStateModelID { get; set; }

        [Display(Name = "Driving License")]
        public string drivingLicense { get; set; }

        [Display(Name = "Linkdln Profile")]
        public string linkedln { get; set; }

        [Display(Name = "Skype Profile")]
        public string skype { get; set; }

        [Display(Name = "Work Email")]
        public string workEmail { get; set; }

        public HttpPostedFileBase image { get; set; }
    }

    public class EmployeeInfoViewModelJson
    {
        public string userId { get; set; }
        
        public string Name { get; set; }
        
        public string position { get; set; }
        
        public string dateEnrollDay { get; set; }

        public string away { get; set; }
    }

    public class RegisterEmployeeHR
    {
        [Required(ErrorMessage ="Field {0} is required")]
        [Display(Name ="Work Email")]
        public string email { get; set; }

        [Required(ErrorMessage = "Field {0} is required")]
        [Display(Name = "Name")]
        public string name { get; set; }

        [Required(ErrorMessage = "Field {0} is required")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [Required(ErrorMessage = "Field {0} is required")]
        [Display(Name = "Employee start date")]
        public DateTime enrolledOn { get; set; }

        [Required(ErrorMessage = "Field {0} is required")]
        [Display(Name = "Image")]
        public HttpPostedFileBase file { get; set; }
    }
}