﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.TeamLeaders
{
    public class TeamLeadersViewModel
    {
        public string userId { get; set; }

        public string userName { get; set; }

        public string selected { get; set; }
    }

    public class PostTeamLeadersViewModel
    {
        [Required]
        public int teamID { get; set; }

        [Required]
        [Display(Name = "Team Name")]
        public string teamName { get; set; }

        public string[] state { get; set; }
    }
}