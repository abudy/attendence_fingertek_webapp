//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HRWebAPI.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class notificationsmodel
    {
        public int ID { get; set; }
        public System.DateTime eventDate { get; set; }
        public string eventDescription { get; set; }
        public string madeByName { get; set; }
        public string madeById { get; set; }
        public string eventURL { get; set; }
        public string ApplicationUserID { get; set; }
        public bool shown { get; set; }
    
        public virtual aspnetuser aspnetuser { get; set; }
    }
}
