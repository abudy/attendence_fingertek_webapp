//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HRWebAPI.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class aspnetuser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public aspnetuser()
        {
            this.actioncompletionmodels = new HashSet<actioncompletionmodel>();
            this.aspnetuserclaims = new HashSet<aspnetuserclaim>();
            this.aspnetuserlogins = new HashSet<aspnetuserlogin>();
            this.aspnetusers1 = new HashSet<aspnetuser>();
            this.assestsmodels = new HashSet<assestsmodel>();
            this.checkpointlogmodels = new HashSet<checkpointlogmodel>();
            this.checkpointsummerymodels = new HashSet<checkpointsummerymodel>();
            this.emergenceycontactmodels = new HashSet<emergenceycontactmodel>();
            this.notesmodels = new HashSet<notesmodel>();
            this.notificationsmodels = new HashSet<notificationsmodel>();
            this.vacationsmodels = new HashSet<vacationsmodel>();
            this.aspnetroles = new HashSet<aspnetrole>();
        }
    
        public string Id { get; set; }
        public Nullable<int> EnrollNumber { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeName_En { get; set; }
        public string EmployeePassword { get; set; }
        public Nullable<int> EmployeeGender { get; set; }
        public string EmployeeBirthDate { get; set; }
        public Nullable<System.DateTime> EmployeeLastLogin { get; set; }
        public bool EmployeeShow { get; set; }
        public Nullable<int> SchedulesModelID { get; set; }
        public Nullable<int> DepartmentsModelID { get; set; }
        public Nullable<int> BranchesModelID { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }
        public string userCakeHRNumber { get; set; }
        public Nullable<int> TeamsModelID { get; set; }
        public Nullable<System.DateTime> empBirthDay { get; set; }
        public Nullable<System.DateTime> dateEnrollDay { get; set; }
        public string ApplicationUserId { get; set; }
        public bool branchManager { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string perssonalEmail { get; set; }
        public string linkedln { get; set; }
        public string drivingLicense { get; set; }
        public string skype { get; set; }
        public string street_1 { get; set; }
        public string street_2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postCode { get; set; }
        public string homePhone { get; set; }
        public string workPhone { get; set; }
        public Nullable<int> TitleModelID { get; set; }
        public Nullable<int> EmploymentStateModelID { get; set; }
        public Nullable<int> PositionModelID { get; set; }
        public Nullable<int> CountryModelID { get; set; }
        public Nullable<int> NationalityModelID { get; set; }
        public Nullable<int> MaritalStatusModelID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<actioncompletionmodel> actioncompletionmodels { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<aspnetuserclaim> aspnetuserclaims { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<aspnetuserlogin> aspnetuserlogins { get; set; }
        public virtual employmentstatemodel employmentstatemodel { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<aspnetuser> aspnetusers1 { get; set; }
        public virtual aspnetuser aspnetuser1 { get; set; }
        public virtual branchesmodel branchesmodel { get; set; }
        public virtual countrymodel countrymodel { get; set; }
        public virtual departmentsmodel departmentsmodel { get; set; }
        public virtual maritalstatusmodel maritalstatusmodel { get; set; }
        public virtual nationalitymodel nationalitymodel { get; set; }
        public virtual positionmodel positionmodel { get; set; }
        public virtual schedulesmodel schedulesmodel { get; set; }
        public virtual titlemodel titlemodel { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<assestsmodel> assestsmodels { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<checkpointlogmodel> checkpointlogmodels { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<checkpointsummerymodel> checkpointsummerymodels { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<emergenceycontactmodel> emergenceycontactmodels { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<notesmodel> notesmodels { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<notificationsmodel> notificationsmodels { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<vacationsmodel> vacationsmodels { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<aspnetrole> aspnetroles { get; set; }
    }
}
