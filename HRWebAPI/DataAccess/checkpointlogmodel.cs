//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HRWebAPI.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class checkpointlogmodel
    {
        public int ID { get; set; }
        public System.DateTime CurrentDay { get; set; }
        public Nullable<System.DateTime> CheckTimeIn { get; set; }
        public Nullable<System.DateTime> CheckTimeOut { get; set; }
        public Nullable<int> CheckTypeModelsID { get; set; }
        public Nullable<int> lateMin { get; set; }
        public string recordIdentifer { get; set; }
        public string userId { get; set; }
        public Nullable<int> BranchesModelID { get; set; }
        public Nullable<int> CheckPointSummeryModelID { get; set; }
        public Nullable<int> vacationMin { get; set; }
        public Nullable<int> extraTime { get; set; }
        public string ApplicationUserId { get; set; }
    
        public virtual aspnetuser aspnetuser { get; set; }
        public virtual branchesmodel branchesmodel { get; set; }
        public virtual checktypemodel checktypemodel { get; set; }
        public virtual checkpointsummerymodel checkpointsummerymodel { get; set; }
    }
}
