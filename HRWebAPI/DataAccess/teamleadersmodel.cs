//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HRWebAPI.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class teamleadersmodel
    {
        public int ID { get; set; }
        public string userId { get; set; }
        public string userName { get; set; }
        public Nullable<int> TeamsModelID { get; set; }
    
        public virtual teamsmodel teamsmodel { get; set; }
    }
}
