﻿using JqueryDTWithMVC.Helpers;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.CheckPointLog;
using JqueryDTWithMVC.Models.DAL.CheckType;
using JqueryDTWithMVC.Models.ViewModels;
using JqueryDTWithMVC.Models.ViewModels.DevicePoint;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace JqueryDTWithMVC.AspNet
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [Startup.Title(Title = "خدمة الويب")]
    public class SyncDBWithDevice : System.Web.Services.WebService
    {
        ApplicationDbContext db = new ApplicationDbContext();
        //BioBridgeSDKLib.BioBridgeSDKClass sdk = new BioBridgeSDKLib.BioBridgeSDKClass();
        List<objSDK> list = new List<objSDK>();


        [WebMethod]
        [Startup.Title(Title = "مزامنة النقاط")]
        [Startup.ModifiedAuthorize]
        public void SyncReports(string TimeDateFrom, string TimeDateTo, int? deviceID)
        {
            if (deviceID == null)
            {
                return;
            }

            var t = new Thread( () => CallSDK(deviceID.Value));
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }

        public void CallSDK(int? deviceId)
        {
            AxBioBridgeSDKLib.AxBioBridgeSDK sdk = new AxBioBridgeSDKLib.AxBioBridgeSDK();
            try
            {
                string enrollNo = "0";
                int yr = 0;
                int mth = 0;
                int day_Renamed = 0;
                int hr = 0;
                int min = 0;
                int sec = 0;
                int ver = 0;
                int io = 0;
                int work = 0;
                int iSize = 0;
                if (deviceId.HasValue)
                {
                    var device = db.BranchesTable.First(a => a.ID == deviceId.Value);
                    {
                        sdk.CreateControl();
                        sdk.Refresh();
                        if (sdk.Connect_TCPIP("", device.ID, device.branch_ip, device.branch_port, Convert.ToInt32(device.branch_key)) == 0)
                        {
                            //  list.Add(new objSDK() { ipaddress = device.branch_ip, sdk = sdk });
                            if (sdk.ReadGeneralLog(ref iSize) == 0)
                            {
                                List<DevicePointViewModel> dvViewModel = new List<DevicePointViewModel>();

                                while (sdk.SSR_GetGeneralLog(ref enrollNo, ref yr, ref mth, ref day_Renamed, ref hr,
                                                                ref min, ref sec, ref ver, ref io, ref work) == 0)
                                {
                                    dvViewModel.Add(new DevicePointViewModel()
                                    {
                                        AttState = io,
                                        branchID = deviceId.Value,
                                        Year = yr,
                                        Month = mth,
                                        Day = day_Renamed,
                                        Hour = hr,
                                        Minute = min,
                                        Second = sec,
                                        IsInValid = 0,
                                        EnrollNo = enrollNo,
                                        VerifyMode = ver,
                                        WorkCode = work
                                    });
                                }

                                SendToDB(dvViewModel);
                            }
                        }
                        else
                        {
                            LogWriter log = new LogWriter("Error: Cann't connect to fingerprint device for Branch with ID: " + deviceId + " ");
                        }
                    }
                }
            }
            catch (Exception ll)
            {
                LogWriter log = new LogWriter("Exception: Branch ID: " + deviceId + " - Reason: " + ll.Message);
            }
        }

        private void SendToDB(List<DevicePointViewModel> devPointsList)
        {
            foreach(var point in devPointsList)
            {
                //EnrollNo = "14";
                int enroll = Convert.ToInt32(point.EnrollNo);
                try
                {
                    using (ApplicationDbContext entityDB = new ApplicationDbContext())
                    {
                        // if user exist in DB
                        if (entityDB.Users.Any(a => a.EnrollNumber == enroll && a.BranchesModelID.HasValue ? a.BranchesModelID.Value == point.branchID : false))
                        {
                            // get user and convert datetime string into valid date
                            var user = entityDB.Users.Include(a => a.relatedSchedule).First(a => a.EnrollNumber == enroll && a.BranchesModelID.Value == point.branchID);
                            IFormatProvider culture = new System.Globalization.CultureInfo("ar-IQ", true);
                            DateTime dt2 = DateTime.Parse(point.Day + "/" + point.Month + "/" + point.Year + " " + point.Hour + ":" + point.Minute + ":" + point.Second + "." + 0, culture, System.Globalization.DateTimeStyles.AssumeLocal);

                            // first record for user on today, dno't care what is the AttState 
                            if (!entityDB.CheckPointLogTable.Any(a => a.userId == user.Id && a.CheckTimeIn.HasValue ?
                            (a.CheckTimeIn.Value.Year == dt2.Year && a.CheckTimeIn.Value.Month == dt2.Month && a.CheckTimeIn.Value.Day == dt2.Day) : false))
                            {
                                CheckPointLogModels model = new CheckPointLogModels()
                                {
                                    BranchesModelID = user.BranchesModelID,
                                    CheckTimeIn = dt2,
                                    CurrentDay = DateTime.Parse(dt2.ToString("yyyy-MM-dd"), culture),
                                    userId = user.Id,
                                    CheckTypeModelsID = entityDB.CheckTypeTable.Where(a => a.CheckName == "غير متوفر").First().ID
                                };

                                entityDB.CheckPointLogTable.Add(model);
                                entityDB.SaveChanges();
                                return;
                            }

                            CheckPointLogModels userLogModel = entityDB.CheckPointLogTable.Include(aa => aa.checkType).Include(aa => aa.checkType).FirstOrDefault(a =>
                                 a.userId == user.Id && a.CheckTimeIn.HasValue ?
                                 (a.CheckTimeIn.Value.Year == dt2.Year && a.CheckTimeIn.Value.Month == dt2.Month && a.CheckTimeIn.Value.Day == dt2.Day) : false);

                            switch (point.AttState)
                            {
                                case 0: // user checked in twice
                                    {
                                        var timeSpan = dt2.Subtract(userLogModel.CheckTimeIn.Value);
                                        // anything above 5 minutes is considered a checkout 
                                        if (timeSpan.TotalMinutes > 5)
                                        {
                                            var tt = userLogModel.CheckTimeOut.HasValue ? userLogModel.CheckTimeOut.Value.AddMinutes(userLogModel.CheckTimeOut.Value.Subtract(dt2).TotalMinutes) : userLogModel.CheckTimeOut = dt2;
                                            entityDB.Entry(userLogModel).State = EntityState.Modified;
                                            break;
                                        }

                                        LogWriter log = new LogWriter("Checked IN Twice: User No." + user.EnrollNumber + " Checkout Twice");
                                        break;
                                    }
                                case 1: // user check out: Normal case
                                    {
                                        if (userLogModel != null)
                                        {
                                            var tt = userLogModel.CheckTimeOut.HasValue ? userLogModel.CheckTimeOut.Value.AddMinutes(userLogModel.CheckTimeOut.Value.Subtract(dt2).TotalMinutes) : userLogModel.CheckTimeOut = dt2;
                                            entityDB.Entry(userLogModel).State = EntityState.Modified;
                                            break;
                                        }
                                        LogWriter log = new LogWriter("Deleted Record: User No." + user.EnrollNumber + "  record was found, then it suddenly disappeareds");
                                        break;
                                    }
                            }

                            ScheduleValues sc = new ScheduleValues();
                            var schedules = sc.GetScheduleValues(user.Id, userLogModel.CurrentDay.Day, userLogModel.CurrentDay);

                            var res = CheckOutCheckInCompare(userLogModel.CheckTimeIn, schedules.Item3,
                                 userLogModel.CheckTimeOut, schedules.Item4, schedules.Item2, userLogModel.checkType.state);

                            userLogModel.CheckTypeModelsID = res.Item1;
                            userLogModel.lateMin = res.Item2;

                            entityDB.SaveChanges();
                            return;
                        }
                        new LogWriter("Employee Not Found: User with No." + enroll + " wasn't found in current branch, maybe it wasn't added to the system yet or the branch is not set " + " \n What to DO: Add the user or assign his/her branch and resync the website");
                    }
                }
                catch (Exception tt)
                {
                    LogWriter log = new LogWriter(tt.Message);
                }

                System.Diagnostics.Debug.WriteLine(("No: " + point.EnrollNo + " Date: " + Convert.ToString(point.Day) + "/" + Convert.ToString(point.Month) + "/" 
                    + Convert.ToString(point.Year) + " Time: " + Convert.ToString(point.Hour) + ":" + Convert.ToString(point.Minute) + ":" 
                    + Convert.ToString(point.Second) + " Verify: " + Convert.ToString(point.VerifyMode) 
                    + " I/O: " + Convert.ToString(point.AttState) + " Work Code: " + Convert.ToString(point.WorkCode)));
            }

        }

        public Tuple<int, int> CheckOutCheckInCompare(DateTime? attIn, DateTime? scIn, DateTime? attOut, DateTime? scOut, int allowMin, LogStatus state)
        {
            if ((int)state >= (int)LogStatus.غير_معرف && scIn.HasValue && scOut.HasValue)
            {
                if ((attOut == null || attIn == null))
                {
                    return Tuple.Create<int, int>(db.CheckTypeTable.First(a => a.state == LogStatus.غير_معرف).ID, 0);
                }
                else
                {
                    int resID = db.CheckTypeTable.First(a => a.state == LogStatus.حضور).ID;
                    int latBy = 0;
                    if (attIn.Value.Subtract(scIn.Value).TotalMinutes > allowMin)
                    {
                        resID = db.CheckTypeTable.First(a => a.state == LogStatus.تاخير).ID;
                        latBy = (int)attIn.Value.Subtract(scIn.Value).TotalMinutes - allowMin;
                    }

                    if (attOut.Value.Subtract(scOut.Value).TotalMinutes < 0)
                    {
                        resID = db.CheckTypeTable.First(a => a.state == LogStatus.خروج_مبكر).ID;
                        latBy = (int)attOut.Value.Subtract(scOut.Value).TotalMinutes;
                    }

                    return Tuple.Create<int, int>(resID, latBy);
                }
            }

            return Tuple.Create<int, int>(db.CheckTypeTable.First(a => a.state == state).ID, 0);
        }


        [WebMethod]
        [Startup.Title(Title = "مزامنة توقيت الاجهزة")]
        [Startup.ModifiedAuthorize]
        public bool SyncTime(int? deviceId)
        {
            int Year = 0, Month = 0, Day = 0, Hour = 0, Minute = 0, Second = 0;
            if (deviceId.HasValue)
            {
                AxBioBridgeSDKLib.AxBioBridgeSDK sdk = new AxBioBridgeSDKLib.AxBioBridgeSDK();
                sdk.CreateControl();
                sdk.Refresh();

                foreach (var device in db.BranchesTable.Where(a => deviceId.HasValue).ToList())
                {
                    if (sdk.Connect_TCPIP("", device.ID, device.branch_ip, device.branch_port, Convert.ToInt32(device.branch_key)) == 0)
                    {
                        sdk.GetDeviceTime(ref Year, ref Month, ref Day, ref Hour, ref Minute, ref Second);
                        IFormatProvider culture = new System.Globalization.CultureInfo("ar-IQ", true);
                        DateTime dt2 = DateTime.Parse(Day + "/" + Month + "/" + Year + " " + Hour + ":" + Minute + ":" + Second + "." + 0, culture, System.Globalization.DateTimeStyles.AssumeLocal);

                        if (System.DateTime.Now.Subtract(dt2).Minutes > 3)
                        {
                            sdk.SetDeviceTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day
                                , System.DateTime.Now.Hour, System.DateTime.Now.Minute, System.DateTime.Now.Second);
                        }
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
