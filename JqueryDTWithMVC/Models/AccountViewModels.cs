﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JqueryDTWithMVC.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage ="يجب ادخال البريد الكتروني")]
        [Display(Name = "البريد الكتروني")]
        [EmailAddress(ErrorMessage ="البريد الكتروني المدخل غير صالح .. الرجاء منة")]
        public string Email { get; set; }

        [Required(ErrorMessage = "كلمة السر مطلوبة")]
        [DataType(DataType.Password)]
        [Display(Name = "كلمة السر")]
        public string Password { get; set; }

        [Display(Name = "تذكرني؟")]
        public bool RememberMe { get; set; }

        public string siteLog { get; set; }
        public string siteName { get; set; }
    }

    public class RegisterViewModel
    {
        public string action { get; set; }

        public string EmpID { get; set; }

        [Required(ErrorMessage = "الحقل مطلوب")]
        public string EmpArName { get; set; }

        [Required(ErrorMessage = "الحقل مطلوب")]
        public string EmpEnName { get; set; }

        [Required(ErrorMessage = "الحقل مطلوب")]
        public string EmpEmail { get; set; }

        [Required(ErrorMessage = "الحقل مطلوب")]
        public int EmpFP { get; set; }

        [Required(ErrorMessage = "الحقل مطلوب")]
        public int EmpGender { get; set; }

        [Required(ErrorMessage = "الحقل مطلوب")]
        public int EmpBranch { get; set; }

        [Required(ErrorMessage = "الحقل مطلوب")]
        public int EmpDepart { get; set; }

        public string EmpBirth { get; set; }

        [Required(ErrorMessage = "الحقل مطلوب")]
        public int EmpSchedule { get; set; }

        public bool EmpEnable { get; set; }
        
        public string EmpcakeHRID { get; set; }

        public string password { get; set; }
        public string confirmPassword { get; set; }
        public string role { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "يجب ادخال البريد الكتروني")]
        [Display(Name = "البريد الكتروني")]
        [EmailAddress(ErrorMessage = "البريد الكتروني المدخل غير صالح .. الرجاء منة")]
        public string Email { get; set; }

        [Required(ErrorMessage = "كلمة السر مطلوبة")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "كلمة السر يجب ان تكون اكبر من 6 رموز ", MinimumLength = 6)]
        [Display(Name = "كلمة السر")]
        public string Password { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "تاكيد كلمة السر")]
        [Compare("Password", ErrorMessage = "حقل كلمة السر وتاكيد كلمة السر غير متوافقان")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage ="البريد الكتروني مطلوب")]
        [EmailAddress(ErrorMessage ="البريد الكتروني المدخل غير صحيح")]
        [Display(Name = "البريد الكتروني")]
        public string Email { get; set; }

        public string siteLog { get; set; }
        public string siteName { get; set; }
    }
}
