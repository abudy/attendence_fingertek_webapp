﻿using JqueryDTWithMVC.Models.DAL.Branches;
using JqueryDTWithMVC.Models.DAL.Teams;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JqueryDTWithMVC.Models.ViewModels.Teams
{
    public class TeamsViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage ="{0} Field is Required")]
        [Display(Name = "Team Name")]
        [StringLength(30, MinimumLength = 2)]
        [Editable(true)]
        public string teamName { get; set; }

        [Display(Name = "Team Branch")]
        public int? BranchesModelID { get; set; }
        public BranchesModel relatedBranch { get; set; }

        [Required(ErrorMessage = "{0} Field is Required")]
        [Display(Name = "Team Leader")]
        public string TeamLeadersModelsID { get; set; }

        [Display(Name = "Team Type")]
        public TeamType teamType { get; set; }

        [Required(ErrorMessage = "{0} Field is Required")]
        public string[] relatedUsers { get; set; }

        public List<ApplicationUser> users { get; set; }
    }

    public class TeamViewModelJson
    {
        public int ID { get; set; }

        public int BranchID { get;set;}

        public string teamName { get; set; }
    }
}