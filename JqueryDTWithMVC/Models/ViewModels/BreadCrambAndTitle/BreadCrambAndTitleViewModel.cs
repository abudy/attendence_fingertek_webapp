﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace JqueryDTWithMVC.Models.ViewModels.BreadCrambAndTitle
{
    public class BreadCrambAndTitleViewModel
    {
        public string controllerName { get; set; }

        public string actionName { get; set; }
    }
}