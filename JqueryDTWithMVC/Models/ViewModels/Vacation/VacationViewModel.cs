﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.Vacation
{
    public class VacationViewModel
    {
        public int ID { get; set; }

        public string EmployeeName { get; set; }

        public string EmployeeID { get; set; }

        public string fromDate { get; set; }

        public string textDes { get; set; }

        public string message { get; set; }

        public string vacationName { get; set; }

    }
}