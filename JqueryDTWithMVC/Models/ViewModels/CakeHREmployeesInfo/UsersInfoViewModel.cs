﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.CakeHREmployeesInfo
{
    public class UsersInfoViewModel
    {
        public Account Account { get; set; }
    }

    public class Account
    {
        public string id { get; set; }
        public string email { get; set; }
        public string first { get; set; }
        public string last { get; set; }
        public string picture { get; set; }
        public string team_id { get; set; }
        public string account_created { get; set; }
        public string login_enabled { get; set; }
        public string last_accessed { get; set; }
        public string last_updated { get; set; }
        public object date_of_birth { get; set; }
        public string street_first { get; set; }
        public string street_second { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string post_code { get; set; }
        public string country { get; set; }
        public string nationality { get; set; }
        public string home_phone { get; set; }
        public string work_phone { get; set; }
        public string mobile_phone { get; set; }
        public string email_personal { get; set; }
        public string gender { get; set; }
        public string marital_status { get; set; }
        public string employee_since { get; set; }
        public CustomFields custom_fields { get; set; }
        public string team { get; set; }
        public string terminated { get; set; }
        public string termination_date { get; set; }
        public string reports_to { get; set; }
    }


    public class CustomFields
    {
        public string hobbies { get; set; }
        public string coffee { get; set; }
    }

}