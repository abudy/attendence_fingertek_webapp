﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.CheckType
{
    public class CheckTypeViewModel
    {
        public int CheckID { get; set; }

        public string CheckName { get; set; }

        public int DT_RowId { get; set; }
    }
}