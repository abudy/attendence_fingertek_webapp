﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JqueryDTWithMVC;
namespace JqueryDTWithMVC.Models.ViewModels.UsersWithRoles
{
    public class UsersWithRole
    {
        public string userId { get; set; }

        public string userName { get; set; }

        public string role { get; set; }
    }
}