﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.ActionCompletion
{
    public class ActionCompletionViewModel
    {
        public int ID { get; set; }

        public string eventDescription { get; set; }

        public string user { get; set; }

        public string userId { get; set; }

        public bool shown { get; set; }
    }
}