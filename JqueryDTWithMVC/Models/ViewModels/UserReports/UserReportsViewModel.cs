﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.UserReports
{
    public class UserReportsViewModel
    {
        public string ID { get; set; }
        public string DT_RowId { get; set; }
        public string EmployeeAName { get; set; }
        public int CountDays { get; set; }
        public double WorkTime { get; set; }
        public double WorkedTime { get; set; }
        public int Rest { get; set; }
        public int Absence { get; set; }
        public int RestCount { get; set; }
        public int AbsenceHour { get; set;}
        public int AbsenceCount { get; set; }
        public int VacationMinCount { get; set; }
    }
}