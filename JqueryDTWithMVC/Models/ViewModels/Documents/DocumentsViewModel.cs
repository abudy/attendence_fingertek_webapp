﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.Documents
{
    public class DocumentsViewModel
    {
        public int ID { get; set; }

        [Display(Name = "Select a Document")]
        public HttpPostedFileBase uploadedFile { get; set; }

        [Display(Name = "Document Category")]
        public int DocumentsCategoryModelsID { get; set; }

        [Display(Name = "Shared With")]
        public string[] sharedWithUsers { get; set; }

        public List<JqueryDTWithMVC.Models.DAL.Documents.DocumentsWithUsersModels> users { get; set; }
    }
}