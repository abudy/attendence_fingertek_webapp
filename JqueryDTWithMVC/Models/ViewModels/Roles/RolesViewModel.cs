﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JqueryDTWithMVC;

namespace JqueryDTWithMVC.Models.ViewModels.Roles
{
    public class RolesViewModel
    {
        public string roleName { get; set; }
    }

    public class RolesViewModelWithId
    {
        public string Id { get; set; }
        public string roleName { get; set; }
        public string seleID { get; set; }
    }
}