﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels
{
    public class objSDK
    {
        public BioBridgeSDKLib.BioBridgeSDKClass sdk { get; set; }
        public string ipaddress { get; set; }
    }
}