﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.WSSettings
{
    public class WebSiteSettingsViewModel
    {
        public string SITENAME { get; set; }
        public string ADDRESS { get; set; }
        public string TEL { get; set; }
        public string SITE { get; set; }
        public string EMAIL { get; set; }
        public string LOGO { get; set; }
        public string ALLOWTIME { get; set; }
        public string SITEDIR { get; set; }
        public HttpPostedFileBase Img_Logo { get; set; }
    }

    public class WebSiteSettingsForEditViewModel
    {
        public string setting_coName { get; set; }
        public string setting_coAddress { get; set; }
        public string setting_coMobile { get; set; }
        public string setting_coSite { get; set; }
        public string setting_dSite { get; set; }
        public string setting_coEMail { get; set; }
        public string setting_coClock { get; set; }
        public HttpPostedFileBase setting_coLogo { get; set; }
    }
}