﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.Personnal
{
    public class PersonnalViewModel
    {
        public string userId { get; set; }


        [Display(Name = "Date of Birth")]
        public DateTime? dateOfBirth { get; set; }

        [Display(Name = "Street 1")]
        public string street_1 { get; set; }

        [Display(Name = "Street 2")]
        public string street_2 { get; set; }

        [Display(Name = "City")]
        public string city { get; set; }

        [Display(Name = "State")]
        public string state { get; set; }

        [Display(Name = "Post Code")]
        public string postCode { get; set; }

        [Display(Name = "Country")]
        public int? CountryModelID { get; set; }

        [Display(Name = "Nationality")]
        public int? NationalityModelID { get; set; }

        [Display(Name = "Marital Status")]
        public int? MaritalStatusModelID { get; set; }

        [Display(Name = "Home Phone")]
        public string homePhone { get; set; }

        [Display(Name = "Work Phone")]
        public string workPhone { get; set; }

        [Display(Name = "Gender")]
        public Gender? EmployeeGender { get; set; }

        public string firstName { get; set; }
        public string lastName { get; set; }
    }
}

 