﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.SummeryReport
{
    public class AbsenceSummeryViewModel
    {

        public string CheckRBranch { get; set; }
        public string EmployeeBranch { get; set; }
        public string EmployeeAName { get; set; }
        public int DT_RowId { get; set; }
        public int CheckRType { get; set; }
        public int CheckRLength { get; set; }
        public string CheckName { get; set; }

    }
}