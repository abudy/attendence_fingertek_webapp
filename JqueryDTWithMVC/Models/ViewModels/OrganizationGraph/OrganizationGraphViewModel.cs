﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.ViewModels.OrganizationGraph
{
    public class OrganizationGraphViewModel
    {
        public int key { get; set; }
        public string userId { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public int parent { get; set; }
    }
}