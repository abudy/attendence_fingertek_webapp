﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System;
using JqueryDTWithMVC.Models.DAL.Branches;
using JqueryDTWithMVC.Models.DAL.Schedules;
using JqueryDTWithMVC.Models.DAL.Settings;
using JqueryDTWithMVC.Models.DAL.Departments;
using System.Collections.Generic;
using JqueryDTWithMVC.Models.DAL.CheckPointLog;
using JqueryDTWithMVC.Models.DAL.ActivityLog;
using JqueryDTWithMVC.Models.DAL.CheckType;
using JqueryDTWithMVC.Models.DAL.CheckPointSummery;
using JqueryDTWithMVC.Models.DAL.AccessControl;
using JqueryDTWithMVC.Models.DAL.Vacation;
using JqueryDTWithMVC.Models.DAL.VacationType;
using JqueryDTWithMVC.Models.DAL.Teams;
using JqueryDTWithMVC.Models.DAL.UsersWithTeams;
using JqueryDTWithMVC.Models.DAL.TeamLeaders;
using JqueryDTWithMVC.Models.DAL.Notifications;
using JqueryDTWithMVC.Models.DAL.ActionCompletion;
using JqueryDTWithMVC.Models.DAL.Documents;
using JqueryDTWithMVC.Models.DAL.DocumentsCategory;
using JqueryDTWithMVC.Models.DAL.Title;
using JqueryDTWithMVC.Models.DAL.Position;
using JqueryDTWithMVC.Models.DAL.EmploymentState;
using JqueryDTWithMVC.Models.DAL.Notes;
using JqueryDTWithMVC.Models.DAL.Country;
using JqueryDTWithMVC.Models.DAL.Nationality;
using JqueryDTWithMVC.Models.DAL.MaritalStatus;
using JqueryDTWithMVC.Models.DAL.Assests;
using JqueryDTWithMVC.Models.DAL.EmergenceyContact;

namespace JqueryDTWithMVC.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public enum Gender
    {
        ذكر, انثى
    }

    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        [Display(Name = "رقم البصمة")]
        public int? EnrollNumber { get; set; }

        [Required]
        [StringLength(200)]
        [Display(Name = "اسم الموظف باللغة العربية")]
        public string EmployeeName { get; set; }

        [Required]
        [StringLength(200)]
        [Display(Name = "اسم الموظف باللغة الانكليزية")]
        public string EmployeeName_En { get; set; }

        [Display(Name = "كلمة السر")]
        public string EmployeePassword { get; set; }

        [Display(Name = "جنس الموظف")]
        public Gender? EmployeeGender { get; set; }

        [Display(Name = "تاريخ الميلاد")]
        public string EmployeeBirthDate { get; set; }

        [Display(Name = "تاريخ الميلاد")]
        public DateTime? empBirthDay { get; set; }

        [Display(Name = "تاريخ بداية العمل")]
        public DateTime? dateEnrollDay { get; set; }

        [Display(Name = "تاريخ اخر تسجيل دخول")]
        public DateTime? EmployeeLastLogin { get; set; }

        [Display(Name = "اظهار في التقارير")]
        public bool EmployeeShow { get; set; }

        [Display(Name = "رقم الحساب عل نظام ال CakeHR")]
        public string userCakeHRNumber { get; set; }

        [Display(Name = "First Name")]
        public string firstName { get; set; }

        [Display(Name = "Last Name")]
        public string lastName { get; set; }

        [Display(Name = "Personnal Email")]
        public string perssonalEmail { get; set; }

        [Display(Name = "Linkdln Profile")]
        public string linkedln { get; set; }

        [Display(Name = "Driving License")]
        public string drivingLicense { get; set; }

        [Display(Name = "Skype Profile")]
        public string skype { get; set; }
        // Personnal
        [Display(Name = "Street 1")]
        public string street_1 { get; set; }
        [Display(Name = "Street 2")]
        public string street_2 { get; set; }
        [Display(Name = "City")]
        public string city { get; set; }
        [Display(Name = "State")]
        public string state { get; set; }
        [Display(Name = "Post Code")]
        public string postCode { get; set; }
        [Display(Name = "Home Phone")]
        public string homePhone { get; set; }
        [Display(Name = "Work Phone")]
        public string workPhone { get; set; }

        [Display(Name = "توقيت العمل")]
        public int? SchedulesModelID { get; set; }
        public SchedulesModel relatedSchedule { get; set; }

        [Display(Name = "القسم")]
        public int? DepartmentsModelID { get; set; }
        public DepartmentsModel relatedDepartment { get; set; }

        [Display(Name = "المقر")]
        public int? BranchesModelID { get; set; }
        public BranchesModel relatedBranch { get; set; }

        [Display(Name = "الفريق")]
        public int? TeamsModelID { get; set; }
        public TeamsModel relatedTeam { get; set; }
        // hr data
        // Employee:
        [Display(Name = "Title")]
        public int? TitleModelID { get; set; }
        public TitleModel userTitle { get; set; }

        [Display(Name = "Employment Status")]
        public int? EmploymentStateModelID { get; set; }
        public EmploymentStateModel employmentState { get; set; }

        [Display(Name = "Position")]
        public int? PositionModelID { get; set; }
        public PositionModel positon { get; set; }

        [Display(Name = "Country")]
        public int? CountryModelID { get; set; }
        public CountryModel country { get; set; }

        [Display(Name = "Nationality")]
        public int? NationalityModelID { get; set; }
        public NationalityModel nation { get; set; }

        [Display(Name = "Marital Status")]
        public int? MaritalStatusModelID { get; set; }
        public MaritalStatusModel martialStatus { get; set; }

        public List<AssestsModel> userAssests { get; set; }
        public List<EmergenceyContactModel> userContacts { get; set; }
        public List<NotesModel> notes { get; set; }
        public List<CheckPointSummeryModel> CheckPointSummeryList { get; set; }
        public List<CheckPointLogModels> CheckPointLogList { get; set; }
        public List<VacationsModel> associatedVacation { get; set; }
        public List<NotificationsModel> notificationsList { get; set; }
        public List<ActionCompletionModel> actionCompletionList { get; set; }
        public List<DocumentsWithUsersModels> documentsShared { get; set; }

        [Display(Name = "Branch Manager")]
        public bool branchManager { get; set; }

        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser user { get; set; }
        public List<ApplicationUser> reportToMe { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        static ApplicationDbContext()
        {
            Database.SetInitializer(new MySqlInitializer());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CheckPointSummeryModel>()
                .HasRequired(p => p.CheckPointLogModels)
                .WithOptional(p => p.CheckPointSummeryModel);
        }

        public DbSet<DepartmentsModel> DepartmentsTable { get; set; }
        public DbSet<BranchesModel> BranchesTable { get; set; }
        public DbSet<SettingsModel> SettingsTable { get; set; }
        public DbSet<SchedulesModel> SchedulesTable { get; set; }
        public DbSet<CheckPointLogModels> CheckPointLogTable { get; set; }
        public DbSet<ActivityLogModel> ActivityLogTable { get; set; }
        public DbSet<CheckTypeModels> CheckTypeTable { get; set; }
        public DbSet<CheckPointSummeryModel> CheckPointSummeryTable { get; set; }
        public DbSet<AccessControlModel> AccessControlTable { get; set; }
        public DbSet<VacationTypeModel> VacationTypeTable { get; set; }
        public DbSet<VacationsModel> VacationsTable { get; set; }
        public DbSet<TeamsModel> TeamsTable { get; set; }
        public DbSet<NotificationsModel> NotificationsTable { get; set; }
        public DbSet<ActionCompletionModel> ActionCompletionTable { get; set; }
        public DbSet<DAL.SideBarItems.SideBarItemsModels> SideBarItemsTable { get; set; }
        public DbSet<DocumentsCategoryModels> DocumentsCategoryTable { get; set; }
        public DbSet<DocumentsWithUsersModels> DocumentsWithUsersTable { get; set; }
        public DbSet<DocumentsModels> DocumentsTable { get; set; }

        public DbSet<AssestsModel> AssestsTable { get; set; }
        public DbSet<EmergenceyContactModel> EmergenceyContactTable { get; set; }
        public DbSet<MaritalStatusModel> MaritalStatusTable { get; set; }
        public DbSet<NationalityModel> NationalityTable { get; set; }
        public DbSet<CountryModel> CountryTable { get; set; }
        public DbSet<NotesModel> NotesTable { get; set; }
        public DbSet<TeamLeadersModels> teamLeaderTable { get; set; }
        public DbSet<EmploymentStateModel> EmploymentStateTable { get; set; }
        public DbSet<PositionModel> PositionTable { get; set; }
        public DbSet<TitleModel> TitleTable { get; set; }
        public DbSet<AssestsCategoryModel> AssestsCategoryTable { get; set; }

    }
}