﻿using JqueryDTWithMVC.Models.DAL.MaritalStatus;
using JqueryDTWithMVC.Models.DAL.VacationType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.Repository.MartialStatus
{
    public interface IMartialStatusRepository : IRepository<MaritalStatusModel>
    {
    }
}