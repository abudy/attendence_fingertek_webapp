﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using JqueryDTWithMVC.Models.DAL.Vacation;
using JqueryDTWithMVC.Models.DAL.VacationType;
using JqueryDTWithMVC.Models.DAL.MaritalStatus;

namespace JqueryDTWithMVC.Models.Repository.MartialStatus
{
    public class MartialStatusRepository : Repository.Repository<MaritalStatusModel> , IMartialStatusRepository
    {
        public MartialStatusRepository(ApplicationDbContext context) 
            : base(context)
        {
        }

        public ApplicationDbContext ApplicationDbContext
        {
            get
            {
                return Context as ApplicationDbContext;
            }
        }
        
    }
}