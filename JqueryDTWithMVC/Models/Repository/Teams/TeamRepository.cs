﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using JqueryDTWithMVC.Models.DAL.Teams;

namespace JqueryDTWithMVC.Models.Repository.Teams
{
    public class TeamRepository : Repository.Repository<TeamsModel>, ITeamRepository
    {
        public TeamRepository(ApplicationDbContext context)
            : base(context)
        {
        }
        public ApplicationDbContext ApplicationDbContext
        {
            get
            {
                return Context as ApplicationDbContext;
            }
        }

        public Tuple<TeamsModel, string[]> ToModel(JqueryDTWithMVC.Models.ViewModels.Teams.TeamsViewModel vModel)
        {
            var userList = vModel.relatedUsers;
            return Tuple.Create(new TeamsModel()
            {
                ID = vModel.ID,
                BranchesModelID = vModel.BranchesModelID,
                teamName = vModel.teamName,
                //userId = vModel.TeamLeadersModelsID,
       //         userName = ApplicationDbContext.Users.FirstOrDefault(a => a.Id == vModel.TeamLeadersModelsID).EmployeeName
            }, userList);
        }

        public void RemoveMember(string userId)
        {
            var user = ApplicationDbContext.Users.FirstOrDefault(a => a.EmployeeName == userId);
            if (user != null)
            {
                user.TeamsModelID = null;
                ApplicationDbContext.Entry(user).State = EntityState.Modified;
            }
        }

        public void AddMember(int teamId, string userId)
        {
            var user = ApplicationDbContext.Users.FirstOrDefault(a=>a.EmployeeName == userId);
            if(user != null)
            {
                user.TeamsModelID = teamId;
                ApplicationDbContext.Entry(user).State = EntityState.Modified;
            }
        }

        public void AddMemberById(int teamId, string userId)
        {
            var user = ApplicationDbContext.Users.Find(userId);
            if (user != null)
            {
                user.TeamsModelID = teamId;
                ApplicationDbContext.Entry(user).State = EntityState.Modified;
            }
        }

        public void RemoveMemberById(string userId)
        {
            var user = ApplicationDbContext.Users.Find(userId);
            if (user != null)
            {
                user.TeamsModelID = null;
                ApplicationDbContext.Entry(user).State = EntityState.Modified;
            }
        }

        public void UpdateTeam(TeamsModel model)
        {
            // first remove all users
            foreach(var users in ApplicationDbContext.Users.Where(a=>a.TeamsModelID.HasValue ? 
                                a.TeamsModelID.Value == model.ID : false))
            {
                users.TeamsModelID = null;
                ApplicationDbContext.Entry(users).State = EntityState.Modified;
            }

            ApplicationDbContext.Entry(model).State = EntityState.Modified;
        }

        public IEnumerable<ApplicationUser> GetTeamMembers(int teamId)
        {
            return ApplicationDbContext.TeamsTable.Include(aa => aa.relatedUsers).FirstOrDefault
                (a => a.ID == teamId).relatedUsers;
        }

        public void RemoveTeam(int Id)
        {
            var team = ApplicationDbContext.TeamsTable.Include(aa => aa.relatedUsers).FirstOrDefault(a => a.ID == Id);
            foreach (var user in team.relatedUsers.ToList())
            {
                user.TeamsModelID = null;
                ApplicationDbContext.Entry(user).State = EntityState.Modified;
            }

            if(team.leadersList != null)
            {
                foreach (var leader in team.leadersList.ToList())
                {
                    ApplicationDbContext.teamLeaderTable.Remove(leader);
                }
            }


            ApplicationDbContext.TeamsTable.Remove(team);
        }

        public IEnumerable<ApplicationUser> GetTeamLeader()
        {
            return ApplicationDbContext.Users.ToList();
        }
    }
}