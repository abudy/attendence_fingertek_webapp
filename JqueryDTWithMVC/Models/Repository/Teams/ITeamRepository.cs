﻿using JqueryDTWithMVC.Models.DAL.Teams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.Repository.Teams
{
    public interface ITeamRepository : IRepository<TeamsModel>
    {
        Tuple<TeamsModel, string[]> ToModel(JqueryDTWithMVC.Models.ViewModels.Teams.TeamsViewModel vModel);
        IEnumerable<ApplicationUser> GetTeamMembers(int teamId);
        IEnumerable<ApplicationUser> GetTeamLeader();

        void RemoveMember(string userId);
        void AddMember(int teamId, string userId);
        void RemoveTeam(int Id);
        void RemoveMemberById(string userId);
        void AddMemberById(int teamId, string userId);
        void UpdateTeam(TeamsModel model);
    }
}