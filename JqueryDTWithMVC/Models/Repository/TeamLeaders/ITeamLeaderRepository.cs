﻿using JqueryDTWithMVC.Models.DAL.MaritalStatus;
using JqueryDTWithMVC.Models.DAL.Nationality;
using JqueryDTWithMVC.Models.DAL.Notes;
using JqueryDTWithMVC.Models.DAL.TeamLeaders;
using JqueryDTWithMVC.Models.DAL.VacationType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.Repository.TeamLeader
{
    public interface ITeamLeaderRepository : IRepository<TeamLeadersModels>
    {
        bool isTeamLeader(int teamId, string userId);
        void UpdateTeamLeaders(string[] leaders, int teamID);
    }
}