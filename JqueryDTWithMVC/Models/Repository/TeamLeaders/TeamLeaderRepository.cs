﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using JqueryDTWithMVC.Models.DAL.Vacation;
using JqueryDTWithMVC.Models.DAL.VacationType;
using JqueryDTWithMVC.Models.DAL.MaritalStatus;
using JqueryDTWithMVC.Models.DAL.Nationality;
using JqueryDTWithMVC.Models.DAL.Notes;
using JqueryDTWithMVC.Models.Repository.TeamLeader;
using JqueryDTWithMVC.Models.DAL.TeamLeaders;

namespace JqueryDTWithMVC.Models.Repository.Nationality
{
    public class TeamLeaderRepository : Repository.Repository<TeamLeadersModels> , ITeamLeaderRepository
    {
        public TeamLeaderRepository(ApplicationDbContext context) 
            : base(context)
        {
        }

        public ApplicationDbContext ApplicationDbContext
        {
            get
            {
                return Context as ApplicationDbContext;
            }
        }

        public void UpdateTeamLeaders(string[] leaders, int teamID)
        {
            foreach(var leader in ApplicationDbContext.teamLeaderTable.Where(aa=>aa.TeamsModelID.HasValue ? aa.TeamsModelID.Value == teamID : false).ToList())
            {
                ApplicationDbContext.teamLeaderTable.Remove(leader);
            }

            foreach(var lID in leaders)
            {
                TeamLeadersModels model = new TeamLeadersModels()
                {
                    userName = ApplicationDbContext.Users.Find(lID).UserName,
                    userId = lID,
                    TeamsModelID = teamID
                };
                ApplicationDbContext.teamLeaderTable.Add(model);
            }
        }

        bool ITeamLeaderRepository.isTeamLeader(int teamId, string userId)
        {
            return ApplicationDbContext.teamLeaderTable.Any(aa => aa.TeamsModelID.HasValue ? (aa.TeamsModelID.Value == teamId && aa.userId == userId) : false);
        }


    }
}