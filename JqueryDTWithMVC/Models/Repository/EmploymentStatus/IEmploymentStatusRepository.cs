﻿using JqueryDTWithMVC.Models.DAL.EmploymentState;
using JqueryDTWithMVC.Models.DAL.Vacation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.Repository.EmploymentStatus
{
    public interface IEmploymentStatusRepository : IRepository<EmploymentStateModel>
    {
        void UpdateStatus(string userId, int empStateID);
    }
}