﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using JqueryDTWithMVC.Models.DAL.EmploymentState;

namespace JqueryDTWithMVC.Models.Repository.EmploymentStatus
{
    public class EmploymentStatusRepository : Repository.Repository<EmploymentStateModel> , IEmploymentStatusRepository
    {
        public EmploymentStatusRepository(ApplicationDbContext context) 
            : base(context)
        {
        }

        public ApplicationDbContext ApplicationDbContext
        {
            get
            {
                return Context as ApplicationDbContext;
            }
        }

        public void UpdateStatus(string userId, int empStateID)
        {

        }
    }
}