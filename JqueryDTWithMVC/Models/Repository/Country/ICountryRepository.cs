﻿using JqueryDTWithMVC.Models.DAL.Country;
using JqueryDTWithMVC.Models.DAL.Vacation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.Repository.Country
{
    public interface ICountryRepository: IRepository<CountryModel>
    {
    }
}