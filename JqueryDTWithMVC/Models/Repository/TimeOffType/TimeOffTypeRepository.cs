﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using JqueryDTWithMVC.Models.DAL.Vacation;
using JqueryDTWithMVC.Models.DAL.VacationType;

namespace JqueryDTWithMVC.Models.Repository.TimeOff
{
    public class TimeOffTypeRepository : Repository.Repository<VacationTypeModel> ,ITimeOffTypeRepository
    {
        public TimeOffTypeRepository(ApplicationDbContext context) 
            : base(context)
        {
        }

        public ApplicationDbContext ApplicationDbContext
        {
            get
            {
                return Context as ApplicationDbContext;
            }
        }
        
        public void EditTimeOff(VacationTypeModel model)
        {
            Context.Entry(model).State = EntityState.Modified;
        }   
    }
}