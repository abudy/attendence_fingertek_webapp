﻿using JqueryDTWithMVC.Models.DAL.VacationType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.Repository.TimeOff
{
    public interface ITimeOffTypeRepository : IRepository<VacationTypeModel>
    {
        void EditTimeOff(VacationTypeModel model);
    }
}