﻿using JqueryDTWithMVC.Models.DAL.EmergenceyContact;
using JqueryDTWithMVC.Models.DAL.Title;
using JqueryDTWithMVC.Models.DAL.VacationType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.Repository.EmergenceyContact
{
    public interface IEmergenceyContactRepository : IRepository<EmergenceyContactModel>
    {
        void Update(EmergenceyContactModel info);
        IQueryable<EmergenceyContactModel> GetEmergenceyContactForUser(string userId);
    }
}