﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using JqueryDTWithMVC.Models.DAL.Vacation;
using JqueryDTWithMVC.Models.DAL.VacationType;
using JqueryDTWithMVC.Models.DAL.Title;
using JqueryDTWithMVC.Models.DAL.EmergenceyContact;

namespace JqueryDTWithMVC.Models.Repository.EmergenceyContact
{
    public class EmergenceyContactRepository : Repository.Repository<EmergenceyContactModel> , IEmergenceyContactRepository
    {
        public EmergenceyContactRepository(ApplicationDbContext context) 
            : base(context)
        {
        }

        public ApplicationDbContext ApplicationDbContext
        {
            get
            {
                return Context as ApplicationDbContext;
            }
        } 

        public void Update(EmergenceyContactModel info)
        {
            ApplicationDbContext.Entry(info).State = EntityState.Modified;
        }

        public IQueryable<EmergenceyContactModel> GetEmergenceyContactForUser(string userId)
        {
            return ApplicationDbContext.EmergenceyContactTable.Where(aa => aa.ApplicationUserId == userId);
        }
    }
}