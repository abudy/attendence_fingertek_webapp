﻿using JqueryDTWithMVC.Models.DAL.Branches;
using JqueryDTWithMVC.Models.DAL.Teams;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.Repository.Branches
{
    public interface IBranchesRepository : IRepository<BranchesModel>
    {
        int getUserBranch(string userId);
    }
}