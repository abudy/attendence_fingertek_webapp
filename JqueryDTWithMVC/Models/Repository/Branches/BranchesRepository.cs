﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using JqueryDTWithMVC.Models.DAL.Branches;

namespace JqueryDTWithMVC.Models.Repository.Branches
{
    public class BranchesRepository : Repository.Repository<BranchesModel>, IBranchesRepository
    {
        public BranchesRepository(ApplicationDbContext context)
            : base(context)
        {
        }
        public ApplicationDbContext ApplicationDbContext
        {
            get
            {
                return Context as ApplicationDbContext;
            }
        }

        public int getUserBranch(string userId)
        {
           return ApplicationDbContext.Users.Find(userId).BranchesModelID.HasValue ? 
                        ApplicationDbContext.Users.Find(userId).BranchesModelID.Value : 0;
        }
       
    }
}