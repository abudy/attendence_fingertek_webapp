﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JqueryDTWithMVC.Models.Repository.Teams;
using JqueryDTWithMVC.Models.Repository.TimeOff;
using JqueryDTWithMVC.Models.Repository.Branches;
using JqueryDTWithMVC.Models.Repository.Documents;
using JqueryDTWithMVC.Models.Repository.DocumentsCategory;
using JqueryDTWithMVC.Models.Repository.Position;
using JqueryDTWithMVC.Models.Repository.Title;
using JqueryDTWithMVC.Models.Repository.EmploymentStatus;
using JqueryDTWithMVC.Models.Repository.Nationality;
using JqueryDTWithMVC.Models.Repository.MartialStatus;
using JqueryDTWithMVC.Models.Repository.Country;
using JqueryDTWithMVC.Models.Repository.Notes;
using JqueryDTWithMVC.Models.Repository.EmergenceyContact;
using JqueryDTWithMVC.Models.Repository.TeamLeader;

namespace JqueryDTWithMVC.Models.Repository.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        protected readonly ApplicationDbContext _context;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Teams = new TeamRepository(_context);
            TimeOffs = new TimeOffRepository(_context);
            TimeOffType = new TimeOffTypeRepository(_context);
            Users = new UserRepository(_context);
            Branches = new BranchesRepository(_context);
            Documents = new DocumentsRepository(_context);
            DocumentsCategory = new DocumentsCategoryModelsRepository(_context);
            Position = new PositionRepository(_context);
            Title = new TitleRepository(_context);
            EmploymentStatus = new EmploymentStatusRepository(_context);
            Nationality = new NationalityRepository(_context);
            MartialStatus = new MartialStatusRepository(_context);
            Country = new CountryRepository(_context);
            Notes = new NotesRepository(_context);
            EmergenceyContact = new EmergenceyContactRepository(_context);
            TeamLeader = new TeamLeaderRepository(_context);
        }

        public ITeamRepository Teams { get; private set; }
        public ITimeOffRepository TimeOffs { get; private set; }
        public ITimeOffTypeRepository TimeOffType { get; private set; }
        public IUserRepository Users { get; private set; }
        public IBranchesRepository Branches { get; private set; }
        public IDocumentsRepository Documents { get; private set; }
        public IDocumentsCategoryRepository DocumentsCategory { get; private set; }
        public ITitleRepository Title { get; set; }
        public IPositionRepository Position { get; set; }
        public IEmploymentStatusRepository EmploymentStatus { get; set; }
        public INationalityRepository Nationality { get; set; }
        public IMartialStatusRepository MartialStatus { get; set; }
        public ICountryRepository Country { get; set; }
        public INotesRepository Notes { get; set; }
        public IEmergenceyContactRepository EmergenceyContact { get; set; }
        public ITeamLeaderRepository TeamLeader { get; set; }

        public int Complete(bool sendNotify = true, string optionalMsg = "Action Completed Successfully")
        {
            try
            {
                int res = _context.SaveChanges();
                if(sendNotify)
                    Startup.MiscellaneousMethod.ActionCompletionNotify(optionalMsg);

                return res;
            }
            catch (Exception ll)
            {
                Startup.MiscellaneousMethod.ActionCompletionNotify("Operation Failed: " + ll.Message);
                return 0;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }

    }
}