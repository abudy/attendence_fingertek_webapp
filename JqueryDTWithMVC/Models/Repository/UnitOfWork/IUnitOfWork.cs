﻿using JqueryDTWithMVC.Models.Repository.Branches;
using JqueryDTWithMVC.Models.Repository.Documents;
using JqueryDTWithMVC.Models.Repository.DocumentsCategory;
using JqueryDTWithMVC.Models.Repository.Teams;
using JqueryDTWithMVC.Models.Repository.TimeOff;
using JqueryDTWithMVC.Models.Repository.Position;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JqueryDTWithMVC.Models.Repository.Title;
using JqueryDTWithMVC.Models.Repository.Country;
using JqueryDTWithMVC.Models.Repository.Nationality;
using JqueryDTWithMVC.Models.Repository.MartialStatus;
using JqueryDTWithMVC.Models.Repository.EmploymentStatus;
using JqueryDTWithMVC.Models.Repository.Notes;
using JqueryDTWithMVC.Models.Repository.TeamLeader;

namespace JqueryDTWithMVC.Models.Repository.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        ITeamRepository Teams { get; }
        ITimeOffRepository TimeOffs { get; }
        ITimeOffTypeRepository TimeOffType { get; }
        IUserRepository Users { get;}
        IBranchesRepository Branches { get; }
        IDocumentsRepository Documents { get; }
        IDocumentsCategoryRepository DocumentsCategory { get; }
        ITitleRepository Title { get; set; }
        IPositionRepository Position { get; set; }
        IEmploymentStatusRepository EmploymentStatus { get; set; }
        INationalityRepository Nationality { get; set; }
        IMartialStatusRepository MartialStatus { get; set; }
        ICountryRepository Country { get; set; }
        INotesRepository Notes { get; set; }
        ITeamLeaderRepository TeamLeader { get; set; }
        int Complete(bool sendNotify, string optionalMsg);
    }
}
