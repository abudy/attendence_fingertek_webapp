﻿using JqueryDTWithMVC.Models.DAL.MaritalStatus;
using JqueryDTWithMVC.Models.DAL.Nationality;
using JqueryDTWithMVC.Models.DAL.Notes;
using JqueryDTWithMVC.Models.DAL.VacationType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.Repository.Notes
{
    public interface INotesRepository : IRepository<NotesModel>
    {
        IQueryable<NotesModel> GetNotesForUser(string userId);
    }
}