﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using JqueryDTWithMVC.Models.DAL.Vacation;
using JqueryDTWithMVC.Models.DAL.VacationType;
using JqueryDTWithMVC.Models.DAL.MaritalStatus;
using JqueryDTWithMVC.Models.DAL.Nationality;
using JqueryDTWithMVC.Models.DAL.Notes;
using JqueryDTWithMVC.Models.Repository.Notes;

namespace JqueryDTWithMVC.Models.Repository.Nationality
{
    public class NotesRepository : Repository.Repository<NotesModel> , INotesRepository
    {
        public NotesRepository(ApplicationDbContext context) 
            : base(context)
        {
        }

        public ApplicationDbContext ApplicationDbContext
        {
            get
            {
                return Context as ApplicationDbContext;
            }
        }

        public IQueryable<NotesModel> GetNotesForUser(string userId)
        {
            return ApplicationDbContext.NotesTable.Where(a => a.ApplicationUserId == userId);
        }
        
    }
}