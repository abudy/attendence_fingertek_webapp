﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using JqueryDTWithMVC.Models.DAL.Documents;
using JqueryDTWithMVC.Models.ViewModels.Documents;

namespace JqueryDTWithMVC.Models.Repository.Documents
{
    public class DocumentsRepository : Repository.Repository<DocumentsModels>, IDocumentsRepository
    {
        public DocumentsRepository(ApplicationDbContext context)
            : base(context)
        {
        }
        public ApplicationDbContext ApplicationDbContext
        {
            get
            {
                return Context as ApplicationDbContext;
            }
        }


        public Tuple<DocumentsModels, string[]> ToModel(DocumentsViewModel vModel)
        {
            if (Any(a => a.ID == vModel.ID) || vModel.uploadedFile == null)
            {
                var doc = Get(vModel.ID);
                doc.DocumentsCategoryModelsID = vModel.DocumentsCategoryModelsID;
                doc.fileName = Get(vModel.ID).fileName;
                doc.filePath = Get(vModel.ID).filePath;
                return Tuple.Create(doc, vModel.sharedWithUsers);
            }
            else
            {
                return Tuple.Create(new DocumentsModels()
                {
                    ID = vModel.ID,
                    DocumentsCategoryModelsID = vModel.DocumentsCategoryModelsID,
                    fileName = vModel.uploadedFile.FileName,
                    filePath = "/Content/EmployeeFiles/" + vModel.uploadedFile.FileName,
                }, vModel.sharedWithUsers);
            }

        }

        public void AddDocument(DocumentsModels model)
        {
            ApplicationDbContext.DocumentsTable.Add(model);
        }

        public void UpdateDocument(DocumentsViewModel documentsViewModels)
        {
            var res = ToModel(documentsViewModels);
            ApplicationDbContext.Entry(res.Item1).State = EntityState.Modified;

            foreach(var user in ApplicationDbContext.DocumentsWithUsersTable.
                Where(aa=>aa.DocumentsModelsID == res.Item1.ID))
            {
                ApplicationDbContext.DocumentsWithUsersTable.Remove(user);              
            }

            // add users to document exist
            if(res.Item2 != null)
            {
                for (int i = 0; i < res.Item2.Length; i++)
                {
                    AddUserShare(Find(a => a.ID == documentsViewModels.ID)
                        .OrderByDescending(a => a.ID).First().ID // get the last file added by this name
                        , res.Item2[i]);
                }
            }

        }

        public void RemoveDocument(int id)
        {
            var doc = ApplicationDbContext.DocumentsTable.Include(aa => aa.sharedWithUsers).First(a => a.ID == id);
            foreach (var sharedWith in doc.sharedWithUsers)
            {
                ApplicationDbContext.DocumentsWithUsersTable.Remove(sharedWith);
            }

            ApplicationDbContext.DocumentsTable.Remove(doc);
        }

        public void RemoveUserShare()
        {
        }

        public void AddUserShare(int docId, string userId)
        {
            var user = ApplicationDbContext.Users.FirstOrDefault(a => a.Id == userId);
          //  if (!ApplicationDbContext.DocumentsWithUsersTable.Any(a => a.ApplicationUserID == user.Id
            //        && a.DocumentsModelsID == docId))
            {
                DAL.Documents.DocumentsWithUsersModels relation = new DocumentsWithUsersModels()
                {
                    ApplicationUserId = user.Id,
                    DocumentsModelsID = docId
                };

                ApplicationDbContext.DocumentsWithUsersTable.Add(relation);
            }
        }

        public void StoreFile(HttpPostedFileBase file, string path)
        {
            JqueryDTWithMVC.Helper.FileStorageClass.StoreFile(file, path);
        }
    }
}