﻿using JqueryDTWithMVC.Models.DAL.Documents;
using JqueryDTWithMVC.Models.ViewModels.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.Repository.Documents
{
    public interface IDocumentsRepository : IRepository<DocumentsModels>
    {
        Tuple<DocumentsModels, string[]> ToModel(DocumentsViewModel vModel);

        void AddDocument(DocumentsModels model);
        void UpdateDocument(DocumentsViewModel documentsViewModels);
        void RemoveUserShare();
        void AddUserShare(int docId, string userId);
        void RemoveDocument(int id);

        void StoreFile(HttpPostedFileBase file, string path);
    }
}