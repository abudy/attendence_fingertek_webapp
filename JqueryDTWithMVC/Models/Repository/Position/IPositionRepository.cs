﻿using JqueryDTWithMVC.Models.DAL.Position;
using JqueryDTWithMVC.Models.DAL.Title;
using JqueryDTWithMVC.Models.DAL.VacationType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.Repository.Position
{
    public interface IPositionRepository : IRepository<PositionModel>
    {
        
    }
}