﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using JqueryDTWithMVC.Models.DAL.Vacation;
using JqueryDTWithMVC.Models.DAL.VacationType;
using JqueryDTWithMVC.Models.DAL.Title;
using JqueryDTWithMVC.Models.DAL.Position;

namespace JqueryDTWithMVC.Models.Repository.Position
{
    public class PositionRepository : Repository.Repository<PositionModel> , IPositionRepository
    {
        public PositionRepository(ApplicationDbContext context) 
            : base(context)
        {
        }

        public ApplicationDbContext ApplicationDbContext
        {
            get
            {
                return Context as ApplicationDbContext;
            }
        } 
    }
}