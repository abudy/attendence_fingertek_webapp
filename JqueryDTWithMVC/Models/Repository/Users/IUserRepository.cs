﻿using JqueryDTWithMVC.Models.DAL.ActionCompletion;
using JqueryDTWithMVC.Models.DAL.Teams;
using JqueryDTWithMVC.Models.ViewModels.EmployeeInfo;
using JqueryDTWithMVC.Models.ViewModels.Personnal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.Repository.Teams
{
    public interface IUserRepository : IRepository<ApplicationUser>
    {
        int? GetUserBranch(string Id);
        void UpdateUser(string Id);

        void UpdateUserEmployeeInformations(EmployeeInfoViewModel empInfo);
        EmployeeInfoViewModel UserToEmployeeInfo(string userId);

        void UpdateUserPersonnalInformations(PersonnalViewModel personnalInfo);
        PersonnalViewModel UserToPersonnalInfo(string userId);

        IQueryable<ActionCompletionModel> GetCompletedWorkForUser(string userId);
        void AddCompletedAction(ActionCompletionModel action);
        IQueryable<ApplicationUser> GetAllUsersWithInclude();
    }
}