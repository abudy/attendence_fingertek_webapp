﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using JqueryDTWithMVC.Models.DAL.Teams;
using JqueryDTWithMVC.Models.ViewModels.EmployeeInfo;
using JqueryDTWithMVC.Models.ViewModels.Personnal;
using JqueryDTWithMVC.Models.DAL.ActionCompletion;

namespace JqueryDTWithMVC.Models.Repository.Teams
{
    public class UserRepository : Repository.Repository<ApplicationUser> , IUserRepository
    {
        public UserRepository(ApplicationDbContext context) 
            : base(context)
        {
        }
        public ApplicationDbContext ApplicationDbContext
        {
            get
            {
                return Context as ApplicationDbContext;
            }
        }

        public int? GetUserBranch(string Id)
        {
            var user = ApplicationDbContext.Users.Find(Id);
            return user.BranchesModelID;
        }

        public void UpdateUser(string Id)
        {
            var user = ApplicationDbContext.Users.Find(Id);
            ApplicationDbContext.Entry(user).State = EntityState.Modified;
        }

        public void UpdateUserEmployeeInformations(EmployeeInfoViewModel empInfo)
        {      
            var user = ApplicationDbContext.Users.Find(empInfo.userId);
            user.TeamsModelID = empInfo.TeamsModelID;
            user.TitleModelID = empInfo.TitleModelID;
            user.perssonalEmail = empInfo.workEmail;
            user.PositionModelID = empInfo.PositionModelID;
            user.ApplicationUserId = empInfo.ApplicationUserId;
            user.dateEnrollDay = user.dateEnrollDay.HasValue ? empInfo.dateEnrollDay.Value.AddDays(empInfo.dateEnrollDay.Value.Subtract(user.dateEnrollDay.Value).TotalDays) : empInfo.dateEnrollDay;
            user.skype = empInfo.skype;
            user.linkedln = empInfo.linkedln;
            user.drivingLicense = empInfo.drivingLicense;
            user.firstName = empInfo.firstName;
            user.lastName = empInfo.lastName;
            user.EmploymentStateModelID = empInfo.EmploymentStateModelID;
            ApplicationDbContext.Entry(user).State = EntityState.Modified;
        }


        public EmployeeInfoViewModel UserToEmployeeInfo(string userId)
        {
            var user = ApplicationDbContext.Users.Find(userId);
            return new EmployeeInfoViewModel()
            {
                TeamsModelID = user.TeamsModelID,
                ApplicationUserId = user.ApplicationUserId,
                dateEnrollDay = user.dateEnrollDay,
                drivingLicense = user.drivingLicense,
                EmploymentStateModelID = user.EmploymentStateModelID,
                PositionModelID = user.PositionModelID,
                firstName = user.firstName,
                lastName = user.lastName,
                linkedln = user.linkedln,
                skype  = user.skype,
                TitleModelID = user.TitleModelID,
                userId = user.Id,
                workEmail = user.perssonalEmail
            };
        }


        public void UpdateUserPersonnalInformations(PersonnalViewModel personnalInfo)
        {
            var user = ApplicationDbContext.Users.Find(personnalInfo.userId);

            user.city = personnalInfo.city;
            user.street_1 = personnalInfo.street_1;
            user.street_2 = personnalInfo.street_2;
            user.homePhone = personnalInfo.homePhone;
            user.workPhone = personnalInfo.workPhone;
            user.NationalityModelID = personnalInfo.NationalityModelID;
            user.MaritalStatusModelID = personnalInfo.MaritalStatusModelID;
            user.postCode = personnalInfo.postCode;
            user.CountryModelID = personnalInfo.CountryModelID;
            user.NationalityModelID = personnalInfo.NationalityModelID;
            user.EmployeeGender = personnalInfo.EmployeeGender;

            ApplicationDbContext.Entry(user).State = EntityState.Modified;
        }

        public PersonnalViewModel UserToPersonnalInfo(string userId)
        {
            var user = ApplicationDbContext.Users.Find(userId);

            return new PersonnalViewModel()
            {
                city = user.city,
                street_2 = user.street_2,
                street_1 = user.street_1,
                CountryModelID = user.CountryModelID,
                dateOfBirth = user.empBirthDay,
                EmployeeGender = user.EmployeeGender,
                homePhone = user.homePhone,
                workPhone = user.workPhone,
                MaritalStatusModelID = user.MaritalStatusModelID,
                NationalityModelID = user.NationalityModelID,
                postCode = user.postCode,
                state  = user.state,
                userId = userId,
                firstName = String.IsNullOrEmpty( user.firstName) ? user.EmployeeName : user.firstName,
                lastName = user.lastName
            };
        }

        public IQueryable<ActionCompletionModel> GetCompletedWorkForUser(string userId)
        {
            return ApplicationDbContext.ActionCompletionTable.Where(a => a.ApplicationUserID == userId);
        }

        public IQueryable<ApplicationUser> GetAllUsersWithInclude()
        {
            return ApplicationDbContext.Users.Include(aa => aa.user.relatedTeam.leadersList);
        }


        public void AddCompletedAction(ActionCompletionModel action)
        {
            ApplicationDbContext.ActionCompletionTable.Add(action);
        }
    }
}