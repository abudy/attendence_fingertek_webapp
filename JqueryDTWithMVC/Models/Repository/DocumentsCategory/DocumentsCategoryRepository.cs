﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using JqueryDTWithMVC.Models.DAL.DocumentsCategory;
using JqueryDTWithMVC.Models.Repository.DocumentsCategory;

namespace JqueryDTWithMVC.Models.Repository.DocumentsCategory
{
    public class DocumentsCategoryModelsRepository : Repository.Repository<DocumentsCategoryModels>, IDocumentsCategoryRepository
    {
        public DocumentsCategoryModelsRepository(ApplicationDbContext context)
            : base(context)
        {
        }
        public ApplicationDbContext ApplicationDbContext
        {
            get
            {
                return Context as ApplicationDbContext;
            }
        }
    
    }
}