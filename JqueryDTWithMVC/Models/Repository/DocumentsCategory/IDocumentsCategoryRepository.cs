﻿using JqueryDTWithMVC.Models.DAL.DocumentsCategory;
using JqueryDTWithMVC.Models.ViewModels.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.Repository.DocumentsCategory
{
    public interface IDocumentsCategoryRepository : IRepository<DocumentsCategoryModels>
    {
   
    }
}