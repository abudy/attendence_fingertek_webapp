﻿using JqueryDTWithMVC.Models.DAL.Vacation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.Repository.TimeOff
{
    public interface ITimeOffRepository: IRepository<VacationsModel>
    {
        IEnumerable<VacationsModel> GetTimeOffForBranch(int brId);
        IEnumerable<VacationsModel> GetTimeOffForUser(string userId);
        void UpdateTimeOff(VacationsModel model);
        IQueryable<VacationsModel> GetVactionsAllWithInclude();
        void ChangeTimeOffStatus(int id, ReqState state);
    }
}