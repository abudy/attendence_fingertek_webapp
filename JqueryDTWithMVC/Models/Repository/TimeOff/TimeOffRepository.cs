﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using JqueryDTWithMVC.Models.DAL.Vacation;

namespace JqueryDTWithMVC.Models.Repository.TimeOff
{
    public class TimeOffRepository : Repository.Repository<VacationsModel> ,ITimeOffRepository
    {
        public TimeOffRepository(ApplicationDbContext context) 
            : base(context)
        {
        }

        public ApplicationDbContext ApplicationDbContext
        {
            get
            {
                return Context as ApplicationDbContext;
            }
        }

        public void ChangeTimeOffStatus(int id, ReqState state)
        {
            var vac = Get(id);
            vac.approvalstate = state;

            UpdateTimeOff(vac);
        }

        public IEnumerable<VacationsModel> GetTimeOffForBranch(int brId)
        {
            return ApplicationDbContext.VacationsTable.Include(aa=>aa.user)
                .Where(a =>  a.user.BranchesModelID.HasValue ? a.user.BranchesModelID == brId : false);
        }

        public IEnumerable<VacationsModel> GetTimeOffForUser(string userId)
        {
            return ApplicationDbContext.VacationsTable
                .Where(a => a.ApplicationUserId == userId);
        }

        public void UpdateTimeOff(VacationsModel model)
        {
            ApplicationDbContext.Entry(model).State = EntityState.Modified;
        }

        public IQueryable<VacationsModel> GetVactionsAllWithInclude()
        {
            return ApplicationDbContext.VacationsTable.Include(aa => aa.user.relatedTeam.leadersList);
        }
    }
}