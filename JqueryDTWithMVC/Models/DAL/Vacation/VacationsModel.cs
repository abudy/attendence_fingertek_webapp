﻿using JqueryDTWithMVC.Models.DAL.Branches;
using JqueryDTWithMVC.Models.DAL.VacationType;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.Vacation
{
    public enum ReqState
    {
        pending, approved, canceled, declined
    }

    public enum PartOfDay
    {
        all, first, second
    }

    public class VacationsModel
    {
        public int ID { get; set; }

        [Display(Name = "Part of Day ")]
        public PartOfDay day_part { get; set; }

        [Display(Name ="From Date")]
        [Required(ErrorMessage ="{0} Field is Required")]
        public DateTime fromDate { get; set; }

        [Display(Name = "To Date")]
        public DateTime? toDate { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "Hours: ")]
        public int? hours { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "Status")]
        public ReqState approvalstate { get; set; }

        [Display(Name = "Reason")]
        [Required(ErrorMessage = "{0} Field is Required")]
        public string message { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "Created on")]
        public DateTime date_requested { get; set; }

        [Display(Name = "TimeOff Type")]
        [Required(ErrorMessage = "{0} Field is Required")]
        public int VacationTypeModelID { get; set; }
        public virtual VacationTypeModel vacType { get; set; }

        [Display(Name = "Employee")]
        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser user { get; set; }

        public int? BranchesModelID { get; set; }
        public virtual BranchesModel assoicatedBranch { get; set; }
    }

}