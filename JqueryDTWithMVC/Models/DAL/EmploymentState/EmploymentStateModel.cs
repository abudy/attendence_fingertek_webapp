﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.EmploymentState
{
    public enum employemntStatus
    {
        FullTime, PartTime, Contact, Intern
    }

    public class EmploymentStateModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Field {0} is required")]
        [Display(Name = "Employment State")]
        public string title { get; set; }

        [Display(Name = "Employment State Reference")]
        public employemntStatus state { get; set; }

        public List<ApplicationUser> usersList { get; set; }
    }
}