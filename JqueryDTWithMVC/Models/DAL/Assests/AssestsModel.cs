﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.Assests
{
    public enum AssestsState
    {
        Loaned, Returned
    }

    public class AssestsModel
    {
        public int ID { get; set; }

        [Display(Name = "Assests Category")]
        public int AssestsCategoryModelID { get; set; }
        public AssestsCategoryModel assetsCatgeory { get; set; }

        [Display(Name ="Assests State")]
        public AssestsState assestValue { get; set; }
        
        [Display(Name ="Assest Description")]
        [DataType(DataType.MultilineText)]
        public string assestDesription { get; set; }

        [Display(Name = "Serial Number")]
        public string SerialNumber { get; set; }

        [Display(Name = "Date Loaned")]
        [ScaffoldColumn(false)]
        public DateTime dateLoaned { get; set; }

        [Display(Name = "Date Returned")]
        public DateTime? dateRetunred { get; set; }

        public string ApplicationUserId { get; set; }
        public ApplicationUser user { get; set; }       
    }

    public class AssestsCategoryModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Field {0} is required")]
        [Display(Name = "Category Name")]
        public string categoryName { get; set; }

        public List<AssestsModel> assests { get; set; }
    }
}