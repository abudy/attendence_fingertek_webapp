﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.SideBarItems
{
    public class SideBarItemsModels
    {
        public int Id { get; set; }

        [DisplayName("المتحكم المطلوب ")]
        public string ControllerName { get; set; }

        [DisplayName("الدالة المطلوبة ")]
        public string ActionMethodName { get; set; }

        [DisplayName("الاسم بالعربي")]
        public string Description { get; set; }

        [DisplayName("الاسم بالانكليزي")]
        public string DescriptionEng { get; set; }

        [DisplayName("صلاحيات من (اتركة فراغا اذا كان للجميع)")]
        public string AccessedByRoles { get; set; }

        [DisplayName("اختر الايقونة: ")]
        public string selectedIcon { get; set; }

        [DisplayName("اختر الفئه العليا")]
        public int? SideBarItemsModelsID { get; set; }
        [DisplayName("اختر الفئه العليا")]
        public SideBarItemsModels parentCategory { get; set; }
        public List<SideBarItemsModels> childsCategories { get; set; }
    }
}