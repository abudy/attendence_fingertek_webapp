﻿using JqueryDTWithMVC.Models.DAL.CheckPointLog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.CheckType
{
    public enum LogStatus
    {
        حضور, اجازة, غير_معرف, تاخير , خروج_مبكر, غياب
    }

    public class CheckTypeModels
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [StringLength(30)]
        [Display(Name ="اتواع الحضور")]
        public string CheckName { get; set; }

        [StringLength(30)]
        [Display(Name = "انواع الحضور من ال CakeHR")]
        public string CheckNameFromCakeHR { get; set; }

        [ScaffoldColumn(false)]
        public LogStatus state { get; set; } 

        public List<CheckPointLogModels> relatedPointLog { get; set; }
    }
}