﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.MaritalStatus
{
    public class MaritalStatusModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Field {0} is required")]
        [Display(Name = "Status Name")]
        public string status { get; set; }

        public List<ApplicationUser> usersList { get; set; }
    }
}