﻿using JqueryDTWithMVC.Models.DAL.Documents;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.DocumentsCategory
{
    public class DocumentsCategoryModels
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "{0} Field is Required")]
        [Display(Name ="Document Category Name")]
        [MaxLength(100, ErrorMessage ="{0} Field cann't be larger than 100 Character")]
        [MinLength(1, ErrorMessage = "{0} Field cann't be smaller than 1 Character")]   
        public string catgeoryName { get; set; }

        public List<DocumentsModels> associatedList { get; set; }
    }
}