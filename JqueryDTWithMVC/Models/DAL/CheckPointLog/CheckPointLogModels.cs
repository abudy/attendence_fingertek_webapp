﻿using JqueryDTWithMVC.Models.DAL.Branches;
using JqueryDTWithMVC.Models.DAL.CheckPointSummery;
using JqueryDTWithMVC.Models.DAL.CheckType;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.CheckPointLog
{
    public class CheckPointLogModels
    {
        public int ID { get; set; }

        [Display(Name = "اليوم")]
        public DateTime CurrentDay { get; set; }

        [Display(Name = "وقت الدخول")]
        public DateTime? CheckTimeIn { get; set; }

        [Display(Name = "وقت الخروج")]
        public DateTime? CheckTimeOut { get; set; }

        [Display(Name = "نوع الحضور")]
        public int? CheckTypeModelsID { get; set; }
        public CheckTypeModels checkType { get; set; }

        // those two fields are added to make it easy to:
          /// <summary>
          ///  make it easy to calculte employee lagging in minutes
          /// </summary>
        public int? lateMin { get; set; } // number of minutes employee was late
        public int? extraTime { get; set; }
        public int? vacationMin { get; set; } // number of minutes employee was in vacation

        /// <summary>
        /// identify record coming from finger print device for specific employee consist of:       
        ///             year + month + day + enrollNumber + branchID
        /// </summary>
        public string recordIdentifer { get; set; }

        [Display(Name = "المستخدم")]
        public string userId { get; set; }

        public string ApplicationUserId { get; set; }
        public ApplicationUser user { get; set; }

        [Display(Name = "المقر")]
        public int? BranchesModelID { get; set; }
        public BranchesModel branch { get; set; }
    
        [ScaffoldColumn(false)]
        public int? CheckPointSummeryModelID { get; set; }
        public virtual CheckPointSummeryModel CheckPointSummeryModel { get; set; }
    }
}