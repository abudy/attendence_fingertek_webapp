﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.ActivityLog
{
    public enum ActionType
    {
        Create, Edit, Delete, Printing, Error, Checked_IN_Twice, Employee_Not_Found
    }

    public class ActivityLogModel
    {
        public int ID { get; set; }

        [DisplayName("الرقم التعريفي")]
        public string userId { get; set; }

        [DisplayName("اسم المستخدم")]
        public string userName { get; set; }

        [DisplayName("وقت الفعالية")]
        public DateTime activityTime { get; set; }

        [DisplayName("المدة")]
        public int duration { get; set; }

        [DisplayName("نوع العملية")]
        public ActionType actionType { get; set; }

        [DisplayName("وصف الفعالية")]
        public string actionDescription { get; set; }

        [DisplayName("المتحكم")]
        public string controller { get; set; }

        [DisplayName("الرابط")]
        public string actionresult { get; set; }

        [DisplayName("القيم المرسلة")]
        public string valueLog { get; set; }
    }
}