﻿using JqueryDTWithMVC.Models.DAL.CheckPointLog;
using JqueryDTWithMVC.Models.DAL.Departments;
using JqueryDTWithMVC.Models.DAL.Vacation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JqueryDTWithMVC.Models.DAL.Branches
{
    public class BranchesModel
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [StringLength(80)]
        [Display(Name ="اسم المقر")]
        public string branch_name { get; set; }

        [Required]
        [StringLength(16)]
        [Display(Name = "عنوان بروتوكول الانترنيت (IP)")]
        [RegularExpression(@"^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$")]
        [System.Web.Mvc.Remote("CheckExistingIPAddress", "RemoteValidation", HttpMethod = "POST", ErrorMessage = "عنوان الايبي  موجود مسبقأ")]
        public string branch_ip { get; set; }

        [Display(Name = "رقم المنفذ (Port Number)")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "رقم البورت يجب ان يتكون من ارقام حصرا")]
        public int branch_port { get; set; }

        [Display(Name = "المفتاح السري لجهاز البصمة")]
        public string branch_key { get; set; }

        [Required]
        [Display(Name = "موقع المقر")]
        public string branch_location { get; set; }

        [Display(Name ="مفتاح الاتصال بال cakeHR")]
        public string apiKey { get; set; }

        public List<DepartmentsModel> depLists { get; set; }
        public List<CheckPointLogModels> CheckPointLogLists { get; set; }

        public List<VacationsModel> associatedVacations { get; set; }
    }
}