﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.Settings
{
    public class SettingsModel
    {
        public int ID { get; set; }

        [Required]
        [StringLength(1000)]
        [Display(Name = " الاعداد")]
        public string Key { get; set; }

        [Required]
        [StringLength(1000)]
        [Display(Name = " القيمة")]
        public string Value { get; set; }
    }
}