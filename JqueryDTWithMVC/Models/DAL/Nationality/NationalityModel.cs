﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.Nationality
{
    public class NationalityModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Field {0} is required")]
        [Display(Name = "Nation")]
        public string nation { get; set; }

        public List<ApplicationUser> usersList { get; set; }
    }
}