﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace JqueryDTWithMVC.Models.DAL.AccessControl
{
    public class AccessControlModel
    {
        public int ID { get; set; }

        [Required]
        [DisplayName("اسم الفعالية")]
        public string functionalityName { get; set; }

        [Required]
        [DisplayName("اسم المتحكم")]
        public string controllerName { get; set; }

        [Required]
        [DisplayName("اسم الدالة")]
        public string actionMethodName { get; set; }

        [DisplayName("الادوار المسموحة")]
        public string allowRoles { get; set; }

        [DisplayName("الادوار الممنوعة")]
        public string preventedRoles { get; set; }

        [DisplayName("المستخدمين المسموح لهم بالدخول")]
        public string allowedUsers { get; set; }
    }
}