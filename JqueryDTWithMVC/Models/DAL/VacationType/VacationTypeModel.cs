﻿using JqueryDTWithMVC.Models.DAL.Vacation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.VacationType
{
    public enum ResetEvery
    {
        None, Yearly, Monthly, Weekly
    }

    public class VacationTypeModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage ="TimeOff Policy Name Field is Required")]
        [Display(Name = "TimeOff Policy Name* ")]
        public string vacationName { get; set; }

        [Required(ErrorMessage = "Time Off Policy Reset Every Field is Required")]
        [Display(Name = "Time Off Policy Reset Every* ")]
        public ResetEvery policyResetEvery { get; set; }

        [Display(Name = "Balance ")]
        public int? balance { get; set; }

        [Display(Name = "CakeHR Id ")]
        public int? timeOff_Id { get; set; } // for backward compatability with cakeHR

        public List<VacationsModel> associatedVacations { get; set; }
    }
}