﻿using JqueryDTWithMVC.Models.DAL.Branches;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace JqueryDTWithMVC.Models.DAL.Departments
{
    public class DepartmentsModel
    {
        public int ID { get; set; }

        [Required]
        [StringLength(120)]
        [Display(Name = "اسم القسم")]
        public string depName { get; set; }

        public string managerId { get; set; }

        [Display(Name = "المقر")]
        public int? BranchesModelID { get; set; }
        public BranchesModel relatedBranch { get; set; }

        public List<ApplicationUser> usersList { get; set; }
    }
}