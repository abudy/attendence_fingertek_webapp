﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.EmergenceyContact
{
    public class EmergenceyContactModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage ="field {0} is required")]
        [Display(Name="Name")]
        public string Name { get; set; }

        [Display(Name = "RelationShip")]
        public string RelationShip { get; set; }

        [Display(Name = "Home Phone")]
        public string HomePhone { get; set; }

        [Display(Name = "Work Phone")]
        public string WorkPhone { get; set; }

        [Display(Name = "Mobile Phone")]
        public string MobilePhone { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }

        [Display(Name = "Address 1")]
        public string Address1 { get; set; }

        [Display(Name = "Address 2")]
        public string Address2 { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Display(Name = "Region")]
        public string Region { get; set; }

        [Display(Name = "PostCode")]
        public string PostCode { get; set; }

        public string ApplicationUserId { get; set; }
        public ApplicationUser user { get; set; }
    }
}