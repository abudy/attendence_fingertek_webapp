﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.ActionCompletion
{
    public class ActionCompletionModel
    {
        [Key]
        public int ID { get; set; }

        public string eventDescription { get; set; }

        public string ApplicationUserID { get; set; }
        public virtual ApplicationUser TargetUser { get; set; }

        public bool shown { get; set; }
    }
}