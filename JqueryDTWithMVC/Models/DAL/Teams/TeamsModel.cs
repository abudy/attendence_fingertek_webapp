﻿using JqueryDTWithMVC.Models.DAL.Branches;
using JqueryDTWithMVC.Models.DAL.TeamLeaders;
using JqueryDTWithMVC.Models.DAL.UsersWithTeams;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JqueryDTWithMVC.Models.DAL.Teams
{
    public enum TeamType
    {
        Managers, Employees
    }

    public class TeamsModel
    {
        public int ID { get; set; }

        [Required]
        [Display(Name ="Team Name")]
        [StringLength(30, MinimumLength = 2)]
        [Remote("IsTeam_Available", "Validation")]
        [Editable(true)]
        public string teamName { get; set; }

        [Display(Name = "Team Branch")]
        public int? BranchesModelID { get; set; }
        public virtual BranchesModel relatedBranch { get; set; }

        //[Required]
        //[ScaffoldColumn(false)]
        //public string userId { get; set; }
        //[Required]
        //[Display(Name = "Team Leader Name")]
        //public string userName { get; set; }

        [Display(Name = "Team Type")]
        public TeamType teamType { get; set; }

        public virtual List<ApplicationUser> relatedUsers { get; set; }
        public virtual List<TeamLeadersModels> leadersList { get; set; }
    }
}