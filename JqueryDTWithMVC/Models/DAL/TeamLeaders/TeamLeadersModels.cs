﻿using JqueryDTWithMVC.Models.DAL.Teams;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.TeamLeaders
{
    public class TeamLeadersModels
    {
        public int ID { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public string userId { get; set; }

        [Required]
        [Display(Name = "Team Leader Name")]
        public string userName { get; set; }

        [Display(Name = "Related Team")]
        public int? TeamsModelID { get; set; }
        public TeamsModel team { get; set; }
    }
}