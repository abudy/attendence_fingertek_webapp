﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.Notifications
{
    public class NotificationsModel
    {
        [Key]
        public int ID { get; set; }

        public DateTime eventDate { get; set; }

        public string eventDescription { get; set; }

        public string madeByName { get; set; }

        public string madeById { get; set; }

        public string eventURL { get; set; }

        public string ApplicationUserID { get; set; }
        public virtual ApplicationUser TargetUser { get; set; }

        public bool shown { get; set; }
    }
}