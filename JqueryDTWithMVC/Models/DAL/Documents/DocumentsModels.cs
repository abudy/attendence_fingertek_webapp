﻿using JqueryDTWithMVC.Models.DAL.DocumentsCategory;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
 

namespace JqueryDTWithMVC.Models.DAL.Documents
{
    public class DocumentsModels
    {
        public int ID { get; set; }

        [Display(Name = "Document Path")]
        [MaxLength(100, ErrorMessage = "{0} Field cann't be larger than 100 Character")]
        [MinLength(1, ErrorMessage = "{0} Field cann't be smaller than 1 Character")]
        public string filePath { get; set; }

        [Display(Name = "Document Name")]
        [MaxLength(100, ErrorMessage = "{0} Field cann't be larger than 100 Character")]
        [MinLength(1, ErrorMessage = "{0} Field cann't be smaller than 1 Character")]
        public string fileName { get; set; }

        public int DocumentsCategoryModelsID { get; set; }
        public virtual DocumentsCategoryModels documentCatgeory { get; set; }

        public virtual List<DocumentsWithUsersModels> sharedWithUsers { get; set; }
    }
}