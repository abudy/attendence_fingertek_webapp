﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.Documents
{
    public class DocumentsWithUsersModels
    {
        public int ID { get; set; }

        public int? DocumentsModelsID { get; set; }
        public virtual DocumentsModels document { get; set; }

        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser users { get; set; }
    }
}