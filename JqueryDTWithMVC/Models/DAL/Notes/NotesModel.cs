﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.Notes
{
    public class NotesModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Field {0} is required")]
        [Display(Name = "Note")]
        public string notesString { get; set; }

        public string ApplicationUserId { get; set; }
        public ApplicationUser user { get; set; }
    }
}