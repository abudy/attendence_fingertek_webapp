﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.Title
{
    public class TitleModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage ="Field {0} is required")]
        [Display(Name ="Title")]
        public string title { get; set; }

        public List<ApplicationUser> usersList { get; set; }
    }
}