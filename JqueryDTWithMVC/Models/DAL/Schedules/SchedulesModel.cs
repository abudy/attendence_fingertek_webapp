﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.Schedules
{
    public class SchedulesModel
    {
        [Key]
        public int ID { get; set; }

        [Display(Name = "اسم التوقيت")]
        [Required]
        public string schedule_name { get; set; }

        [Display(Name = "الوقت السماح")]
        [Required]
        public int schedule_allowtime { get; set; }

        [Display(Name = "السبت")]
        public string schedule_saturday { get; set; }

        [Display(Name = "السبت")]
        public string schedule_saturday2 { get; set; }

        [Display(Name = "الاحد")]
        public string schedule_sunday { get; set; }

        [Display(Name = "الاحد")]
        public string schedule_sunday2 { get; set; }

        [Display(Name = "الاثنين")]
        public string schedule_monday { get; set; }

        [Display(Name = "الاثنين")]
        public string schedule_monday2 { get; set; }

        [Display(Name = "الثلاثاء")]
        public string schedule_tuesday { get; set; }

        [Display(Name = "الثلاثاء")]
        public string schedule_tuesday2 { get; set; }

        [Display(Name = "الاربعاء")]
        public string schedule_wednesday { get; set; }

        [Display(Name = "الاربعاء")]
        public string schedule_wednesday2 { get; set; }

        [Display(Name = "الخميس")]
        public string schedule_thursday { get; set; }

        [Display(Name = "الخميس")]
        public string schedule_thursday2 { get; set; }

        [Display(Name = "الجمعة")]
        public string schedule_friday { get; set; }

        [Display(Name = "الجمعة")]
        public string schedule_friday2 { get; set; }

        public List<ApplicationUser> users { get; set; }
    }
}