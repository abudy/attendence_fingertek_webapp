﻿using JqueryDTWithMVC.Models.DAL.CheckPointLog;
using JqueryDTWithMVC.Models.DAL.CheckType;
using JqueryDTWithMVC.Models.DAL.Schedules;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Models.DAL.CheckPointSummery
{
    public class CheckPointSummeryModel
    {
        [Key]
        public int ID { get; set; }

        [Display(Name = "التاخير بالدقائق")]
        public double? LateBy { get; set; }
        [Display(Name = "دقائق الاجازة")]
        public double? VacationMin { get; set; }
        [Display(Name = "دقائق الاضافية")]
        public double? extraTime { get; set; }

        public DateTime currentDate { get; set; }

        [Display(Name = "المستخدم")]
        public string ApplicationUserId { get; set; }
        public ApplicationUser user { get; set; }

        public int CheckTypeModelsID { get; set; }
        public CheckTypeModels checkType { get; set; }

        [Display(Name = "الجدول")]
        public int SchedulesModelID { get; set; }
        public SchedulesModel relatedSchedule { get; set; }

        public virtual int CheckPointLogModelsID { get; set; }
        public virtual CheckPointLogModels CheckPointLogModels { get; set; }
    }
}