﻿using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.CheckType;
using JqueryDTWithMVC.Models.ViewModels.Schedule;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JqueryDTWithMVC.Helpers
{
    public class ScheduleValues
    {
        public int ID { get; set; }

        [Display(Name = "اسم التوقيت")]
        public string schedule_name { get; set; }

        [Display(Name = "الوقت السماح")]
        public string schedule_allowtime { get; set; }

        [Display(Name = "السبت")]
        public string schedule_saturday { get; set; }

        [Display(Name = "السبت")]
        public string schedule_saturday2 { get; set; }

        [Display(Name = "الاحد")]
        public string schedule_sunday { get; set; } // day number = 0

        [Display(Name = "الاحد")]
        public string schedule_sunday2 { get; set; }

        [Display(Name = "الاثنين")]
        public string schedule_monday { get; set; }

        [Display(Name = "الاثنين")]
        public string schedule_monday2 { get; set; }

        [Display(Name = "الثلاثاء")]
        public string schedule_tuesday { get; set; }

        [Display(Name = "الثلاثاء")]
        public string schedule_tuesday2 { get; set; }

        [Display(Name = "الاربعاء")]
        public string schedule_wednesday { get; set; }

        [Display(Name = "الاربعاء")]
        public string schedule_wednesday2 { get; set; }

        [Display(Name = "الخميس")]
        public string schedule_thursday { get; set; }

        [Display(Name = "الخميس")]
        public string schedule_thursday2 { get; set; }

        [Display(Name = "الجمعة")]
        public string schedule_friday { get; set; } // day number = 7

        [Display(Name = "الجمعة")]
        public string schedule_friday2 { get; set; }

        public Tuple<int, int, DateTime?, DateTime?> GetScheduleValues(string userID, int dayNumber, DateTime dt2)
        {
            using (ApplicationDbContext entityDb = new ApplicationDbContext())
            {
                var user = entityDb.Users.Include(aa => aa.relatedSchedule).First(a => a.Id == userID);


                if (user.relatedSchedule == null || user.SchedulesModelID.HasValue == false)
                {
                    new LogWriter("Schedule Not Associated: User with No." + user.EnrollNumber + " deson't have schedule associated with his account");
                    return Tuple.Create<int, int, DateTime?, DateTime?>(0, 0, null, null);
                }

                switch (dayNumber)
                {
                    case 0: // sunday
                        {
                            return Tuple.Create<int, int, DateTime?, DateTime?>(user.SchedulesModelID.Value, user.relatedSchedule.schedule_allowtime,
                                    ConvertToDateTime(dt2, user.relatedSchedule.schedule_sunday),
                                    ConvertToDateTime(dt2, user.relatedSchedule.schedule_sunday2)
                                );
                        }
                    case 1:
                        {
                            return Tuple.Create<int, int, DateTime?, DateTime?>(user.SchedulesModelID.Value, user.relatedSchedule.schedule_allowtime,
                                    ConvertToDateTime(dt2, user.relatedSchedule.schedule_monday),
                                    ConvertToDateTime(dt2, user.relatedSchedule.schedule_monday2)
                                );
                        }
                    case 2:
                        {
                            return Tuple.Create<int, int, DateTime?, DateTime?>(user.SchedulesModelID.Value, user.relatedSchedule.schedule_allowtime,
                                    ConvertToDateTime(dt2, user.relatedSchedule.schedule_thursday),
                                    ConvertToDateTime(dt2, user.relatedSchedule.schedule_thursday2));

                        }
                    case 3:
                        {
                            return Tuple.Create<int, int, DateTime?, DateTime?>(user.SchedulesModelID.Value, user.relatedSchedule.schedule_allowtime,
                                    ConvertToDateTime(dt2, user.relatedSchedule.schedule_wednesday),
                                    ConvertToDateTime(dt2, user.relatedSchedule.schedule_wednesday2));

                        }
                    case 4:
                        {
                            return Tuple.Create<int, int, DateTime?, DateTime?>(user.SchedulesModelID.Value, user.relatedSchedule.schedule_allowtime,
                                    ConvertToDateTime(dt2, user.relatedSchedule.schedule_thursday),
                                    ConvertToDateTime(dt2, user.relatedSchedule.schedule_thursday2));

                        }
                    case 5:
                        {
                            //if (String.IsNullOrEmpty(user.relatedSchedule.schedule_friday))
                            //{
                            //    return Tuple.Create<int, int, DateTime?, DateTime?>(user.SchedulesModelID, user.relatedSchedule.schedule_allowtime,
                            //            ConvertToDateTime(dt2, "23:59:59"),
                            //            ConvertToDateTime(dt2, "00:00:00"));
                            //}
                            //else
                            //{
                            return Tuple.Create<int, int, DateTime?, DateTime?>(user.SchedulesModelID.Value, user.relatedSchedule.schedule_allowtime,
                                   ConvertToDateTime(dt2, user.relatedSchedule.schedule_friday),
                                   ConvertToDateTime(dt2, user.relatedSchedule.schedule_friday2));
                            // }
                        }
                    case 6:
                        {
                            return Tuple.Create<int, int, DateTime?, DateTime?>(user.SchedulesModelID.Value, user.relatedSchedule.schedule_allowtime,
                                    ConvertToDateTime(dt2, user.relatedSchedule.schedule_saturday),
                                    ConvertToDateTime(dt2, user.relatedSchedule.schedule_saturday2));
                        }
                }

                return Tuple.Create<int, int, DateTime?, DateTime?>(0, 0, null, null);
            }
        }

        public bool UserHasDayOfResponability(string userID, int dayNumber)
        {
            using (ApplicationDbContext entityDb = new ApplicationDbContext())
            {
                var user = entityDb.Users.Include(aa => aa.relatedSchedule).First(a => a.Id == userID);
                if (user.relatedSchedule == null)
                {
                    new LogWriter("Schedule Not Associated: User with No." + user.EnrollNumber + " deson't have schedule associated with his account");
                    return false;
                }

                switch (dayNumber)
                {
                    case 0: // sunday
                        {
                            return String.IsNullOrEmpty(user.relatedSchedule.schedule_sunday) ? false : true;
                        }
                    case 1:
                        {
                            return String.IsNullOrEmpty(user.relatedSchedule.schedule_monday) ? false : true;
                        }
                    case 2:
                        {
                            return String.IsNullOrEmpty(user.relatedSchedule.schedule_thursday) ? false : true;

                        }
                    case 3:
                        {
                            return String.IsNullOrEmpty(user.relatedSchedule.schedule_wednesday) ? false : true;

                        }
                    case 4:
                        {
                            return String.IsNullOrEmpty(user.relatedSchedule.schedule_tuesday) ? false : true;

                        }
                    case 5:
                        {
                            return (String.IsNullOrEmpty(user.relatedSchedule.schedule_friday) || user.relatedSchedule.schedule_friday == "null") ? false : true;

                        }
                    case 6:
                        {
                            return String.IsNullOrEmpty(user.relatedSchedule.schedule_saturday) || user.relatedSchedule.schedule_saturday == "null" ? false : true;
                        }
                }

                return false;
            }
        }

        public SchedulesGeneralInfo ScheduleGeneralnfo(int id, DateTime fromDate, DateTime todate)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                List<DateTime> firstPart = new List<DateTime>();
                List<DateTime> secondPart = new List<DateTime>();
                SchedulesGeneralInfo geneInfo = new SchedulesGeneralInfo();

                double totalMinOfWork = 0;
                int numberOfWorkDaysPerSelectedDate = 0;

                var item = db.SchedulesTable.Find(id);
                DateTime u = fromDate;
                var restest = todate.Subtract(fromDate).TotalDays;

                for (int i = 0; i < todate.Subtract(fromDate).TotalDays; i++)
                {
                    switch ((int)u.DayOfWeek)
                    {
                        case 0: // sunday
                            {
                                if (!String.IsNullOrEmpty(item.schedule_sunday))
                                {
                                    totalMinOfWork += (int)ConvertToDateTime(System.DateTime.Now, item.schedule_sunday2).Subtract(ConvertToDateTime(System.DateTime.Now, item.schedule_sunday)).TotalMinutes;
                                    numberOfWorkDaysPerSelectedDate += 1;
                                }
                                break;
                            }
                        case 1:
                            {
                                if (!String.IsNullOrEmpty(item.schedule_monday))
                                {
                                    totalMinOfWork += (int)ConvertToDateTime(System.DateTime.Now, item.schedule_monday2).Subtract(ConvertToDateTime(System.DateTime.Now, item.schedule_monday)).TotalMinutes;
                                    numberOfWorkDaysPerSelectedDate += 1;

                                }
                                break;
                            }
                        case 2:
                            {
                                if (!String.IsNullOrEmpty(item.schedule_thursday))
                                {
                                    totalMinOfWork += (int)ConvertToDateTime(System.DateTime.Now, item.schedule_thursday2).Subtract(ConvertToDateTime(System.DateTime.Now, item.schedule_thursday)).TotalMinutes;
                                    numberOfWorkDaysPerSelectedDate += 1;

                                }
                                break;

                            }
                        case 3:
                            {
                                if (!String.IsNullOrEmpty(item.schedule_wednesday))
                                {
                                    totalMinOfWork += (int)ConvertToDateTime(System.DateTime.Now, item.schedule_wednesday2).Subtract(ConvertToDateTime(System.DateTime.Now, item.schedule_wednesday)).TotalMinutes;
                                    numberOfWorkDaysPerSelectedDate += 1;

                                }
                                break;

                            }
                        case 4:
                            {
                                if (!String.IsNullOrEmpty(item.schedule_tuesday))
                                {
                                    totalMinOfWork += (int)ConvertToDateTime(System.DateTime.Now, item.schedule_tuesday2).Subtract(ConvertToDateTime(System.DateTime.Now, item.schedule_tuesday)).TotalMinutes;
                                    numberOfWorkDaysPerSelectedDate += 1;

                                }
                                break;

                            }
                        case 5:
                            {
                                if (!String.IsNullOrEmpty(item.schedule_friday))
                                {
                                    totalMinOfWork += (int)ConvertToDateTime(System.DateTime.Now, item.schedule_friday2).Subtract(ConvertToDateTime(System.DateTime.Now, item.schedule_friday)).TotalMinutes;
                                    numberOfWorkDaysPerSelectedDate += 1;

                                }
                                break;

                            }
                        case 6:
                            {
                                if (!String.IsNullOrEmpty(item.schedule_saturday))
                                {
                                    totalMinOfWork += (int)ConvertToDateTime(System.DateTime.Now, item.schedule_saturday2).Subtract(ConvertToDateTime(System.DateTime.Now, item.schedule_saturday)).TotalMinutes;
                                    numberOfWorkDaysPerSelectedDate += 1;

                                }
                                break;

                            }
                    }

                    u = u.AddDays(1);
                }


                //foreach (var col in item.GetType().GetProperties().Where(p => p.Name.Contains("day") && !p.Name.Contains("2")))
                //{
                //    var col2 = item.GetType().GetProperties().ToList().First(a => a.Name == (col.Name + "2"));
                //    if (!String.IsNullOrEmpty((string)col.GetValue(item, null)) && !String.IsNullOrEmpty((string)col2.GetValue(item, null)))
                //    {
                //        var beg = ConvertToDateTime(System.DateTime.Now, (string)col.GetValue(item, null));
                //        var end = ConvertToDateTime(System.DateTime.Now, (string)col2.GetValue(item, null));
                //        totalMinPerWeek += (int)end.Subtract(beg).TotalMinutes - (int)item.GetType().GetProperties().First(p => p.Name.Contains("allowtime")).GetValue(item, null);
                //        totalWorkDaysPerWeek += 1;
                //    }
                //}

                return new SchedulesGeneralInfo()
                {
                    ID = id,
                    numberOfWorkDaysPerWeek = 0,
                    numOfWorkMinutesPerWeek = 0,
                    numOfMinWorkPerSelectedDate = totalMinOfWork,
                    numberOfWorkDaysPerSelectedDate = numberOfWorkDaysPerSelectedDate
                };
            }
        }

        public Tuple<int, int> CheckOutCheckInCompare(DateTime? attIn, DateTime? scIn, DateTime? attOut, DateTime? scOut, int allowMin, LogStatus state)
        {
            using (ApplicationDbContext entityDb = new ApplicationDbContext())
            {
                if ((int)state >= (int)LogStatus.غير_معرف && scIn.HasValue && scOut.HasValue)
                {
                    if ((attOut == null || attIn == null))
                    {
                        return Tuple.Create<int, int>(entityDb.CheckTypeTable.First(a => a.state == LogStatus.غير_معرف).ID, 0);
                    }
                    else
                    {
                        // sloving problem where some users are checked In after office hours
                        attIn = attIn > scOut ? scOut.Value : attIn;

                        int resID = entityDb.CheckTypeTable.First(a => a.state == LogStatus.حضور).ID;
                        int latBy = 0;
                        if (attIn.Value.Subtract(scIn.Value).TotalMinutes > allowMin) // for checkin 
                        {
                            resID = entityDb.CheckTypeTable.First(a => a.state == LogStatus.تاخير).ID;
                            latBy = Math.Abs((int)attIn.Value.Subtract(scIn.Value).TotalMinutes);
                        }
                        if (attOut.Value.Subtract(scOut.Value).TotalMinutes < 0) // for checkout
                        {
                            resID = entityDb.CheckTypeTable.First(a => a.state == LogStatus.خروج_مبكر).ID;
                            latBy += Math.Abs((int)attOut.Value.Subtract(scOut.Value).TotalMinutes);
                        }

                        return Tuple.Create<int, int>(resID, latBy);
                    }
                }
                return Tuple.Create<int, int>(entityDb.CheckTypeTable.First(a => a.state == state).ID, 0);
            }
        }

        public int CheckInCompare(DateTime? attIn, DateTime? scIn, int allowMin, LogStatus state)
        {
            using (ApplicationDbContext entityDb = new ApplicationDbContext())
            {
                if ((int)state >= (int)LogStatus.غير_معرف && scIn.HasValue)
                {
                    int latBy = 0;
                    if (attIn.Value.Subtract(scIn.Value).TotalMinutes > allowMin) // for checkin 
                    {
                        latBy = Math.Abs((int)attIn.Value.Subtract(scIn.Value).TotalMinutes);
                    }
                    return latBy;

                }
                return 0;
            }
        }

        public DateTime ConvertToDateTime(DateTime dt2, string hhMMss, string cul = "ar-IQ")
        {
            IFormatProvider culture = new System.Globalization.CultureInfo(cul, true);
            return DateTime.Parse(dt2.Day + "/" + dt2.Month + "/" + dt2.Year + " " + hhMMss, culture, System.Globalization.DateTimeStyles.AssumeLocal);
        }
    }
}
