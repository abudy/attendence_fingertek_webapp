﻿using JqueryDTWithMVC.Helpers;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.CheckPointLog;
using JqueryDTWithMVC.Models.DAL.CheckPointSummery;
using JqueryDTWithMVC.Models.DAL.CheckType;
using JqueryDTWithMVC.Models.ViewModels.DevicePoint;
using JqueryDTWithMVC.Models.ViewModels.UserInfoFromDevice;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace JqueryDTWithMVC.Helper
{
    public class SyncPointViewModel
    {
        public List<CheckPointLogModels> CheckPointLogTable { get; set; }
        public List<ApplicationUser> Users { get; set; }
        public List<CheckPointSummeryModel> CheckPointSummeryTable { get; set; }
    }

    public class ThreadContext
    {
        public int Value { get; set; }
        public int Result { get; set; }
    }

    public static class SyncPointsFromDevices
    {
        public static List<DevicePointViewModel> CallSDKForSpecificDevice(int? deviceId, DateTime t1, DateTime t2)
        {
            AxBioBridgeSDKv3.AxBioBridgeSDKv3lib sdk = new AxBioBridgeSDKv3.AxBioBridgeSDKv3lib();

            using (ApplicationDbContext entity = new ApplicationDbContext())
            {
                try
                {
                    string enrollNo = "0";
                    int yr = 0;
                    int mth = 0;
                    int day_Renamed = 0;
                    int hr = 0;
                    int min = 0;
                    int sec = 0;
                    int ver = 0;
                    int io = 0;
                    int work = 0;
                    int iSize = 0;

                    var device = entity.BranchesTable.First(a => a.ID == deviceId);
                    sdk.CreateControl();
                    sdk.Refresh();

                    if (sdk.Connect_TCPIP("", device.ID, device.branch_ip, device.branch_port, Convert.ToInt32(device.branch_key)) == 0)
                    {
                        //  list.Add(new objSDK() { ipaddress = device.branch_ip, sdk = sdk });
                        if (sdk.ReadGeneralLog(ref iSize) == 0)
                        {
                            List<DevicePointViewModel> dvViewModel = new List<DevicePointViewModel>();

                            while (sdk.SSR_GetGeneralLog(ref enrollNo, ref yr, ref mth, ref day_Renamed, ref hr,
                                                            ref min, ref sec, ref ver, ref io, ref work) == 0)
                            {
                                IFormatProvider culture = new System.Globalization.CultureInfo("ar-IQ", true);
                                DateTime dt2 = DateTime.Parse(day_Renamed + "/" + mth + "/" + yr + " " + hr + ":" + min + ":" + sec + "." + 0, culture, System.Globalization.DateTimeStyles.AssumeLocal);

                                if (enrollNo == "80" || enrollNo == "82")
                                {
                                    string kk = "";
                                }

                                if (dt2 >= t1 && dt2 <= t2)
                                {
                                    dvViewModel.Add(new DevicePointViewModel()
                                    {
                                        AttState = io,
                                        branchID = deviceId.Value,
                                        Year = yr,
                                        Month = mth,
                                        Day = day_Renamed,
                                        Hour = hr,
                                        Minute = min,
                                        Second = sec,
                                        IsInValid = 0,
                                        EnrollNo = enrollNo,
                                        VerifyMode = ver,
                                        WorkCode = work
                                    });
                                }
                            }

                            sdk.Disconnect();
                            return dvViewModel;
                        }
                    }
                    else
                    {
                        LogWriter log = new LogWriter("Error: Cann't connect to fingerprint device for Branch with ID: " + deviceId + " ");
                    }
                }
                catch (Exception ll)
                {
                    LogWriter log = new LogWriter("Exception: Branch ID: " + deviceId + " - Reason: " + ll.Message);
                }

                return new List<DevicePointViewModel>();
            }
        }

        public static List<DevicePointViewModel> CallSDKForSpecificDeviceWithTimer(int? deviceId, DateTime t1, DateTime t2)
        {
            AxBioBridgeSDKv3.AxBioBridgeSDKv3lib sdk = new AxBioBridgeSDKv3.AxBioBridgeSDKv3lib();

            using (ApplicationDbContext entity = new ApplicationDbContext())
            {
                try
                {

                    string enrollNo = "0";
                    int yr = 0;
                    int mth = 0;
                    int day_Renamed = 0;
                    int hr = 0;
                    int min = 0;
                    int sec = 0;
                    int ver = 0;
                    int io = 0;
                    int work = 0;
                    int iSize = 0;

                    var device = entity.BranchesTable.First(a => a.ID == deviceId);
                    sdk.CreateControl();
                    sdk.Refresh();

                    if (sdk.Connect_TCPIP("", device.ID, device.branch_ip, device.branch_port, Convert.ToInt32(device.branch_key)) == 0)
                    {
                        //  list.Add(new objSDK() { ipaddress = device.branch_ip, sdk = sdk });
                        if (sdk.ReadGeneralLog(ref iSize) == 0)
                        {
                            List<DevicePointViewModel> dvViewModel = new List<DevicePointViewModel>();
                            while (sdk.SSR_GetGeneralLog(ref enrollNo, ref yr, ref mth, ref day_Renamed, ref hr,
                                                            ref min, ref sec, ref ver, ref io, ref work) == 0)
                            {
                                IFormatProvider culture = new System.Globalization.CultureInfo("ar-IQ", true);
                                DateTime dt2 = DateTime.Parse(day_Renamed + "/" + mth + "/" + yr + " " + hr + ":" + min + ":" + sec + "." + 0, culture, System.Globalization.DateTimeStyles.AssumeLocal);

                                if (dt2 >= t1 && dt2 <= t2)
                                {
                                    dvViewModel.Add(new DevicePointViewModel()
                                    {
                                        AttState = io,
                                        branchID = deviceId.Value,
                                        Year = yr,
                                        Month = mth,
                                        Day = day_Renamed,
                                        Hour = hr,
                                        Minute = min,
                                        Second = sec,
                                        IsInValid = 0,
                                        EnrollNo = enrollNo,
                                        VerifyMode = ver,
                                        WorkCode = work
                                    });
                                }
                            }
                            sdk.Disconnect();

                            try
                            {
                                List<int> nonSynchedBranches = new List<int>();
                                //       using (Models.ApplicationDbContext entityDB = new ApplicationDbContext())
                                {
                                    foreach (var userr in dvViewModel.Select(a => a.EnrollNo).Distinct().ToList())
                                    {
                                        int enrollNum = Convert.ToInt32(userr);
                                        if (!entity.Users.Any(a => a.EnrollNumber == enrollNum && a.BranchesModelID.HasValue ? a.BranchesModelID.Value == deviceId.Value : false))
                                        {
                                            var missingUserResult = CallSDKForUserSync(deviceId.Value);
                                            RegisterFromDevice(missingUserResult);
                                            break; // run it, only one time
                                        }
                                    }
                                }
                            }
                            catch (Exception tt)
                            {
                                LogWriter log = new LogWriter(tt.Message);
                            }

                            return dvViewModel;
                        }
                    }
                    else
                    {
                        LogWriter log = new LogWriter("Error: Cann't connect to fingerprint device for Branch with ID: " + deviceId + " ");
                    }
                }
                catch (Exception ll)
                {
                    LogWriter log = new LogWriter("Exception: Branch ID: " + deviceId + " - Reason: " + ll.Message);
                }

                return new List<DevicePointViewModel>();
            }
        }

        public static List<DevicePointViewModel> CallSDKForAllDevices(DateTime t1, DateTime t2)
        {
            AxBioBridgeSDKLib.AxBioBridgeSDK sdk = new AxBioBridgeSDKLib.AxBioBridgeSDK();
            List<DevicePointViewModel> dvViewModelAll = new List<DevicePointViewModel>();
            using (ApplicationDbContext entity = new ApplicationDbContext())
            {
                foreach (var device in entity.BranchesTable.ToList())
                {
                    try
                    {
                        string enrollNo = "0";
                        int yr = 0;
                        int mth = 0;
                        int day_Renamed = 0;
                        int hr = 0;
                        int min = 0;
                        int sec = 0;
                        int ver = 0;
                        int io = 0;
                        int work = 0;
                        int iSize = 0;
                        sdk.CreateControl();
                        sdk.Refresh();

                        if (sdk.Connect_TCPIP("", device.ID, device.branch_ip, device.branch_port, Convert.ToInt32(device.branch_key)) == 0)
                        {
                            //  list.Add(new objSDK() { ipaddress = device.branch_ip, sdk = sdk });
                            if (sdk.ReadGeneralLog(ref iSize) == 0)
                            {
                                List<DevicePointViewModel> dvViewModel = new List<DevicePointViewModel>();

                                while (sdk.SSR_GetGeneralLog(ref enrollNo, ref yr, ref mth, ref day_Renamed, ref hr,
                                                                ref min, ref sec, ref ver, ref io, ref work) == 0)
                                {
                                    IFormatProvider culture = new System.Globalization.CultureInfo("ar-IQ", true);
                                    DateTime dt2 = DateTime.Parse(day_Renamed + "/" + mth + "/" + yr + " " + hr + ":" + min + ":" + sec + "." + 0, culture, System.Globalization.DateTimeStyles.AssumeLocal);

                                    if (dt2 >= t1 && dt2 <= t2)
                                    {
                                        dvViewModel.Add(new DevicePointViewModel()
                                        {
                                            AttState = io,
                                            branchID = device.ID,
                                            Year = yr,
                                            Month = mth,
                                            Day = day_Renamed,
                                            Hour = hr,
                                            Minute = min,
                                            Second = sec,
                                            IsInValid = 0,
                                            EnrollNo = enrollNo,
                                            VerifyMode = ver,
                                            WorkCode = work
                                        });
                                    }
                                }

                                sdk.Disconnect();
                                dvViewModelAll.AddRange(dvViewModel);
                            }
                        }
                        else
                        {
                            LogWriter log = new LogWriter("Error: Cann't connect to fingerprint device for Branch with ID: " + device.ID + " ");
                        }

                    }
                    catch (Exception ll)
                    {
                        LogWriter log = new LogWriter("Exception: Branch ID: " + device.ID + " - Reason: " + ll.Message);
                    }
                }

                return dvViewModelAll;
            }
        }

        public static List<UserInfoFromDeviceViewModel> CallSDKForUserSync(int deviceId)
        {
            AxBioBridgeSDKLib.AxBioBridgeSDK sdk = new AxBioBridgeSDKLib.AxBioBridgeSDK();

            try
            {
                using (ApplicationDbContext entity = new ApplicationDbContext())
                {
                    int iSize = 0;
                    string enrollNo = "0";
                    string name = "";
                    string pwd = "";
                    int level = 0;
                    bool status = false;

                    var device = entity.BranchesTable.First(a => a.ID == deviceId);
                    sdk.CreateControl();
                    sdk.Refresh();

                    List<UserInfoFromDeviceViewModel> dvViewModel = new List<UserInfoFromDeviceViewModel>();
                    if (sdk.Connect_TCPIP("", device.ID, device.branch_ip, device.branch_port, Convert.ToInt32(device.branch_key)) == 0)
                    {
                        //  list.Add(new objSDK() { ipaddress = device.branch_ip, sdk = sdk });
                        if (sdk.ReadAllUserInfo(ref iSize) == 0)
                        {
                            while (sdk.SSR_GetAllUserInfo(ref enrollNo, ref name, ref pwd, ref level, ref status) == 0)
                            {
                                int en = Convert.ToInt32(enrollNo);
                                if (en == 14)
                                {
                                    string ll = "";
                                }
                                if (!entity.Users.Any(a => a.EnrollNumber == en && a.BranchesModelID != null ? a.BranchesModelID == deviceId : false))
                                {
                                    dvViewModel.Add(new UserInfoFromDeviceViewModel()
                                    {
                                        enrollNo = en,
                                        level = level,
                                        name = name,
                                        pwd = pwd,
                                        status = status,
                                        branchID = deviceId
                                    });
                                }
                                else
                                {
                                    string kk = "";
                                }
                            }

                            sdk.Disconnect();
                            return dvViewModel;
                        }
                    }
                    else
                    {
                        LogWriter log = new LogWriter("Error: Cann't connect to fingerprint device for Branch with ID: " + deviceId + " ");
                    }
                }
            }
            catch (Exception ll)
            {
                LogWriter log = new LogWriter("Exception: Branch ID: " + deviceId + " - Reason: " + ll.Message);
            }

            return new List<UserInfoFromDeviceViewModel>();

        }

        public static void SendToDBFast(List<DevicePointViewModel> devPointsList)
        {
            var userattRes = devPointsList.GroupBy(b => new { b.branchID, b.EnrollNo }).ToList();
            try
            {
                Parallel.ForEach(userattRes, (currentUser) =>
                {
                    foreach (var point in currentUser)
                    {
                        int enroll = Convert.ToInt32(point.EnrollNo);
                        try
                        {
                            using (Models.ApplicationDbContext entityDB = new ApplicationDbContext())
                            {
                                // if user exist in DB
                                if (entityDB.Users.Any(a => a.EnrollNumber == enroll && a.BranchesModelID.HasValue ? a.BranchesModelID.Value == point.branchID : false))
                                {
                                    // get user and convert datetime string into valid date
                                    var user = entityDB.Users.Include(a => a.relatedSchedule).First(a => a.EnrollNumber == enroll && a.BranchesModelID.Value == point.branchID);
                                    IFormatProvider culture = new System.Globalization.CultureInfo("ar-IQ", true);
                                    DateTime dt2 = DateTime.Parse(point.Day + "/" + point.Month + "/" + point.Year + " " + point.Hour + ":" + point.Minute + ":" + point.Second + "." + 0, culture, System.Globalization.DateTimeStyles.AssumeLocal);

                                    // first record for user on today, dno't care what is the AttState 
                                    if (!entityDB.CheckPointLogTable.Any(a => a.userId == user.Id &&
                                    (a.CurrentDay.Year == dt2.Year && a.CurrentDay.Month == dt2.Month && a.CurrentDay.Day == dt2.Day)))
                                    {
                                        CheckPointLogModels model = new CheckPointLogModels()
                                        {
                                            BranchesModelID = user.BranchesModelID,
                                            CheckTimeIn = dt2,
                                            CurrentDay = DateTime.Parse(dt2.ToString("yyyy-MM-dd"), culture),
                                            userId = user.Id,
                                            ApplicationUserId = user.Id,
                                            CheckTypeModelsID = entityDB.CheckTypeTable.Where(a => a.CheckName == "غير متوفر").First().ID
                                        };

                                        entityDB.CheckPointLogTable.Add(model);
                                        entityDB.SaveChanges();
                                        continue;
                                    }

                                    Models.DAL.CheckPointLog.CheckPointLogModels userLogModel = entityDB.CheckPointLogTable.Include(aa => aa.checkType).FirstOrDefault(a =>
                                           a.userId == user.Id &&
                                          (a.CurrentDay.Year == dt2.Year && a.CurrentDay.Month == dt2.Month && a.CurrentDay.Day == dt2.Day));

                                    ScheduleValues sc = new ScheduleValues();
                                    switch (point.AttState)
                                    {
                                        case 0: // user checked in twice or registered absent befor he can do that
                                            {
                                                TimeSpan timeSpan = new TimeSpan();
                                                if (userLogModel.CheckTimeIn.HasValue)
                                                {
                                                    timeSpan = dt2.Subtract(userLogModel.CheckTimeIn.Value);
                                                }
                                                else // if user was late and registered as absent by time, then he came in 
                                                {
                                                    userLogModel.CheckTimeIn = dt2;
                                                    if (userLogModel.CheckTimeOut.HasValue == false)
                                                        userLogModel.CheckTypeModelsID = entityDB.CheckTypeTable.Where(a => a.CheckName == "غير متوفر").First().ID;

                                                    entityDB.Entry(userLogModel).State = EntityState.Modified;
                                                    entityDB.SaveChanges();
                                                    continue;
                                                }

                                                // anything above 5 minutes is considered a checkout 
                                                if (timeSpan.TotalMinutes > 50)
                                                {
                                                    var tt = userLogModel.CheckTimeOut.HasValue ? userLogModel.CheckTimeOut.Value.AddMinutes(userLogModel.CheckTimeOut.Value.Subtract(dt2).TotalMinutes) : userLogModel.CheckTimeOut = dt2;

                                                    var schedules = sc.GetScheduleValues(user.Id, (int)userLogModel.CurrentDay.DayOfWeek, userLogModel.CurrentDay);
                                                    var res = sc.CheckOutCheckInCompare(userLogModel.CheckTimeIn, schedules.Item3,
                                                         userLogModel.CheckTimeOut, schedules.Item4, schedules.Item2, userLogModel.checkType.state);

                                                    userLogModel.CheckTypeModelsID = res.Item1;

                                                    bool responbilityForUser = sc.UserHasDayOfResponability(userLogModel.userId, (int)userLogModel.CurrentDay.DayOfWeek);
                                                    if (responbilityForUser)
                                                        userLogModel.lateMin = res.Item2;
                                                    else
                                                    {
                                                        userLogModel.extraTime = userLogModel.CheckTimeOut.HasValue ? (int)userLogModel.CheckTimeOut.Value.Subtract(userLogModel.CheckTimeIn.Value).TotalMinutes : 0;
                                                        userLogModel.CheckTypeModelsID = userLogModel.checkType.state != LogStatus.حضور ? entityDB.CheckTypeTable.FirstOrDefault(aa => aa.state == LogStatus.حضور).ID : userLogModel.CheckTypeModelsID;
                                                    }

                                                    if (userLogModel.lateMin > 0)
                                                    {
                                                        if (!entityDB.CheckPointSummeryTable.Any(a => a.currentDate == dt2.Date && a.ApplicationUserId == user.Id))
                                                        {
                                                            CheckPointSummeryModel model = new CheckPointSummeryModel()
                                                            {
                                                                CheckTypeModelsID = userLogModel.CheckTypeModelsID.Value,
                                                                ApplicationUserId = user.Id,
                                                                LateBy = responbilityForUser ? userLogModel.lateMin : 0,
                                                                SchedulesModelID = user.SchedulesModelID.Value,
                                                                currentDate = dt2.Date,
                                                                CheckPointLogModelsID = userLogModel.ID,
                                                                CheckPointLogModels = userLogModel,
                                                                extraTime = responbilityForUser ? 0 : userLogModel.extraTime
                                                            };

                                                            entityDB.CheckPointSummeryTable.Add(model);
                                                        }
                                                    }

                                                    entityDB.Entry(userLogModel).State = EntityState.Modified;
                                                    entityDB.SaveChanges();

                                                    continue;
                                                }

                                                LogWriter log = new LogWriter("Checked IN Twice: User No." + user.EnrollNumber + " Checkout Twice");
                                                continue;
                                            }
                                        case 1: // user check out: Normal case
                                            {
                                                if (userLogModel != null)
                                                {
                                                    var tt = userLogModel.CheckTimeOut.HasValue ? userLogModel.CheckTimeOut.Value.AddMinutes(userLogModel.CheckTimeOut.Value.Subtract(dt2).TotalMinutes) : userLogModel.CheckTimeOut = dt2;

                                                    var schedules = sc.GetScheduleValues(user.Id, (int)userLogModel.CurrentDay.DayOfWeek, userLogModel.CurrentDay);

                                                    var res = sc.CheckOutCheckInCompare(userLogModel.CheckTimeIn, schedules.Item3,
                                                         userLogModel.CheckTimeOut, schedules.Item4, schedules.Item2, userLogModel.checkType.state);

                                                    userLogModel.CheckTypeModelsID = res.Item1;
                                                    bool responbilityForUser = sc.UserHasDayOfResponability(userLogModel.userId, (int)userLogModel.CurrentDay.DayOfWeek);
                                                    if (responbilityForUser)
                                                    {
                                                        userLogModel.lateMin = res.Item2;
                                                    }
                                                    else
                                                    {
                                                        userLogModel.extraTime = userLogModel.CheckTimeOut.HasValue ? (int)userLogModel.CheckTimeOut.Value.Subtract(userLogModel.CheckTimeIn.Value).TotalMinutes : 0;
                                                        userLogModel.CheckTypeModelsID = userLogModel.checkType.state != LogStatus.حضور ? entityDB.CheckTypeTable.FirstOrDefault(aa => aa.state == LogStatus.حضور).ID : userLogModel.CheckTypeModelsID;
                                                    }

                                                    if (userLogModel.lateMin > 0)
                                                    {
                                                        if (!entityDB.CheckPointSummeryTable.Any(a => a.currentDate == dt2.Date && a.ApplicationUserId == user.Id))
                                                        {
                                                            CheckPointSummeryModel model = new CheckPointSummeryModel()
                                                            {
                                                                CheckTypeModelsID = userLogModel.CheckTypeModelsID.Value,
                                                                ApplicationUserId = user.Id,
                                                                LateBy = responbilityForUser ? userLogModel.lateMin : 0,
                                                                SchedulesModelID = user.SchedulesModelID.Value,
                                                                currentDate = dt2.Date,
                                                                CheckPointLogModelsID = userLogModel.ID,
                                                                CheckPointLogModels = userLogModel,
                                                                extraTime = responbilityForUser ? 0 : userLogModel.extraTime
                                                            };

                                                            entityDB.CheckPointSummeryTable.Add(model);
                                                        }
                                                    }

                                                    entityDB.Entry(userLogModel).State = EntityState.Modified;
                                                    entityDB.SaveChanges();
                                                    continue;
                                                }
                                                LogWriter log = new LogWriter("Deleted Record: User No." + user.EnrollNumber + "  record was found, then it suddenly disappeareds");
                                                continue;
                                            }
                                    }
                                }
                                else
                                {
                                }
                                new LogWriter("Employee Not Found: User with No." + enroll + " wasn't found in current branch, maybe it wasn't added to the system yet or the branch is not set " + " \n What to DO: Add the user or assign his/her branch and resync the website");
                            }
                        }
                        catch (Exception tt)
                        {
                            LogWriter log = new LogWriter(tt.Message);
                        }
                    }
                });
            }
            catch (Exception kk)
            {
                new LogWriter("General Exception in SendToDBFast : " + kk.Message);
            }
        }

        // this method gets only the full day absence
        public static void GetUserAbsenceFast(DateTime fromDate, DateTime toDate, int? branchID, List<ApplicationUser> users)
        {
            try
            {
                fromDate = fromDate > System.DateTime.Now.Date ? System.DateTime.Now.Date : fromDate;
                toDate = toDate > System.DateTime.Now ? System.DateTime.Now : toDate;
                ScheduleValues sc = new ScheduleValues();

                var userRes = users.Where(a => branchID.HasValue ? a.BranchesModelID == branchID.Value : true).GroupBy(a => new { Id = a.Id });

                Parallel.ForEach(userRes.ToList(), user =>
                {
                    //     foreach (var user in currentUser.ToList())
                    {
                        using (ApplicationDbContext entityDB = new ApplicationDbContext())
                        {
                            var firstPointDate = entityDB.CheckPointLogTable.OrderBy(a => a.CurrentDay).FirstOrDefault();
                            double diff = 0;
                            if (firstPointDate != null)
                            {
                                diff = toDate.Subtract(firstPointDate.CurrentDay).TotalDays;
                            }

                            for (double i = 0; i < diff; i++)
                            {
                                var currentDate = fromDate.AddDays(i); // its correct don't adjust it
                                // checking user if it doesn't exist on the above, log point list  
                                string uuuid = user.First().Id;
                                if (!entityDB.CheckPointLogTable.Any(a => (a.CurrentDay.Day == currentDate.Day && a.CurrentDay.Month == currentDate.Month && currentDate.Year == currentDate.Year)
                                                                        && a.userId == uuuid))
                                {
                                    if (sc.UserHasDayOfResponability(uuuid, (int)currentDate.DayOfWeek))
                                    {
                                        try
                                        {
                                            var res = sc.GetScheduleValues(uuuid, (int)currentDate.DayOfWeek, currentDate.Date);
                                            CheckPointLogModels checkPoint = new CheckPointLogModels()
                                            {
                                                BranchesModelID = user.First().BranchesModelID,
                                                CurrentDay = currentDate.Date,
                                                lateMin = res.Item4.HasValue ? (int)res.Item4.Value.Subtract(res.Item3.Value).TotalMinutes : 0,
                                                userId = user.First().Id,
                                                CheckTypeModelsID = entityDB.CheckTypeTable.Where(a => a.state == Models.DAL.CheckType.LogStatus.غياب).First().ID
                                            };
                                            entityDB.CheckPointLogTable.Add(checkPoint);
                                            entityDB.SaveChanges();

                                            CheckPointSummeryModel model = new CheckPointSummeryModel()
                                            {
                                                CheckTypeModelsID = entityDB.CheckTypeTable.Where(a => a.state == Models.DAL.CheckType.LogStatus.غياب).First().ID,
                                                ApplicationUserId = user.First().Id,
                                                LateBy = res.Item4.HasValue ? (int)res.Item4.Value.Subtract(res.Item3.Value).TotalMinutes : 0,
                                                SchedulesModelID = user.First().SchedulesModelID.Value,
                                                currentDate = currentDate,
                                                CheckPointLogModelsID = checkPoint.ID,
                                                CheckPointLogModels = checkPoint
                                            };
                                            entityDB.CheckPointSummeryTable.Add(model);
                                            entityDB.SaveChanges();
                                        }
                                        catch (Exception kk)
                                        {
                                            new LogWriter("General Exception inside Parallel.Foreach in GetUserAbsenceFast : " + kk.Message);
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            }
            catch (Exception kk)
            {
                new LogWriter("General Exception in GetUserAbsenceFast : " + kk.Message);
            }
        }

        public static void ComputeCakeHR(DateTime fromDate, DateTime toDate, int? branchID, List<ApplicationUser> users)
        {
            try
            {
                fromDate = fromDate > System.DateTime.Now.Date ? System.DateTime.Now.Date : fromDate;
                toDate = toDate > System.DateTime.Now ? System.DateTime.Now : toDate;

                bool cakeHR = true;
                var cakeHRData = CakeHRServiceHelper.GetCakeHRTimeOff(branchID.Value, toDate, fromDate, ref cakeHR);

                Parallel.ForEach(users, (user) =>
               {
                   for (double i = 0; i < (toDate.Subtract(fromDate).Days + 1); i++)
                   {
                       using (ApplicationDbContext entityDB = new ApplicationDbContext())
                       {
                           int totalCakePermitMin = 0;
                           int absenseTypeID = 0;
                           var currentDate = fromDate.AddDays(i);

                           var qq = from permit in cakeHRData
                                    let fromCakeDate = permit.@from.SafeParse()
                                    let toCakeDate = permit.to.SafeParse()
                                    where (permit.user.Id.ToString() == user.userCakeHRNumber
                                               && (currentDate.Date >= fromCakeDate && currentDate.Date <= toCakeDate))
                                    select new { permit.hours, fromCakeDate, toCakeDate, permit.timeoff_id, permit.approval };
                           var q = qq.ToList();

                           if (!cakeHR)
                           {
                               qq = from permit in cakeHRData
                                    let fromCakeDate = permit.@from.SafeParse()
                                    let toCakeDate = permit.to.SafeParse()
                                    where (permit.user.Id.ToString() == user.Id
                                               && (currentDate.Date >= fromCakeDate && currentDate.Date <= toCakeDate))
                                    select new { permit.hours, fromCakeDate, toCakeDate, permit.timeoff_id, permit.approval };
                               q.Clear();
                               q = qq.ToList();
                           }


                           if (q.Count() > 0 && entityDB.CheckPointLogTable.Any(a => (a.CurrentDay.Day == currentDate.Day
                                                                                   && a.CurrentDay.Month == currentDate.Month
                                                                                   && currentDate.Year == currentDate.Year)
                                                                                   && a.userId == user.Id))
                           {
                               string hh = q.FirstOrDefault().timeoff_id.ToString();
                               if (Convert.ToDouble(q.First().hours) > 8)
                               {
                                   totalCakePermitMin = 8 * 60;
                               }
                               else
                               {
                                   totalCakePermitMin = (Convert.ToInt32(Math.Round(Convert.ToDouble(q.First().hours) * 60)));
                               }

                               if (!cakeHR)
                               {
                                   string name = q.FirstOrDefault().approval;
                                   absenseTypeID = entityDB.CheckTypeTable.FirstOrDefault(a => a.CheckName == name).ID;

                               }
                               else
                               {
                                   absenseTypeID = entityDB.CheckTypeTable.FirstOrDefault(a => a.CheckNameFromCakeHR == hh).ID;
                               }

                               var logPoint = entityDB.CheckPointLogTable.First(a => (a.CurrentDay.Day == currentDate.Day
                                                                                        && a.CurrentDay.Month == currentDate.Month
                                                                                        && currentDate.Year == currentDate.Year)
                                                                                        && a.userId == user.Id);

                               logPoint.lateMin = logPoint.lateMin.HasValue ? ((logPoint.lateMin - totalCakePermitMin) <= 0
                                                        ? 0 : logPoint.lateMin - totalCakePermitMin) : 0;

                               logPoint.vacationMin = totalCakePermitMin;
                               logPoint.CheckTypeModelsID = absenseTypeID == 0 ? entityDB.CheckTypeTable.Where(a => a.state == Models.DAL.CheckType.LogStatus.غياب).First().ID : absenseTypeID;
                               entityDB.Entry(logPoint).State = EntityState.Modified;

                               if (logPoint.CheckPointSummeryModel != null)
                               {
                                   logPoint.CheckPointSummeryModel.LateBy = logPoint.lateMin.HasValue ?
                                            ((logPoint.lateMin - totalCakePermitMin) <= 0 ? 0 : logPoint.lateMin - totalCakePermitMin) : 0;


                                   logPoint.CheckPointSummeryModel.CheckTypeModelsID = absenseTypeID == 0 ?
                                            entityDB.CheckTypeTable.Where(a => a.state == Models.DAL.CheckType.LogStatus.غياب).First().ID : absenseTypeID;

                                   logPoint.CheckPointSummeryModel.VacationMin = totalCakePermitMin;

                                   entityDB.Entry(logPoint.CheckPointSummeryModel).State = EntityState.Modified;
                               }

                               entityDB.SaveChanges();
                           }
                       }
                   }
               });
            }
            catch (Exception kk)
            {
                new LogWriter("General Exception in GetUserAbsenceFast : " + kk.Message);
            }
        }

        public static void ReCalculatePointsTypeNoCake(List<CheckPointLogModels> chkPointlogList)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                foreach (var chkPointlog in chkPointlogList)
                {
                    try
                    {
                        ScheduleValues sc = new ScheduleValues();
                        var schedules = sc.GetScheduleValues(chkPointlog.userId, (int)chkPointlog.CurrentDay.DayOfWeek, chkPointlog.CurrentDay);
                        var res = sc.CheckOutCheckInCompare(chkPointlog.CheckTimeIn, schedules.Item3,
                             chkPointlog.CheckTimeOut, schedules.Item4, schedules.Item2, chkPointlog.checkType.state);

                        chkPointlog.lateMin = res.Item2;

                        if (chkPointlog.CheckPointSummeryModel == null)
                        {
                            CheckPointSummeryModel model = new CheckPointSummeryModel()
                            {
                                CheckTypeModelsID = res.Item1,
                                LateBy = res.Item2,
                                ApplicationUserId = chkPointlog.userId,
                                SchedulesModelID = chkPointlog.user.SchedulesModelID.Value,
                                currentDate = chkPointlog.CheckTimeIn.Value.Date,
                                CheckPointLogModelsID = chkPointlog.ID,
                                CheckPointLogModels = chkPointlog
                            };
                            db.CheckPointSummeryTable.Add(model);

                        }
                        else
                        {
                            chkPointlog.CheckPointSummeryModel.LateBy = res.Item2;
                            chkPointlog.CheckPointSummeryModel.SchedulesModelID = chkPointlog.user.SchedulesModelID.Value;

                            db.Entry(chkPointlog).State = EntityState.Modified;
                            db.Entry(chkPointlog.CheckPointSummeryModel).State = EntityState.Modified;
                        }

                        db.SaveChanges();
                    }
                    catch (Exception tt)
                    {
                        string yy = "";
                    }
                }
            }

        }

        //public static void ReCalculatePointsTypeCake(List<CheckPointLogModels> chkPointlogList)
        //{
        //    Parallel.ForEach(chkPointlogList, chkPointlog =>
        //    {
        //        try
        //        {
        //            using (ApplicationDbContext db = new ApplicationDbContext())
        //            {
        //                ScheduleValues sc = new ScheduleValues();
        //                var schedules = sc.GetScheduleValues(chkPointlog.userId, (int)chkPointlog.CurrentDay.DayOfWeek, chkPointlog.CurrentDay);
        //                var res = sc.CheckOutCheckInCompare(chkPointlog.CheckTimeIn, schedules.Item3,
        //                     chkPointlog.CheckTimeOut, schedules.Item4, schedules.Item2, chkPointlog.checkType.state);

        //                CheckPointLogModels chkPoint = db.CheckPointLogTable.Include(aa => aa.CheckPointSummeryModel).First(aa => aa.ID == chkPointlog.ID);
        //                chkPoint.lateMin = res.Item2;
        //                chkPoint.CheckTypeModelsID = res.Item1;

        //                if (chkPoint.CheckPointSummeryModel == null
        //                                                //!db.CheckPointSummeryTable.Any(a => a.currentDate.Year == chkPointlog.CheckTimeIn.Value.Year
        //                                                //                                && a.currentDate.Month == chkPointlog.CheckTimeIn.Value.Month
        //                                                //                                && a.currentDate.Day == chkPointlog.CheckTimeIn.Value.Day
        //                                                //                                && a.ApplicationUserId == chkPointlog.userId)
        //                                                )
        //                {
        //                    CheckPointSummeryModel model = new CheckPointSummeryModel()
        //                    {
        //                        CheckTypeModelsID = res.Item1,
        //                        ApplicationUserId = chkPointlog.userId,
        //                        LateBy = res.Item2,
        //                        SchedulesModelID = chkPointlog.user.SchedulesModelID.Value,
        //                        currentDate = chkPointlog.CheckTimeIn.Value.Date,
        //                        CheckPointLogModelsID = chkPointlog.ID,
        //                        CheckPointLogModels = chkPointlog
        //                    };
        //                    db.CheckPointSummeryTable.Add(model);

        //                }
        //                else
        //                {
        //                    var summery = chkPoint.CheckPointSummeryModel;
        //                    //db.CheckPointSummeryTable.First(a => a.currentDate.Year == chkPointlog.CheckTimeIn.Value.Year
        //                    //                            && a.currentDate.Month == chkPointlog.CheckTimeIn.Value.Month
        //                    //                            && a.currentDate.Day == chkPointlog.CheckTimeIn.Value.Day
        //                    //                            && a.ApplicationUserId == chkPointlog.userId);

        //                    summery.LateBy = res.Item2;
        //                    summery.CheckTypeModelsID = chkPointlog.CheckTypeModelsID.Value;

        //                    summery.CheckTypeModelsID = res.Item1;
        //                    summery.SchedulesModelID = chkPointlog.user.SchedulesModelID.Value;
        //                    db.Entry(summery).State = EntityState.Modified;
        //                }

        //                db.Entry(chkPoint).State = EntityState.Modified;
        //                db.SaveChanges();
        //            }

        //        }
        //        catch (Exception tt)
        //        {
        //            string yy = "";
        //        }
        //    });
        //}

        public static bool RegisterFromDevice(List<UserInfoFromDeviceViewModel> model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    foreach (var x in model)
                    {
                        ApplicationUser user = new ApplicationUser()
                        {
                            BranchesModelID = x.branchID,
                            Email = GenerateEmailAddress(),
                            EmployeePassword = "Hello90#",
                            EmployeeBirthDate = System.DateTime.Now.ToString("yyyy-MM-dd"),
                            SchedulesModelID = db.SchedulesTable.First().ID,
                            DepartmentsModelID = db.DepartmentsTable.First().ID,
                            EmployeeName = x.name,
                            EmployeeName_En = x.name,
                            EmployeeGender = Gender.ذكر,
                            EnrollNumber = x.enrollNo,
                            EmployeeShow = true,

                            UserName = x.enrollNo.ToString() + x.branchID.ToString(),
                        };

                        var userStore = new UserStore<ApplicationUser>(db);
                        var manager = new UserManager<ApplicationUser>(userStore);
                        IdentityResult result = manager.Create(user, "Hello90#");

                        if (result.Succeeded)
                        {
                            switch (x.level)
                            {
                                case 0:
                                    {
                                        manager.AddToRole(user.Id, "موظف");
                                        continue;
                                    }
                                case 1:
                                    {
                                        manager.AddToRole(user.Id, "الموارد البشرية");
                                        continue;
                                    }
                                default:
                                    {
                                        manager.AddToRole(user.Id, "مدير النظام");
                                        continue;
                                    }
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception kk)
            {
                string ui = "";
            }

            return false;
        }

        public static bool DeleteUserFromDevice(ApplicationUser user)
        {
            AxBioBridgeSDKLib.AxBioBridgeSDK sdk = new AxBioBridgeSDKLib.AxBioBridgeSDK();

            try
            {
                using (ApplicationDbContext entity = new ApplicationDbContext())
                {
                    var device = entity.BranchesTable.First(a => a.ID == user.BranchesModelID);
                    sdk.CreateControl();
                    sdk.Refresh();

                    List<UserInfoFromDeviceViewModel> dvViewModel = new List<UserInfoFromDeviceViewModel>();
                    if (sdk.Connect_TCPIP("", device.ID, device.branch_ip, device.branch_port, Convert.ToInt32(device.branch_key)) == 0)
                    {
                        //  list.Add(new objSDK() { ipaddress = device.branch_ip, sdk = sdk });

                        if (sdk.DeleteUserData(user.EnrollNumber.Value) == 0)
                        {
                            sdk.Disconnect();
                            return true;

                        }

                        sdk.Disconnect();
                        return false;

                    }
                    else
                    {
                        LogWriter log = new LogWriter("Error: Cann't connect to fingerprint device for Branch with ID:  ");
                    }
                }
            }
            catch (Exception ll)
            {
                LogWriter log = new LogWriter("Exception: Branch ID: " + "couldnot delete user: " + user.UserName + " - Reason: " + ll.Message);
            }

            return false;
        }

        // Generate Random Email Address 
        public static string GenerateEmailAddress()
        {
            return "User" + GetRandomNumber() + "@al-tameer.com";
        }

        // Generate random number for email address
        public static int GetRandomNumber()
        {
            return new Random().Next(100000, 100000000);
        }
    }

    public static class GetData
    {
        public static SyncPointViewModel GetInfoFromDB(int deviceId, DateTime t1, DateTime t2)
        {
            using (ApplicationDbContext entityDB = new ApplicationDbContext())
            {
                SyncPointViewModel sss = new SyncPointViewModel()
                {
                    CheckPointLogTable = entityDB.CheckPointLogTable.Where(a => a.BranchesModelID == deviceId && a.CurrentDay >= t1 && a.CurrentDay <= t2).ToList(),
                    CheckPointSummeryTable = entityDB.CheckPointSummeryTable.Where(a => a.CheckPointLogModels.BranchesModelID.Value == deviceId && a.currentDate >= t1 && a.currentDate <= t2).ToList(),
                    Users = entityDB.Users.Where(a => a.BranchesModelID == deviceId).ToList()
                };

                return sss;
            }
        }
    }

}