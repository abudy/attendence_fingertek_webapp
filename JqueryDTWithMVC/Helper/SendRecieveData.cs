﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace JqueryDTWithMVC.Helper
{
    public static class SendRecieveData<T>
    {
        public static T FetchDataAsyncNoToken(string url, string token = "")
        {
            try
            {
                var baseAddress = new Uri(url);
                using (var httpClient = new HttpClient { BaseAddress = baseAddress })
                {
                
                    using (var response = httpClient.GetAsync(baseAddress).Result)
                    {
                        string responseData = response.Content.ReadAsStringAsync().Result;
                        T ss = JsonConvert.DeserializeObject<T>(responseData);
                        return ss;
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error contacting the server", new HttpRequestException());
            }

        }
    }
}