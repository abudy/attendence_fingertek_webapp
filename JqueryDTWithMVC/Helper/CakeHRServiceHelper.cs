﻿using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.ViewModels.CakeHREmployeesInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace JqueryDTWithMVC.Helper
{
    public static class CakeHRServiceHelper
    {
        public static List<CakeHRTimeOffViewModel> GetCakeHRTimeOff(int branchID, DateTime TimeDateTo, DateTime TimeDateFrom, ref bool cakeHR)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    string apiKey = db.BranchesTable.Find(branchID).apiKey;
                    if (!String.IsNullOrEmpty(apiKey))
                    {
                        cakeHR = true;
                        var ttt = Helper.SendRecieveData<List<CakeHRTimeOffViewModel>>.FetchDataAsyncNoToken("https://cake.hr/api/" + apiKey + "/calendar/?approval=approved&from=" +
                            TimeDateFrom.ToString("yyyy-MM-dd") + "&to=" + TimeDateTo.ToString("yyyy-MM-dd"), "");
                        return ttt;
                    }
                    else
                    {
                        cakeHR = false;
                        var ttt = db.VacationsTable.Where(aa => (aa.BranchesModelID.HasValue ? aa.BranchesModelID.Value == branchID : false) && 
                                        aa.approvalstate == Models.DAL.Vacation.ReqState.approved && (aa.fromDate >= TimeDateFrom && (aa.toDate.HasValue ? aa.toDate <= TimeDateTo : true)))
                                        .ToList().Select(aa=> new CakeHRTimeOffViewModel() {
                                            date_requested = aa.date_requested.ToString("yyyy-MM-dd HH:mm:ss"),
                                            day_part = aa.day_part.ToString(),
                                            hours = aa.hours.ToString(),
                                            userId = aa.ApplicationUserId,
                                            user = aa.user,
                                            from = aa.fromDate.ToString("yyyy-MM-dd"),
                                            to = aa.toDate.Value.ToString("yyyy-MM-dd"),
                                            message = aa.message,
                                            timeoff_id = db.CheckTypeTable.FirstOrDefault(b=>b.CheckName == aa.vacType.vacationName) != null ?
                                            db.CheckTypeTable.FirstOrDefault(b => b.CheckName == aa.vacType.vacationName).ID  : 4998,
                                            approval = aa.vacType.vacationName,
                                            id = aa.ID
                                        }).ToList();
                        return ttt;
                    }
                    //else if(db.VacationsTable.Any(a=>a.BranchesModelID.HasValue ? a.BranchesModelID == branchID : false))
                    //{
                    //    cakeHR = false;
                    //    return db.VacationsTable.Include("user").Include("vacType").Where(a => a.BranchesModelID == branchID && a.approvalstate == Models.DAL.Vacation.ReqState.approved 
                    //    && a.date_requested >= TimeDateFrom && a.date_requested <= TimeDateTo).ToList().Select(a => new CakeHRTimeOffViewModel() {
                    //     approval = "approved",
                    //     date_requested = a.date_requested.ToString("dd-MM-yyyy"),
                    //     day_part  =a.day_part.ToString(),
                    //     from = a.fromDate.ToString("dd-MM-yyyy"),
                    //     to = a.toDate.ToString("dd-MM-yyyy"),
                    //     hours = a.hours.ToString(),
                    //     message  =a.message,
                    //     timeoff_id  = a.vacType.timeOff_Id.HasValue ? a.vacType.timeOff_Id.Value : 0,
                    //     user =  new User
                    //     {
                    //         first = a.user.EmployeeName,
                    //         id  = 0, // pay attention to this
                    //         last = a.user.EmployeeName,
                    //         userId = a.ApplicationUserId
                    //     }
                    //    }).ToList();
                    //}
                }
            }
            catch (Exception kk)
            {
                int A = 0;
            }
            return new List<CakeHRTimeOffViewModel>();
        }

    }
}