﻿using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Helper
{
    public static class NotificationService
    {
        public static void SendNotifications(string targetUserId, string message, string url, string makerUserId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                //var AssignedMakerName = db.Users.Find(makerUserId).AssignedName;
                var targetUser = db.Users.Find(targetUserId);
                NotificationsModel nm = new NotificationsModel
                {
                    eventDate = System.DateTime.Now,
                    shown = false,
                    eventDescription = message,
                    madeById = "",
                    madeByName = "",
                    ApplicationUserID = targetUser.Id,
                    TargetUser = targetUser,
                    eventURL = url,
                };

                db.NotificationsTable.Add(nm);
                db.SaveChanges();
            }
        }
    }
}