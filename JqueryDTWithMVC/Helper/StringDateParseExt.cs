﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Helper
{
    public static class StringDateParseExt
    {
        public static DateTime SafeParse(this string any)
        {
            DateTime parsedDate;
            DateTime.TryParseExact(any,
                  "yyyy-MM-dd",
                  System.Globalization.CultureInfo.InvariantCulture,
                  System.Globalization.DateTimeStyles.None,
                  out parsedDate);
            return parsedDate;
        }
    }
}