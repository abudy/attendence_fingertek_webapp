﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Helpers
{
    public static class T4Helpers
    {
        public static string GetPluralName(string viewDataTypeName,string propertyName)
        {
            string title = "العناصر";
            Type typeModel = Type.GetType(viewDataTypeName);

            if (typeModel != null)
            {
                ModelsName modelName = (ModelsName)Attribute.GetCustomAttribute(typeModel, typeof(ModelsName));
                return modelName.PluralName;
            }

            return title;
        }

        public static string GetSingularName(string viewDataTypeName, string propertyName)
        {
            string title = "العنصر";
            Type typeModel = Type.GetType(viewDataTypeName);

            if (typeModel != null)
            {
                ModelsName modelName = (ModelsName)Attribute.GetCustomAttribute(typeModel, typeof(ModelsName));
                return modelName.SingularName;
            }

            return title;
        }
    }
}