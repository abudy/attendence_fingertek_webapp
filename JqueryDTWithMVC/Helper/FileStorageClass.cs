﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
namespace JqueryDTWithMVC.Helper
{
    public static class FileStorageClass
    {
        /* take posted image, is importer and serverMapPath (becuase server.map dosn't work here)
        / and return result path */
        public static string StoreImage(bool importer, HttpPostedFileBase PostImagePath, string userID, string serverMapPath)
        {
            if (PostImagePath != null)
            {
                if (PostImagePath.ContentLength > 0)
                {
                    Image.FromStream(PostImagePath.InputStream, true, true);
                    try
                    {
                        System.Drawing.Image image_file;
                        if (PostImagePath.ContentLength > 0)
                        {
                            image_file = System.Drawing.Image.FromStream(PostImagePath.InputStream);
                            Bitmap bitmap_file = new Bitmap(image_file, image_file.Width, image_file.Height);

                            bitmap_file.Save(serverMapPath + userID + "_.jpg", ImageFormat.Jpeg);
                            return serverMapPath + userID + "_.jpg";
                        }
                    }
                    catch (Exception gg)
                    {
                        return "Error";
                    }
                }
            }

            return "Error";
        }

        public static string StoreFile(HttpPostedFileBase PostFilePath, string serverMapPath)
        {
            if (PostFilePath != null)
            {
                if (PostFilePath.ContentLength > 0)
                {
                    try
                    {
                        var fileName = PostFilePath.FileName;
                        PostFilePath.SaveAs(serverMapPath + fileName);
                    }
                    catch (Exception gg)
                    {
                        return "Error";
                    }
                }
            }

            return "Error";
        }

    }
}