﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JqueryDTWithMVC.Helpers
{
    [AttributeUsage(AttributeTargets.All)]
    public class ModelsName: System.Attribute
    {
        // Private fields.
        private string singularName;
        private string pluralName;
        // This constructor defines two required parameters: name and level.

        public ModelsName(string singularName, string pluralName)
        {
            this.singularName = singularName;
            this.pluralName = pluralName;
        }

        // Define Name property.
        // This is a read-only attribute.

        public virtual string SingularName
        {
            get { return singularName; }
        }

        // Define Level property.
        // This is a read-only attribute.

        public virtual string PluralName
        {
            get { return pluralName; }
        }
    }
}