﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.Nationality;

namespace JqueryDTWithMVC.Controllers
{
    public class NationalityModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: NationalityModels
        public ActionResult Index()
        {
            return View(db.NationalityTable.ToList());
        }

        // GET: NationalityModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NationalityModel nationalityModel = db.NationalityTable.Find(id);
            if (nationalityModel == null)
            {
                return HttpNotFound();
            }
            return View(nationalityModel);
        }

        // GET: NationalityModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: NationalityModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,nation")] NationalityModel nationalityModel)
        {
            if (ModelState.IsValid)
            {
                db.NationalityTable.Add(nationalityModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(nationalityModel);
        }

        // GET: NationalityModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NationalityModel nationalityModel = db.NationalityTable.Find(id);
            if (nationalityModel == null)
            {
                return HttpNotFound();
            }
            return View(nationalityModel);
        }

        // POST: NationalityModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,nation")] NationalityModel nationalityModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(nationalityModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(nationalityModel);
        }

        // GET: NationalityModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NationalityModel nationalityModel = db.NationalityTable.Find(id);
            if (nationalityModel == null)
            {
                return HttpNotFound();
            }
            return View(nationalityModel);
        }

        // POST: NationalityModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NationalityModel nationalityModel = db.NationalityTable.Find(id);
            db.NationalityTable.Remove(nationalityModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
