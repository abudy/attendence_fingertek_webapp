﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.VacationType;
using JqueryDTWithMVC.Models.Repository.UnitOfWork;

namespace JqueryDTWithMVC.Controllers
{
    [Startup.Title(Title ="TimeOff Types")]
    public class VacationTypeModelsController : Controller
    {
        // GET: VacationTypeModels
        [Startup.Title(Title = "TimeOff Types List")]
        [Startup.ModifiedAuthorize]
        public ActionResult Index()
        {
            using(var unitOfWork = new UnitOfWork(new ApplicationDbContext()))
            {
                return View(unitOfWork.TimeOffType.GetAll());
            }
        }

        // GET: VacationTypeModels/Details/5
        [Startup.Title(Title = "TimeOff Type Details")]
        [Startup.ModifiedAuthorize]
        public ActionResult Details(int id)
        {
            using (var unitOfWork = new UnitOfWork(new ApplicationDbContext()))
            {
                if (!unitOfWork.TimeOffType.Any(a=>a.ID == id))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var vacationTypeModel = unitOfWork.TimeOffType.Get(id);
                if (vacationTypeModel == null)
                {
                    return HttpNotFound();
                }

                return View(vacationTypeModel);
            }
        }

        // GET: VacationTypeModels/Create
        [Startup.Title(Title = "Create TimeOff Type")]
        [Startup.ModifiedAuthorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: VacationTypeModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Startup.ModifiedAuthorize]
        public ActionResult Create([Bind(Include = "ID,vacationName,policyResetEvery,balance,timeOff_Id")] VacationTypeModel vacationTypeModel)
        {
            using (var unitOfWork = new UnitOfWork(new ApplicationDbContext()))
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.TimeOffType.Add(vacationTypeModel);
                    unitOfWork.Complete();

                    return RedirectToAction("Index");
                }

                return View(vacationTypeModel);
            }
        }

        // GET: VacationTypeModels/Edit/5
        [Startup.Title(Title = "Edit TimeOff Type")]
        [Startup.ModifiedAuthorize]
        public ActionResult Edit(int? id)
        {
            using (var unitOfWork = new UnitOfWork(new ApplicationDbContext()))
            {
                if (!unitOfWork.TimeOffType.Any(a => a.ID == id))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var vacationTypeModel = unitOfWork.TimeOffType.Get(id.Value);
                if (vacationTypeModel == null)
                {
                    return HttpNotFound();
                }

                return View(vacationTypeModel);
            }
        }

        // POST: VacationTypeModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Startup.ModifiedAuthorize]
        public ActionResult Edit([Bind(Include = "ID,vacationName,policyResetEvery,balance,timeOff_Id")] VacationTypeModel vacationTypeModel)
        {
            using (var unitOfWork = new UnitOfWork(new ApplicationDbContext()))
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.TimeOffType.EditTimeOff(vacationTypeModel);
                    unitOfWork.Complete();

                    return RedirectToAction("Index");
                }
                return View(vacationTypeModel);
            }

        }

        // GET: VacationTypeModels/Delete/5
        [Startup.Title(Title = "Delete TimeOff Type")]
        [Startup.ModifiedAuthorize]
        public ActionResult Delete(int? id)
        {
            using (var unitOfWork = new UnitOfWork(new ApplicationDbContext()))
            {
                if (!unitOfWork.TimeOffType.Any(a => a.ID == id))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var vacationTypeModel = unitOfWork.TimeOffType.Get(id.Value);
                if (vacationTypeModel == null)
                {
                    return HttpNotFound();
                }

                return View(vacationTypeModel);
            }
        }

        // POST: VacationTypeModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Startup.ModifiedAuthorize]
        public ActionResult DeleteConfirmed(int id)
        {
            using (var unitOfWork = new UnitOfWork(new ApplicationDbContext()))
            {
                if (!unitOfWork.TimeOffType.Any(a => a.ID == id))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                unitOfWork.TimeOffType.Remove(unitOfWork.TimeOffType.SingleOrDefault(a => a.ID == id));
                unitOfWork.Complete();
            }

            return RedirectToAction("Index");
        }
    }
}
