﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.DocumentsCategory;

namespace JqueryDTWithMVC.Controllers
{
    public class DocumentsCategoryModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: DocumentsCategoryModels
        public ActionResult Index()
        {
            return View(db.DocumentsCategoryTable.ToList());
        }

        // GET: DocumentsCategoryModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DocumentsCategoryModels documentsCategoryModels = db.DocumentsCategoryTable.Find(id);
            if (documentsCategoryModels == null)
            {
                return HttpNotFound();
            }
            return View(documentsCategoryModels);
        }

        // GET: DocumentsCategoryModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DocumentsCategoryModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,catgeoryName")] DocumentsCategoryModels documentsCategoryModels)
        {
            if (ModelState.IsValid)
            {
                db.DocumentsCategoryTable.Add(documentsCategoryModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(documentsCategoryModels);
        }

        // GET: DocumentsCategoryModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DocumentsCategoryModels documentsCategoryModels = db.DocumentsCategoryTable.Find(id);
            if (documentsCategoryModels == null)
            {
                return HttpNotFound();
            }
            return View(documentsCategoryModels);
        }

        // POST: DocumentsCategoryModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,catgeoryName")] DocumentsCategoryModels documentsCategoryModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(documentsCategoryModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(documentsCategoryModels);
        }

        // GET: DocumentsCategoryModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DocumentsCategoryModels documentsCategoryModels = db.DocumentsCategoryTable.Find(id);
            if (documentsCategoryModels == null)
            {
                return HttpNotFound();
            }
            return View(documentsCategoryModels);
        }

        // POST: DocumentsCategoryModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DocumentsCategoryModels documentsCategoryModels = db.DocumentsCategoryTable.Find(id);
            db.DocumentsCategoryTable.Remove(documentsCategoryModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
