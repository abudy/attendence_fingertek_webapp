﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.MaritalStatus;

namespace JqueryDTWithMVC.Controllers
{
    public class MaritalStatusModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: MaritalStatusModels
        public ActionResult Index()
        {
            return View(db.MaritalStatusTable.ToList());
        }

        // GET: MaritalStatusModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaritalStatusModel maritalStatusModel = db.MaritalStatusTable.Find(id);
            if (maritalStatusModel == null)
            {
                return HttpNotFound();
            }
            return View(maritalStatusModel);
        }

        // GET: MaritalStatusModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MaritalStatusModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,status")] MaritalStatusModel maritalStatusModel)
        {
            if (ModelState.IsValid)
            {
                db.MaritalStatusTable.Add(maritalStatusModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(maritalStatusModel);
        }

        // GET: MaritalStatusModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaritalStatusModel maritalStatusModel = db.MaritalStatusTable.Find(id);
            if (maritalStatusModel == null)
            {
                return HttpNotFound();
            }
            return View(maritalStatusModel);
        }

        // POST: MaritalStatusModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,status")] MaritalStatusModel maritalStatusModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(maritalStatusModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(maritalStatusModel);
        }

        // GET: MaritalStatusModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MaritalStatusModel maritalStatusModel = db.MaritalStatusTable.Find(id);
            if (maritalStatusModel == null)
            {
                return HttpNotFound();
            }
            return View(maritalStatusModel);
        }

        // POST: MaritalStatusModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MaritalStatusModel maritalStatusModel = db.MaritalStatusTable.Find(id);
            db.MaritalStatusTable.Remove(maritalStatusModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
