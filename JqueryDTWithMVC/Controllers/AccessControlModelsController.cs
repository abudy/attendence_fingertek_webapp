﻿using JqueryDTWithMVC.Helper;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.AccessControl;
using JqueryDTWithMVC.Models.ViewModels.TreeStruct;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using static JqueryDTWithMVC.Startup;

namespace JqueryDTWithMVC.Controllers
{
    [Title(Title = "ربط الصلاحيات بمكونات النظام")]
    public class AccessControlModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: AccessControlModels
        [Title(Title = "جدول ربط الصلاحيات بمكونات النظام")]
        [ModifiedAuthorize]
        public ActionResult Index()
        {
            return View(db.AccessControlTable.ToList());
        }

        // GET: AccessControlModels/Details/5
        [Title(Title = "تفاصيل ربط الصلاحيات بمكونات النظام")]
        [ModifiedAuthorize]
        public ActionResult Details(int? id)
        {
            List<string> list = new List<string>();
            Assembly assembly = Assembly.GetAssembly(typeof(JqueryDTWithMVC.MvcApplication));
            IEnumerable<Type> types = assembly.GetTypes().Where(type => typeof(Controller).IsAssignableFrom(type)).OrderBy(x => x.Name);
            foreach (Type cls in types.Where(a => a.Name == "HomeController"))
            {
                if (cls.IsDefined(typeof(TitleAttribute)))
                {
                    var tttt = cls.GetCustomAttribute<TitleAttribute>();
                    Debug.WriteLine(tttt.Title);
                    int a = 0;
                }
                IEnumerable<MemberInfo> memberInfo = cls.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public).Where(m => !m.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true).Any()).OrderBy(x => x.Name);
                foreach (MemberInfo method in memberInfo)
                {
                    if (method.ReflectedType.IsPublic && !method.IsDefined(typeof(NonActionAttribute)) && method.IsDefined(typeof(TitleAttribute)))
                    {
                        var tttt = method.GetCustomAttribute<TitleAttribute>();
                        Debug.WriteLine(tttt.Title);
                        int a = 0;
                    }
                }
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccessControlModel accessControlModel = db.AccessControlTable.Find(id);
            if (accessControlModel == null)
            {
                return HttpNotFound();
            }
            return View(accessControlModel);
        }

        // GET: AccessControlModels/Create
        [Title(Title = "انشاء ربط صلاحية بمكونات النظام")]
        [ModifiedAuthorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: AccessControlModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        public ActionResult Create([Bind(Include = "ID,functionalityName,controllerName,actionMethodName,allowRoles,preventedRoles,allowedUsers")] AccessControlModel accessControlModel)
        {
            if (ModelState.IsValid)
            {
                db.AccessControlTable.Add(accessControlModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(accessControlModel);
        }

        // GET: AccessControlModels/Edit/5
        [Title(Title = "تعديل ربط صلاحية بمكونات النظام")]
        [ModifiedAuthorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccessControlModel accessControlModel = db.AccessControlTable.Find(id);
            if (accessControlModel == null)
            {
                return HttpNotFound();
            }
            return View(accessControlModel);
        }

        // POST: AccessControlModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        public ActionResult Edit([Bind(Include = "ID,functionalityName,controllerName,actionMethodName,allowRoles,preventedRoles,allowedUsers")] AccessControlModel accessControlModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(accessControlModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(accessControlModel);
        }

        // GET: AccessControlModels/Delete/5
        [Title(Title = "مسح ربط صلاحية بمكونات النظام")]
        [ModifiedAuthorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccessControlModel accessControlModel = db.AccessControlTable.Find(id);
            if (accessControlModel == null)
            {
                return HttpNotFound();
            }
            return View(accessControlModel);
        }

        // POST: AccessControlModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Title(Title = "تاكيد مسح ربط صلاحية بمكونات النظام")]
        [ModifiedAuthorize]
        public ActionResult DeleteConfirmed(int id)
        {
            AccessControlModel accessControlModel = db.AccessControlTable.Find(id);
            db.AccessControlTable.Remove(accessControlModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [Title(Title = "التحكم الكامل بربط صلاحية بمكونات النظام")]
        public ActionResult GetTree(string sentRole, string sentUser)
        {
            string user = "";
            string role = String.IsNullOrEmpty(sentRole) ? db.Roles.First().Name : db.Roles.First(a => a.Name == sentRole).Name;

            if (db.Users.Any(a => a.UserName == sentUser))
                user = String.IsNullOrEmpty(sentUser) ? db.Users.First().UserName : db.Users.First(a => a.UserName == sentUser).UserName;
            else
                user = db.Users.First().UserName;
            // get controllers naes
            List<string> controllerNames = new List<string>();
            HelperClasses.GetSubClasses<Controller>().ForEach(type => controllerNames.Add(type.Name));

            var tree = new TreeNode("MainClass", "هيكلية الموقع");
            foreach (var _controller in controllerNames)
            {

                // get all sub assemblies with specified type, then filter only ones inherited from controller 
                Assembly assembly = Assembly.GetAssembly(typeof(JqueryDTWithMVC.MvcApplication));
                IEnumerable<Type> types = assembly.GetTypes().Where(type => typeof(Controller).IsAssignableFrom(type)).OrderBy(x => x.Name);

                // first controller matching name and Title attribute get his name
                var attribute = types.FirstOrDefault(a => a.Name == _controller).GetCustomAttributes(typeof(Startup.TitleAttribute), true)
                     .Cast<Startup.TitleAttribute>()
                     .FirstOrDefault();

                if (attribute != null)
                {
                    var cont = new TreeNode(attribute.Title, _controller);
                    // loop through all type with maching contoller name
                    foreach (Type cls in types.Where(a => a.Name == _controller))
                    {

                        IEnumerable<MemberInfo> memberInfo = cls.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public).
                            Where(m => !m.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true).Any()).OrderBy(x => x.Name);

                        // get all controller AM
                        foreach (MemberInfo method in memberInfo)
                        {
                            if (method.ReflectedType.IsPublic && method.IsDefined(typeof(Startup.TitleAttribute)))
                            {
                                bool chk = db.AccessControlTable.Any(a => a.allowRoles.Contains(role) && a.actionMethodName == method.Name && a.controllerName == _controller);
                                bool chkAllowedUsers = db.AccessControlTable.Any(a => a.allowedUsers.Contains(user) && a.actionMethodName == method.Name && a.controllerName == _controller);

                                var acMethodAtt = method.GetCustomAttributes(typeof(Startup.TitleAttribute), true)
                                                    .Cast<Startup.TitleAttribute>()
                                                    .FirstOrDefault();

                                if (acMethodAtt != null)
                                {
                                    var action = new TreeNode(acMethodAtt.Title, _controller + "_____" + method.Name, chk, chkAllowedUsers);
                                    //     var actionUser = new TreeNode(acMethodAtt.Title, _controller + "_____" + method.Name, chk, chkAllowedUsers);
                                    if (!cont.ContainsChild(attribute.Title))
                                    {
                                        cont.Add(action);
                                    }
                                }
                            }
                        }
                    }

                    if (!tree.ContainsChild(attribute.Title))
                    {
                        tree.Add(cont);
                    }
                }

            }

            ViewBag.roles = new SelectList(db.Roles, "Name", "Name", role);
            ViewBag.sentUsers = new SelectList(db.Users, "UserName", "UserName", user);

            return View(tree);
        }

        [HttpPost]
        public ActionResult GetTree(string roles, string sentUsers, string authorized, string authorized2, int dealWithUsers2 = 0)
        {
            bool dealWithUsers = dealWithUsers2 == 0 ? false : true;
            if (!dealWithUsers)
            {
                foreach (var accPolicy in db.AccessControlTable.Where(a => a.allowRoles.Contains(roles)).ToList())
                {
                    if (accPolicy.allowRoles.StartsWith(roles))
                    {
                        if (accPolicy.allowRoles.Contains(','))
                            accPolicy.allowRoles = accPolicy.allowRoles.Replace(roles + ",", "");
                        else
                            accPolicy.allowRoles = accPolicy.allowRoles.Replace(roles, "");
                    }
                    else
                    {
                        accPolicy.allowRoles = accPolicy.allowRoles.Replace("," + roles, "");
                    }

                    db.Entry(accPolicy).State = EntityState.Modified;
                    db.SaveChanges();
                }

            }
            else
            {
                foreach (var accPolicy in db.AccessControlTable.Where(a => a.allowedUsers.Contains(sentUsers)).ToList())
                {
                    if (accPolicy.allowedUsers.StartsWith(sentUsers))
                    {
                        if (accPolicy.allowedUsers.Contains(','))
                            accPolicy.allowedUsers = accPolicy.allowedUsers.Replace(sentUsers + ",", "");
                        else
                            accPolicy.allowedUsers = accPolicy.allowedUsers.Replace(sentUsers, "");
                    }
                    else
                    {
                        accPolicy.allowedUsers = accPolicy.allowedUsers.Replace("," + sentUsers, "");
                    }

                    db.Entry(accPolicy).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }



            if (String.IsNullOrEmpty(authorized) && String.IsNullOrEmpty(authorized2))
            {
                return RedirectToAction("GetTree");
            }

            var array = dealWithUsers ? authorized2.Split(',') : authorized.Split(',');
            foreach (string arr in array)
            {
                if (arr.Contains("_____")) // meaning the an action method was sent, not just a controller
                {
                    var cA = arr.Split(new string[] { "_____" }, StringSplitOptions.None);
                    string ca = cA[0];
                    string am = cA[1];
                    if (!db.AccessControlTable.Any(a => a.actionMethodName == am && a.controllerName == ca))
                    {
                        AccessControlModel model = new AccessControlModel()
                        {
                            actionMethodName = cA[1],
                            controllerName = cA[0],
                            allowedUsers = dealWithUsers ? sentUsers : "",
                            allowRoles = !dealWithUsers ? roles : "",
                            functionalityName = "action method",
                            preventedRoles = ""
                        };
                        db.AccessControlTable.Add(model);
                        db.SaveChanges();
                    }
                    else
                    {
                        var accPolicy = db.AccessControlTable.First(a => a.actionMethodName == am && a.controllerName == ca);

                        switch (dealWithUsers)
                        {
                            case false:
                                {
                                    if (accPolicy.allowRoles.Length > 0) // contains other role
                                        accPolicy.allowRoles = accPolicy.allowRoles + "," + roles;
                                    else
                                        accPolicy.allowRoles = roles;
                                    break;
                                }
                            case true:
                                {
                                    if (accPolicy.allowedUsers.Length > 0) // contains other role
                                        accPolicy.allowedUsers = accPolicy.allowedUsers + "," + sentUsers;
                                    else
                                        accPolicy.allowedUsers = sentUsers;
                                    break;
                                }
                        }

                        db.Entry(accPolicy).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
            }

            return RedirectToAction("GetTree", new { sentRole = roles, sentUser = sentUsers });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}