﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.Notes;
using JqueryDTWithMVC.Models.Repository.UnitOfWork;
using static JqueryDTWithMVC.Startup;
using Microsoft.AspNet.Identity;

namespace JqueryDTWithMVC.Controllers
{
    [Title(Title = "Notes")]
    public class NotesModelsController : Controller
    {
        private UnitOfWork unit = new UnitOfWork(new ApplicationDbContext());

        // GET: NotesModels
        [Title(Title = "Notes List")]
        [ModifiedAuthorize]
        public ActionResult Index(string userId)
        {
            ViewBag.userId = String.IsNullOrEmpty(userId) ? User.Identity.GetUserId() : userId;
            return View(unit.Notes.GetNotesForUser(userId).ToList());
        }

        // GET: NotesModels/Details/5
        [Title(Title ="Note Details")]
        [ModifiedAuthorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            NotesModel notesModel = unit.Notes.SingleOrDefault(a => a.ID == id);
            ViewBag.userId = notesModel.ApplicationUserId;
            if (notesModel == null)
            {
                return HttpNotFound();
            }
            return View(notesModel);
        }

        // GET: NotesModels/Create
        [Title(Title = "Create Note")]
        [ModifiedAuthorize]
        public ActionResult Create(string userId)
        {
            ViewBag.ApplicationUserId = new SelectList(unit.Users.Find(a => a.Id == userId), "Id", "EmployeeName");
            ViewBag.userId = userId;
            return View();
        }

        // POST: NotesModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        public ActionResult Create([Bind(Include = "ID,notesString,ApplicationUserId")] NotesModel notesModel)
        {
            if (ModelState.IsValid)
            {
                unit.Notes.Add(notesModel);
                unit.Complete();
                return RedirectToAction("Index", new { userId = notesModel.ApplicationUserId });
            }

            ViewBag.userId = notesModel.ApplicationUserId;
            ViewBag.ApplicationUserId = new SelectList(unit.Users.Find(a => a.Id == notesModel.ApplicationUserId),
                "Id", "EmployeeName", notesModel.ApplicationUserId);

            return View(notesModel);
        }

        // GET: NotesModels/Edit/5
        [Title(Title = "Edit Note")]
        [ModifiedAuthorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NotesModel notesModel = unit.Notes.SingleOrDefault(a => a.ID == id);
            ViewBag.userId = notesModel.ApplicationUserId;
            if (notesModel == null)
            {
                return HttpNotFound();
            }

            ViewBag.ApplicationUserId = new SelectList(unit.Users.GetAll().Where(aa => aa.ApplicationUserId == notesModel.ApplicationUserId),
                "Id", "EmployeeName", notesModel.ApplicationUserId);
            return View(notesModel);
        }

        // POST: NotesModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        public ActionResult Edit([Bind(Include = "ID,notesString,ApplicationUserId")] NotesModel notesModel)
        {
            if (ModelState.IsValid)
            {
                unit.Notes.Add(notesModel);
                unit.Complete();

                return RedirectToAction("Index", new { userId = notesModel.ApplicationUserId });
            }

            ViewBag.userId = notesModel.ApplicationUserId;
            ViewBag.ApplicationUserId = new SelectList(unit.Users.Find(aa => aa.ApplicationUserId == notesModel.ApplicationUserId),
                "Id", "EmployeeName", notesModel.ApplicationUserId);
            return View(notesModel);
        }

        // GET: NotesModels/Delete/5
        [Title(Title = "Delete Note")]
        [ModifiedAuthorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            NotesModel notesModel = unit.Notes.SingleOrDefault(a => a.ID == id);
            ViewBag.userId = notesModel.ApplicationUserId;

            if (notesModel == null)
            {
                return HttpNotFound();
            }
            return View(notesModel);
        }

        // POST: NotesModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        public ActionResult DeleteConfirmed(int id)
        {
            NotesModel notesModel = unit.Notes.SingleOrDefault(a => a.ID == id);
            string userId = notesModel.ApplicationUserId;

            unit.Notes.Remove(notesModel);
            unit.Complete();

            return RedirectToAction("Index", new { userId = userId });
        }
    }
}
