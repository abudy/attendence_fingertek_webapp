﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.Assests;

namespace JqueryDTWithMVC.Controllers
{
    public class AssestsModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: AssestsModels
        public ActionResult Index()
        {
            var assestsModels = db.AssestsTable.Include(a => a.assetsCatgeory).Include(a => a.user);
            return View(assestsModels.ToList());
        }

        // GET: AssestsModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssestsModel assestsModel = db.AssestsTable.Find(id);
            if (assestsModel == null)
            {
                return HttpNotFound();
            }
            return View(assestsModel);
        }

        // GET: AssestsModels/Create
        public ActionResult Create()
        {
            ViewBag.AssestsCategoryModelID = new SelectList(db.AssestsCategoryTable, "ID", "categoryName");
            ViewBag.ApplicationUserId = new SelectList(db.Users, "Id", "EmployeeName");
            return View();
        }

        // POST: AssestsModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,AssestsCategoryModelID,assestValue,assestDesription,SerialNumber,dateLoaned,dateRetunred,ApplicationUserId")] AssestsModel assestsModel)
        {
            if (ModelState.IsValid)
            {
                db.AssestsTable.Add(assestsModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AssestsCategoryModelID = new SelectList(db.AssestsCategoryTable, "ID", "categoryName", assestsModel.AssestsCategoryModelID);
            ViewBag.ApplicationUserId = new SelectList(db.Users, "Id", "EmployeeName", assestsModel.ApplicationUserId);
            return View(assestsModel);
        }

        // GET: AssestsModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssestsModel assestsModel = db.AssestsTable.Find(id);
            if (assestsModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.AssestsCategoryModelID = new SelectList(db.AssestsCategoryTable, "ID", "categoryName", assestsModel.AssestsCategoryModelID);
            ViewBag.ApplicationUserId = new SelectList(db.Users, "Id", "EmployeeName", assestsModel.ApplicationUserId);
            return View(assestsModel);
        }

        // POST: AssestsModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,AssestsCategoryModelID,assestValue,assestDesription,SerialNumber,dateLoaned,dateRetunred,ApplicationUserId")] AssestsModel assestsModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(assestsModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AssestsCategoryModelID = new SelectList(db.AssestsCategoryTable, "ID", "categoryName", assestsModel.AssestsCategoryModelID);
            ViewBag.ApplicationUserId = new SelectList(db.Users, "Id", "EmployeeName", assestsModel.ApplicationUserId);
            return View(assestsModel);
        }

        // GET: AssestsModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssestsModel assestsModel = db.AssestsTable.Find(id);
            if (assestsModel == null)
            {
                return HttpNotFound();
            }
            return View(assestsModel);
        }

        // POST: AssestsModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AssestsModel assestsModel = db.AssestsTable.Find(id);
            db.AssestsTable.Remove(assestsModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
