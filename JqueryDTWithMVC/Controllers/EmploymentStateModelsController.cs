﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.EmploymentState;

namespace JqueryDTWithMVC.Controllers
{
    public class EmploymentStateModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: EmploymentStateModels
        public ActionResult Index()
        {
            return View(db.EmploymentStateTable.ToList());
        }

        // GET: EmploymentStateModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmploymentStateModel employmentStateModel = db.EmploymentStateTable.Find(id);
            if (employmentStateModel == null)
            {
                return HttpNotFound();
            }
            return View(employmentStateModel);
        }

        // GET: EmploymentStateModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EmploymentStateModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,title,state")] EmploymentStateModel employmentStateModel)
        {
            if (ModelState.IsValid)
            {
                db.EmploymentStateTable.Add(employmentStateModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(employmentStateModel);
        }

        // GET: EmploymentStateModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmploymentStateModel employmentStateModel = db.EmploymentStateTable.Find(id);
            if (employmentStateModel == null)
            {
                return HttpNotFound();
            }
            return View(employmentStateModel);
        }

        // POST: EmploymentStateModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,title,state")] EmploymentStateModel employmentStateModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employmentStateModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employmentStateModel);
        }

        // GET: EmploymentStateModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmploymentStateModel employmentStateModel = db.EmploymentStateTable.Find(id);
            if (employmentStateModel == null)
            {
                return HttpNotFound();
            }
            return View(employmentStateModel);
        }

        // POST: EmploymentStateModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EmploymentStateModel employmentStateModel = db.EmploymentStateTable.Find(id);
            db.EmploymentStateTable.Remove(employmentStateModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
