﻿using JqueryDTWithMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JqueryDTWithMVC.Controllers
{
    public class RemoteValidationController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: RemoteValidation
        [AllowAnonymous]
        [HttpPost]
        public ActionResult CheckExistingIPAddress(string branch_ip)
        {
            try
            {
                return Json(IsIPAddressExists(branch_ip), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        private bool IsIPAddressExists(string branch_ip)
        {
            return true;
            //if (db.BranchesTable.Any(a => a.branch_ip == branch_ip))
            //{
            //    return false;
            //}
            //else
            //{
            //    return true;
            //}
        }
    }
}