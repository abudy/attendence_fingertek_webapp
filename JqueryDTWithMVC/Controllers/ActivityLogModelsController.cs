﻿using JqueryDTWithMVC.Helper;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.ActivityLog;
using JqueryDTWithMVC.Models.ViewModels.ActivityLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static JqueryDTWithMVC.Startup;

namespace JqueryDTWithMVC.Controllers
{
    [Title(Title = "متحكم سجل الحركات")]
    public class ActivityLogModelsController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: ActivityLogModels
        [ModifiedAuthorize]
        [Title(Title = "جدول الحركات")]
        public ActionResult Index(int draw = 0, int start = 0, int length = 10)
        {
            try
            {
                JqueryDTWithMVC.Helper.DataTableModels<ActivityLogViewModel>._data = db.ActivityLogTable.ToList().Select(a => new ActivityLogViewModel()
                {
                    DT_RowId = a.ID,
                    sys_action = a.valueLog,
                    manager_name = db.Users.Find(a.userId).EmployeeName,
                    sys_date = a.activityTime.ToString("yyyy-MM-dd HH:mm:ss"),
                    sys_id = a.ID
                }).ToList();

                DataTableData<ActivityLogViewModel> dataTableData = new DataTableData<ActivityLogViewModel>();
                dataTableData.draw = draw;
                dataTableData.recordsTotal = DataTableModels<ActivityLogViewModel>._data.Count;
                int recordsFiltered = 0;
                dataTableData.data = DataTableModels<ActivityLogViewModel>._data; /*DataTableModels<UserViewModel>.FilterData(ref recordsFiltered, start, length, search, sortColumn, sortDirection);*/
                dataTableData.recordsFiltered = recordsFiltered;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception tt)
            {
                return Json("error");
            }
        }
    }
}