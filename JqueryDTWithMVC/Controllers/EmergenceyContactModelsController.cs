﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.EmergenceyContact;
using Microsoft.AspNet.Identity;
using JqueryDTWithMVC.Models.Repository.UnitOfWork;

namespace JqueryDTWithMVC.Controllers
{
    public class EmergenceyContactModelsController : Controller
    {
        private UnitOfWork unit = new UnitOfWork(new ApplicationDbContext());

        // GET: EmergenceyContactModels
        public ActionResult Index(string userId)
        {
            var uId = String.IsNullOrEmpty(userId) ? User.Identity.GetUserId() : userId;
            var emergencey = unit.EmergenceyContact.GetAll();


            if (User.IsInRole("موظف"))
            {
                emergencey = unit.EmergenceyContact.GetEmergenceyContactForUser(userId);
            }

            ViewBag.userId = uId;
            return View(emergencey.ToList());
        }

        // GET: EmergenceyContactModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmergenceyContactModel emergenceyContactModel = unit.EmergenceyContact.SingleOrDefault(aa=>aa.ID == id);

            if (emergenceyContactModel == null)
            {
                return HttpNotFound();
            }

            ViewBag.userId = emergenceyContactModel.ApplicationUserId;
            return View(emergenceyContactModel);
        }

        // GET: EmergenceyContactModels/Create
        public ActionResult Create(string userId)
        {
            ViewBag.ApplicationUserId = new SelectList(unit.Users.Find(aa=>aa.ApplicationUserId == userId), "Id", "EmployeeName");

            ViewBag.userId = userId;
            return View();
        }

        // POST: EmergenceyContactModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,RelationShip,HomePhone,WorkPhone,MobilePhone,Email,Country,Address1,Address2,City,Region,PostCode,ApplicationUserId")] EmergenceyContactModel emergenceyContactModel)
        {
            if (ModelState.IsValid)
            {
                unit.EmergenceyContact.Add(emergenceyContactModel);
                unit.Complete();

                return RedirectToAction("Index", new { userId = emergenceyContactModel.ApplicationUserId });
            }

            ViewBag.ApplicationUserId = new SelectList(unit.Users.
                Find(aa => aa.ApplicationUserId == emergenceyContactModel.ApplicationUserId), "Id", "EmployeeName",emergenceyContactModel.ApplicationUserId);
            ViewBag.userId = emergenceyContactModel.ApplicationUserId;
            return View(emergenceyContactModel);
        }

        // GET: EmergenceyContactModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmergenceyContactModel emergenceyContactModel = unit.EmergenceyContact.SingleOrDefault(aa => aa.ID == id);
            if (emergenceyContactModel == null)
            {
                return HttpNotFound();
            }

            ViewBag.ApplicationUserId = new SelectList(unit.Users.
                Find(aa => aa.ApplicationUserId == emergenceyContactModel.ApplicationUserId), "Id", "EmployeeName", emergenceyContactModel.ApplicationUserId);
            ViewBag.userId = emergenceyContactModel.ApplicationUserId;
            return View(emergenceyContactModel);
        }

        // POST: EmergenceyContactModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,RelationShip,HomePhone,WorkPhone,MobilePhone,Email,Country,Address1,Address2,City,Region,PostCode,ApplicationUserId")] EmergenceyContactModel emergenceyContactModel)
        {
            if (ModelState.IsValid)
            {
                unit.EmergenceyContact.Update(emergenceyContactModel);
                unit.Complete();

                return RedirectToAction("Index", new { userId = emergenceyContactModel.ApplicationUserId });
            }

            ViewBag.ApplicationUserId = new SelectList(unit.Users.
                    Find(aa => aa.ApplicationUserId == emergenceyContactModel.ApplicationUserId), "Id", "EmployeeName", emergenceyContactModel.ApplicationUserId);

            ViewBag.userId = emergenceyContactModel.ApplicationUserId;
            return View(emergenceyContactModel);
        }

        // GET: EmergenceyContactModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            EmergenceyContactModel emergenceyContactModel = unit.EmergenceyContact.SingleOrDefault(aa => aa.ID == id);

            if (emergenceyContactModel == null)
            {
                return HttpNotFound();
            }

            ViewBag.userId = emergenceyContactModel.ApplicationUserId;
            return View(emergenceyContactModel);
        }

        // POST: EmergenceyContactModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EmergenceyContactModel emergenceyContactModel = unit.EmergenceyContact.SingleOrDefault(aa => aa.ID == id);
            string userId = emergenceyContactModel.ApplicationUserId;

            unit.EmergenceyContact.Remove(emergenceyContactModel);
            unit.Complete();

            return RedirectToAction("Index", new { userId = userId });
        }

    }
}
