﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.CheckType;
using JqueryDTWithMVC.Helper;
using JqueryDTWithMVC.Models.ViewModels.CheckType;
using static JqueryDTWithMVC.Startup;

namespace JqueryDTWithMVC.Controllers
{
    [Title(Title = "اسباب الغياب")]
    public class CheckTypeModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: CheckTypeModels
        [ModifiedAuthorize]
        [Title(Title = "جدول اسباب الغياب")]
        public ActionResult Index(int draw = 0, int start = 0, int length = 10)
        {
            try
            {
                JqueryDTWithMVC.Helper.DataTableModels<CheckTypeViewModel>._data = db.CheckTypeTable.ToList().Select(a => new CheckTypeViewModel()
                {
                    CheckID = a.ID,
                    CheckName = a.CheckName,
                    DT_RowId = a.ID
                }).ToList();

                DataTableData<CheckTypeViewModel> dataTableData = new DataTableData<CheckTypeViewModel>();
                dataTableData.draw = draw;
                dataTableData.recordsTotal = DataTableModels<CheckTypeViewModel>._data.Count;
                int recordsFiltered = 0;
                dataTableData.data = DataTableModels<CheckTypeViewModel>._data; /*DataTableModels<UserViewModel>.FilterData(ref recordsFiltered, start, length, search, sortColumn, sortDirection);*/
                dataTableData.recordsFiltered = recordsFiltered;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception tt)
            {
                return Json("error");
            }
        }

        // POST: CheckTypeModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ModifiedAuthorize]
        [Title(Title = "انشاء سبب غياب")]
        public async Task<ActionResult> Create([Bind(Include = "ID,CheckName,CheckNameFromCakeHR,state")] CheckTypeModels checkTypeModels)
        {
            if (ModelState.IsValid)
            {
                db.CheckTypeTable.Add(checkTypeModels);
                await db.SaveChangesAsync();
                return Json("done");
            }

            return Json("error");
        }

        // POST: CheckTypeModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ModifiedAuthorize]
        [Title(Title = "تعديل سبب غياب")]
        public async Task<ActionResult> Edit([Bind(Include = "ID,CheckName,CheckNameFromCakeHR,state")] CheckTypeModels checkTypeModels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(checkTypeModels).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return Json("done");
            }
            return Json("error");
        }

        // POST: CheckTypeModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ModifiedAuthorize]
        [Title(Title = "مسح سبب غياب")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                CheckTypeModels checkTypeModels = await db.CheckTypeTable.FindAsync(id);
                db.CheckTypeTable.Remove(checkTypeModels);
                await db.SaveChangesAsync();
                return Json("done");
            }
            catch
            {
                return Json("error");
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
