﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.Assests;

namespace JqueryDTWithMVC.Controllers
{
    public class AssestsCategoryModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: AssestsCategoryModels
        public ActionResult Index()
        {
            return View(db.AssestsCategoryTable.ToList());
        }

        // GET: AssestsCategoryModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssestsCategoryModel assestsCategoryModel = db.AssestsCategoryTable.Find(id);
            if (assestsCategoryModel == null)
            {
                return HttpNotFound();
            }
            return View(assestsCategoryModel);
        }

        // GET: AssestsCategoryModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AssestsCategoryModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,categoryName")] AssestsCategoryModel assestsCategoryModel)
        {
            if (ModelState.IsValid)
            {
                db.AssestsCategoryTable.Add(assestsCategoryModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(assestsCategoryModel);
        }

        // GET: AssestsCategoryModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssestsCategoryModel assestsCategoryModel = db.AssestsCategoryTable.Find(id);
            if (assestsCategoryModel == null)
            {
                return HttpNotFound();
            }
            return View(assestsCategoryModel);
        }

        // POST: AssestsCategoryModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,categoryName")] AssestsCategoryModel assestsCategoryModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(assestsCategoryModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(assestsCategoryModel);
        }

        // GET: AssestsCategoryModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssestsCategoryModel assestsCategoryModel = db.AssestsCategoryTable.Find(id);
            if (assestsCategoryModel == null)
            {
                return HttpNotFound();
            }
            return View(assestsCategoryModel);
        }

        // POST: AssestsCategoryModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AssestsCategoryModel assestsCategoryModel = db.AssestsCategoryTable.Find(id);
            db.AssestsCategoryTable.Remove(assestsCategoryModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
