﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.Title;

namespace JqueryDTWithMVC.Controllers
{
    public class TitleModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TitleModels
        public ActionResult Index()
        {
            return View(db.TitleTable.ToList());
        }

        // GET: TitleModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TitleModel titleModel = db.TitleTable.Find(id);
            if (titleModel == null)
            {
                return HttpNotFound();
            }
            return View(titleModel);
        }

        // GET: TitleModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TitleModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,title")] TitleModel titleModel)
        {
            if (ModelState.IsValid)
            {
                db.TitleTable.Add(titleModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(titleModel);
        }

        // GET: TitleModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TitleModel titleModel = db.TitleTable.Find(id);
            if (titleModel == null)
            {
                return HttpNotFound();
            }
            return View(titleModel);
        }

        // POST: TitleModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,title")] TitleModel titleModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(titleModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(titleModel);
        }

        // GET: TitleModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TitleModel titleModel = db.TitleTable.Find(id);
            if (titleModel == null)
            {
                return HttpNotFound();
            }
            return View(titleModel);
        }

        // POST: TitleModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TitleModel titleModel = db.TitleTable.Find(id);
            db.TitleTable.Remove(titleModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
