﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.Schedules;
using JqueryDTWithMVC.Helper;
using JqueryDTWithMVC.Models.ViewModels.Schedule;
using static JqueryDTWithMVC.Startup;

namespace JqueryDTWithMVC.Controllers
{
    [Title(Title = "اوقات الدوام")]
    public class SchedulesModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: SchedulesModels
        // GET: CheckTypeModels
        [ModifiedAuthorize]
        [Title(Title = "جدول اوقات الدوام")]
        public ActionResult Index(int draw = 0, int start = 0, int length = 10)
        {
            try
            {
                JqueryDTWithMVC.Helper.DataTableModels<ScheduleViewModel>._data = db.SchedulesTable.ToList().Select(a => new ScheduleViewModel()
                {
                    DT_RowId = a.ID.ToString(),
                    schedule_allowtime  = a.schedule_allowtime.ToString(),
                    schedule_friday = a.schedule_friday == null ? "" : a.schedule_friday,
                    schedule_friday2 = a.schedule_friday2 == null ? "" : a.schedule_friday2,
                    schedule_id = a.ID,
                    schedule_monday  =a.schedule_monday == null ? "" : a.schedule_monday,
                    schedule_monday2 = a.schedule_monday2 == null ? "" : a.schedule_monday2,
                    schedule_saturday = a.schedule_saturday == null ? "" : a.schedule_saturday,
                    schedule_saturday2 = a.schedule_saturday2 == null ? "" : a.schedule_saturday2,
                    schedule_sunday = a.schedule_sunday == null ? "" : a.schedule_sunday,
                    schedule_sunday2 = a.schedule_sunday2 == null ? "" : a.schedule_sunday2,
                    schedule_thursday= a.schedule_thursday == null ? "" : a.schedule_thursday,
                    schedule_thursday2 = a.schedule_thursday2 == null ? "" :a.schedule_thursday2,
                    schedule_tuesday = a.schedule_tuesday == null ? "" : a.schedule_tuesday,
                    schedule_tuesday2 = a.schedule_tuesday2 == null ? "" : a.schedule_tuesday2,
                    schedule_wednesday = a.schedule_wednesday == null ? "" : a.schedule_wednesday,
                    schedule_wednesday2 = a.schedule_wednesday2 == null ? "" : a.schedule_wednesday2,
                    schedule_name = a.schedule_name,

                }).ToList();

                DataTableData<ScheduleViewModel> dataTableData = new DataTableData<ScheduleViewModel>();
                dataTableData.draw = draw;
                dataTableData.recordsTotal = DataTableModels<ScheduleViewModel>._data.Count;
                int recordsFiltered = 0;
                dataTableData.data = DataTableModels<ScheduleViewModel>._data; /*DataTableModels<UserViewModel>.FilterData(ref recordsFiltered, start, length, search, sortColumn, sortDirection);*/
                dataTableData.recordsFiltered = recordsFiltered;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception tt)
            {
                return Json("");
            }
        }

        // POST: SchedulesModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ModifiedAuthorize]
        [Title(Title = "انشاء وقت الدوام")]
        public ActionResult Create(ScheduleViewModelPosting schedulesViewModel)
        {
            if (ModelState.IsValid)
            {
                SchedulesModel schedulesModel = new SchedulesModel()
                {
                    schedule_allowtime = schedulesViewModel.ScheduleAllowTime,
                    schedule_friday  = schedulesViewModel.ScheduleFriday ,
                    schedule_friday2 = schedulesViewModel.ScheduleFriday2 ,
                    schedule_name = schedulesViewModel.ScheduleName,
                    schedule_monday = schedulesViewModel.ScheduleMonday,
                    schedule_monday2 = schedulesViewModel.ScheduleMonday2 ,
                    schedule_saturday = schedulesViewModel.ScheduleSaturday ,
                    schedule_saturday2 = schedulesViewModel.ScheduleSaturday2 ,
                    schedule_sunday = schedulesViewModel.ScheduleSunday ,
                    schedule_sunday2 = schedulesViewModel.ScheduleSunday2 ,
                    schedule_thursday = schedulesViewModel.ScheduleThursday,
                    schedule_thursday2 = schedulesViewModel.ScheduleThursday2 ,
                    schedule_tuesday = schedulesViewModel.ScheduleTuesday ,
                    schedule_tuesday2 = schedulesViewModel.ScheduleTuesday2 ,
                    schedule_wednesday = schedulesViewModel.ScheduleWednesday ,
                    schedule_wednesday2 = schedulesViewModel.ScheduleWednesday2,
                };

                db.SchedulesTable.Add(schedulesModel);
                db.SaveChanges();
                return Json("done");
            }

            return Json("error");
        }

        // POST: SchedulesModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ModifiedAuthorize]
        [Title(Title ="تعديل وقت الدوام")]
        public ActionResult Edit(ScheduleViewModelPosting schedulesViewModel)
        {
            if (ModelState.IsValid)
            {

                SchedulesModel schedulesModel = db.SchedulesTable.Find(schedulesViewModel.ScheduleID);
                schedulesModel.schedule_allowtime = schedulesViewModel.ScheduleAllowTime;
                schedulesModel.schedule_name = schedulesViewModel.ScheduleName;

                schedulesModel.schedule_wednesday2 = schedulesViewModel.ScheduleWednesday2;
                schedulesModel.schedule_wednesday = schedulesViewModel.ScheduleWednesday;

                schedulesModel.schedule_tuesday = schedulesViewModel.ScheduleTuesday;
                schedulesModel.schedule_tuesday2 = schedulesViewModel.ScheduleWednesday2;

                schedulesModel.schedule_thursday = schedulesViewModel.ScheduleThursday;
                schedulesModel.schedule_thursday2 = schedulesViewModel.ScheduleThursday2;

                schedulesModel.schedule_sunday = schedulesViewModel.ScheduleSunday;
                schedulesModel.schedule_sunday2 = schedulesViewModel.ScheduleSunday2;

                schedulesModel.schedule_saturday = schedulesViewModel.ScheduleSaturday;
                schedulesModel.schedule_saturday2 = schedulesViewModel.ScheduleSaturday2;

                schedulesModel.schedule_friday = schedulesViewModel.ScheduleFriday;
                schedulesModel.schedule_friday2 = schedulesViewModel.ScheduleFriday2;

                schedulesModel.schedule_monday = schedulesViewModel.ScheduleMonday;
                schedulesModel.schedule_monday2 = schedulesViewModel.ScheduleMonday2;

                db.Entry(schedulesModel).State = EntityState.Modified;
                db.SaveChanges();
                return Json("done");
            }

            return Json("error");
        }

        // POST: SchedulesModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ModifiedAuthorize]
        [Title(Title = "مسح وقت الدوام")]
        public ActionResult DeleteConfirmed(int ScheduleID)
        {
            try
            {
                SchedulesModel schedulesModel = db.SchedulesTable.Find(ScheduleID);
                db.SchedulesTable.Remove(schedulesModel);
                db.SaveChanges();
                return Json("done");
            }
            catch
            {
                return Json("Error");
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
