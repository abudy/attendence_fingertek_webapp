﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.CheckPointLog;
using JqueryDTWithMVC.Models.ViewModels.PointsLog;
using JqueryDTWithMVC.Helper;
using System.Globalization;
using static JqueryDTWithMVC.Startup;
using JqueryDTWithMVC.Models.DAL.CheckType;
using System.Threading;
using JqueryDTWithMVC.Helpers;
using JqueryDTWithMVC.Models.ViewModels.DevicePoint;
using JqueryDTWithMVC.Models.ViewModels.SummeryReport;
using JqueryDTWithMVC.Models.DAL.CheckPointSummery;
using JqueryDTWithMVC.Models.ViewModels.Schedule;
using JqueryDTWithMVC.Models.ViewModels.UserReports;
using JqueryDTWithMVC.Models.DAL.Branches;
using Newtonsoft.Json;
using System.Data.Entity.SqlServer;

namespace JqueryDTWithMVC.Controllers
{
    [Title(Title = " متحكم نقاط الحضور")]
    public class CheckPointLogModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: CheckPointLogModels
        [HttpPost]
        [ModifiedAuthorize]
        [Title(Title = "جدول نقاط الحضور")]
        public ActionResult Index(DateTime TimeDateFrom, DateTime TimeDateTo, int Branch, int draw = 0, int start = 0, int length = 10)
        {
            try
            {
                TimeDateTo = TimeDateTo > System.DateTime.Now.Date ? System.DateTime.Now.Date : TimeDateTo;
                TimeDateFrom = TimeDateFrom > System.DateTime.Now.Date ? System.DateTime.Now.Date : TimeDateFrom;

                JqueryDTWithMVC.Helper.DataTableModels<PointsLogViewModel>._data = db.CheckPointLogTable.Include(aa => aa.user).Include(aa => aa.checkType)
                    .Include(aa => aa.branch).Where(a => a.CurrentDay >= TimeDateFrom && a.CurrentDay <= TimeDateTo && a.BranchesModelID == Branch).ToList()
                        .Where(aa => aa.user.EmployeeShow).Select(a => new PointsLogViewModel()
                        {
                            CheckBranch = a.BranchesModelID.HasValue ? a.BranchesModelID.Value : 0,
                            CheckDay = a.CurrentDay.ToString("dddd", new CultureInfo("ar-AE")),
                            CheckDate = a.CurrentDay.ToString("yyyy-MM-dd"),
                            CheckTimeIn = a.CheckTimeIn.HasValue ? a.CheckTimeIn.Value.ToString("HH:mm:ss") : "00:00:00",
                            CheckTimeOut = a.CheckTimeOut.HasValue ? a.CheckTimeOut.Value.ToString("HH:mm:ss") : "00:00:00",
                            EmployeeAName = a.user != null ? a.user.EmployeeName : "",
                            CheckName = a.CheckTypeModelsID != null ? a.checkType.CheckName : "غير متوفر",
                            CheckRLength = a.lateMin.HasValue ? a.lateMin.Value : 0,
                            CheckEmpID = a.userId,
                            CheckID = a.CheckTypeModelsID != null ? a.CheckTypeModelsID.Value : 7,
                            CheckType = a.CheckTypeModelsID != null ? (int)a.checkType.state : 7,
                            DT_RowId = a.ID,
                            CheckRBranch = a.BranchesModelID != null ? a.BranchesModelID.Value.ToString() : "0",
                            CheckRType = a.CheckTypeModelsID != null ? a.CheckTypeModelsID.Value.ToString() : "7",
                            EmployeeBranch = a.BranchesModelID != null ? a.BranchesModelID.Value.ToString() : "0",
                            EmployeeID = a.userId
                        }).ToList();

                DataTableData<PointsLogViewModel> dataTableData = new DataTableData<PointsLogViewModel>();
                dataTableData.draw = draw;
                dataTableData.recordsTotal = DataTableModels<PointsLogViewModel>._data.Count;
                int recordsFiltered = 0;
                dataTableData.data = DataTableModels<PointsLogViewModel>._data; 
                dataTableData.recordsFiltered = recordsFiltered;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception tt)
            {
                return Json("");
            }
        }

        [HttpPost]
        [ModifiedAuthorize]
        [Title(Title = "جدول  تقارير نقاط الحضور")]
        public ActionResult IndexPointsReport(DateTime TimeDateFrom, DateTime TimeDateTo, int Branch, int draw = 0, int start = 0, int length = 10)
        {
            try
            {
                JqueryDTWithMVC.Helper.DataTableModels<AbsenceSummeryViewModel>._data = db.CheckPointSummeryTable
                    .Include(aa => aa.user).Include(aa => aa.relatedSchedule).Include(aa => aa.user.relatedBranch)
                    .Include(a => a.checkType).Where(a => a.currentDate >= TimeDateFrom && a.currentDate <= TimeDateTo && a.user.BranchesModelID == Branch)
                    .ToList().Where(aa => aa.user.EmployeeShow).GroupBy(a => new { a.ApplicationUserId, a.checkType.CheckName }).Select(a => new AbsenceSummeryViewModel()
                    {
                        CheckName = a.First().checkType.CheckName,
                        CheckRBranch = a.First().user.relatedBranch.branch_name,
                        CheckRLength = a.Sum(g => g.LateBy.HasValue ? (int)g.LateBy.Value : 0),
                        DT_RowId = a.First().ID,
                        EmployeeAName = a.First().user.EmployeeName,
                        EmployeeBranch = a.First().user.relatedBranch.branch_name,
                        CheckRType = (int)a.First().checkType.state
                    }).ToList();

                DataTableData<AbsenceSummeryViewModel> dataTableData = new DataTableData<AbsenceSummeryViewModel>();
                dataTableData.draw = draw;
                dataTableData.recordsTotal = DataTableModels<AbsenceSummeryViewModel>._data.Count;
                int recordsFiltered = 0;
                dataTableData.data = DataTableModels<AbsenceSummeryViewModel>._data;
                dataTableData.recordsFiltered = recordsFiltered;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception tt)
            {
                return Json("Error: " + tt.Message);
            }
        }

        [HttpPost]
        [ModifiedAuthorize]
        [Title(Title = "تقرير الحضور والغيابات")]
        public ActionResult GetUserReport(DateTime TimeDateFrom, DateTime TimeDateTo, int Branch, int draw = 0, int start = 0, int length = 10)
        {
            try
            {
                TimeDateTo = TimeDateTo > DateTime.Now ? DateTime.Now : TimeDateTo;
                TimeDateFrom = TimeDateFrom > DateTime.Now ? DateTime.Now : TimeDateFrom;

                List<SchedulesGeneralInfo> scheduleModel = new List<SchedulesGeneralInfo>();
                ScheduleValues sc = new ScheduleValues();
                foreach (var schdule in db.SchedulesTable.ToList())
                {
                    scheduleModel.Add(sc.ScheduleGeneralnfo(schdule.ID, TimeDateFrom, TimeDateTo));
                }

                JqueryDTWithMVC.Helper.DataTableModels<UserReportsViewModel>._data = db.CheckPointSummeryTable
                    .Include(aa => aa.user).Include(aa => aa.relatedSchedule).Include(aa => aa.user.relatedBranch)
                    .Include(a => a.checkType).Where(a => a.currentDate >= TimeDateFrom && a.currentDate <= TimeDateTo && a.user.BranchesModelID == Branch)
                    .ToList().Where(a => a.user.EmployeeShow).GroupBy(a => a.ApplicationUserId).Select(a => new UserReportsViewModel()
                    {
                        DT_RowId = a.Key,
                        ID = a.Key,

                        CountDays = scheduleModel.Find(b => b.ID == db.Users.Find(a.Key).SchedulesModelID).numberOfWorkDaysPerSelectedDate,
                        WorkTime = scheduleModel.Find(b => b.ID == db.Users.Find(a.Key).SchedulesModelID).numOfMinWorkPerSelectedDate,
                        VacationMinCount = (int)a.Sum(c => c.VacationMin.HasValue ? c.VacationMin.Value : 0),

                        EmployeeAName = db.Users.Find(a.Key).EmployeeName,
                        WorkedTime = scheduleModel.Find(b => b.ID == db.Users.Find(a.Key).SchedulesModelID).numOfMinWorkPerSelectedDate
                                                                - (int)a.Sum(c => c.LateBy.HasValue ? c.LateBy : 0),
                        Absence = (int)a.Sum(c => c.LateBy.HasValue ? c.LateBy : 0),
                        AbsenceCount = (int)a.Sum(c => c.LateBy.HasValue ? c.LateBy : 0) / (60),

                        RestCount = a.Count(c => c.checkType.state == LogStatus.اجازة),
                        Rest = a.Count(c => c.checkType.state == LogStatus.غياب)
                    }).ToList();

                DataTableData<UserReportsViewModel> dataTableData = new DataTableData<UserReportsViewModel>();
                dataTableData.draw = draw;
                dataTableData.recordsTotal = DataTableModels<UserReportsViewModel>._data.Count;
                int recordsFiltered = 0;
                dataTableData.data = DataTableModels<UserReportsViewModel>._data;
                dataTableData.recordsFiltered = recordsFiltered;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception tt)
            {
                return Json("Error: " + tt.Message);
            }
        }

        // recalculate absense for un identified status: غير معرف والاجازات 
        [HttpPost]
        [ModifiedAuthorize]
        [Title(Title = "اعادة حساب جدول نقاط الحضور")]
        public ActionResult ReCalculatePointsType(DateTime TimeDateFrom, DateTime TimeDateTo, int Branch, int draw = 0, int start = 0, int length = 10)
        {

            TimeDateTo = TimeDateTo > System.DateTime.Now.Date ? System.DateTime.Now.Date : TimeDateTo;
            TimeDateFrom = TimeDateFrom > System.DateTime.Now.Date ? System.DateTime.Now.Date : TimeDateFrom;
            ScheduleValues sc = new ScheduleValues();

            List<CheckPointLogModels> res;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                res = db.CheckPointLogTable.Include(aa => aa.user).Include(aa => aa.CheckPointSummeryModel).Where(a => a.CheckTimeIn != null && a.CurrentDay >= TimeDateFrom && a.CurrentDay <= TimeDateTo && a.BranchesModelID == Branch)
                .ToList();
            }

            SyncPointsFromDevices.ReCalculatePointsTypeNoCake(res);

            SyncPointsFromDevices.GetUserAbsenceFast(TimeDateFrom, TimeDateTo, Branch, db.Users.Where(a => a.BranchesModelID.HasValue ?
                                                                                   a.BranchesModelID == Branch : false).ToList()); // get users who are absent today
            SyncPointsFromDevices.ComputeCakeHR(TimeDateFrom, TimeDateTo, Branch, db.Users.Where(a => a.BranchesModelID.HasValue ?
                                                                                   a.BranchesModelID == Branch : false).ToList());
            return Json("done");
        }


        // recalculate absense for un identified status: غير معرف والاجازات 
        [HttpPost]
        [ModifiedAuthorize]
        [Title(Title = "اعادة حساب جدول نقاط الحضور عند ادخال حالة النقاط يدويا")]
        public ActionResult ReCalculatePointsTypeNoCake(DateTime TimeDateFrom, DateTime TimeDateTo, int Branch, int draw = 0, int start = 0, int length = 10)
        {
            TimeDateTo = TimeDateTo > System.DateTime.Now.Date ? System.DateTime.Now.Date : TimeDateTo;
            TimeDateFrom = TimeDateFrom > System.DateTime.Now.Date ? System.DateTime.Now.Date : TimeDateFrom;

            List<CheckPointLogModels> res;
            using(ApplicationDbContext db = new ApplicationDbContext())
            {
                res = db.CheckPointLogTable.Include(aa => aa.user).Include(aa => aa.CheckPointSummeryModel).Include(a => a.checkType).Where(a => a.CheckTimeIn != null && a.CurrentDay >= TimeDateFrom && a.CurrentDay <= TimeDateTo && a.BranchesModelID == Branch)
                .ToList();
            }
           
            SyncPointsFromDevices.ReCalculatePointsTypeNoCake(res);
            return Json("done");
        }

        //// recalculate absense for un identified status: غير معرف والاجازات 
        //[HttpPost]
        //[ModifiedAuthorize]
        //[Title(Title = "اعادة حساب جدول نقاط الحضور")]
        //public ActionResult ReCalculatePointsType(DateTime TimeDateFrom, DateTime TimeDateTo, int Branch, int draw = 0, int start = 0, int length = 10)
        //{
        //    //try
        //    //{
        //    TimeDateTo = TimeDateTo > System.DateTime.Now.Date ? System.DateTime.Now.Date : TimeDateTo;
        //    TimeDateFrom = TimeDateFrom > System.DateTime.Now.Date ? System.DateTime.Now.Date : TimeDateFrom;
        //    ScheduleValues sc = new ScheduleValues();
        //    var ck = db.CheckPointSummeryTable.ToList();

        //    foreach (var chkPointlog in db.CheckPointLogTable.Include(a => a.checkType).Where(a => a.CheckTimeIn != null && a.CurrentDay >= TimeDateFrom && a.CurrentDay <= TimeDateTo && a.BranchesModelID == Branch)
        //        .ToList())
        //    {
        //        //          if (chkPointlog.checkType.state == LogStatus.غير_معرف && chkPointlog.CheckTimeOut.HasValue && chkPointlog.CheckTimeIn.HasValue)
        //        {
        //            try
        //            {
        //                var schedules = sc.GetScheduleValues(chkPointlog.userId, (int)chkPointlog.CurrentDay.DayOfWeek, chkPointlog.CurrentDay);
        //                var res = sc.CheckOutCheckInCompare(chkPointlog.CheckTimeIn, schedules.Item3,
        //                     chkPointlog.CheckTimeOut, schedules.Item4, schedules.Item2, chkPointlog.checkType.state);

        //                //chkPointlog.CheckTypeModelsID = res.Item1;
        //                //chkPointlog.lateMin = res.Item2;



        //                if (!ck.Any(a => a.currentDate == chkPointlog.CheckTimeIn.Value.Date && a.ApplicationUserId == chkPointlog.userId))
        //                {
        //                    CheckPointSummeryModel model = new CheckPointSummeryModel()
        //                    {
        //                        CheckTypeModelsID = res.Item1,
        //                        ApplicationUserId = chkPointlog.userId,
        //                        LateBy = chkPointlog.lateMin,
        //                        SchedulesModelID = db.Users.Find(chkPointlog.userId).SchedulesModelID.Value,
        //                        currentDate = chkPointlog.CheckTimeIn.Value.Date,
        //                        CheckPointLogModelsID = chkPointlog.ID,
        //                        CheckPointLogModels = chkPointlog
        //                    };
        //                    db.CheckPointSummeryTable.Add(model);
        //                    db.SaveChanges();
        //                }
        //                else
        //                {
        //                    if (chkPointlog.userId == "db3393e6-da1b-4b93-bf4e-5f7b3876e3cf")
        //                    {
        //                        string jj = "bre";
        //                    }
        //                    var summery = ck.First(a => a.currentDate == chkPointlog.CheckTimeIn.Value.Date
        //                    && a.ApplicationUserId == chkPointlog.userId);

        //                    summery.LateBy = chkPointlog.lateMin;
        //                    summery.CheckTypeModelsID = chkPointlog.CheckTypeModelsID.Value;
        //                    summery.SchedulesModelID = db.Users.Find(chkPointlog.userId).SchedulesModelID.Value;
        //                    db.Entry(summery).State = EntityState.Modified;
        //                    db.SaveChanges();
        //                }
        //            }
        //            catch (Exception ll)
        //            {
        //                return Json("Error: " + ll.Message);
        //            }



        //        }

        //        //    db.Entry(chkPointlog).State = EntityState.Modified;



        //        //if(chkPointlog.checkType.state == LogStatus.اجازة && chkPointlog.CheckTimeIn != null)
        //        //{
        //        //    var schedules = sc.GetScheduleValues(chkPointlog.userId, (int)chkPointlog.CurrentDay.DayOfWeek, chkPointlog.CurrentDay);
        //        //    var res = sc.CheckInCompare(chkPointlog.CheckTimeIn, schedules.Item3, schedules.Item2, chkPointlog.checkType.state);
        //        //    chkPointlog.lateMin = res;

        //        //    if (chkPointlog.lateMin > 0)
        //        //    {
        //        //        if (!ck.Any(a => a.currentDate == chkPointlog.CheckTimeIn.Value.Date
        //        //                                            && a.ApplicationUserId == chkPointlog.userId))
        //        //        {
        //        //            CheckPointSummeryModel model = new CheckPointSummeryModel()
        //        //            {
        //        //                CheckTypeModelsID = chkPointlog.CheckTypeModelsID.Value,
        //        //                ApplicationUserId = chkPointlog.userId,
        //        //                LateBy = chkPointlog.lateMin,
        //        //                SchedulesModelID = db.Users.Find(chkPointlog.userId).SchedulesModelID.Value,
        //        //                currentDate = chkPointlog.CheckTimeIn.Value.Date,
        //        //                CheckPointLogModelsID = chkPointlog.ID,
        //        //                CheckPointLogModels = chkPointlog
        //        //            };

        //        //            db.CheckPointSummeryTable.Add(model);
        //        //        }
        //        //        else
        //        //        {
        //        //            var summery = ck.First(a => a.currentDate == chkPointlog.CheckTimeIn.Value.Date
        //        //            && a.ApplicationUserId == chkPointlog.userId);

        //        //            summery.LateBy = chkPointlog.lateMin;
        //        //            summery.CheckTypeModelsID = chkPointlog.CheckTypeModelsID.Value;
        //        //            summery.SchedulesModelID = db.Users.Find(chkPointlog.userId).SchedulesModelID.Value;
        //        //            db.Entry(summery).State = EntityState.Modified;
        //        //        }
        //        //    }

        //        //    db.Entry(chkPointlog).State = EntityState.Modified;
        //        //    db.SaveChanges();
        //        //}
        //    }

        //    //SyncPointsFromDevices.GetUserAbsenceFast(TimeDateFrom, TimeDateTo, Branch, db.Users.Where(a => a.BranchesModelID.HasValue ?
        //    //                                                                       a.BranchesModelID == Branch : false).ToList()); // get users who are absent today
        //    //SyncPointsFromDevices.ComputeCakeHR(TimeDateFrom, TimeDateTo, Branch, db.Users.Where(a => a.BranchesModelID.HasValue ?
        //    //                                                                       a.BranchesModelID == Branch : false).ToList());
        //    return Json("done");
        //}

        ////catch (Exception tt)
        ////{
        ////    return Json("Error: " + tt.Message);
        ////}


        // POST: CheckPointLogModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        [HttpPost]
        [ModifiedAuthorize]
        [Log(actionDescription = "انشاء نقطة حضور", typeOfAction = Models.DAL.ActivityLog.ActionType.Edit)]
        [Title(Title = "انشاء نقطة حضور")]
        public async Task<ActionResult> Create(CreatePointLogModel viewCheckPointLogModels)
        {
            if (ModelState.IsValid)
            {
                // ?  :
                CheckPointLogModels checkPointLogModels = new CheckPointLogModels()
                {
                    BranchesModelID = viewCheckPointLogModels.BranchPoint,
                    userId = viewCheckPointLogModels.PointsEmp,
                    CurrentDay = viewCheckPointLogModels.PointsDate.Value,
                    CheckTypeModelsID = viewCheckPointLogModels.PointsType
                };

                if (!String.IsNullOrEmpty(viewCheckPointLogModels.PointsEnd))
                {
                    checkPointLogModels.CheckTimeOut = DateTime.Parse(viewCheckPointLogModels.PointsDate.Value.ToString("yyyy-MM-dd") + " " + viewCheckPointLogModels.PointsEnd);
                }
                if (!String.IsNullOrEmpty(viewCheckPointLogModels.PointsStart))
                {
                    checkPointLogModels.CheckTimeIn = DateTime.Parse(viewCheckPointLogModels.PointsDate.Value.ToString("yyyy-MM-dd") + " " + viewCheckPointLogModels.PointsStart);
                }

                db.CheckPointLogTable.Add(checkPointLogModels);
                await db.SaveChangesAsync();
                return Json("done");
            }

            return Json("error");
        }

        // POST: CheckPointLogModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ModifiedAuthorize]
        [Log(actionDescription = "تعديل نقاط الحضور", typeOfAction = Models.DAL.ActivityLog.ActionType.Edit)]
        [Title(Title = "تعديل نقاط الحضور")]
        public async Task<ActionResult> Edit(EditPointLogModel checkPointLogModels)
        {
            if (ModelState.IsValid)
            {
                var logPoint = db.CheckPointLogTable.Include(aa => aa.CheckPointSummeryModel).First(a => a.ID == checkPointLogModels.PointID);

                if (db.CheckTypeTable.Find(checkPointLogModels.PointType.Value).state != LogStatus.اجازة)
                {
                    DateTime yy = DateTime.Parse(logPoint.CurrentDay.ToString("yyyy-MM-dd") + " " + checkPointLogModels.PointIn);
                    DateTime yy2 = DateTime.Parse(logPoint.CurrentDay.ToString("yyyy-MM-dd") + " " + checkPointLogModels.PointOut);

                    logPoint.CheckTimeIn = logPoint.CheckTimeIn.HasValue ? DateTime.Parse(logPoint.CurrentDay.ToString("yyyy-MM-dd") + " " + checkPointLogModels.PointIn) : DateTime.Parse(logPoint.CurrentDay.ToString("yyyy-MM-dd") + " " + checkPointLogModels.PointIn);
                    logPoint.CheckTimeOut = logPoint.CheckTimeOut.HasValue ? DateTime.Parse(logPoint.CurrentDay.ToString("yyyy-MM-dd") + " " + checkPointLogModels.PointOut) : DateTime.Parse(logPoint.CurrentDay.ToString("yyyy-MM-dd") + " " + checkPointLogModels.PointOut);
                }
                else
                {
                    logPoint.lateMin = null;
                    logPoint.CheckPointSummeryModel.LateBy = null;
                }

                logPoint.CheckTypeModelsID = checkPointLogModels.PointType;
                if (logPoint.CheckPointSummeryModel != null)
                {
                    logPoint.CheckPointSummeryModel.CheckTypeModelsID = checkPointLogModels.PointType.Value;
                    db.Entry(logPoint.CheckPointSummeryModel).State = EntityState.Modified;
                }

                db.Entry(logPoint).State = EntityState.Modified;
                await db.SaveChangesAsync();

                return Json("done");
            }
            return Json("error");
        }

        // POST: CheckPointLogModels/Delete/5
        [HttpPost]
        [ModifiedAuthorize]
        [Log(actionDescription = "مسح نقطة حضور", typeOfAction = Models.DAL.ActivityLog.ActionType.Delete)]
        [Title(Title = "مسح نقطة حضور")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                CheckPointLogModels checkPointLogModels = await db.CheckPointLogTable.FindAsync(id);
                if (checkPointLogModels.CheckPointSummeryModel != null)
                {
                    db.CheckPointSummeryTable.Remove(checkPointLogModels.CheckPointSummeryModel);
                }

                db.CheckPointLogTable.Remove(checkPointLogModels);
                await db.SaveChangesAsync();
                return Json("done");
            }
            catch
            {
                return Json("error");
            }

        }

        // POST: CheckPointLogModels/Delete/5
        [HttpPost]
        [ModifiedAuthorize]
        [Log(actionDescription = "مسح جميع تقاط الحضور", typeOfAction = Models.DAL.ActivityLog.ActionType.Delete)]
        [Title(Title = "مسح جميع تقاط الحضور")]
        public ActionResult DeleteAllPoints(int branchId, DateTime TimeDateFrom, DateTime TimeDateTo)
        {
            try
            {
                foreach (var point in db.CheckPointLogTable.Where(a => a.BranchesModelID == branchId && a.CurrentDay >= TimeDateFrom && a.CurrentDay <= TimeDateTo).ToList())
                {
                    if (point.CheckPointSummeryModel != null)
                    {
                        db.CheckPointSummeryTable.Remove(point.CheckPointSummeryModel);
                    }
                    db.CheckPointLogTable.Remove(point);
                    db.SaveChanges();
                }
                return Json("done");
            }
            catch
            {
                return Json("error");
            }

        }

        [HttpPost]
        [ModifiedAuthorize]
        [Log(actionDescription = "مزامنة نقاط الاجهزة", typeOfAction = Models.DAL.ActivityLog.ActionType.Printing)]
        [Title(Title = "مزامنة نقاط الاجهزة")]
        public ActionResult SyncPoints(int deviceId, DateTime t1, DateTime t2)
        {
            try
            {
                List<DevicePointViewModel> log = new List<DevicePointViewModel>();
                var t = new Thread(() => log = SyncPointsFromDevices.CallSDKForSpecificDeviceWithTimer(deviceId, t1, t2));
                t.IsBackground = true;
                t.Priority = ThreadPriority.Highest;
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                t.Join();

                if (log.Count > 0)
                {
                    SyncPointsFromDevices.SendToDBFast(log);
                    SyncPointsFromDevices.GetUserAbsenceFast(t1, t2, deviceId, db.Users.Where(a => a.BranchesModelID == deviceId && a.SchedulesModelID.HasValue).ToList());
                    SyncPointsFromDevices.ComputeCakeHR(t1, t2, deviceId, db.Users.Where(a => a.BranchesModelID == deviceId && a.SchedulesModelID.HasValue).ToList());
                }

                return Json("done", JsonRequestBehavior.AllowGet);
            }
            catch (Exception tt)
            {
                return Json("error: " + tt.Message);
            }
        }

        public ActionResult SyncPointsWithTimer()
        {
            var branches = new List<BranchesModel>();
            branches = db.BranchesTable.ToList();

            foreach (var branch in branches)
            {
                try
                {
                    getCheckPointsFromDevice(branch.ID);
                }
                catch (Exception kk)
                {
                    string error = "";
                }
            }

            return Json("Done", JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        private bool getCheckPointsFromDevice(int deviceId)
        {
            DateTime t1 = System.DateTime.Now.Date;
            DateTime t2 = System.DateTime.Now.Date.AddHours(23);

            try
            {
                List<DevicePointViewModel> log = new List<DevicePointViewModel>();
                var t = new Thread(() => log = SyncPointsFromDevices.CallSDKForSpecificDeviceWithTimer(deviceId, t1, t2));
                t.IsBackground = true;
                t.Priority = ThreadPriority.Highest;
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                t.Join();

                if (log.Count > 0)
                {
                    SyncPointsFromDevices.SendToDBFast(log);
                    SyncPointsFromDevices.GetUserAbsenceFast(t1, t2, deviceId, db.Users.Where(a => a.BranchesModelID == deviceId && a.SchedulesModelID.HasValue).ToList());
                    SyncPointsFromDevices.ComputeCakeHR(t1, t2, deviceId, db.Users.Where(a => a.BranchesModelID == deviceId && a.SchedulesModelID.HasValue).ToList());
                }

                return true;
            }
            catch (Exception tt)
            {
                return false;
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
