﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.SideBarItems;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;
using JqueryDTWithMVC.Models.ViewModels.SideBar;
using static JqueryDTWithMVC.Startup;
using System.Reflection;
using JqueryDTWithMVC.Helper;
using JqueryDTWithMVC.Models.ViewModels.BreadCrambAndTitle;

namespace JqueryDTWithMVC.Controllers
{
    [AllowAnonymous]
    public class SideBarItemsModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: SideBarItemsModels
        [Title(Title = " جدول الشريط الجانبي")]
        [ModifiedAuthorize]
        public ActionResult Index()
        {
            return View(db.SideBarItemsTable.ToList());
        }

        // GET: SideBarItemsModels/Details/5
        [Title(Title = " تفاصيل  عنصر في الشريط الجانبي")]
        [ModifiedAuthorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SideBarItemsModels sideBarItemsModels = db.SideBarItemsTable.Find(id);
            if (sideBarItemsModels == null)
            {
                return HttpNotFound();
            }
            return View(sideBarItemsModels);
        }

        // GET: SideBarItemsModels/Create
        [Title(Title = " انشاء  عنصر في شريط الجانبي")]
        [ModifiedAuthorize]
        public ActionResult Create()
        {
            ViewBag.AccessedByRoles = new SelectList(db.Roles, "Name", "Name");
            return View();
        }

        // POST: SideBarItemsModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        [Log(actionDescription = "اضافة عنصر للشريط الجانبي", typeOfAction = Models.DAL.ActivityLog.ActionType.Create)]
        public ActionResult Create([Bind(Include = "Id,ControllerName,ActionMethodName,Description,selectedIcon,DescriptionEng,AccessedByRoles")] SideBarItemsModels sideBarItemsModels
            )
        {
            if (ModelState.IsValid)
            {
                sideBarItemsModels.ControllerName = sideBarItemsModels.ControllerName.Replace("string:", "");
                sideBarItemsModels.ControllerName = sideBarItemsModels.ControllerName.Replace("Controller", "");
                sideBarItemsModels.ActionMethodName = sideBarItemsModels.ActionMethodName.Replace("string:", "");
                sideBarItemsModels.ActionMethodName = "/" + sideBarItemsModels.ControllerName + "/" + sideBarItemsModels.ActionMethodName;

                db.SideBarItemsTable.Add(sideBarItemsModels);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            ViewBag.AccessedByRoles = new SelectList(db.Roles, "Name", "Name");
            return View(sideBarItemsModels);
        }

        // GET: SideBarItemsModels/Edit/5
        [Title(Title = " تعديل  عنصر في الشريط الجانبي")]
        [ModifiedAuthorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SideBarItemsModels sideBarItemsModels = db.SideBarItemsTable.Find(id);
            if (sideBarItemsModels == null)
            {
                return HttpNotFound();
            }

            ViewBag.AccessedByRoles = new SelectList(db.Roles, "Name", "Name");
            return View(sideBarItemsModels);
        }

        // POST: SideBarItemsModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        [Log(actionDescription = "نعديل عنصر في الشريط الجانبي", typeOfAction = Models.DAL.ActivityLog.ActionType.Edit)]
        public ActionResult Edit([Bind(Include = "Id,ControllerName,ActionMethodName,selectedIcon,Description,DescriptionEng,AccessedByRoles")] SideBarItemsModels sideBarItemsModels)
        {
            if (ModelState.IsValid)
            {
                sideBarItemsModels.ControllerName = sideBarItemsModels.ControllerName.Replace("string:", "");
                sideBarItemsModels.ControllerName = sideBarItemsModels.ControllerName.Replace("Controller", "");
                sideBarItemsModels.ActionMethodName = sideBarItemsModels.ActionMethodName.Replace("string:", "");
                sideBarItemsModels.ActionMethodName = "/" + sideBarItemsModels.ControllerName + "/" + sideBarItemsModels.ActionMethodName;

                db.Entry(sideBarItemsModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sideBarItemsModels);
        }

        // GET: SideBarItemsModels/Delete/5
        [Title(Title = " مسح  عنصر في الشريط الجانبي")]
        [ModifiedAuthorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SideBarItemsModels sideBarItemsModels = db.SideBarItemsTable.Find(id);
            if (sideBarItemsModels == null)
            {
                return HttpNotFound();
            }
            return View(sideBarItemsModels);
        }

        [HttpGet]
        [Authorize]
        public ContentResult FetchSideBarItems()
        {
            string roles = string.Join(",", UserManager.GetRolesAsync(User.Identity.GetUserId()).Result);

            var listSideBar = db.SideBarItemsTable.Where(a => roles.Contains(a.AccessedByRoles) || a.AccessedByRoles == "" || a.AccessedByRoles == null).Select(a => new
            SideBarViewModel()
            {
                AccessedByRoles = a.AccessedByRoles,
                ActionMethodName = a.ActionMethodName,
                ControllerName = a.ControllerName,
                Description = a.Description,
                DescriptionEng = a.DescriptionEng,
                selectedIcon = a.selectedIcon
            }
            );

            var list = JsonConvert.SerializeObject(listSideBar, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            return Content(list, "application/json");
        }

        [HttpGet]
        public ContentResult FetchBreadCrambAndTitle(string con, string act)
        {
            try
            {
                con = con + "Controller";
                Assembly assembly = Assembly.GetAssembly(typeof(JqueryDTWithMVC.MvcApplication));
                IEnumerable<Type> types = assembly.GetTypes().Where(type => typeof(Controller).IsAssignableFrom(type)).OrderBy(x => x.Name);

                // first controller matching name and Title attribute get his name
                var attribute = types.FirstOrDefault(a => a.Name == con).GetCustomAttributes(typeof(Startup.TitleAttribute), true)
                     .Cast<Startup.TitleAttribute>()
                     .FirstOrDefault();


                var memberInfo = types.First(a => a.Name == con).GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public).
                    Where(m => !m.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true).Any()).OrderBy(x => x.Name)
                    .First(a => a.Name == act).GetCustomAttributes(typeof(Startup.TitleAttribute), true)
                                            .Cast<Startup.TitleAttribute>()
                                            .FirstOrDefault();

                BreadCrambAndTitleViewModel model = new BreadCrambAndTitleViewModel()
                {
                    actionName = memberInfo.Title,
                    controllerName = attribute.Title
                };

                var list = JsonConvert.SerializeObject(model,
                            Formatting.None, new JsonSerializerSettings()
                            {
                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                            });
                return Content(list, "application/json");

            }
            catch (Exception e)
            {
                var list = JsonConvert.SerializeObject(new List<string>(),
                            Formatting.None, new JsonSerializerSettings()
                            {
                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                            });
                return Content(list, "application/json");
            }

        }

        // POST: SideBarItemsModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Title(Title = " تاكيد مسح عنصر في الشريط الجانبي")]
        [ModifiedAuthorize]
        [Log(actionDescription = "مسح عنصر في الشريط الجانبي", typeOfAction = Models.DAL.ActivityLog.ActionType.Delete)]
        public ActionResult DeleteConfirmed(int id)
        {
            SideBarItemsModels sideBarItemsModels = db.SideBarItemsTable.Find(id);
            db.SideBarItemsTable.Remove(sideBarItemsModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ContentResult FetchControllersNames()
        {
            var Plist = JsonConvert.SerializeObject(HelperClasses.GetControllerNames(), Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            return Content(Plist, "application/json");
        }

        [HttpPost]
        public ContentResult FetchActionMethod(string controllerName)
        {
            List<string> list = new List<string>();
            Assembly assembly = Assembly.GetAssembly(typeof(JqueryDTWithMVC.MvcApplication));
            IEnumerable<Type> types = assembly.GetTypes().Where(type => typeof(Controller).IsAssignableFrom(type)).OrderBy(x => x.Name);
            foreach (Type cls in types.Where(a => a.Name == controllerName))
            {
                IEnumerable<MemberInfo> memberInfo = cls.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public).Where(m => !m.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true).Any()).OrderBy(x => x.Name);
                foreach (MemberInfo method in memberInfo)
                {
                    if (method.ReflectedType.IsPublic && !method.IsDefined(typeof(NonActionAttribute)))
                    {
                        list.Add(method.Name.ToString());
                    }
                }
            }

            var Plist = JsonConvert.SerializeObject(list, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            return Content(Plist, "application/json");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
