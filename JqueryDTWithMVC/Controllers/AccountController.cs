﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.ViewModels.User;
using System.Data.Entity;
using JqueryDTWithMVC.Helper;
using JqueryDTWithMVC.Models.ViewModels.Roles;
using System.Threading;
using System.Collections.Generic;
using JqueryDTWithMVC.Models.ViewModels.UserInfoFromDevice;
using JqueryDTWithMVC.Models.Repository.UnitOfWork;
using JqueryDTWithMVC.Models.ViewModels.EmployeeInfo;
using JqueryDTWithMVC.Models.ViewModels.Personnal;
using static JqueryDTWithMVC.Startup;

namespace JqueryDTWithMVC.Controllers
{
    [Startup.Title(Title = "ادارة الحسابات")]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        UnitOfWork unit = new UnitOfWork(new ApplicationDbContext());

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                LoginViewModel login = new LoginViewModel()
                {
                    siteName = db.SettingsTable.First().Value,
                    siteLog = "/Content/css/" + db.SettingsTable.First(a => a.Key == "LOGO").Value
                };
                return View(login);
            }

        }

        [Startup.Title(Title = "Register New Employee (HR)")]
        public ActionResult RegisterFromHRSystem()
        {
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                model.siteName = db.SettingsTable.FirstOrDefault().Value;
                model.siteLog = "/Content/css/" + db.SettingsTable.FirstOrDefault(a => a.Key == "LOGO").Value;
            }

            string userName = model.Email;
            if (UserManager.FindByEmail(userName) != null)
            {
                var user = UserManager.FindByEmail(userName);
                if (user != null)
                {
                    userName = user.UserName;
                }
                else
                {
                    ModelState.AddModelError("", "خطأ اثناء تسجيل الدخول .... تاكد من معلومات حسابك");
                    return View(model);
                }
            }


            var result = await SignInManager.PasswordSignInAsync(userName, model.Password, model.RememberMe, shouldLockout: false);

            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new
                    {
                        ReturnUrl = returnUrl,
                        RememberMe = model.RememberMe
                    });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "خطأ اثناء تسجيل الدخول .... تاكد من معلومات حسابك");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [Startup.ModifiedAuthorize]
        [Startup.Title(Title = "تسجيل مستخدم جديد")]
        public ActionResult Register()
        {
            return View();
        }


        [Startup.Title(Title = "Employee Directory")]
        [Startup.ModifiedAuthorize]
        public ActionResult Directory()
        {
            
            {
                string userId = User.Identity.GetUserId();

                int? branchesModelID = unit.Users.SingleOrDefault(a => a.Id == userId).BranchesModelID;
                var empInfo = unit.Users.UserToEmployeeInfo(userId);

                return View(empInfo);
            }
        }

        [Authorize]
        public ActionResult GetAllBranchEmployees()
        {
            
            {
                try
                {
                    int bInt = unit.Branches.getUserBranch(User.Identity.GetUserId());
                    var empInfo = unit.Users.Find(aa => aa.BranchesModelID.HasValue ? aa.BranchesModelID.Value == bInt : false).ToList().Select(aa => new EmployeeInfoViewModelJson()
                    {
                        dateEnrollDay = aa.dateEnrollDay.HasValue ? aa.dateEnrollDay.Value.ToString("yyyy-MM-dd") : "Unknown",
                        Name = aa.UserName,
                        position = aa.TeamsModelID != null ? unit.Teams.SingleOrDefault(bb => bb.ID == aa.TeamsModelID.Value).teamName : "Unknown",
                        userId = aa.ApplicationUserId,
                        away = unit.TimeOffs.GetAll().ToList().Any(bb => bb.fromDate.Date == System.DateTime.Now.Date) ? "true" : "false"
                    });

                    return Json(empInfo.ToList());
                }
                catch (Exception ll)
                {
                    return Json(new List<EmployeeInfoViewModelJson>());
                }

            }
        }

        [Authorize]
        public ActionResult Employee(string userId)
        {
            
            {
                if (String.IsNullOrEmpty(userId))
                    userId = User.Identity.GetUserId();

                ViewBag.userId = userId;
                int? branchesModelID = unit.Users.SingleOrDefault(a => a.Id == userId).BranchesModelID;
                var empInfo = unit.Users.UserToEmployeeInfo(userId);

                ViewBag.TitleModelID = new SelectList(unit.Title.GetAll(), "ID", "title", empInfo.TitleModelID);
                ViewBag.PositionModelID = new SelectList(unit.Position.GetAll(), "ID", "title", empInfo.PositionModelID);
                ViewBag.TeamsModelID = new SelectList(unit.Teams.GetAll().Where(aa => aa.BranchesModelID.HasValue ? aa.BranchesModelID == branchesModelID : false), "ID", "teamName", empInfo.TeamsModelID);
                ViewBag.ApplicationUserId = new SelectList(unit.Users.GetAll().Where(aa => aa.BranchesModelID.HasValue ? aa.BranchesModelID == branchesModelID : false), "Id", "EmployeeName", empInfo.ApplicationUserId);
                ViewBag.EmploymentStateModelID = new SelectList(unit.EmploymentStatus.GetAll(), "ID", "title", empInfo.PositionModelID);

                return View(empInfo);
            }
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Employee([Bind(Include = "userId,TitleModelID,firstName,lastName,PositionModelID,dateEnrollDay,ApplicationUserId,TeamsModelID,EmploymentStateModelID,drivingLicense,linkedln,skype,workEmail,image")] EmployeeInfoViewModel empInfo)
        {
            
            {
                if (ModelState.IsValid)
                {
                    unit.Users.UpdateUserEmployeeInformations(empInfo);
                    unit.Complete();

                    Helper.FileStorageClass.StoreImage(false, empInfo.image, empInfo.userId, Server.MapPath("~/Content/SiteImage/UsersImage/"));
                }

                int? branchesModelID = unit.Users.SingleOrDefault(a => a.Id == empInfo.userId).BranchesModelID;

                ViewBag.TitleModelID = new SelectList(unit.Title.GetAll(), "ID", "title", empInfo.TitleModelID);
                ViewBag.PositionModelID = new SelectList(unit.Position.GetAll(), "ID", "title", empInfo.PositionModelID);
                ViewBag.TeamsModelID = new SelectList(unit.Teams.GetAll().Where(aa => aa.BranchesModelID.HasValue ? aa.BranchesModelID == branchesModelID : false), "ID", "teamName", empInfo.TeamsModelID);
                ViewBag.ApplicationUserId = new SelectList(unit.Users.GetAll().Where(aa => aa.BranchesModelID.HasValue ? aa.BranchesModelID == branchesModelID : false), "Id", "EmployeeName", empInfo.ApplicationUserId);
                ViewBag.EmploymentStateModelID = new SelectList(unit.EmploymentStatus.GetAll(), "ID", "title", empInfo.PositionModelID);

                ViewBag.userId = empInfo.userId;
                return View(empInfo);
            }
        }

        [ChildActionOnly]
        [Authorize]
        [Title(Title ="اضافة موظف جديد من نظام ال HR")]
        public ActionResult RegisterNewEmployeeHR()
        {
            return PartialView();
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> RegisterNewEmployeeHR([Bind(Include = "email,name,password,enrolledOn,file")] RegisterEmployeeHR model, string returnURL)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ApplicationUser user = new ApplicationUser()
                    {
                        UserName = model.name,
                        EmployeeName = model.name,
                        EmployeeName_En = model.name,
                        Email = model.email,
                        dateEnrollDay = model.enrolledOn,
                    };

                    var result = await UserManager.CreateAsync(user, model.password);

                    if (result.Succeeded)
                    {
                        UserManager.AddToRole(user.Id, "موظف");
                        var res = Helper.FileStorageClass.StoreImage(false, model.file, user.ApplicationUserId, Server.MapPath("~/Content/SiteImage/UsersImage/"));
                        Startup.MiscellaneousMethod.ActionCompletionNotify("Employee Added Successfully");
                        return Redirect(returnURL);
                    }
                }
            }
            catch(Exception ll)
            {
                string lgl = "";
            }


            return Redirect(Request.UrlReferrer.ToString());
        }


        [Startup.ModifiedAuthorize]
        public ActionResult Personnal(string userId)
        {
            
            {
                if (String.IsNullOrEmpty(userId))
                    userId = User.Identity.GetUserId();

                var empInfo = unit.Users.UserToPersonnalInfo(userId);
              
                ViewBag.CountryModelID = new SelectList(unit.Country.GetAll(), "ID", "countryName", empInfo.CountryModelID);
                ViewBag.NationalityModelID = new SelectList(unit.Nationality.GetAll(), "ID", "nation", empInfo.NationalityModelID);
                ViewBag.MaritalStatusModelID = new SelectList(unit.MartialStatus.GetAll(), "ID", "status", empInfo.MaritalStatusModelID);
                ViewBag.userId = userId;

                return View(empInfo);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Personnal([Bind(Include = "userId,dateOfBirth,street_1,street_2 ,city,state,postCode,CountryModelID,NationalityModelID,MaritalStatusModelID,homePhone,workPhone,EmployeeGender")] PersonnalViewModel empInfo)
        {
            
            {
                if (ModelState.IsValid)
                {
                    unit.Users.UpdateUserPersonnalInformations(empInfo);
                    unit.Complete();
                }

                ViewBag.CountryModelID = new SelectList(unit.Country.GetAll(), "ID", "countryName", empInfo.CountryModelID);
                ViewBag.NationalityModelID = new SelectList(unit.Nationality.GetAll(), "ID", "nation", empInfo.NationalityModelID);
                ViewBag.MaritalStatusModelID = new SelectList(unit.MartialStatus.GetAll(), "ID", "status", empInfo.MaritalStatusModelID);
                ViewBag.userId = empInfo.userId; 

                return View(empInfo);
            }
        }

        [ChildActionOnly]
        public ActionResult UserProfileList(string userId)
        {
            
                return PartialView(unit.Users.SingleOrDefault(aa => aa.Id == userId));
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [Startup.ModifiedAuthorize]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            try
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    if (db.Users.Any(a => a.EnrollNumber == model.EmpFP))
                    {
                        return Json("fp");
                    }
                    else if (db.Users.Any(a => a.EnrollNumber == model.EmpFP))
                    {
                        return Json("em");
                    }
                }

                if (ModelState.IsValid)
                {
                    var user = new ApplicationUser
                    {
                        UserName = model.EmpArName,
                        Email = model.EmpEmail,
                        EmployeeGender = (Gender)model.EmpGender,
                        EmployeeBirthDate = model.EmpBirth,
                        BranchesModelID = model.EmpBranch,
                        DepartmentsModelID = (model.EmpDepart > 0) ? model.EmpDepart : (int?)null,
                        EmployeeName = model.EmpArName,
                        EmployeeName_En = model.EmpEnName,
                        EnrollNumber = model.EmpFP,
                        SchedulesModelID = model.EmpSchedule,
                        userCakeHRNumber = model.EmpcakeHRID
                    };
                    var result = await UserManager.CreateAsync(user, model.password);
                    if (result.Succeeded)
                    {
                        UserManager.AddToRole(user.Id, model.role);
                        //await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);              
                        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                        // Send an email with this link
                        // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                        return Json("done");
                    }
                }

                // If we got this far, something failed, redisplay form
                return Json("failed");
            }
            catch (Exception rr)
            {
                return Json("failed: " + rr.Message);
            }

        }

        [HttpPost]
        [Startup.ModifiedAuthorize]
        [Startup.Title(Title = "احضار جميع المستخدمين")]
        public ActionResult GetAllUsers(string action, int BranchID, int draw = 0, int start = 0, int length = 10)
        {
            try
            {
                JqueryDTWithMVC.Helper.DataTableModels<UserViewModel>._data = UserManager.Users.Include("relatedSchedule")
                    .Include("relatedDepartment")
                    .Include("relatedBranch").Where(a => a.BranchesModelID == BranchID && a.EmployeeShow == true).ToList().Select(a => new UserViewModel()
                    {
                        DT_RowId = a.Id,
                        EmployeeAName = a.EmployeeName,
                        EmployeeEName = a.EmployeeName_En,
                        EmployeeBirthDate = a.EmployeeBirthDate,
                        branch_name = a.BranchesModelID.HasValue ? a.relatedBranch.branch_name : "غير متوفر",
                        depart_name = a.DepartmentsModelID.HasValue ? a.relatedDepartment.depName : "غير متوفر",
                        EmployeeEmail = a.Email,
                        EmployeeEnable = a.LockoutEndDateUtc.HasValue ? "0" : "1",
                        EmployeeLastLogin = a.EmployeeLastLogin.HasValue ? a.EmployeeLastLogin.Value.ToString("yyyy-MM-dd HH:mm:ss") : "غير متوفر",
                        EmployeeGender = a.EmployeeGender.HasValue ? (int)a.EmployeeGender.Value + 1 : 0 + 1,
                        EmployeeID = a.EnrollNumber.HasValue ? a.EnrollNumber.Value.ToString() : "0",
                        EmployeeIsManager = UserManager.GetRoles(a.Id).Count > 0 ? UserManager.GetRoles(a.Id).Any(b => b != "موظف") ? "0" : "1" : "1",
                        schedule_name = a.relatedSchedule.schedule_name,
                        Id = a.Id,
                        branch_id = a.BranchesModelID.HasValue ? a.BranchesModelID.Value : 0,
                        EmployeeDepart = a.DepartmentsModelID.HasValue ? a.DepartmentsModelID.Value : 0,
                        EmployeeShow = a.EmployeeShow ? 1 : 0,
                        roleId = a.Roles.FirstOrDefault() != null ? a.Roles.First().RoleId : "",
                        roleName = UserManager.GetRoles(a.Id).Count > 0 ? UserManager.GetRoles(a.Id).FirstOrDefault() : "",
                        EmpcakeHRID = a.userCakeHRNumber
                    }).ToList();

                DataTableData<UserViewModel> dataTableData = new DataTableData<UserViewModel>();
                dataTableData.draw = draw;
                dataTableData.recordsTotal = DataTableModels<UserViewModel>._data.Count;
                int recordsFiltered = 0;
                dataTableData.data = JqueryDTWithMVC.Helper.DataTableModels<UserViewModel>._data; /*DataTableModels<UserViewModel>.FilterData(ref recordsFiltered, start, length, search, sortColumn, sortDirection);*/
                dataTableData.recordsFiltered = recordsFiltered;

                return Json(dataTableData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception rr)
            {
                return Json(rr.Message);
            }
        }

        [HttpPost]
        [Startup.ModifiedAuthorize]
        [Startup.Title(Title = "مزامنة الموطفين")]
        public ActionResult SyncAllUsers(int BranchID)
        {
            try
            {
                List<UserInfoFromDeviceViewModel> log = new List<UserInfoFromDeviceViewModel>();
                var t = new Thread(() => log = SyncPointsFromDevices.CallSDKForUserSync(BranchID));
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                t.Join();
                if (log.Count > 0)
                {
                    SyncPointsFromDevices.RegisterFromDevice(log);
                }

                return Json("done", JsonRequestBehavior.AllowGet);
            }
            catch (Exception rr)
            {
                return Json(rr.Message);
            }
        }


        // khalid old program wrapper
        [HttpPost]
        [Startup.ModifiedAuthorize]
        [Startup.Title(Title = "تعديل معلومات مستخدم")]
        public ActionResult EditUser(EditUserViewModel userData)
        {
            try
            {
                if (UserManager.Users.Any(a => a.Email == userData.EmpEmail && userData.EmpID != a.Id))
                {
                    return Json("email");
                }
                else if (UserManager.Users.Any(a => (a.EnrollNumber.HasValue ? a.EnrollNumber.Value == userData.EmpFP : false) && userData.EmpID != a.Id && userData.Branch == a.BranchesModelID))
                {
                    return Json("fp");
                }

                ApplicationUser user = UserManager.FindById(userData.EmpID);
                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    user.Id = userData.EmpID;
                    user.EnrollNumber = userData.EmpFP;

                    if (db.DepartmentsTable.Any(a => a.ID == userData.EmpDepart))
                    {
                        user.DepartmentsModelID = userData.EmpDepart;
                    }
                    user.BranchesModelID = userData.Branch;
                    if (userData.EmpGender == 1 || userData.EmpGender == 2)
                        user.EmployeeGender = (Gender)(userData.EmpGender - 1);

                    user.Email = userData.EmpEmail;
                    user.EmployeeBirthDate = userData.EmpBirth.ToString("yyyy-MM-dd");
                    user.EmployeeName = userData.EmpArName;
                    user.EmployeeName_En = userData.EmpEnName;
                    user.UserName = userData.EmpEmail;
                    user.userCakeHRNumber = userData.EmpcakeHRID;
                    if (userData.EmployeeShow == 0 || userData.EmployeeShow == 1)
                        user.EmployeeShow = Convert.ToBoolean(userData.EmployeeShow);

                    if (!String.IsNullOrEmpty(userData.password))
                    {
                        user.PasswordHash = UserManager.PasswordHasher.HashPassword(userData.password);
                    }

                    var result = UserManager.Update(user);
                    if (!result.Succeeded)
                    {
                        return Json("error:  updating password");
                    }
                }

                if (!UserManager.IsInRole(user.Id, userData.role))
                {
                    UserManager.RemoveFromRole(user.Id, userData.role);
                    UserManager.AddToRole(user.Id, userData.role);
                }
                switch (userData.EmpEnable)
                {
                    case 1: // Enable the user
                        {
                            var t = user.LockoutEndDateUtc.HasValue ? BlockOpenUserAccount(user.Id).Result : false;
                            break;
                        }
                    case 0: // Block the user
                        {
                            var t = user.LockoutEndDateUtc == null ? BlockOpenUserAccount(user.Id).Result : false;
                            break;
                        }
                }

                return Json("done");
            }
            catch (Exception e)
            {
                return Json("error");
            }
        }


        [Startup.ModifiedAuthorize]
        [Startup.Title(Title = "مسح مستخدم من جهاز البصمة")]
        public ActionResult DeleteUserFromDevice(string action, string EmpID)
        {
            try
            {
                bool log = false;
                if (UserManager.FindById(EmpID) == null)
                    return Json("Error: No User exist");

                var t = new Thread(() => log = SyncPointsFromDevices.DeleteUserFromDevice(UserManager.FindById(EmpID)));
                t.IsBackground = true;
                t.Priority = ThreadPriority.Highest;
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                t.Join();
                
                using(UnitOfWork unit = new UnitOfWork(new ApplicationDbContext()))
                {
                    unit.Users.Remove(UserManager.FindById(EmpID));
                    unit.Complete();
                }

                return Json("done");

            }
            catch (Exception ee)
            {
                return Json("Error: " + ee.Message);
            }
        }

        [Startup.ModifiedAuthorize]
        [Startup.Title(Title = "مسح مستخدم")]
        public ActionResult DeleteUser(string action, string EmpID)
        {
            try
            {
                if (UserManager.FindById(EmpID) == null)
                    return Json("Error: No User exist");

                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    var user = UserManager.FindById(EmpID);
                    user.EmployeeShow = false;
                    UserManager.Update(user);
                    return Json("done");
                }
            }
            catch (Exception ee)
            {
                return Json("Error: " + ee.Message);
            }

        }

        [Startup.ModifiedAuthorize]
        [Startup.Title(Title = "ايقاف حساب مستخدم")]
        public async Task<bool> BlockOpenUserAccount(string id)
        {
            try
            {
                var user = UserManager.FindById(id);
                if (UserManager.GetLockoutEndDate(id).DateTime > System.DateTime.Now) // user is locked, unlock it
                {
                    var result = await UserManager.SetLockoutEnabledAsync(user.Id, false);
                    user.LockoutEndDateUtc = null;
                    UserManager.Update(user);

                    if (result.Succeeded)
                    {
                        await UserManager.ResetAccessFailedCountAsync(user.Id);
                    }
                }
                else // user unlocked - lock it
                {
                    int forDays = 10000;
                    var result = await UserManager.SetLockoutEnabledAsync(user.Id, true);
                    if (result.Succeeded)
                    {
                        result = await UserManager.SetLockoutEndDateAsync(user.Id, DateTimeOffset.UtcNow.AddDays(forDays));
                        UserManager.Update(user);
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        [Authorize]
        [Startup.Title(Title = "تغيير كلمة السر")]
        public ActionResult ChangePassword(string PasswordOld, string PasswordNew)
        {
            try
            {
                string id = User.Identity.GetUserId();
                var result = UserManager.ChangePassword(id, PasswordOld, PasswordNew);

                if (!result.Succeeded)
                {
                    return Json("old");
                }

                return Json("done");
            }
            catch
            {
                return Json("error");
            }
        }

        [Startup.Title(Title = "قائمة الصلاحيات")]
        public ActionResult ListRoles(string userId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                try
                {
                    if (String.IsNullOrEmpty(userId))
                    {
                        var res = db.Roles.Select(a => new RolesViewModelWithId()
                        {
                            Id = a.Id,
                            roleName = a.Name,
                            seleID = ""
                        }).ToList();
                        return Json(res, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        string role = UserManager.GetRoles(userId).FirstOrDefault();
                        var res = db.Roles.Include(aa => aa.Users).Select(a => new RolesViewModelWithId()
                        {
                            Id = a.Id,
                            roleName = a.Name,
                            seleID = role
                        }).ToList();
                        return Json(res, JsonRequestBehavior.AllowGet);
                    }

                }
                catch
                {
                    return Json("error");
                }
            }

        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null/* || !(await UserManager.IsEmailConfirmedAsync(user.Id))*/)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                return RedirectToAction("ResetPassword", "Account", new { userId = user.Id, code = code });
                //       await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                //     return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return Redirect(Request.UrlReferrer.ToString());
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}