﻿using JqueryDTWithMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BioBridgeSDK;
using BioBridgeSDKLib;
using static JqueryDTWithMVC.Startup;
using JqueryDTWithMVC.Models.Repository.UnitOfWork;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;
using JqueryDTWithMVC.Models.ViewModels.UserNotification;
using System.Reflection;
using JqueryDTWithMVC.Models.ViewModels.BreadCrambAndTitle;
using JqueryDTWithMVC.Models.ViewModels.TimeOffBalance;
using System.Globalization;
using JqueryDTWithMVC.Models.ViewModels.Vacation;
using JqueryDTWithMVC.Models.ViewModels.ActionCompletion;

namespace JqueryDTWithMVC.Controllers
{
    static class DateTimeExtensions
    {
        static GregorianCalendar _gc = new GregorianCalendar();
        public static int GetWeekOfMonth(this DateTime time)
        {
            DateTime first = new DateTime(time.Year, time.Month, 1);
            return time.GetWeekOfYear() - first.GetWeekOfYear() + 1;
        }

        static int GetWeekOfYear(this DateTime time)
        {
            return _gc.GetWeekOfYear(time, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
        }
    }

    [Title(Title = "الرئيسية")]
    public class HomeController : Controller
    {
        UnitOfWork unitOfWork = new UnitOfWork(new ApplicationDbContext());

        [ModifiedAuthorize]
        [Title(Title = "الصفحة الرئيسية")]
        public ActionResult Index()
        {
            return View();
        }

        [ModifiedAuthorize]
        [Title(Title = "HR Dashboard Page")]
        public ActionResult MainPage()
        {
            string userId = User.Identity.GetUserId();
            int bId = unitOfWork.Branches.getUserBranch(userId);
            var roles = User.IsInRole("الموارد البشرية");

            if (User.IsInRole("مدير النظام") || User.IsInRole("الموارد البشرية"))
            {
                var list = unitOfWork.TimeOffs.GetAll().Where(a => a.approvalstate == Models.DAL.Vacation.ReqState.pending && (a.user.BranchesModelID.HasValue ? a.user.BranchesModelID.Value == bId : false));
                return View(list.ToList());
            }
            else
            {

                var list = unitOfWork.TimeOffs.GetVactionsAllWithInclude().Where(a => a.approvalstate == Models.DAL.Vacation.ReqState.pending && (a.ApplicationUserId == userId || a.user.relatedTeam.leadersList.Any(aaa => aaa.userId == userId))
                                && a.fromDate.Year == System.DateTime.Now.Year);
                return View(list.ToList());
            }

        }

        [ChildActionOnly]
        [AllowAnonymous]
        public ActionResult SideBarItems()
        {
            return PartialView();
        }

        [Authorize]
        public ActionResult FetchNotification(int? lastID)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var usr = User.Identity.GetUserId();
                var x = db.NotificationsTable.OrderByDescending(a => a.eventDate).Where(a => a.ApplicationUserID == usr && a.shown == false && a.ID > lastID);
                int len = x.Count();

                var ll = x.Take(8).Select(a => new UserNotificationViewModel
                {
                    id = a.ID,
                    description = a.eventDescription,
                    url = a.eventURL,
                    eventDate = a.eventDate,
                    shown = a.shown,
                    madeBy = a.madeById,
                    countNotshown = len
                });

                return Json(ll.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public ActionResult ChangeNotificationsState(int? lastID)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                bool result = false;
                if (lastID != null)
                {
                    var userId = User.Identity.GetUserId();
                    foreach (var x in db.NotificationsTable.Where(a => a.ApplicationUserID == userId && a.ID <= lastID).ToList())
                    {
                        x.shown = true;
                        db.Entry(x).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    result = true;
                }

                return Json(result);
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult FetchCompletedActions()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var userId = User.Identity.GetUserId();
                string action = "";

                if (db.ActionCompletionTable.Any(a => a.ApplicationUserID == userId && a.shown == false))
                {
                    var x = db.ActionCompletionTable.FirstOrDefault
                        (a => a.ApplicationUserID == userId && a.shown == false);

                    action = x.eventDescription;
                    x.shown = true;
                    db.Entry(x).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }

                return Json(action, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public ContentResult FetchBreadCrambAndTitle(string con, string act)
        {
            try
            {
                con = con + "Controller";
                Assembly assembly = Assembly.GetAssembly(typeof(JqueryDTWithMVC.MvcApplication));
                IEnumerable<Type> types = assembly.GetTypes().Where(type => typeof(Controller).IsAssignableFrom(type)).OrderBy(x => x.Name);

                // first controller matching name and Title attribute get his name
                var attribute = types.FirstOrDefault(a => a.Name == con).GetCustomAttributes(typeof(Startup.TitleAttribute), true)
                     .Cast<Startup.TitleAttribute>()
                     .FirstOrDefault();


                var memberInfo = types.First(a => a.Name == con).GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public).
                    Where(m => !m.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true).Any()).OrderBy(x => x.Name)
                    .First(a => a.Name == act).GetCustomAttributes(typeof(Startup.TitleAttribute), true)
                                            .Cast<Startup.TitleAttribute>()
                                            .FirstOrDefault();

                BreadCrambAndTitleViewModel model = new BreadCrambAndTitleViewModel()
                {
                    actionName = memberInfo.Title,
                    controllerName = attribute.Title
                };

                var list = JsonConvert.SerializeObject(model,
                            Formatting.None, new JsonSerializerSettings()
                            {
                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                            });
                return Content(list, "application/json");

            }
            catch (Exception e)
            {
                var list = JsonConvert.SerializeObject(new List<string>(),
                            Formatting.None, new JsonSerializerSettings()
                            {
                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                            });
                return Content(list, "application/json");
            }

        }

        [Authorize]
        [HttpGet]
        public ActionResult EmployeeStatus()
        {
            var userId = User.Identity.GetUserId();
            using (var unitOfWork = new UnitOfWork(new ApplicationDbContext()))
            {
                var res = new
                {
                    HoursAbsents = 0,
                    TimeOffApproved = unitOfWork.TimeOffs.GetAll().Where(a => a.ApplicationUserId == userId && a.approvalstate == Models.DAL.Vacation.ReqState.approved).Count(),
                    TimeOffWaiting = unitOfWork.TimeOffs.GetAll().Where(a => a.ApplicationUserId == userId && a.approvalstate == Models.DAL.Vacation.ReqState.pending).Count(),
                    TimeOffRejected = unitOfWork.TimeOffs.GetAll().Where(a => a.ApplicationUserId == userId && a.approvalstate == Models.DAL.Vacation.ReqState.declined).Count()
                };

                return Json(res, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult EmployeeTimeOffStatus()
        {
            var userId = User.Identity.GetUserId();
            List<TimeOffBalanceViewModel> res = new List<TimeOffBalanceViewModel>();
            using (var unitOfWork = new UnitOfWork(new ApplicationDbContext()))
            {
                int currentYear = System.DateTime.Now.Year;
                int currentMonth = System.DateTime.Now.Month;
                int weekNumber = System.DateTime.Now.GetWeekOfMonth();
                int bId = unitOfWork.Branches.getUserBranch(userId);


                // get timeOff values
                foreach (var timeOfftype in unitOfWork.TimeOffType.GetAll())
                {
                    // get all balances
                    var balance = unitOfWork.TimeOffs.GetAll().Where(aa => aa.approvalstate == Models.DAL.Vacation.ReqState.approved && aa.ApplicationUserId == userId
                                            && aa.VacationTypeModelID == timeOfftype.ID);

                    // this will get the available balance for each type of policy
                    switch (timeOfftype.policyResetEvery)
                    {
                        case Models.DAL.VacationType.ResetEvery.Yearly:
                            {
                                balance = balance.Where(a => a.fromDate.Year == currentYear);
                                break;
                            }
                        case Models.DAL.VacationType.ResetEvery.Monthly:
                            {
                                balance = balance.Where(a => a.fromDate.Year == currentYear && a.fromDate.Month == currentMonth);
                                break;
                            }
                        case Models.DAL.VacationType.ResetEvery.Weekly:
                            {
                                balance = balance.Where(a => a.fromDate.Year == currentYear && a.fromDate.Month == currentMonth && a.fromDate.GetWeekOfMonth() == weekNumber);
                                break;
                            }
                    }

                    TimeOffBalanceViewModel bala = new TimeOffBalanceViewModel()
                    {
                        typelimit = timeOfftype.balance.HasValue ? timeOfftype.balance.Value.ToString() : "",
                        balance = balance.Where(aa => aa.VacationTypeModelID == timeOfftype.ID).Count().ToString(),
                        timeOffTypeName = timeOfftype.vacationName,
                        message = timeOfftype.balance.HasValue ? (timeOfftype.balance.Value - balance.Count()).ToString() : "",
                        Id = timeOfftype.ID.ToString()
                    };

                    res.Add(bala);
                }

                return Json(res, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult SpecificEmployeeUpcomingTimeOff()
        {
            
            string userId = User.Identity.GetUserId();

            List<VacationViewModel> list = new List<VacationViewModel>();
            foreach (var timeOff in unitOfWork.TimeOffs.Find(a => a.ApplicationUserId == userId && a.fromDate > System.DateTime.Now))
            {
                VacationViewModel vac = new VacationViewModel()
                {
                    ID = timeOff.ID,
                    EmployeeName = timeOff.user.EmployeeName,
                    fromDate = timeOff.fromDate.ToString("yyyy-MM-dd"),
                    vacationName = timeOff.vacType.vacationName,
                    message = timeOff.message,
                    textDes = timeOff.hours > 8 ? TimeSpan.FromHours(timeOff.hours.Value).TotalDays.ToString()
                                    + " days" : timeOff.hours.ToString() + " hours"
                };

                list.Add(vac);
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpGet]
        public ActionResult GetActivityForEmployee()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                
                string userId = User.Identity.GetUserId();

                List<ActionCompletionViewModel> list = new List<ActionCompletionViewModel>();
                foreach (var activity in db.ActionCompletionTable.Include("TargetUser").Where(aa => aa.ApplicationUserID == userId)
                    .OrderByDescending(aa => aa.ID).Take(10))
                {
                    ActionCompletionViewModel vac = new ActionCompletionViewModel()
                    {
                        eventDescription = activity.eventDescription,
                        user = activity.TargetUser.EmployeeName,
                        shown = activity.shown,
                        ID = activity.ID
                    };

                    list.Add(vac);
                }

                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetMissionsForToday()
        {
            
            string userId = User.Identity.GetUserId();
            int bId = unitOfWork.Branches.getUserBranch(userId);
            List<VacationViewModel> list = new List<VacationViewModel>();

            try
            {
                var date = System.DateTime.Now.Date;
                foreach (var timeOff in unitOfWork.TimeOffs.Find(a => a.vacType.policyResetEvery == Models.DAL.VacationType.ResetEvery.None && a.fromDate == date
                                && (a.user.BranchesModelID.HasValue ? a.user.BranchesModelID.Value == bId : false)))
                {
                    VacationViewModel vac = new VacationViewModel()
                    {
                        ID = timeOff.ID,
                        EmployeeName = timeOff.user.EmployeeName,
                        fromDate = timeOff.fromDate.ToString("yyyy-MM-dd"),
                        vacationName = timeOff.vacType.vacationName,
                        message = timeOff.message,
                        textDes = timeOff.hours > 8 ? TimeSpan.FromHours(timeOff.hours.Value).TotalDays.ToString()
                                        + " days" : timeOff.hours.ToString() + " hours"
                    };

                    list.Add(vac);
                }
            }
            catch (Exception ll)
            {
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetBirthdays()
        {
            
            string userId = User.Identity.GetUserId();
            int bId = unitOfWork.Branches.getUserBranch(userId);
            List<VacationViewModel> list = new List<VacationViewModel>();

            DateTime afterDate = System.DateTime.Now.AddDays(14);

            foreach (var bd in unitOfWork.Users.Find(a => a.empBirthDay.HasValue ? a.empBirthDay.Value < afterDate && a.empBirthDay.Value > DateTime.Now : false
            && (a.user.BranchesModelID.HasValue ? a.user.BranchesModelID.Value == bId : false)).ToList())
            {
                VacationViewModel vac = new VacationViewModel()
                {
                    ID = 0,
                    EmployeeName = bd.EmployeeName,
                    textDes = bd.empBirthDay.Value.ToString("yyyy-MM-dd")
                };

                list.Add(vac);
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetWorkAnniversaries()
        {
            
            string userId = User.Identity.GetUserId();
            int bId = unitOfWork.Branches.getUserBranch(userId);

            List<VacationViewModel> list = new List<VacationViewModel>();

            DateTime afterDate = System.DateTime.Now.AddDays(14);
            foreach (var bd in unitOfWork.Users.Find(a => a.dateEnrollDay.HasValue ? a.dateEnrollDay.Value < afterDate
            && a.dateEnrollDay.Value > DateTime.Now : false && (a.user.BranchesModelID.HasValue ? a.user.BranchesModelID.Value == bId : false)).ToList())
            {
                VacationViewModel vac = new VacationViewModel()
                {
                    ID = 0,
                    EmployeeName = bd.EmployeeName,
                    textDes = bd.empBirthDay.Value.ToString("yyyy-MM-dd")
                };

                list.Add(vac);
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}