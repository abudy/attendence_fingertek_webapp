﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.Settings;
using static JqueryDTWithMVC.Startup;
using JqueryDTWithMVC.Models.ViewModels.WSSettings;
using JqueryDTWithMVC.Helper;

namespace JqueryDTWithMVC.Controllers
{
    [Title(Title = "اعدادت الموقع")]
    public class SettingsModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: SettingsModels
        [Title(Title = "جدول اعدادت الموقع")]
        [AllowAnonymous]
        public ActionResult Index()
        {
            var setting = db.SettingsTable.ToArray();
            WebSiteSettingsViewModel wsSet = new WebSiteSettingsViewModel()
            {
                SITENAME = setting[0].Value,
                ADDRESS = setting[1].Value,
                TEL = setting[2].Value,
                SITE = setting[3].Value,
                EMAIL = setting[4].Value,
                LOGO = setting[5].Value,
                ALLOWTIME = setting[6].Value,
                SITEDIR = setting[7].Value
            };

            return Json(wsSet, JsonRequestBehavior.AllowGet);
        }

        // POST: SettingsModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ModifiedAuthorize]
        [Title(Title = "تعديل اعدادت الموقع")]
        public async Task<ActionResult> Edit(WebSiteSettingsViewModel settingsModel, HttpPostedFileBase setting_coClock)
        {
            if (ModelState.IsValid)
            {

                if (settingsModel.Img_Logo != null)
                {
                    if (settingsModel.Img_Logo.ContentLength > 0)
                    {
                        FileStorageClass.StoreImage(false, settingsModel.Img_Logo, "logo", Server.MapPath("/Content/css/"));
                    }
                }

                var setting = db.SettingsTable.ToArray();
                setting[0].Value = settingsModel.SITENAME;
                db.Entry(setting[0]).State = EntityState.Modified;
                await db.SaveChangesAsync();

                setting[1].Value = settingsModel.ADDRESS;
                db.Entry(setting[1]).State = EntityState.Modified;
                await db.SaveChangesAsync();

                setting[2].Value = settingsModel.TEL;
                db.Entry(setting[2]).State = EntityState.Modified;
                await db.SaveChangesAsync();

                setting[3].Value = settingsModel.SITE;
                db.Entry(setting[3]).State = EntityState.Modified;
                await db.SaveChangesAsync();

                setting[4].Value = settingsModel.EMAIL;
                db.Entry(setting[4]).State = EntityState.Modified;
                await db.SaveChangesAsync();

                setting[6].Value = settingsModel.ALLOWTIME;
                db.Entry(setting[6]).State = EntityState.Modified;
                await db.SaveChangesAsync();

                setting[7].Value = settingsModel.SITEDIR;
                db.Entry(setting[7]).State = EntityState.Modified;
                await db.SaveChangesAsync();

                return Json("done");
            }
            return Json("error");
        }

        [ModifiedAuthorize]
        [Title(Title = "تعديل شعار الموقع")]
        public ActionResult EditLogo()
        {
            return View();
        }

        [HttpPost]
        [ModifiedAuthorize]
        public ActionResult EditLogo( HttpPostedFileBase Logo)
        { 
            if (ModelState.IsValid)
            {
                if (Logo != null)
                {
                    if (Logo.ContentLength > 0)
                    {
                        FileStorageClass.StoreImage(false, Logo, "logo", Server.MapPath("/Content/SiteImage/"));
                    }
                }

                return View();
            }
            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
