﻿using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.ViewModels.Roles;
using JqueryDTWithMVC.Models.ViewModels.UsersWithRoles;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static JqueryDTWithMVC.Startup;

namespace JqueryDTWithMVC.Controllers
{
    [Title(Title = "ادارة الصلاحيات")]
    public class RolesViewModelsController : Controller
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: RolesViewModels
        [Title(Title = "جدول الصلاحيات")]
        [ModifiedAuthorize]
        public ActionResult Index()
        {
            return View(db.Roles);
        }

        [HttpPost]
        [Startup.ModifiedAuthorize]
        [Startup.Title(Title = "الصلاحيات")]
        public ActionResult GetAllRoles()
        {
            return Json(db.Roles.Select(a => a.Name).ToList());
        }

        [HttpPost]
        [Startup.ModifiedAuthorize]
        [Startup.Title(Title = "الصلاحيات")]
        public async Task<ActionResult> GetRoleForUser(string id)
        {
            UsersWithRole role = new UsersWithRole()
            {
                userName = UserManager.FindByIdAsync(id).Result.EmployeeName,
                role = UserManager.GetRolesAsync(id).Result.First(),
                userId = id
            };

            return Json(role);
        }

        // GET: RolesViewModels/Create
        [Title(Title = "انشاء صلاحية")]
        [ModifiedAuthorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: RolesViewModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        [Log(actionDescription = "اضافة صلاحية بالنظام", typeOfAction = Models.DAL.ActivityLog.ActionType.Create)]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name")] IdentityRole rolesViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Roles.Add(rolesViewModel);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(rolesViewModel);
        }

        // GET: RolesViewModels/Edit/5
        [Title(Title = "تعديل صلاحية")]
        [ModifiedAuthorize]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IdentityRole rolesViewModel = db.Roles.Find(id);
            if (rolesViewModel == null)
            {
                return HttpNotFound();
            }
            return View(rolesViewModel);
        }

        // POST: RolesViewModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        [Log(actionDescription = "تعديل صلاحية بالنظام", typeOfAction = Models.DAL.ActivityLog.ActionType.Edit)]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name")] IdentityRole rolesViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rolesViewModel).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(rolesViewModel);
        }

        // GET: RolesViewModels/Delete/5
        [Title(Title = "مسح صلاحية")]
        [ModifiedAuthorize]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IdentityRole rolesViewModel = db.Roles.Find(id);
            if (rolesViewModel == null)
            {
                return HttpNotFound();
            }
            return View(rolesViewModel);
        }

        // POST: RolesViewModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Title(Title = "تاكيد مسح صلاحية")]
        [ModifiedAuthorize]
        [Log(actionDescription = "مسح صلاحية بالنظام", typeOfAction = Models.DAL.ActivityLog.ActionType.Delete)]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            IdentityRole rolesViewModel = db.Roles.Find(id);
            db.Roles.Remove(rolesViewModel);
            await db.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}