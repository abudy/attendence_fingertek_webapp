﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.Departments;
using JqueryDTWithMVC.Helper;
using JqueryDTWithMVC.Models.ViewModels.Department;
using static JqueryDTWithMVC.Startup;

namespace JqueryDTWithMVC.Controllers
{
    [Title(Title = "الاقسام")]
    public class DepartmentsModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: DepartmentsModels
        [HttpPost]
        [ModifiedAuthorize]
        [Title(Title = "جدول الاقسام")]
        //[Authorize(Roles = "مدير النظام,موظف موارد بشرية")]
        public ActionResult Index(int? Branch, string action)
        {
            var depList = db.DepartmentsTable.Include("relatedBranch").Where(a => Branch != null ? a.BranchesModelID == Branch : true).ToList().Select(a => new DepartmentViewModel()
            {
                DT_RowId = a.ID,
                depart_id = a.ID,
                depart_name = a.depName,
                depart_branch = a.BranchesModelID != null ? a.BranchesModelID.Value : 0,
                depart_manager = String.IsNullOrEmpty(a.managerId) ? "غير متوفر" : ( db.Users.Find(a.managerId) != null ? db.Users.Find(a.managerId).EmployeeName : "غير متوفر")
                ,branch_name = a.BranchesModelID != null ? a.relatedBranch.branch_name : "غير متوفر",
            }).ToList();


            return Json(depList, JsonRequestBehavior.AllowGet);
        }

        [ModifiedAuthorize]
        [Title(Title = "جدول الاقسام كجدول بيانات")]
        [HttpPost]
        public ActionResult IndexJQDT(int draw = 0, int start = 0, int length = 10)
        {
            JqueryDTWithMVC.Helper.DataTableModels<DepartmentViewModel>._data = db.DepartmentsTable.Include("relatedBranch").ToList().Select(a => new DepartmentViewModel()
            {
                DT_RowId = a.ID,
                depart_id = a.ID,
                depart_name = a.depName,
                depart_branch = a.BranchesModelID != null ? a.BranchesModelID.Value : 0,
                depart_manager = String.IsNullOrEmpty(a.managerId) ? "غير متوفر" : (db.Users.Find(a.managerId) != null ? db.Users.Find(a.managerId).EmployeeName : "غير متوفر"),
                branch_name = a.BranchesModelID != null ? a.relatedBranch.branch_name : "غير متوفر",
                EmployeeAName = String.IsNullOrEmpty(a.managerId) ? "غير متوفر" : (db.Users.Find(a.managerId) != null ? db.Users.Find(a.managerId).EmployeeName : "غير متوفر")
            }).ToList();


            DataTableData<DepartmentViewModel> dataTableData = new DataTableData<DepartmentViewModel>();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = DataTableModels<DepartmentViewModel>._data.Count;
            int recordsFiltered = 0;
            dataTableData.data = DataTableModels<DepartmentViewModel>._data; /*DataTableModels<UserViewModel>.FilterData(ref recordsFiltered, start, length, search, sortColumn, sortDirection);*/
            dataTableData.recordsFiltered = recordsFiltered;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }
        // POST: DepartmentsModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ModifiedAuthorize]
        [Title(Title = "انشاء قسم")]
        public ActionResult Create([Bind(Include = "depart_name,depart_manager,depart_branch")] DepartmentViewModel departmentsModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DepartmentsModel dep = new DepartmentsModel()
                    {
                        depName = departmentsModel.depart_name,
                        BranchesModelID = departmentsModel.depart_branch,
                        managerId = db.Users.Any(a=>a.Id == departmentsModel.depart_manager) ? db.Users.Find(departmentsModel.depart_manager).Id : ""
                    };
                    db.DepartmentsTable.Add(dep);
                    db.SaveChanges();
                    return Json("done");
                }
            }
            catch
            {        
            }
            return Json("Error");
        }

        // POST: DepartmentsModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ModifiedAuthorize]
        [Title(Title = "تعديل قسم")]
        public ActionResult Edit([Bind(Include = "DepartmentID,DepartmentName,DepartmentBranch,DepartmentManager")] EditDepartmentViewModel depViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var departmentsModel = db.DepartmentsTable.Find(depViewModel.DepartmentID);
                    departmentsModel.depName = depViewModel.DepartmentName;
                    departmentsModel.BranchesModelID = depViewModel.DepartmentBranch;
                    departmentsModel.managerId = depViewModel.DepartmentManager;

                    db.Entry(departmentsModel).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json("done");
                }

            }
            catch(Exception tt)
            {
                return Json(tt.Message);
            }

            return Json("Error!");
        }

        // POST: DepartmentsModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ModifiedAuthorize]
        [Title(Title = "مسح قسم")]
        public ActionResult DeleteConfirmed(int DepartmentID)
        {
            try
            {
                DepartmentsModel departmentsModel = db.DepartmentsTable.Find(DepartmentID);
                db.DepartmentsTable.Remove(departmentsModel);
                db.SaveChanges();

                return Json("done");
            }
            catch(Exception error)
            {
                return Json(error.Message);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
