﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.Vacation;
using Microsoft.AspNet.Identity;
using JqueryDTWithMVC.Models.Repository.UnitOfWork;
using JqueryDTWithMVC.Models.ViewModels.TimeOffBalance;
using static JqueryDTWithMVC.Startup;

namespace JqueryDTWithMVC.Controllers
{
    [Startup.Title(Title = "TimeOff Management")]
    public class VacationsModelsController : Controller
    {
        UnitOfWork unitOfWork = new UnitOfWork(new ApplicationDbContext());

        // GET: VacationsModels
        [Startup.Title(Title = "TimeOff Requests List")]
        [Startup.ModifiedAuthorize]
        public ActionResult Index()
        {
            var vacationsModels = new List<VacationsModel>();
            

            string userId = User.Identity.GetUserId();
            int? empBranch = unitOfWork.Users.GetUserBranch(userId);
            if (empBranch != null)
            {
                vacationsModels = unitOfWork.TimeOffs.GetTimeOffForBranch(empBranch.Value).ToList();
                if (User.IsInRole("موظف"))
                {
                    vacationsModels = vacationsModels.Where(a => a.ApplicationUserId == userId).ToList();
                }
            }

            return View(vacationsModels);
        }

        [Authorize]
        [Title(Title ="Specific User TimeOffs")]
        public ActionResult UserTimeOff(string userId)
        {
            var vacationsModels = new List<VacationsModel>();
            
            ViewBag.userId = User.Identity.GetUserId();

            vacationsModels = vacationsModels.Where(a => a.ApplicationUserId == userId).ToList();

            return View();
        }

        // GET: VacationsModels/Details/5
        [Startup.Title(Title = "TimeOff Request Details")]
        [Startup.ModifiedAuthorize]
        public ActionResult Details(int? id)
        {
            
            {
                if (!unitOfWork.TimeOffs.Any(a => a.ID == id))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var vacationModel = unitOfWork.TimeOffs.Get(id.Value);
                if (vacationModel == null)
                {
                    return HttpNotFound();
                }

                return View(vacationModel);
            }
        }

        // GET: VacationsModels/Create
        [Startup.Title(Title = "Create New TimeOff")]
        [Authorize]
        public ActionResult Create()
        {
            using (var unitOfWork = new UnitOfWork(new ApplicationDbContext()))
            {
                ViewBag.VacationTypeModelID = new SelectList(unitOfWork.TimeOffType.GetAll(), "ID", "vacationName");
                return View();
            }
        }

        // POST: VacationsModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "ID,day_part,fromDate,toDate,hours,approvalstate,message,date_requested,VacationTypeModelID,ApplicationUserId")] VacationsModel vacationsModel,
            bool fromPartial = false)
        {
            ViewBag.errorBalance = "";
            
            {
                vacationsModel.date_requested = System.DateTime.Now;
                vacationsModel.BranchesModelID = unitOfWork.Users.GetUserBranch(User.Identity.GetUserId());

                vacationsModel.toDate = vacationsModel.toDate.HasValue ? vacationsModel.toDate : vacationsModel.fromDate;

                if (vacationsModel.fromDate.Date == vacationsModel.toDate.Value.Date && vacationsModel.hours == null)
                {
                    vacationsModel.hours = vacationsModel.day_part == PartOfDay.all ? 8 : 4;
                }
                else if (vacationsModel.fromDate.Date != vacationsModel.toDate.Value.Date)
                {
                    vacationsModel.hours = (int)vacationsModel.toDate.Value.Subtract(vacationsModel.fromDate).TotalDays * 8;
                }

                // checking: 
                int currentYear = System.DateTime.Now.Year;
                int currentMonth = System.DateTime.Now.Month;
                int weekNumber = System.DateTime.Now.GetWeekOfMonth();
                string userID = User.Identity.GetUserId();

                var balance = unitOfWork.TimeOffs.GetAll().Where(aa => aa.approvalstate == Models.DAL.Vacation.ReqState.approved
                                                && aa.ApplicationUserId == userID); // get timeOff for specific users
                switch (unitOfWork.TimeOffType.Get(vacationsModel.VacationTypeModelID).policyResetEvery)
                {
                    // now filter all balances and get only the one specified in request
                    case Models.DAL.VacationType.ResetEvery.Yearly:
                        {
                            balance = balance.Where(a => a.fromDate.Year == currentYear);
                            break;
                        }
                    case Models.DAL.VacationType.ResetEvery.Monthly:
                        {
                            balance = balance.Where(a => a.fromDate.Year == currentYear && a.fromDate.Month == currentMonth);
                            break;
                        }
                    case Models.DAL.VacationType.ResetEvery.Weekly:
                        {
                            balance = balance.Where(a => a.fromDate.Year == currentYear && a.fromDate.Month == currentMonth
                                                            && a.fromDate.GetWeekOfMonth() == weekNumber);
                            break;
                        }
                }

                if (unitOfWork.TimeOffType.Get(vacationsModel.VacationTypeModelID).balance.HasValue)
                {
                    int userBalance = balance.Sum(aa => aa.hours).HasValue ? balance.Sum(aa => aa.hours).Value : 0;
                    bool error = (userBalance + vacationsModel.hours) > (unitOfWork.TimeOffType.Get(vacationsModel.VacationTypeModelID).balance * 8) ? true : false;

                    if (error)
                    {
                        ModelState.AddModelError(string.Empty, "The number of days you are trying to book exceeds available days.");
                        Startup.MiscellaneousMethod.ActionCompletionNotify("Error: The number of days you are trying to book exceeds the available days.", false);
                        return Redirect(Request.UrlReferrer.ToString());
                    }
                }
                // end of check

                if (ModelState.IsValid)
                {
                    unitOfWork.TimeOffs.Add(vacationsModel);
                    unitOfWork.Complete(true, "TimeOff Was Added Succesfully");

                    //  return RedirectToAction("Index");
                    return Redirect(Request.UrlReferrer.ToString());
                }

                ViewBag.VacationTypeModelID = new SelectList(unitOfWork.TimeOffType.GetAll(), "ID", "vacationName", vacationsModel.VacationTypeModelID);
                return View(vacationsModel);
            }
        }

        [ChildActionOnly]
        [Startup.Title(Title = "Create New TimeOff (Partial)")]
        [Authorize]
        public ActionResult CreatePartial()
        {
            
            {
                ViewBag.VacationTypeModelID = new SelectList(unitOfWork.TimeOffType.GetAll(), "ID", "vacationName");
                return PartialView();
            }
        }


        // GET: VacationsModels/Edit/5
        [Startup.Title(Title = "Edit TimeOff Request")]
        [Startup.ModifiedAuthorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            
            {
                if (!unitOfWork.TimeOffs.Any(a => a.ID == id))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var vacationModel = unitOfWork.TimeOffs.Get(id.Value);
                if (vacationModel == null)
                {
                    return HttpNotFound();
                }

                ViewBag.ApplicationUserId = User.Identity.GetUserId();
                ViewBag.VacationTypeModelID = new SelectList(unitOfWork.TimeOffType.GetAll(),
                    "ID", "vacationName", vacationModel.VacationTypeModelID);

                return View(vacationModel);
            }
        }

        // POST: VacationsModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Startup.ModifiedAuthorize]
        public ActionResult Edit([Bind(Include = "ID,day_part,fromDate,toDate,hours,approvalstate,message,date_requested,VacationTypeModelID,ApplicationUserId")] VacationsModel vacationsModel)
        {
            
            {
                if (vacationsModel.toDate == null)
                {
                    vacationsModel.toDate = vacationsModel.fromDate;
                }

                if (vacationsModel.fromDate.Date == vacationsModel.toDate.Value.Date && vacationsModel.hours == null)
                {
                    vacationsModel.hours = vacationsModel.day_part == PartOfDay.all ? 8 : 4;
                }
                else if (vacationsModel.fromDate.Date != vacationsModel.toDate.Value.Date)
                {
                    vacationsModel.hours = (int)vacationsModel.toDate.Value.Subtract(vacationsModel.fromDate).TotalDays * 8;
                }

                if (ModelState.IsValid)
                {
                    vacationsModel.fromDate.AddDays(vacationsModel.fromDate.Subtract(unitOfWork.TimeOffs.Get(vacationsModel.ID).fromDate).TotalDays);
                    vacationsModel.toDate.Value.AddDays(vacationsModel.toDate.Value.Subtract(unitOfWork.TimeOffs.Get(vacationsModel.ID).toDate.Value).TotalDays);

                    unitOfWork.TimeOffs.UpdateTimeOff(vacationsModel);
                    unitOfWork.Complete();

                    return RedirectToAction("Index");
                }

                ViewBag.ApplicationUserId = vacationsModel.ApplicationUserId;
                ViewBag.VacationTypeModelID = new SelectList(unitOfWork.TimeOffType.GetAll(), "ID", "vacationName", vacationsModel.VacationTypeModelID);

                return View(vacationsModel);
            }
        }

        [Startup.ModifiedAuthorize]
        [Startup.Title(Title = "Change TimeOff Request Status")]
        public ActionResult ChangeStatus(int id, ReqState state)
        {
            
            {
                unitOfWork.TimeOffs.ChangeTimeOffStatus(id, state);
                unitOfWork.Complete();
            }

            return RedirectToAction("MainPage", "Home");
        }


        // GET: VacationsModels/Delete/5
        [Startup.Title(Title = "Delete TimeOff Request")]
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            
            if (!unitOfWork.TimeOffs.Any(a => a.ID == id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var vacationModel = unitOfWork.TimeOffs.Get(id.Value);
            if (vacationModel == null)
            {
                return HttpNotFound();
            }

            return View(vacationModel);

        }

        // POST: VacationsModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {          
            {
                unitOfWork.TimeOffs.Remove(unitOfWork.TimeOffs.Get(id));
                unitOfWork.Complete();
            }
            return RedirectToAction("Index");
        }
    }
}
