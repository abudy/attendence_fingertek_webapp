﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.Teams;
using JqueryDTWithMVC.Models.ViewModels.Teams;
using JqueryDTWithMVC.Models.Repository.UnitOfWork;
using static JqueryDTWithMVC.Startup;
using Microsoft.AspNet.Identity;
using JqueryDTWithMVC.Models.ViewModels.OrganizationGraph;
using JqueryDTWithMVC.Models.DAL.TeamLeaders;
using JqueryDTWithMVC.Models.ViewModels.TeamLeaders;

namespace JqueryDTWithMVC.Controllers
{
    [Title(Title = "Teams Management")]
    [Authorize]
    public class TeamsModelsController : Controller
    {
        UnitOfWork unit = new UnitOfWork(new ApplicationDbContext());
        // GET: TeamsModels
        [Title(Title = "Teams List")]
        [ModifiedAuthorize]
        public ActionResult Index()
        {
            int bInt = unit.Branches.getUserBranch(User.Identity.GetUserId());
            return View(unit.Teams.GetAll().Where(a => a.BranchesModelID == bInt));
        }

        // GET: TeamsModels
        [Title(Title = "Teams Draggable View")]
        [ModifiedAuthorize]
        public ActionResult TeamsView()
        {
            int bInt = unit.Branches.getUserBranch(User.Identity.GetUserId());
            var teamList = unit.Teams.GetAll().Where(a => a.BranchesModelID == bInt);

            ViewBag.usersWithoutTeams = unit.Users.GetAll().Where(a => a.BranchesModelID == bInt && a.TeamsModelID == null && a.EmployeeShow == true).ToList();

            return View(teamList);
        }

        [HttpPost]
        [ModifiedAuthorize]
        public ActionResult TeamsView([Bind(Include ="state,teamID,teamName")] PostTeamLeadersViewModel teamLead)
        {
            if (ModelState.IsValid)
            {
                unit.TeamLeader.UpdateTeamLeaders(teamLead.state, teamLead.teamID);
                unit.Complete();
            }


            return RedirectToAction("TeamsView");
        }

        [Title(Title = "Json: Teams List")]
        [Authorize]
        public ActionResult TeamList()
        {
            int bInt = unit.Branches.getUserBranch(User.Identity.GetUserId());

            TeamViewModelJson initial = new TeamViewModelJson()
            {
                teamName = "All Teams",
                ID = 0,
                BranchID = bInt
            };

            var list = unit.Teams.GetAll().Where(a => a.BranchesModelID == bInt).Select(aa => new TeamViewModelJson()
            {
                BranchID = bInt,
                ID = aa.ID,
                teamName = aa.teamName

            }).ToList();
            list.Add(initial);

            return Json(list.OrderBy(aa => aa.ID));
        }

        [Title(Title = "Organization Charts")]
        [ModifiedAuthorize]
        public ActionResult OrganizationCharts()
        {
            int bInt = unit.Branches.getUserBranch(User.Identity.GetUserId());
            var teamsOFBranch = unit.Teams.GetAll().Where(a => a.BranchesModelID == bInt);
            if (teamsOFBranch.Any(a => a.teamType == TeamType.Managers) == false)
            {
                return View();
            }

            List<OrganizationGraphViewModel> graph = new List<OrganizationGraphViewModel>();
            foreach (var user in unit.Users.GetAll()
                .Where(aa => (aa.BranchesModelID.HasValue ? aa.BranchesModelID == bInt : false) && aa.EmployeeShow == true))
            {
                OrganizationGraphViewModel graphObj = new OrganizationGraphViewModel()
                {
                    key = Math.Abs(user.Id.GetHashCode()),
                    userId = user.Id,
                    name = user.EmployeeName,
                    parent = String.IsNullOrEmpty(user.ApplicationUserId) ? 0 : Math.Abs(user.ApplicationUserId.GetHashCode()),
                    title = user.relatedTeam != null ? user.relatedTeam.teamName : ""
                };
                graph.Add(graphObj);
            }
            return View(graph);
        }

        [HttpPost]
        [ModifiedAuthorize]
        [Title(Title ="Change Parent")]
        public ActionResult ChangeParent(int selNodeId, int parentNodeId)
        {
            var parent = unit.Users.GetAll().Any(aa => Math.Abs(aa.Id.GetHashCode()) == parentNodeId)
                        ? unit.Users.GetAll().SingleOrDefault(aa => Math.Abs(aa.Id.GetHashCode()) == parentNodeId).Id : "";

            var node = unit.Users.GetAll().SingleOrDefault(aa => Math.Abs(aa.Id.GetHashCode()) == selNodeId);

            node.ApplicationUserId = parent;
            unit.Users.UpdateUser(node.Id);
            unit.Complete();

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ModifiedAuthorize]
        [Title(Title = "Change Team")]
        public ActionResult ChangeTeam(string userId, string teamId)
        {
            int teamID = Convert.ToInt32(teamId.Split('_')[1]);
            if (teamID == 0)
            {
                unit.Teams.RemoveMemberById(userId);
                unit.Complete();

                return Json(true, JsonRequestBehavior.AllowGet);
            }

            unit.Teams.AddMemberById(teamID, userId);
            unit.Complete();

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        // GET: TeamsModels/Details/5
        [Title(Title = "Team Details")]
        [Authorize]
        public ActionResult Details(int? id)
        {
            {
                if (!unit.Teams.Any(a => a.ID == id))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var teamsModel = unit.Teams.Get(id.Value);
                if (teamsModel == null)
                {
                    return HttpNotFound();
                }

                return View(teamsModel);
            }
        }

        // GET: TeamsModels/Create
        [Title(Title = "Add a New Team")]
        [ModifiedAuthorize]
        public ActionResult Create()
        {
            int bInt = unit.Branches.getUserBranch(User.Identity.GetUserId());
            ViewBag.TeamLeadersModelsID = new SelectList(unit.Teams.GetTeamLeader().Where(aa => aa.BranchesModelID.HasValue
                            ? aa.BranchesModelID.Value == bInt : false), "Id", "EmployeeName");

            ViewBag.associatedUsers = new SelectList(unit.Users.GetAll().Where(aa => aa.BranchesModelID.HasValue
                            ? aa.BranchesModelID.Value == bInt : false && aa.EmployeeShow), "Id", "EmployeeName");

            ViewBag.BranchesModelID = new SelectList(unit.Branches.GetAll().Where(aa => aa.ID == bInt), "ID", "branch_name");

            return View();
        }

        // POST: TeamsModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        public ActionResult Create([Bind(Include = "ID,teamName,TeamLeadersModelsID,teamType,relatedUsers,BranchesModelID")] TeamsViewModel teamsModel)
        {

            if (ModelState.IsValid)
            {
                var res = unit.Teams.ToModel(teamsModel);

                unit.Teams.Add(res.Item1);
                unit.Complete(false);

                TeamLeadersModels model = new TeamLeadersModels()
                {
                    userId = teamsModel.TeamLeadersModelsID,
                    TeamsModelID = unit.Teams.Find(a => a.teamName == teamsModel.teamName)
                        .OrderByDescending(a => a.ID).First().ID,
                    userName = unit.Users.SingleOrDefault(aa => aa.Id == teamsModel.TeamLeadersModelsID).UserName
                };
                unit.TeamLeader.Add(model);

                // check if team exist
                for (int i = 0; i < res.Item2.Length; i++)
                {
                    unit.Teams.AddMemberById(unit.Teams.Find(a => a.teamName == teamsModel.teamName)
                        .OrderByDescending(a => a.ID).First().ID, res.Item2[i]);
                }

                unit.Complete();

                return RedirectToAction("Index");
            }

            int bInt = unit.Branches.getUserBranch(User.Identity.GetUserId());
            ViewBag.TeamLeadersModelsID = new SelectList(unit.Teams.GetTeamLeader().Where(aa => aa.BranchesModelID.HasValue
                            ? aa.BranchesModelID.Value == bInt : false), "Id", "EmployeeName", teamsModel.TeamLeadersModelsID);

            ViewBag.associatedUsers = new SelectList(unit.Users.GetAll().Where(aa => aa.BranchesModelID.HasValue
                            ? aa.BranchesModelID.Value == bInt : false && aa.EmployeeShow), "Id", "EmployeeName", teamsModel.BranchesModelID);

            ViewBag.BranchesModelID = new SelectList(unit.Branches.GetAll().Where(aa => aa.ID == bInt), "ID", "branch_name");
            return View(teamsModel);

        }

        // GET: TeamsModels/Edit/5
        [Title(Title = "Edit a Team")]
        [ModifiedAuthorize]
        public ActionResult Edit(int? id)
        {
            if (!unit.Teams.Any(a => a.ID == id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var teamsModel = unit.Teams.Get(id.Value);
            if (teamsModel == null)
            {
                return HttpNotFound();
            }

            int bInt = unit.Branches.getUserBranch(User.Identity.GetUserId());
            ViewBag.TeamLeadersModelsID = new SelectList(unit.Teams.GetTeamLeader().Where(aa => aa.BranchesModelID.HasValue
                            ? aa.BranchesModelID.Value == bInt : false), "Id", "EmployeeName");
            ViewBag.associatedUsers = new SelectList(unit.Users.GetAll().Where(aa => aa.BranchesModelID.HasValue
                ? aa.BranchesModelID.Value == bInt : false && aa.EmployeeShow), "Id", "EmployeeName");
            ViewBag.BranchesModelID = new SelectList(unit.Branches.GetAll().Where(aa => aa.ID == bInt), "ID", "branch_name");

            return View(new TeamsViewModel()
            {
                BranchesModelID = teamsModel.BranchesModelID,
                teamName = teamsModel.teamName,
                teamType = teamsModel.teamType,
                users = teamsModel.relatedUsers
            });
        }

        // POST: TeamsModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        public ActionResult Edit([Bind(Include = "ID,teamName,teamType,TeamLeadersModelsID,relatedUsers,BranchesModelID")] TeamsViewModel teamsModel)
        {
            if (ModelState.IsValid)
            {
                var res = unit.Teams.ToModel(teamsModel);

                unit.Teams.UpdateTeam(res.Item1);
                unit.Complete();

                // check if team exist
                for (int i = 0; i < res.Item2.Length; i++)
                {
                    unit.Teams.AddMemberById(unit.Teams.Find(a => a.teamName == teamsModel.teamName)
                        .OrderByDescending(a => a.ID).First().ID
                        , res.Item2[i]);
                    unit.Complete();
                }

                return RedirectToAction("Index");
            }

            int bInt = unit.Branches.getUserBranch(User.Identity.GetUserId());
            ViewBag.TeamLeadersModelsID = new SelectList(unit.Teams.GetTeamLeader().Where(aa => aa.BranchesModelID.HasValue
                            ? aa.BranchesModelID.Value == bInt : false), "Id", "EmployeeName", teamsModel.TeamLeadersModelsID);

            ViewBag.associatedUsers = new SelectList(unit.Users.GetAll().Where(aa => aa.BranchesModelID.HasValue
                            ? aa.BranchesModelID.Value == bInt : false && aa.EmployeeShow), "Id", "EmployeeName");

            ViewBag.BranchesModelID = new SelectList(unit.Branches.GetAll().Where(aa => aa.ID == bInt), "ID", "branch_name");
            teamsModel.users = unit.Teams.SingleOrDefault(aa => aa.ID == teamsModel.ID).relatedUsers;

            return View(teamsModel);
        }

        [Authorize]
        public ActionResult GetLeaders(int teamID)
        {
            try
            {
                int bInt = unit.Branches.getUserBranch(User.Identity.GetUserId());
                var team = unit.Teams.GetAll().First(aa => aa.ID == teamID);

                var list = new List<TeamLeadersViewModel>();
                foreach (var u in unit.Users.Find(aa => aa.BranchesModelID.HasValue ? aa.BranchesModelID.Value == bInt : false && aa.EmployeeShow).ToList())
                {
                    string kk = unit.TeamLeader.isTeamLeader(teamID, u.Id) ? "selected" : "";
                    TeamLeadersViewModel model = new TeamLeadersViewModel()
                    {
                        userId = u.Id,
                        userName = u.UserName,
                        selected = kk
                    };
                    list.Add(model);
                }

                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ll)
            {
                return Json(new List<TeamLeadersViewModel>(), JsonRequestBehavior.AllowGet);
            }
        }

        // GET: TeamsModels/Delete/5
        [Title(Title = "Delete a Team")]
        [ModifiedAuthorize]
        public ActionResult Delete(int? id)
        {
            if (!unit.Teams.Any(a => a.ID == id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var teamsModel = unit.Teams.Get(id.Value);
            if (teamsModel == null)
            {
                return HttpNotFound();
            }

            return View(teamsModel);
        }

        // POST: TeamsModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        public ActionResult DeleteConfirmed(int id)
        {
            unit.Teams.RemoveTeam(id);
            unit.Complete();
            return RedirectToAction("Index");
        }
    }
}
