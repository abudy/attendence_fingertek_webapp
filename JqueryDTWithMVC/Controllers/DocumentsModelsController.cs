﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.Documents;
using JqueryDTWithMVC.Models.ViewModels.Documents;
using JqueryDTWithMVC.Models.Repository.UnitOfWork;
using static JqueryDTWithMVC.Startup;
using Microsoft.AspNet.Identity;

namespace JqueryDTWithMVC.Controllers
{
    [Title(Title = "Manage Documents")]
    public class DocumentsModelsController : Controller
    {
        UnitOfWork unitOfWork = new UnitOfWork(new ApplicationDbContext());

        // GET: DocumentsModels
        // GET: DocumentsModels
        [Title(Title = "Uploaded Documents List")]
        public ActionResult Index()
        {
            int bInt = unitOfWork.Branches.getUserBranch(User.Identity.GetUserId());
            var res = unitOfWork.Documents.GetAll().Where(aa => aa.sharedWithUsers != null ? (aa.sharedWithUsers.Any(bb => bb.users.BranchesModelID.HasValue ? bb.users.BranchesModelID.Value == bInt : false)) : false);
            var userId = User.Identity.GetUserId();

            if (User.IsInRole("موظف"))
                res = res.Where(a => a.sharedWithUsers!= null ? a.sharedWithUsers.Any(aa => aa.ApplicationUserId == userId) : false);

            return View(res);
        }

        [Title(Title = "Uploaded Documents List For User")]
        public ActionResult UserDocuments(string userId)
        {
            var res = unitOfWork.Documents.GetAll();
            var uId = String.IsNullOrEmpty(userId) ? User.Identity.GetUserId() : userId;
            ViewBag.userId = userId;

            if (User.IsInRole("موظف"))
                res = res.Where(a => a.sharedWithUsers != null ? a.sharedWithUsers.Any(aa => aa.ApplicationUserId == uId) : false);

            return View(res);
        }

        // GET: DocumentsModels/Details/5
        [Title(Title = "Uploaded Document Details")]
        public ActionResult Details(int? id)
        {
            if (!unitOfWork.Documents.Any(a => a.ID == id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var documentsModels = unitOfWork.Documents.Get(id.Value);
            if (documentsModels == null)
            {
                return HttpNotFound();
            }

            return View(documentsModels);
        }

        // GET: DocumentsModels/Create
        [Title(Title = "Upload New Document")]
        public ActionResult Create()
        {
            int bInt = unitOfWork.Branches.getUserBranch(User.Identity.GetUserId());
            ViewBag.DocumentsCategoryModelsID = new SelectList(unitOfWork.DocumentsCategory.GetAll(), "ID", "catgeoryName");
            ViewBag.associatedUsers = new SelectList(unitOfWork.Users.GetAll().Where(aa => aa.BranchesModelID.HasValue ? aa.BranchesModelID.Value == bInt : false), "Id", "EmployeeName");

            return View();
        }

        // POST: DocumentsModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,uploadedFile,sharedWithUsers,DocumentsCategoryModelsID")]
                                DocumentsViewModel documentsViewModels)
        {
            if (ModelState.IsValid)
            {
                documentsViewModels.sharedWithUsers = documentsViewModels.sharedWithUsers.Concat(new string[] { User.Identity.GetUserId()}).ToArray();
                var res = unitOfWork.Documents.ToModel(documentsViewModels);

                unitOfWork.Documents.Add(res.Item1);
                unitOfWork.Complete();

                JqueryDTWithMVC.Helper.FileStorageClass.StoreFile(documentsViewModels.uploadedFile, Server.MapPath("~/Content/EmployeeFiles/"));

                // add users to document exist
                if (documentsViewModels.sharedWithUsers != null)
                {
                    for (int i = 0; i < res.Item2.Length; i++)
                    {
                        unitOfWork.Documents.AddUserShare(unitOfWork.Documents.Find(a => a.fileName == documentsViewModels.uploadedFile.FileName)
                            .OrderByDescending(a => a.ID).First().ID // get the last file added by this name
                            , res.Item2[i]);
                        unitOfWork.Complete();
                    }
                }

                return Redirect(Request.UrlReferrer.ToString());
            }

            int bInt = unitOfWork.Branches.getUserBranch(User.Identity.GetUserId());
            ViewBag.DocumentsCategoryModelsID = new SelectList(unitOfWork.DocumentsCategory.GetAll(), "ID", "catgeoryName",
                documentsViewModels.DocumentsCategoryModelsID);
            ViewBag.associatedUsers = new SelectList(unitOfWork.Users.GetAll().Where(aa => aa.BranchesModelID.HasValue ? aa.BranchesModelID.Value == bInt : false), "Id", "EmployeeName");
            return View(documentsViewModels);
        }

        [ChildActionOnly]
        public ActionResult CreatePartial()
        {
            var bId = unitOfWork.Branches.getUserBranch(User.Identity.GetUserId());

            ViewBag.DocumentsCategoryModelsID = new SelectList(unitOfWork.DocumentsCategory.GetAll(), "ID", "catgeoryName");
            ViewBag.associatedUsers = new SelectList(unitOfWork.Users.GetAll().Where(aa => aa.BranchesModelID.HasValue ? aa.BranchesModelID.Value == bId : false), "Id", "EmployeeName");

            return PartialView();
        }

        // GET: DocumentsModels/Edit/5
        [Title(Title = "Edit Uploaded Document Informations")]
        [ModifiedAuthorize]
        public ActionResult Edit(int? id)
        {
            {
                if (!unitOfWork.Documents.Any(a => a.ID == id))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var documentModel = unitOfWork.Documents.Get(id.Value);
                if (documentModel == null)
                {
                    return HttpNotFound();
                }

                int bInt = unitOfWork.Branches.getUserBranch(User.Identity.GetUserId());
                ViewBag.DocumentsCategoryModelsID = new SelectList(unitOfWork.DocumentsCategory.GetAll(), "ID", "catgeoryName",
                    documentModel.DocumentsCategoryModelsID);
                ViewBag.associatedUsers = new SelectList(unitOfWork.Users.GetAll().Where(aa => aa.BranchesModelID.HasValue ? aa.BranchesModelID.Value == bInt : false), "Id", "EmployeeName");

                var docViewModel = new DocumentsViewModel()
                {
                    DocumentsCategoryModelsID = documentModel.DocumentsCategoryModelsID,
                    users = documentModel.sharedWithUsers
                };
                return View(docViewModel);
            }
        }

        // POST: DocumentsModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        public ActionResult Edit([Bind(Include = "ID,sharedWithUsers,uploadedFile,DocumentsCategoryModelsID")] DocumentsViewModel documentsViewModels)
        {
            if (ModelState.IsValid)
            {
                documentsViewModels.sharedWithUsers = documentsViewModels.sharedWithUsers.Concat(new string[] { User.Identity.GetUserId() }).ToArray();
                unitOfWork.Documents.UpdateDocument(documentsViewModels);

                JqueryDTWithMVC.Helper.FileStorageClass.StoreFile(documentsViewModels.uploadedFile, Server.MapPath("~/Content/EmployeeFiles/"));

                unitOfWork.Complete();
                return RedirectToAction("Index");
            }

            int bInt = unitOfWork.Branches.getUserBranch(User.Identity.GetUserId());
            ViewBag.DocumentsCategoryModelsID = new SelectList(unitOfWork.DocumentsCategory.GetAll(), "ID", "catgeoryName",
                documentsViewModels.DocumentsCategoryModelsID);
            ViewBag.associatedUsers = new SelectList(unitOfWork.Users.GetAll(), "Id", "EmployeeName");

            return View(documentsViewModels);
        }

        // GET: DocumentsModels/Delete/5
        [Title(Title = "Delete Uploaded Document")]
        [ModifiedAuthorize]
        public ActionResult Delete(int? id)
        {
            if (!unitOfWork.Documents.Any(a => a.ID == id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var documentsModels = unitOfWork.Documents.Get(id.Value);
            if (documentsModels == null)
            {
                return HttpNotFound();
            }

            return View(documentsModels);
        }

        // POST: DocumentsModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        [Title(Title = "Delete Uploaded Document Information")]
        public ActionResult DeleteConfirmed(int id)
        {
            unitOfWork.Documents.RemoveDocument(id);
            unitOfWork.Complete();

            return RedirectToAction("Index");
        }
    }
}
