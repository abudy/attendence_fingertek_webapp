﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.DAL.Branches;
using JqueryDTWithMVC.Helper;
using JqueryDTWithMVC.Models.ViewModels.Branch;
using System.Threading.Tasks;

namespace JqueryDTWithMVC.Controllers
{
    [Startup.ModifiedAuthorize]
    [Startup.Title(Title = "ادارة الفروع")]
    public class BranchesModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: BranchesModels
        [Startup.ModifiedAuthorize]
        [Startup.Title(Title = "استدعاء جميع المستخدمين")]
        [HttpPost]
        public ActionResult Index(string action, int draw = 0, int start = 0, int length = 10)
        {
            db.Configuration.LazyLoadingEnabled = false;
            JqueryDTWithMVC.Helper.DataTableModels<BranchViewModel>._data = db.BranchesTable.Select(a => new Models.ViewModels.Branch.BranchViewModel()
            {
                DT_RowId = a.ID,
                branch_id = a.ID,
                branch_name = a.branch_name,
                branch_ip = a.branch_ip,
                branch_key = a.branch_key,
                branch_location  = a.branch_location,
                branch_port = a.branch_port,
                branch_status = 0
            }).ToList();


            //Parallel.ForEach(JqueryDTWithMVC.Helper.DataTableModels<BranchViewModel>._data, (ip) => {
            //    ip.branch_status = PingHost.PingFPDHost(ip.branch_ip) ? 1 : 0;
            //});


            DataTableData<BranchViewModel> dataTableData = new DataTableData<BranchViewModel>();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = DataTableModels<BranchViewModel>._data.Count;
            int recordsFiltered = 0;
            dataTableData.data = DataTableModels<BranchViewModel>._data; /*DataTableModels<UserViewModel>.FilterData(ref recordsFiltered, start, length, search, sortColumn, sortDirection);*/
            dataTableData.recordsFiltered = recordsFiltered;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        // POST: BranchesModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Startup.ModifiedAuthorize]
        [Startup.Title(Title = "انشاء فرع جديد")]
        [HttpPost]
        public ActionResult Create([Bind(Include = "ID,branch_name,branch_ip,branch_port,branch_key,branch_location")] BranchesModel branchesModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.BranchesTable.Add(branchesModel);
                    db.SaveChanges();
                    return Json("done");
                }
            }
            catch
            {
            }

            return Json("error");
        }

        // POST: BranchesModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Startup.ModifiedAuthorize]
        [Startup.Title(Title = "تعديل فرع")]
        public ActionResult Edit([Bind(Include = "ID,branch_name,branch_ip,branch_port,branch_key,branch_location")] BranchesModel branchesModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(branchesModel).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json("done");
                }

            }
            catch
            {
            }

            return Json("error");
        }

        // POST: BranchesModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Startup.ModifiedAuthorize]
        [Startup.Title(Title = "مسح فرع")]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                BranchesModel branchesModel = db.BranchesTable.Find(id);
                db.BranchesTable.Remove(branchesModel);
                db.SaveChanges();
                return Json("done");
            } 
            catch
            {
                return Json("error");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
