﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JqueryDTWithMVC.Models.DAL.Settings;
using JqueryDTWithMVC.Models.DAL.Schedules;
using JqueryDTWithMVC.Models;
using System.Data.Entity.Validation;
using System.Text;
using JqueryDTWithMVC.Models.DAL.Branches;
using JqueryDTWithMVC.Models.DAL.Departments;
using Microsoft.AspNet.Identity.Owin;
using JqueryDTWithMVC.Models.DAL.CheckType;
using JqueryDTWithMVC.Models.DAL.CheckPointLog;
using System.Threading.Tasks;
using JqueryDTWithMVC.Models.ViewModels.CakeHREmployeesInfo;

namespace JqueryDTWithMVC.Controllers
{
    public class MigrationController : Controller
    {
        // altameerEntities OldDb = new altameerEntities();
        altameerEntities OldDb = new altameerEntities();
        ApplicationDbContext newDb = new ApplicationDbContext();
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Migration
        public ActionResult MigirateSettings()
        {
            foreach (var val in OldDb.settings)
            {
                SettingsModel model = new SettingsModel()
                {
                    Key = val.SettingKey,
                    Value = val.SettingVal
                };

                newDb.SettingsTable.Add(model);
                newDb.SaveChanges();
            }

            return View();
        }

        public ActionResult MigirateSchedules()
        {
            foreach (var val in OldDb.schedules)
            {
                try
                {
                    SchedulesModel model = new SchedulesModel()
                    {
                        schedule_allowtime = 0,
                        schedule_saturday = val.schedule_saturday,
                        schedule_saturday2 = val.schedule_saturday2,
                        schedule_sunday = val.schedule_sunday,
                        schedule_sunday2 = val.schedule_sunday2,
                        schedule_monday = val.schedule_monday,
                        schedule_monday2 = val.schedule_monday2,
                        schedule_thursday = val.schedule_thursday,
                        schedule_thursday2 = val.schedule_thursday2,
                        schedule_wednesday = val.schedule_wednesday,
                        schedule_wednesday2 = val.schedule_wednesday2,
                        schedule_tuesday = val.schedule_tuesday,
                        schedule_tuesday2 = val.schedule_tuesday2,
                        schedule_friday = val.schedule_friday,
                        schedule_friday2 = val.schedule_friday2,
                        schedule_name = val.schedule_name,
                    };

                    newDb.SchedulesTable.Add(model);
                    newDb.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    StringBuilder sb = new StringBuilder();

                    foreach (var failure in ex.EntityValidationErrors)
                    {
                        sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                        foreach (var error in failure.ValidationErrors)
                        {
                            sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                            sb.AppendLine();
                        }
                    }

                    throw new DbEntityValidationException(
                        "Entity Validation Failed - errors follow:\n" +
                        sb.ToString(), ex
                    ); // Add the original exception as the innerException
                }

            }

            return View();
        }


        public ActionResult MigirateBranches()
        {
            foreach (var val in OldDb.branches)
            {
                BranchesModel model = new BranchesModel()
                {
                    branch_ip = val.branch_ip,
                    branch_name = val.branch_name,
                    branch_key = val.branch_key.ToString(),
                    branch_location = val.branch_location,
                    branch_port = val.branch_port,

                };
                newDb.BranchesTable.Add(model);
                newDb.SaveChanges();
            }

            return View();
        }

        public ActionResult MigirateDepartments()
        {
            foreach (var val in OldDb.departments.ToList())
            {
                var branchName = OldDb.branches.Find(val.depart_branch).branch_name;
                DepartmentsModel model = new DepartmentsModel()
                {
                    depName = val.depart_name,
                    BranchesModelID = newDb.BranchesTable.First(a => a.branch_name == branchName).ID,
                };
                newDb.DepartmentsTable.Add(model);
                newDb.SaveChanges();
            }

            return View();
        }

        public async System.Threading.Tasks.Task<ActionResult> MigirateUser()
        {
            foreach (var val in OldDb.employees.ToList())
            {
             //   if(val.EmployeeID== 18 && val.EmployeeBranch == 5)
                {
                    int? bId = OldDb.branches.Any(a => a.branch_id == val.EmployeeBranch) ? OldDb.branches.Find(val.EmployeeBranch).branch_id : 0;
                    int? dId = OldDb.departments.Any(a => a.depart_id == val.EmployeeDepart) ? OldDb.departments.Find(val.EmployeeDepart).depart_id : 0;
                    if (OldDb.schedules.Any(a => a.schedule_id == val.EmployeeSchedule))
                    {
                        int sId = OldDb.schedules.Any(a => a.schedule_id == val.EmployeeSchedule) ? OldDb.schedules.Find(val.EmployeeSchedule).schedule_id : 0;
                        switch (sId)
                        {
                            case 3:
                                sId = 1;
                                break;
                            case 6:
                                sId = 2;
                                break;
                            case 7:
                                sId = 3;
                                break;
                            case 8:
                                sId = 4;
                                break;
                            case 9:
                                sId = 5;
                                break;
                            case 10:
                                sId = 6;
                                break;
                            case 11:
                                sId = 7;
                                break;
                            case 12:
                                sId = 8;
                                break;
                            case 13:
                                sId = 9;
                                break;
                            case 15:
                                sId = 10;
                                break;
                            case 16:
                                sId = 11;
                                break;
                        }
                        try
                        {
                            ApplicationUser model = new ApplicationUser()
                            {
                                UserName = val.EmployeeAName,
                                Email = val.EmployeeEmail,
                                EmployeeName = val.EmployeeAName,
                                EmployeeName_En = val.EmployeeEName,
                                EmployeeGender = val.EmployeeGender == 1 ? Gender.ذكر : Gender.انثى,
                                EnrollNumber = val.EmployeeID,
                                EmployeeBirthDate = val.EmployeeBirthDate,
                                BranchesModelID = bId != 0 ? bId : null,
                                DepartmentsModelID = dId != 0 ? (dId - 5) : null,
                                SchedulesModelID = sId,
                                EmployeeShow = val.EmployeeShow == 1 ? true : false
                            };
                            var result = await UserManager.CreateAsync(model, "Hello90#");

                        }
                        catch (DbEntityValidationException ex)
                        {
                            StringBuilder sb = new StringBuilder();

                            foreach (var failure in ex.EntityValidationErrors)
                            {
                                sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                                foreach (var error in failure.ValidationErrors)
                                {
                                    sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                                    sb.AppendLine();
                                }
                            }

                            throw new DbEntityValidationException(
                                "Entity Validation Failed - errors follow:\n" +
                                sb.ToString(), ex
                            ); // Add the original exception as the innerException
                        }

                    }
                    

                }
            }
            return View();
        }

        public async System.Threading.Tasks.Task<ActionResult> AssignRole()
        {
            foreach (var val in newDb.Users.ToList())
            {
                if (val.EmployeeName_En == "Farah Muhsin" || val.EmployeeName_En == "Maryam"
                    || val.EmployeeName_En == "Aya Jamal" || val.EmployeeName_En == "Hind Wael" || val.EmployeeName_En == "Tahira" || val.EmployeeName_En == "Abdalkhalik Hussain")
                {
                    await UserManager.AddToRoleAsync(val.Id, "مدير النظام");
                }
                else
                {
                    await UserManager.AddToRoleAsync(val.Id, "موظف");
                }
            }

            return View();
        }

        public ActionResult MigirateCheckType()
        {
            foreach (var val in OldDb.checktypes.ToList())
            {
                CheckTypeModels model = new CheckTypeModels()
                {
                    CheckName = val.CheckName,
                    CheckNameFromCakeHR = "",

                };
                newDb.CheckTypeTable.Add(model);
                newDb.SaveChanges();
            }

            return View();
        }

        public ActionResult MigirateUserLogPoints()
        {
            foreach (var val in OldDb.checkpoints.ToList())
            {
                if (OldDb.employees.Any(a => a.EmployeeID == val.CheckEmpID))
                {
                    var emp = OldDb.employees.First(a => a.EmployeeID == val.CheckEmpID);

                    try
                    {

                        IFormatProvider culture = new System.Globalization.CultureInfo("ar-IQ", true);
                        DateTime dt2 = DateTime.Parse(val.CheckDate + " " + val.CheckTimeIn + "." + 0, culture, System.Globalization.DateTimeStyles.AssumeLocal);
                        DateTime dt3 = DateTime.Parse(val.CheckDate + " " + val.CheckTimeOut + "." + 0, culture, System.Globalization.DateTimeStyles.AssumeLocal);

                        CheckPointLogModels model = new CheckPointLogModels()
                        {
                            BranchesModelID = val.CheckBranch,
                            CheckTimeIn = dt2,
                            CheckTimeOut = dt3,
                            CurrentDay = DateTime.Parse(val.CheckDate, culture, System.Globalization.DateTimeStyles.AssumeLocal),
                            userId = newDb.Users.First(a => a.EmployeeName_En == emp.EmployeeEName).Id,
                            CheckTypeModelsID = val.CheckType
                        };

                        newDb.CheckPointLogTable.Add(model);
                        newDb.SaveChanges();
                    }
                    catch (Exception ooi)
                    {

                    }

                }
            }

            return View();
        }

        public async Task<ActionResult> GetCakeHRUsersID()
        {
            try
            {
                var res = Helper.SendRecieveData<List<UsersInfoViewModel>>.FetchDataAsyncNoToken("https://cake.hr/api/e02bd4c00946efc3b7e8fa9e41859eac9249b063c8c612bde79ee3f1fd83d8619c467286a707000a/people?type=non-terminated", "");

                foreach (var emp in res)
                {
                    if (newDb.Users.Any(a => a.Email == emp.Account.email))
                    {
                        var user = newDb.Users.First(a => a.Email == emp.Account.email);
                        user.userCakeHRNumber = emp.Account.id;
                        newDb.Entry(user).State = System.Data.Entity.EntityState.Modified;
                        newDb.SaveChanges();
                    }
                }

            }
            catch (Exception kk)
            {
                System.Diagnostics.Debug.WriteLine("");
            }

            return View();
        }

        public async Task<ActionResult> MigirateFromWebSiteToMyUsers()
        {       
            using (attendenceschemaEntities pDb = new attendenceschemaEntities())
            {
                try
                {
                    foreach (var val in pDb.aspnetusers.ToList())
                    {
                        ApplicationUser user = new ApplicationUser()
                        {
                            AccessFailedCount = val.AccessFailedCount,
                            BranchesModelID = val.BranchesModelID,
                            dateEnrollDay = null,
                            DepartmentsModelID = val.DepartmentsModelID,
                            Email = val.Email,
                            EmailConfirmed = true,
                            UserName = val.UserName,
                            empBirthDay = null,
                            EmployeeBirthDate = null,
                            EmployeeGender = (Gender)val.EmployeeGender,
                            EmployeeLastLogin = val.EmployeeLastLogin,
                            EmployeeName = val.EmployeeName,
                            EmployeeName_En = val.EmployeeName_En,
                            EmployeePassword = val.EmployeePassword,
                            EmployeeShow = val.EmployeeShow,
                            EnrollNumber = val.EnrollNumber,
                            userCakeHRNumber = val.userCakeHRNumber,
                            SchedulesModelID = val.SchedulesModelID,
                            TeamsModelID = null,
                        };

                        var result = await UserManager.CreateAsync(user, "Hello90#");
                    }

                    foreach (var val in newDb.Users.ToList())
                    {
                        if (val.EmployeeName_En == "Farah Muhsin" || val.EmployeeName_En == "Maryam"
                            || val.EmployeeName_En == "Aya Jamal" || val.EmployeeName_En == "Hind Wael" || val.EmployeeName_En == "Tahira" || val.EmployeeName_En == "Abdalkhalik Hussain")
                        {
                            await UserManager.AddToRoleAsync(val.Id, "مدير النظام");
                        }
                        else
                        {
                            await UserManager.AddToRoleAsync(val.Id, "موظف");
                        }
                    }
                }
                catch(Exception ll)
                {
                    string uu = "";
                }

                return View();
            }
        }

        public ActionResult UserImage()
        {
            foreach(var user in newDb.Users.ToList())
            {
                if(!System.IO.File.Exists(Server.MapPath("~/Content/SiteImage/UsersImage/") + user.Id + "_.jpg"))
                {
                    System.IO.File.Copy(Server.MapPath("~/Content/SiteImage/UsersImage/") + "1.jpg", Server.MapPath("~/Content/SiteImage/UsersImage/") + user.Id + "_.jpg");
                }
            }

            return View();
        }

        public ActionResult userIdMig()
        {
            using(ApplicationDbContext db = new ApplicationDbContext())
            {
                foreach(var chk in db.CheckPointLogTable.ToList())
                {
                    chk.ApplicationUserId = chk.userId;
                    db.Entry(chk).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
        

                return View();
            }
        }
    }
}