namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fhdhfghddrhdrhffhdhfhft : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VacationsModels", "BranchesModelID", c => c.Int(nullable: false));
            CreateIndex("dbo.VacationsModels", "BranchesModelID");
            AddForeignKey("dbo.VacationsModels", "BranchesModelID", "dbo.BranchesModels", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VacationsModels", "BranchesModelID", "dbo.BranchesModels");
            DropIndex("dbo.VacationsModels", new[] { "BranchesModelID" });
            DropColumn("dbo.VacationsModels", "BranchesModelID");
        }
    }
}
