namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ksjlkjdkljslsioio : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.VacationsModels", "toDate", c => c.DateTime(precision: 0));
            AlterColumn("dbo.VacationsModels", "hours", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.VacationsModels", "hours", c => c.Int(nullable: false));
            AlterColumn("dbo.VacationsModels", "toDate", c => c.DateTime(nullable: false, precision: 0));
        }
    }
}
