namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class oipipdoawidpais : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("TeamLeadersModels", "TeamsModelID", "TeamsModels");
            DropIndex("TeamLeadersModels", new[] { "TeamsModelID" });
            AddColumn("TeamsModels", "TeamLeadersModelsID", c => c.Int());
            CreateIndex("TeamsModels", "TeamLeadersModelsID");
            AddForeignKey("TeamsModels", "TeamLeadersModelsID", "TeamLeadersModels", "ID");
            DropColumn("TeamLeadersModels", "TeamsModelID");
        }
        
        public override void Down()
        {
            AddColumn("TeamLeadersModels", "TeamsModelID", c => c.Int());
            DropForeignKey("TeamsModels", "TeamLeadersModelsID", "TeamLeadersModels");
            DropIndex("TeamsModels", new[] { "TeamLeadersModelsID" });
            DropColumn("TeamsModels", "TeamLeadersModelsID");
            CreateIndex("TeamLeadersModels", "TeamsModelID");
            AddForeignKey("TeamLeadersModels", "TeamsModelID", "TeamsModels", "ID");
        }
    }
}
