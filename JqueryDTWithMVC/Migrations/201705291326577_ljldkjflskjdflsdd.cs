namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ljldkjflskjdflsdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CheckPointLogModels", "ApplicationUserId", c => c.String(unicode: false));
            AlterColumn("dbo.CheckPointLogModels", "userId", c => c.String(maxLength: 128, storeType: "nvarchar"));
            CreateIndex("dbo.CheckPointLogModels", "userId");
            AddForeignKey("dbo.CheckPointLogModels", "userId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CheckPointLogModels", "userId", "dbo.AspNetUsers");
            DropIndex("dbo.CheckPointLogModels", new[] { "userId" });
            AlterColumn("dbo.CheckPointLogModels", "userId", c => c.String(unicode: false));
            DropColumn("dbo.CheckPointLogModels", "ApplicationUserId");
        }
    }
}
