namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class icsoiasposd : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("AspNetUsers", "SchedulesModelID", "SchedulesModels");
            DropIndex("AspNetUsers", new[] { "SchedulesModelID" });
            AlterColumn("AspNetUsers", "SchedulesModelID", c => c.Int());
            CreateIndex("AspNetUsers", "SchedulesModelID");
            AddForeignKey("AspNetUsers", "SchedulesModelID", "SchedulesModels", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("AspNetUsers", "SchedulesModelID", "SchedulesModels");
            DropIndex("AspNetUsers", new[] { "SchedulesModelID" });
            AlterColumn("AspNetUsers", "SchedulesModelID", c => c.Int(nullable: false));
            CreateIndex("AspNetUsers", "SchedulesModelID");
            AddForeignKey("AspNetUsers", "SchedulesModelID", "SchedulesModels", "ID", cascadeDelete: true);
        }
    }
}
