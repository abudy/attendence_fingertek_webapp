namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ksllkclkjdlkajdla : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UsersWithTeamsModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TeamsModelID = c.Int(),
                        ApplicationUserId = c.String(maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TeamsModels", t => t.TeamsModelID)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId)
                .Index(t => t.TeamsModelID)
                .Index(t => t.ApplicationUserId);
            
            CreateTable(
                "dbo.TeamsModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        teamName = c.String(nullable: false, maxLength: 30, storeType: "nvarchar"),
                        BranchesModelID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BranchesModels", t => t.BranchesModelID)
                .Index(t => t.BranchesModelID);
            
            CreateTable(
                "dbo.TeamLeadersModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        userId = c.String(nullable: false, unicode: false),
                        userName = c.String(nullable: false, unicode: false),
                        TeamsModelID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TeamsModels", t => t.TeamsModelID)
                .Index(t => t.TeamsModelID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UsersWithTeamsModels", "ApplicationUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UsersWithTeamsModels", "TeamsModelID", "dbo.TeamsModels");
            DropForeignKey("dbo.TeamsModels", "BranchesModelID", "dbo.BranchesModels");
            DropForeignKey("dbo.TeamLeadersModels", "TeamsModelID", "dbo.TeamsModels");
            DropIndex("dbo.TeamLeadersModels", new[] { "TeamsModelID" });
            DropIndex("dbo.TeamsModels", new[] { "BranchesModelID" });
            DropIndex("dbo.UsersWithTeamsModels", new[] { "ApplicationUserId" });
            DropIndex("dbo.UsersWithTeamsModels", new[] { "TeamsModelID" });
            DropTable("dbo.TeamLeadersModels");
            DropTable("dbo.TeamsModels");
            DropTable("dbo.UsersWithTeamsModels");
        }
    }
}
