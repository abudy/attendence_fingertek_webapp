namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class siiodiapdlskldas : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("TeamsModels", "TeamLeadersModelsID", "TeamLeadersModels");
            DropIndex("TeamsModels", new[] { "TeamLeadersModelsID" });
            AddColumn("TeamsModels", "userId", c => c.String(nullable: false, unicode: false));
            AddColumn("TeamsModels", "userName", c => c.String(nullable: false, unicode: false));
            DropColumn("TeamsModels", "TeamLeadersModelsID");
            DropTable("TeamLeadersModels");
        }
        
        public override void Down()
        {
            CreateTable(
                "TeamLeadersModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        userId = c.String(nullable: false, unicode: false),
                        userName = c.String(nullable: false, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("TeamsModels", "TeamLeadersModelsID", c => c.Int());
            DropColumn("TeamsModels", "userName");
            DropColumn("TeamsModels", "userId");
            CreateIndex("TeamsModels", "TeamLeadersModelsID");
            AddForeignKey("TeamsModels", "TeamLeadersModelsID", "TeamLeadersModels", "ID");
        }
    }
}
