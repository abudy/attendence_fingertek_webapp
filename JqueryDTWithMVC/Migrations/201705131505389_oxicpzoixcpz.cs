namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class oxicpzoixcpz : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "branchManager", c => c.Boolean(nullable: false));
            DropColumn("dbo.AspNetUsers", "reportsTo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "reportsTo", c => c.String(unicode: false));
            DropColumn("dbo.AspNetUsers", "branchManager");
        }
    }
}
