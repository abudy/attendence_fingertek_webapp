namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class kafjdlkjfskldjfsdkl : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SideBarItemsModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ControllerName = c.String(unicode: false),
                        ActionMethodName = c.String(unicode: false),
                        Description = c.String(unicode: false),
                        DescriptionEng = c.String(unicode: false),
                        AccessedByRoles = c.String(unicode: false),
                        selectedIcon = c.String(unicode: false),
                        SideBarItemsModelsID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SideBarItemsModels", t => t.SideBarItemsModelsID)
                .Index(t => t.SideBarItemsModelsID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SideBarItemsModels", "SideBarItemsModelsID", "dbo.SideBarItemsModels");
            DropIndex("dbo.SideBarItemsModels", new[] { "SideBarItemsModelsID" });
            DropTable("dbo.SideBarItemsModels");
        }
    }
}
