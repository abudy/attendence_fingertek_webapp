namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lsjcoizxuoczxicxzs : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("UsersWithTeamsModels", "ApplicationUserId", "AspNetUsers");
            DropIndex("UsersWithTeamsModels", new[] { "TeamsModelID" });
            DropIndex("UsersWithTeamsModels", new[] { "ApplicationUserId" });
            AddColumn("AspNetUsers", "TeamsModelID", c => c.Int());
            CreateIndex("AspNetUsers", "TeamsModelID");
            DropTable("UsersWithTeamsModels");
        }
        
        public override void Down()
        {
            CreateTable(
                "UsersWithTeamsModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TeamsModelID = c.Int(),
                        ApplicationUserId = c.String(maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.ID);
            
            DropIndex("AspNetUsers", new[] { "TeamsModelID" });
            DropColumn("AspNetUsers", "TeamsModelID");
            CreateIndex("UsersWithTeamsModels", "ApplicationUserId");
            CreateIndex("UsersWithTeamsModels", "TeamsModelID");
            //AddForeignKey("UsersWithTeamsModels", "ApplicationUserId", "AspNetUsers", "Id");
        }
    }
}
