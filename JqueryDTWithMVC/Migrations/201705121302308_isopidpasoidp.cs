namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class isopidpasoidp : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TeamsModels", "teamType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeamsModels", "teamType");
        }
    }
}
