namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sodaspodipao : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TeamLeadersModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        userId = c.String(nullable: false, unicode: false),
                        userName = c.String(nullable: false, unicode: false),
                        TeamsModelID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TeamsModels", t => t.TeamsModelID)
                .Index(t => t.TeamsModelID);
            
            DropColumn("dbo.TeamsModels", "userId");
            DropColumn("dbo.TeamsModels", "userName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TeamsModels", "userName", c => c.String(nullable: false, unicode: false));
            AddColumn("dbo.TeamsModels", "userId", c => c.String(nullable: false, unicode: false));
            DropForeignKey("dbo.TeamLeadersModels", "TeamsModelID", "dbo.TeamsModels");
            DropIndex("dbo.TeamLeadersModels", new[] { "TeamsModelID" });
            DropTable("dbo.TeamLeadersModels");
        }
    }
}
