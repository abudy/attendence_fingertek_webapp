namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class slkdaspdoaosodo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CountryModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        countryName = c.String(nullable: false, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EmploymentStateModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        title = c.String(nullable: false, unicode: false),
                        state = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MaritalStatusModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        status = c.String(nullable: false, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.NationalityModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        nation = c.String(nullable: false, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.NotesModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        notesString = c.String(nullable: false, unicode: false),
                        ApplicationUserId = c.String(maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId)
                .Index(t => t.ApplicationUserId);
            
            CreateTable(
                "dbo.PositionModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        title = c.String(nullable: false, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AssestsModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AssestsCategoryModelID = c.Int(nullable: false),
                        assestValue = c.Int(nullable: false),
                        assestDesription = c.String(unicode: false),
                        SerialNumber = c.String(unicode: false),
                        dateLoaned = c.DateTime(nullable: false, precision: 0),
                        dateRetunred = c.DateTime(precision: 0),
                        ApplicationUserId = c.String(maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AssestsCategoryModels", t => t.AssestsCategoryModelID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId)
                .Index(t => t.AssestsCategoryModelID)
                .Index(t => t.ApplicationUserId);
            
            CreateTable(
                "dbo.AssestsCategoryModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        categoryName = c.String(nullable: false, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EmergenceyContactModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, unicode: false),
                        RelationShip = c.String(unicode: false),
                        HomePhone = c.String(unicode: false),
                        WorkPhone = c.String(unicode: false),
                        MobilePhone = c.String(unicode: false),
                        Email = c.String(unicode: false),
                        Country = c.String(unicode: false),
                        Address1 = c.String(unicode: false),
                        Address2 = c.String(unicode: false),
                        City = c.String(unicode: false),
                        Region = c.String(unicode: false),
                        PostCode = c.String(unicode: false),
                        ApplicationUserId = c.String(maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId)
                .Index(t => t.ApplicationUserId);
            
            CreateTable(
                "dbo.TitleModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        title = c.String(nullable: false, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.AspNetUsers", "firstName", c => c.String(unicode: false));
            AddColumn("dbo.AspNetUsers", "lastName", c => c.String(unicode: false));
            AddColumn("dbo.AspNetUsers", "perssonalEmail", c => c.String(unicode: false));
            AddColumn("dbo.AspNetUsers", "linkedln", c => c.String(unicode: false));
            AddColumn("dbo.AspNetUsers", "drivingLicense", c => c.String(unicode: false));
            AddColumn("dbo.AspNetUsers", "skype", c => c.String(unicode: false));
            AddColumn("dbo.AspNetUsers", "street_1", c => c.String(unicode: false));
            AddColumn("dbo.AspNetUsers", "street_2", c => c.String(unicode: false));
            AddColumn("dbo.AspNetUsers", "city", c => c.String(unicode: false));
            AddColumn("dbo.AspNetUsers", "state", c => c.String(unicode: false));
            AddColumn("dbo.AspNetUsers", "postCode", c => c.String(unicode: false));
            AddColumn("dbo.AspNetUsers", "homePhone", c => c.String(unicode: false));
            AddColumn("dbo.AspNetUsers", "workPhone", c => c.String(unicode: false));
            AddColumn("dbo.AspNetUsers", "TitleModelID", c => c.Int());
            AddColumn("dbo.AspNetUsers", "EmploymentStateModelID", c => c.Int());
            AddColumn("dbo.AspNetUsers", "PositionModelID", c => c.Int());
            AddColumn("dbo.AspNetUsers", "CountryModelID", c => c.Int());
            AddColumn("dbo.AspNetUsers", "NationalityModelID", c => c.Int());
            AddColumn("dbo.AspNetUsers", "MaritalStatusModelID", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "TitleModelID");
            CreateIndex("dbo.AspNetUsers", "EmploymentStateModelID");
            CreateIndex("dbo.AspNetUsers", "PositionModelID");
            CreateIndex("dbo.AspNetUsers", "CountryModelID");
            CreateIndex("dbo.AspNetUsers", "NationalityModelID");
            CreateIndex("dbo.AspNetUsers", "MaritalStatusModelID");
            AddForeignKey("dbo.AspNetUsers", "CountryModelID", "dbo.CountryModels", "ID");
            AddForeignKey("dbo.AspNetUsers", "EmploymentStateModelID", "dbo.EmploymentStateModels", "ID");
            AddForeignKey("dbo.AspNetUsers", "MaritalStatusModelID", "dbo.MaritalStatusModels", "ID");
            AddForeignKey("dbo.AspNetUsers", "NationalityModelID", "dbo.NationalityModels", "ID");
            AddForeignKey("dbo.AspNetUsers", "PositionModelID", "dbo.PositionModels", "ID");
            AddForeignKey("dbo.AspNetUsers", "TitleModelID", "dbo.TitleModels", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "TitleModelID", "dbo.TitleModels");
            DropForeignKey("dbo.EmergenceyContactModels", "ApplicationUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AssestsModels", "ApplicationUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AssestsModels", "AssestsCategoryModelID", "dbo.AssestsCategoryModels");
            DropForeignKey("dbo.AspNetUsers", "PositionModelID", "dbo.PositionModels");
            DropForeignKey("dbo.NotesModels", "ApplicationUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "NationalityModelID", "dbo.NationalityModels");
            DropForeignKey("dbo.AspNetUsers", "MaritalStatusModelID", "dbo.MaritalStatusModels");
            DropForeignKey("dbo.AspNetUsers", "EmploymentStateModelID", "dbo.EmploymentStateModels");
            DropForeignKey("dbo.AspNetUsers", "CountryModelID", "dbo.CountryModels");
            DropIndex("dbo.EmergenceyContactModels", new[] { "ApplicationUserId" });
            DropIndex("dbo.AssestsModels", new[] { "ApplicationUserId" });
            DropIndex("dbo.AssestsModels", new[] { "AssestsCategoryModelID" });
            DropIndex("dbo.NotesModels", new[] { "ApplicationUserId" });
            DropIndex("dbo.AspNetUsers", new[] { "MaritalStatusModelID" });
            DropIndex("dbo.AspNetUsers", new[] { "NationalityModelID" });
            DropIndex("dbo.AspNetUsers", new[] { "CountryModelID" });
            DropIndex("dbo.AspNetUsers", new[] { "PositionModelID" });
            DropIndex("dbo.AspNetUsers", new[] { "EmploymentStateModelID" });
            DropIndex("dbo.AspNetUsers", new[] { "TitleModelID" });
            DropColumn("dbo.AspNetUsers", "MaritalStatusModelID");
            DropColumn("dbo.AspNetUsers", "NationalityModelID");
            DropColumn("dbo.AspNetUsers", "CountryModelID");
            DropColumn("dbo.AspNetUsers", "PositionModelID");
            DropColumn("dbo.AspNetUsers", "EmploymentStateModelID");
            DropColumn("dbo.AspNetUsers", "TitleModelID");
            DropColumn("dbo.AspNetUsers", "workPhone");
            DropColumn("dbo.AspNetUsers", "homePhone");
            DropColumn("dbo.AspNetUsers", "postCode");
            DropColumn("dbo.AspNetUsers", "state");
            DropColumn("dbo.AspNetUsers", "city");
            DropColumn("dbo.AspNetUsers", "street_2");
            DropColumn("dbo.AspNetUsers", "street_1");
            DropColumn("dbo.AspNetUsers", "skype");
            DropColumn("dbo.AspNetUsers", "drivingLicense");
            DropColumn("dbo.AspNetUsers", "linkedln");
            DropColumn("dbo.AspNetUsers", "perssonalEmail");
            DropColumn("dbo.AspNetUsers", "lastName");
            DropColumn("dbo.AspNetUsers", "firstName");
            DropTable("dbo.TitleModels");
            DropTable("dbo.EmergenceyContactModels");
            DropTable("dbo.AssestsCategoryModels");
            DropTable("dbo.AssestsModels");
            DropTable("dbo.PositionModels");
            DropTable("dbo.NotesModels");
            DropTable("dbo.NationalityModels");
            DropTable("dbo.MaritalStatusModels");
            DropTable("dbo.EmploymentStateModels");
            DropTable("dbo.CountryModels");
        }
    }
}
