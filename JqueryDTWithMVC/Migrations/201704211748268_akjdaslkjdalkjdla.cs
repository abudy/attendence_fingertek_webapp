namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class akjdaslkjdalkjdla : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DocumentsWithUsersModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DocumentsModelsID = c.Int(),
                        ApplicationUserID = c.String(maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DocumentsModels", t => t.DocumentsModelsID)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserID)
                .Index(t => t.DocumentsModelsID)
                .Index(t => t.ApplicationUserID);
            
            CreateTable(
                "dbo.DocumentsModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        filePath = c.String(maxLength: 100, storeType: "nvarchar"),
                        fileName = c.String(maxLength: 100, storeType: "nvarchar"),
                        DocumentsCategoryModelsID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DocumentsCategoryModels", t => t.DocumentsCategoryModelsID, cascadeDelete: true)
                .Index(t => t.DocumentsCategoryModelsID);
            
            CreateTable(
                "dbo.DocumentsCategoryModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        catgeoryName = c.String(nullable: false, maxLength: 100, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DocumentsWithUsersModels", "ApplicationUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.DocumentsWithUsersModels", "DocumentsModelsID", "dbo.DocumentsModels");
            DropForeignKey("dbo.DocumentsModels", "DocumentsCategoryModelsID", "dbo.DocumentsCategoryModels");
            DropIndex("dbo.DocumentsModels", new[] { "DocumentsCategoryModelsID" });
            DropIndex("dbo.DocumentsWithUsersModels", new[] { "ApplicationUserID" });
            DropIndex("dbo.DocumentsWithUsersModels", new[] { "DocumentsModelsID" });
            DropTable("dbo.DocumentsCategoryModels");
            DropTable("dbo.DocumentsModels");
            DropTable("dbo.DocumentsWithUsersModels");
        }
    }
}
