namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fhdhfghddrhdrhf : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VacationsModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        day_part = c.Int(nullable: false),
                        fromDate = c.DateTime(nullable: false, precision: 0),
                        toDate = c.DateTime(nullable: false, precision: 0),
                        hours = c.Int(nullable: false),
                        approvalstate = c.Int(nullable: false),
                        message = c.String(nullable: false, unicode: false),
                        date_requested = c.DateTime(nullable: false, precision: 0),
                        VacationTypeModelID = c.Int(nullable: false),
                        ApplicationUserId = c.String(maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId)
                .ForeignKey("dbo.VacationTypeModels", t => t.VacationTypeModelID, cascadeDelete: true)
                .Index(t => t.VacationTypeModelID)
                .Index(t => t.ApplicationUserId);
            
            CreateTable(
                "dbo.VacationTypeModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        vacationName = c.String(nullable: false, unicode: false),
                        timeOff_Id = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VacationsModels", "VacationTypeModelID", "dbo.VacationTypeModels");
            DropForeignKey("dbo.VacationsModels", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.VacationsModels", new[] { "ApplicationUserId" });
            DropIndex("dbo.VacationsModels", new[] { "VacationTypeModelID" });
            DropTable("dbo.VacationTypeModels");
            DropTable("dbo.VacationsModels");
        }
    }
}
