namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class skjfdkhdkjhdk : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VacationTypeModels", "policyResetEvery", c => c.Int(nullable: false));
            AddColumn("dbo.VacationTypeModels", "balance", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VacationTypeModels", "balance");
            DropColumn("dbo.VacationTypeModels", "policyResetEvery");
        }
    }
}
