namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fhdhfghteryrtugyjg : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("VacationsModels", "BranchesModelID", "BranchesModels");
            DropIndex("VacationsModels", new[] { "BranchesModelID" });
            AlterColumn("VacationsModels", "BranchesModelID", c => c.Int());
            CreateIndex("VacationsModels", "BranchesModelID");
            AddForeignKey("VacationsModels", "BranchesModelID", "BranchesModels", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("VacationsModels", "BranchesModelID", "BranchesModels");
            DropIndex("VacationsModels", new[] { "BranchesModelID" });
            AlterColumn("VacationsModels", "BranchesModelID", c => c.Int(nullable: false));
            CreateIndex("VacationsModels", "BranchesModelID");
            AddForeignKey("VacationsModels", "BranchesModelID", "BranchesModels", "ID", cascadeDelete: true);
        }
    }
}
