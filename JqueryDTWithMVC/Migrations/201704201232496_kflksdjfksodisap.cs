namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class kflksdjfksodisap : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActionCompletionModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        eventDescription = c.String(unicode: false),
                        ApplicationUserID = c.String(maxLength: 128, storeType: "nvarchar"),
                        shown = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserID)
                .Index(t => t.ApplicationUserID);
            
            CreateTable(
                "dbo.NotificationsModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        eventDate = c.DateTime(nullable: false, precision: 0),
                        eventDescription = c.String(unicode: false),
                        madeByName = c.String(unicode: false),
                        madeById = c.String(unicode: false),
                        eventURL = c.String(unicode: false),
                        ApplicationUserID = c.String(maxLength: 128, storeType: "nvarchar"),
                        shown = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserID)
                .Index(t => t.ApplicationUserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NotificationsModels", "ApplicationUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.ActionCompletionModels", "ApplicationUserID", "dbo.AspNetUsers");
            DropIndex("dbo.NotificationsModels", new[] { "ApplicationUserID" });
            DropIndex("dbo.ActionCompletionModels", new[] { "ApplicationUserID" });
            DropTable("dbo.NotificationsModels");
            DropTable("dbo.ActionCompletionModels");
        }
    }
}
