namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class skljdlaskjdlksajd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CheckPointLogModels", "extraTime", c => c.Int());
            AddColumn("dbo.CheckPointSummeryModels", "extraTime", c => c.Double());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CheckPointSummeryModels", "extraTime");
            DropColumn("dbo.CheckPointLogModels", "extraTime");
        }
    }
}
