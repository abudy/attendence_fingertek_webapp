namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dllksajldkjaslkd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "ApplicationUserId", c => c.String(maxLength: 128, storeType: "nvarchar"));
            CreateIndex("dbo.AspNetUsers", "ApplicationUserId");
            AddForeignKey("dbo.AspNetUsers", "ApplicationUserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "ApplicationUserId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetUsers", new[] { "ApplicationUserId" });
            DropColumn("dbo.AspNetUsers", "ApplicationUserId");
        }
    }
}
