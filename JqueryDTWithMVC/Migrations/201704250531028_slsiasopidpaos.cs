namespace JqueryDTWithMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class slsiasopidpaos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "dateEnrollDay", c => c.DateTime(precision: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "dateEnrollDay");
        }
    }
}
