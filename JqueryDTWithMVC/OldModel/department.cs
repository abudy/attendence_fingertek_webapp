namespace JqueryDTWithMVC.OldModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("altameer.departments")]
    public partial class department
    {
        [Key]
        public int depart_id { get; set; }

        public int depart_branch { get; set; }

        [Required]
        [StringLength(120)]
        public string depart_name { get; set; }

        public int depart_manager { get; set; }
    }
}
