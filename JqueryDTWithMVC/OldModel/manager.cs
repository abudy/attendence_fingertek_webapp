namespace JqueryDTWithMVC.OldModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("altameer.managers")]
    public partial class manager
    {
        [Key]
        public int manager_id { get; set; }

        [Required]
        [StringLength(80)]
        public string manager_name { get; set; }

        [Required]
        [StringLength(80)]
        public string manager_email { get; set; }

        [Required]
        [StringLength(80)]
        public string manager_password { get; set; }

        public int manager_gender { get; set; }

        [Required]
        [StringLength(26)]
        public string manager_lastLogin { get; set; }

        [Required]
        [StringLength(26)]
        public string manager_lastaction { get; set; }

        public int manager_enable { get; set; }

        public int manager_full { get; set; }

        public int manager_addemp { get; set; }

        public int manager_edtemp { get; set; }

        public int manager_viwemp { get; set; }

        public int manager_syncemp { get; set; }

        public int manager_delemp { get; set; }

        public int manager_addbr { get; set; }

        public int manager_edtbr { get; set; }

        public int manager_viwbr { get; set; }

        public int manager_delbr { get; set; }

        public int manager_adddep { get; set; }

        public int manager_edtdep { get; set; }

        public int manager_deldep { get; set; }

        public int manager_viwdep { get; set; }

        public int manager_addpoint { get; set; }

        public int manager_edtpoint { get; set; }

        public int manager_calpoint { get; set; }

        public int manager_syncpoint { get; set; }

        public int manager_delpoint { get; set; }

        public int manager_imppoint { get; set; }

        public int manager_viwpoint { get; set; }

        public int manager_viwtime { get; set; }

        public int manager_viwreport { get; set; }

        public int manager_addman { get; set; }

        public int manager_edtman { get; set; }

        public int manager_edtmanpriv { get; set; }

        public int manager_delman { get; set; }

        public int manager_viwman { get; set; }

        public int manager_deldevicepoint { get; set; }
    }
}
