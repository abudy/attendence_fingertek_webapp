namespace JqueryDTWithMVC.OldModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("altameer.checkpoints")]
    public partial class checkpoint
    {
        [Key]
        public int CheckID { get; set; }

        public int CheckEmpID { get; set; }

        [Required]
        [StringLength(10)]
        public string CheckDate { get; set; }

        [Required]
        [StringLength(8)]
        public string CheckTimeIn { get; set; }

        [Required]
        [StringLength(8)]
        public string CheckTimeOut { get; set; }

        public int CheckType { get; set; }

        public int CheckBranch { get; set; }
    }
}
