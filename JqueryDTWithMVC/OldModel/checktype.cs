namespace JqueryDTWithMVC.OldModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("altameer.checktype")]
    public partial class checktype
    {
        [Key]
        public int CheckID { get; set; }

        [Required]
        [StringLength(30)]
        public string CheckName { get; set; }
    }
}
