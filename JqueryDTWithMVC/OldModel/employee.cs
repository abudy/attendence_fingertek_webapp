namespace JqueryDTWithMVC.OldModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("altameer.employees")]
    public partial class employee
    {
        [Key]
        public int EmpID { get; set; }

        public int EmployeeID { get; set; }

        [Required]
        [StringLength(200)]
        public string EmployeeEName { get; set; }

        [Required]
        [StringLength(200)]
        public string EmployeeAName { get; set; }

        public int EmployeeDepart { get; set; }

        [Required]
        [StringLength(60)]
        public string EmployeeEmail { get; set; }

        [Required]
        [StringLength(60)]
        public string EmployeePassword { get; set; }

        public int EmployeeGender { get; set; }

        [Required]
        [StringLength(12)]
        public string EmployeeBirthDate { get; set; }

        [Required]
        [StringLength(26)]
        public string EmployeeLastLogin { get; set; }

        public int EmployeeBranch { get; set; }

        public int EmployeeSchedule { get; set; }

        public int EmployeeEnable { get; set; }

        public int EmployeeShow { get; set; }

        public int EmployeeIsManager { get; set; }
    }
}
