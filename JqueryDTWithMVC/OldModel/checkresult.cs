namespace JqueryDTWithMVC.OldModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("altameer.checkresult")]
    public partial class checkresult
    {
        [Key]
        public int CheckRID { get; set; }

        public int CheckRPID { get; set; }

        public int CheckRType { get; set; }

        [Required]
        [StringLength(6)]
        public string CheckRLength { get; set; }

        [Required]
        [StringLength(10)]
        public string CheckRDate { get; set; }

        public int CheckRFID { get; set; }

        public int CheckRBranch { get; set; }
    }
}
