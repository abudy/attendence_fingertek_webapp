namespace JqueryDTWithMVC.OldModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("altameer.branches")]
    public partial class branch
    {
        [Key]
        public int branch_id { get; set; }

        [Required]
        [StringLength(80)]
        public string branch_name { get; set; }

        [Required]
        [StringLength(16)]
        public string branch_ip { get; set; }

        public int branch_port { get; set; }

        public int branch_key { get; set; }

        [Required]
        [StringLength(80)]
        public string branch_location { get; set; }
    }
}
