namespace JqueryDTWithMVC.OldModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("altameer.reports")]
    public partial class report
    {
        [Key]
        public int report_id { get; set; }

        [Required]
        [StringLength(10)]
        public string report_from { get; set; }

        [Required]
        [StringLength(10)]
        public string report_to { get; set; }

        public int report_cdate { get; set; }
    }
}
