namespace JqueryDTWithMVC.OldModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class OldModel : DbContext
    {
        public OldModel()
            : base("name=OldModel")
        {
        }

        public virtual DbSet<branch> branches { get; set; }
        public virtual DbSet<checkpoint> checkpoints { get; set; }
        public virtual DbSet<checkresult> checkresults { get; set; }
        public virtual DbSet<checktype> checktypes { get; set; }
        public virtual DbSet<department> departments { get; set; }
        public virtual DbSet<employee> employees { get; set; }
        public virtual DbSet<manager> managers { get; set; }
        public virtual DbSet<report> reports { get; set; }
        public virtual DbSet<schedule> schedules { get; set; }
        public virtual DbSet<setting> settings { get; set; }
        public virtual DbSet<systemlog> systemlogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<branch>()
                .Property(e => e.branch_name)
                .IsUnicode(false);

            modelBuilder.Entity<branch>()
                .Property(e => e.branch_ip)
                .IsUnicode(false);

            modelBuilder.Entity<branch>()
                .Property(e => e.branch_location)
                .IsUnicode(false);

            modelBuilder.Entity<checkpoint>()
                .Property(e => e.CheckDate)
                .IsUnicode(false);

            modelBuilder.Entity<checkpoint>()
                .Property(e => e.CheckTimeIn)
                .IsUnicode(false);

            modelBuilder.Entity<checkpoint>()
                .Property(e => e.CheckTimeOut)
                .IsUnicode(false);

            modelBuilder.Entity<checkresult>()
                .Property(e => e.CheckRLength)
                .IsUnicode(false);

            modelBuilder.Entity<checkresult>()
                .Property(e => e.CheckRDate)
                .IsUnicode(false);

            modelBuilder.Entity<checktype>()
                .Property(e => e.CheckName)
                .IsUnicode(false);

            modelBuilder.Entity<department>()
                .Property(e => e.depart_name)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.EmployeeEName)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.EmployeeAName)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.EmployeeEmail)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.EmployeePassword)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.EmployeeBirthDate)
                .IsUnicode(false);

            modelBuilder.Entity<employee>()
                .Property(e => e.EmployeeLastLogin)
                .IsUnicode(false);

            modelBuilder.Entity<manager>()
                .Property(e => e.manager_name)
                .IsUnicode(false);

            modelBuilder.Entity<manager>()
                .Property(e => e.manager_email)
                .IsUnicode(false);

            modelBuilder.Entity<manager>()
                .Property(e => e.manager_password)
                .IsUnicode(false);

            modelBuilder.Entity<manager>()
                .Property(e => e.manager_lastLogin)
                .IsUnicode(false);

            modelBuilder.Entity<manager>()
                .Property(e => e.manager_lastaction)
                .IsUnicode(false);

            modelBuilder.Entity<report>()
                .Property(e => e.report_from)
                .IsUnicode(false);

            modelBuilder.Entity<report>()
                .Property(e => e.report_to)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_name)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_allowtime)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_saturday)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_saturday2)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_sunday)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_sunday2)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_monday)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_monday2)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_tuesday)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_tuesday2)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_wednesday)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_wednesday2)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_thursday)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_thursday2)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_friday)
                .IsUnicode(false);

            modelBuilder.Entity<schedule>()
                .Property(e => e.schedule_friday2)
                .IsUnicode(false);

            modelBuilder.Entity<setting>()
                .Property(e => e.SettingKey)
                .IsUnicode(false);

            modelBuilder.Entity<setting>()
                .Property(e => e.SettingVal)
                .IsUnicode(false);

            modelBuilder.Entity<systemlog>()
                .Property(e => e.sys_action)
                .IsUnicode(false);

            modelBuilder.Entity<systemlog>()
                .Property(e => e.sys_date)
                .IsUnicode(false);
        }
    }
}
