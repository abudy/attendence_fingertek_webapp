namespace JqueryDTWithMVC.OldModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("altameer.schedules")]
    public partial class schedule
    {
        [Key]
        public int schedule_id { get; set; }

        [Required]
        [StringLength(60)]
        public string schedule_name { get; set; }

        [Required]
        [StringLength(100)]
        public string schedule_allowtime { get; set; }

        [Required]
        [StringLength(10)]
        public string schedule_saturday { get; set; }

        [Required]
        [StringLength(10)]
        public string schedule_saturday2 { get; set; }

        [Required]
        [StringLength(10)]
        public string schedule_sunday { get; set; }

        [Required]
        [StringLength(10)]
        public string schedule_sunday2 { get; set; }

        [Required]
        [StringLength(10)]
        public string schedule_monday { get; set; }

        [Required]
        [StringLength(10)]
        public string schedule_monday2 { get; set; }

        [Required]
        [StringLength(10)]
        public string schedule_tuesday { get; set; }

        [Required]
        [StringLength(10)]
        public string schedule_tuesday2 { get; set; }

        [Required]
        [StringLength(10)]
        public string schedule_wednesday { get; set; }

        [Required]
        [StringLength(10)]
        public string schedule_wednesday2 { get; set; }

        [Required]
        [StringLength(10)]
        public string schedule_thursday { get; set; }

        [Required]
        [StringLength(10)]
        public string schedule_thursday2 { get; set; }

        [Required]
        [StringLength(10)]
        public string schedule_friday { get; set; }

        [Required]
        [StringLength(10)]
        public string schedule_friday2 { get; set; }
    }
}
