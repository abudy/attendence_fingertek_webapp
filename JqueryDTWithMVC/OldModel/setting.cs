namespace JqueryDTWithMVC.OldModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("altameer.settings")]
    public partial class setting
    {
        public int id { get; set; }

        [Required]
        [StringLength(30)]
        public string SettingKey { get; set; }

        [Required]
        [StringLength(100)]
        public string SettingVal { get; set; }
    }
}
