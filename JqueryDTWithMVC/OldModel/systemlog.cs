namespace JqueryDTWithMVC.OldModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("altameer.systemlog")]
    public partial class systemlog
    {
        [Key]
        public int sys_id { get; set; }

        public int? sys_ManagerID { get; set; }

        [StringLength(60)]
        public string sys_action { get; set; }

        [StringLength(20)]
        public string sys_date { get; set; }
    }
}
