﻿
using JqueryDTWithMVC.Helper;
using JqueryDTWithMVC.Helpers;
using JqueryDTWithMVC.Models;
using JqueryDTWithMVC.Models.ViewModels.DevicePoint;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;


namespace JqueryDTWithMVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
     //   public static System.Timers.Timer timer = new System.Timers.Timer(30 * 60 * 1000);
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //timer.AutoReset = true;
            //timer.Enabled = true;
            //timer.Elapsed += Timer_Elapsed;
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                foreach (var branch in db.BranchesTable.ToList())
                {
                    if (!Helper.PingHost.PingFPDHost(branch.branch_ip))
                    {
                        continue;
                    }

                    List<DevicePointViewModel> log = new List<DevicePointViewModel>();
                    var t = new Thread(() => log = SyncPointsFromDevices.CallSDKForSpecificDeviceWithTimer(branch.ID,System.DateTime.Today.Date.AddDays(-1), System.DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59)));

                    t.SetApartmentState(ApartmentState.STA);
                    t.Start();
                    t.Join();

                    if (log.Count > 0)
                    {
                        SyncPointsFromDevices.SendToDBFast(log);   
                        SyncPointsFromDevices.GetUserAbsenceFast(System.DateTime.Today.Date.AddDays(-1), System.DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59), branch.ID, db.Users.Where(b => b.BranchesModelID.HasValue ? b.BranchesModelID.Value == branch.ID : false).ToList());
                        SyncPointsFromDevices.ComputeCakeHR(System.DateTime.Today.Date.AddDays(-1), System.DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59), branch.ID, db.Users.Where(b => b.BranchesModelID.HasValue ? b.BranchesModelID.Value == branch.ID : false).ToList());
                    }
                }
            }
        }
    }
}
