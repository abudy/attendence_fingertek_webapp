﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;
using JqueryDTWithMVC.Models;
using System.Web.Mvc;
using System.Linq;
using System.Web.Routing;
using System.Diagnostics;
using JqueryDTWithMVC.Models.DAL.ActivityLog;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web;
using System.Security.Principal;
using System.Threading;
using JqueryDTWithMVC.Models.ViewModels.UserInfoFromDevice;
using System.Collections.Generic;
using System.Net.Http;
using JqueryDTWithMVC.Models.DAL.ActionCompletion;
using JqueryDTWithMVC.Models.Repository.UnitOfWork;

namespace JqueryDTWithMVC
{
    public class UserAwareViewPage : System.Web.Mvc.WebViewPage
    {
        public IPrincipal User { get { return Thread.CurrentPrincipal; } }

        public override void Execute()
        {

        }
    }

    public partial class Startup
    {        
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            //app.UseFacebookAuthentication(
            //   appId: "",
            //   appSecret: "");

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "",
            //    ClientSecret = ""
            //});
        }

        public class ModifiedAuthorizeAttribute : AuthorizeAttribute
        {
            protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
            {
                if(httpContext.User.IsInRole("مدير النظام"))
                {
                    return true;
                }

                using (var db = new ApplicationDbContext())
                {
                    if (!httpContext.User.Identity.IsAuthenticated)
                    {
                        return false;
                    }

                    string id = httpContext.User.Identity.GetUserId();
                    if (!db.Users.Any(aa => aa.Id == id))
                    {
                        return false;
                    }

                    string controller = httpContext.Request.RequestContext.RouteData.GetRequiredString("controller") + "Controller";
                    string action = httpContext.Request.RequestContext.RouteData.GetRequiredString("action");
                    bool authorize = false;
                    var user = db.Users.Find(httpContext.User.Identity.GetUserId());

                    // check if action method and controller exist in DB
                    if (db.AccessControlTable.Any(a => a.controllerName == controller && a.actionMethodName == action))
                    {

                        var obj = db.AccessControlTable.First(a => a.controllerName == controller && a.actionMethodName == action);

                        // check user if it allowed to access the method
                        if (obj.allowedUsers != null)
                        {
                            // adding , is very crucial to avoid users with part name of the authorized user:  haider.a and haider
                            if (obj.allowedUsers.Contains("," + httpContext.User.Identity.GetUserName()) || obj.allowedUsers.Contains(httpContext.User.Identity.GetUserName() + ","))
                            {
                                return true;
                            }
                            else if (!obj.allowedUsers.Contains(",") && obj.allowedUsers.Contains(httpContext.User.Identity.GetUserName()))
                            {
                                return true;
                            }
                        }

                        if (String.IsNullOrEmpty(obj.preventedRoles))
                        {
                            // now check the roles

                            foreach (var x in db.Roles.Where(b => b.Users.Any(a => a.UserId == user.Id)).Select(a => a.Name))
                            {
                                if (obj.allowRoles != null)
                                {
                                    if (obj.allowRoles.Contains("," + x) || obj.allowRoles.Contains(x + ","))
                                    {
                                        return true;
                                    }
                                    else if (!obj.allowRoles.Contains(",") && obj.allowRoles.Contains(x))
                                    {
                                        return true;
                                    }
                                }

                            }

                        }
                        else
                        {
                            authorize = true;
                            foreach (var x in db.Roles.Where(b => b.Users.Any(a => a.UserId == user.Id)).Select(a => a.Name))
                            {
                                if (obj.preventedRoles != null)
                                {
                                    if (obj.preventedRoles.Contains("," + x) || obj.preventedRoles.Contains(x + ","))
                                    {
                                        return false;
                                    }
                                    else if (!obj.preventedRoles.Contains(",") && obj.preventedRoles.Contains(x))
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                    }

                    return authorize;
                }

            }
            protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        // track logging activity
        public class LogAttribute : ActionFilterAttribute
        {
            protected DateTime start_time;
            public ActionType typeOfAction { get; set; }
            public string actionDescription { get; set; }


            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                start_time = DateTime.Now;
            }

            public override void OnResultExecuted(ResultExecutedContext filterContext)
            {
                try
                {
                    RouteData route_data = filterContext.RouteData;
                    TimeSpan duration = (DateTime.Now - start_time);
                    string controller = (string)route_data.Values["controller"];
                    string action = (string)route_data.Values["action"];
                    string log = ""; int i = 0;
                    foreach (var xx in filterContext.Controller.ViewData.ModelState.Values)
                    {
                        log += filterContext.Controller.ViewData.ModelState.Keys.Skip(i).Take(1).First() + " = " + xx.Value.AttemptedValue + ", ";
                        i = i + 1;
                    }
                    //Save all your required values, including user id and whatnot here.
                    //The duration variable will allow you to see expensive page loads on the controller, this can be useful when clients complain about something being slow.

                    var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));// Get the current logged in User and look up the user in ASP.NET Identity
                    var currentUser = manager.FindById(HttpContext.Current.User.Identity.GetUserId());

                    ApplicationDbContext db = new ApplicationDbContext();
                    ActivityLogModel x = new ActivityLogModel
                    {
                        actionDescription = actionDescription,
                        actionType = typeOfAction,
                        activityTime = System.DateTime.Now,
                        controller = controller,
                        actionresult = "/" + controller + "/" + action,
                        valueLog = log,
                        duration = duration.Seconds,
                        userId = currentUser.Id,
                        userName = currentUser.UserName
                    };
                    db.ActivityLogTable.Add(x);
                    db.SaveChangesAsync();
                }
                catch
                {
                    Debug.WriteLine("errorrrrrrrrrrrrrrrrrrrrrrrrr");
                }
            }
        }

        public class TitleAttribute : ActionFilterAttribute
        {
            public string Title { get; set; }
        }

        public static class MiscellaneousMethod
        {
            public static void ActionCompletionNotify(string description, bool shown = false)
            {
                using (ApplicationDbContext unit = new ApplicationDbContext())
                {
                    string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
                    ActionCompletionModel action = new ActionCompletionModel()
                    {
                        ApplicationUserID = userId,
                        eventDescription = description,
                        shown = shown
                    };

                    unit.ActionCompletionTable.Add(action);
                    unit.SaveChanges();
                }
            }
        }
    }
}