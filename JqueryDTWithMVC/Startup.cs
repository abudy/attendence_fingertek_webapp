﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(JqueryDTWithMVC.Startup))]
namespace JqueryDTWithMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
