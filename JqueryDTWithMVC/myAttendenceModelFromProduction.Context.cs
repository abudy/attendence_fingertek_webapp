﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JqueryDTWithMVC
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class attendenceschemaEntities : DbContext
    {
        public attendenceschemaEntities()
            : base("name=attendenceschemaEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<C__migrationhistory> C__migrationhistory { get; set; }
        public virtual DbSet<accesscontrolmodel> accesscontrolmodels { get; set; }
        public virtual DbSet<actioncompletionmodel> actioncompletionmodels { get; set; }
        public virtual DbSet<activitylogmodel> activitylogmodels { get; set; }
        public virtual DbSet<aspnetrole> aspnetroles { get; set; }
        public virtual DbSet<aspnetuserclaim> aspnetuserclaims { get; set; }
        public virtual DbSet<aspnetuserlogin> aspnetuserlogins { get; set; }
        public virtual DbSet<aspnetuser> aspnetusers { get; set; }
        public virtual DbSet<branchesmodel> branchesmodels { get; set; }
        public virtual DbSet<checkpointlogmodel> checkpointlogmodels { get; set; }
        public virtual DbSet<checkpointsummerymodel> checkpointsummerymodels { get; set; }
        public virtual DbSet<checktypemodel> checktypemodels { get; set; }
        public virtual DbSet<departmentsmodel> departmentsmodels { get; set; }
        public virtual DbSet<documentscategorymodel> documentscategorymodels { get; set; }
        public virtual DbSet<documentsmodel> documentsmodels { get; set; }
        public virtual DbSet<documentswithusersmodel> documentswithusersmodels { get; set; }
        public virtual DbSet<notificationsmodel> notificationsmodels { get; set; }
        public virtual DbSet<schedulesmodel> schedulesmodels { get; set; }
        public virtual DbSet<settingsmodel> settingsmodels { get; set; }
        public virtual DbSet<sidebaritemsmodel> sidebaritemsmodels { get; set; }
        public virtual DbSet<teamsmodel> teamsmodels { get; set; }
        public virtual DbSet<vacationsmodel> vacationsmodels { get; set; }
        public virtual DbSet<vacationtypemodel> vacationtypemodels { get; set; }
    }
}
