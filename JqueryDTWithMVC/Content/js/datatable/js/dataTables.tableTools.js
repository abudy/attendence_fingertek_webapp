var TableTools;
!function (e, t, s) {
    var n = function (n, o) {
        var l = {
            version: "1.0.4-TableTools2", clients: {}, moviePath: "", nextId: 1, $: function (e) {
                return "string" == typeof e && (e = t.getElementById(e)), e.addClass || (e.hide = function () {
                    this.style.display = "none"
                }, e.show = function () {
                    this.style.display = ""
                }, e.addClass = function (e) {
                    this.removeClass(e), this.className += " " + e
                }, e.removeClass = function (e) {
                    this.className = this.className.replace(new RegExp("\\s*" + e + "\\s*"), " ").replace(/^\s+/, "").replace(/\s+$/, "")
                }, e.hasClass = function (e) {
                    return !!this.className.match(new RegExp("\\s*" + e + "\\s*"))
                }), e
            }, setMoviePath: function (e) {
                this.moviePath = e
            }, dispatch: function (e, t, s) {
                (e = this.clients[e]) && e.receiveEvent(t, s)
            }, register: function (e, t) {
                this.clients[e] = t
            }, getDOMObjectPosition: function (e) {
                var t = {
                    left: 0,
                    top: 0,
                    width: e.width ? e.width : e.offsetWidth,
                    height: e.height ? e.height : e.offsetHeight
                };
                for ("" !== e.style.width && (t.width = e.style.width.replace("px", "")), "" !== e.style.height && (t.height = e.style.height.replace("px", "")); e;)t.left += e.offsetLeft, t.top += e.offsetTop, e = e.offsetParent;
                return t
            }, Client: function (e) {
                this.handlers = {}, this.id = l.nextId++, this.movieId = "ZeroClipboard_TableToolsMovie_" + this.id, l.register(this.id, this), e && this.glue(e)
            }
        };
        return l.Client.prototype = {
            id: 0,
            ready: !1,
            movie: null,
            clipText: "",
            fileName: "",
            action: "copy",
            handCursorEnabled: !0,
            cssEffects: !0,
            handlers: null,
            sized: !1,
            glue: function (e, s) {
                this.domElement = l.$(e);
                var n = 99;
                this.domElement.style.zIndex && (n = parseInt(this.domElement.style.zIndex, 10) + 1);
                var o = l.getDOMObjectPosition(this.domElement);
                this.div = t.createElement("div");
                var i = this.div.style;
                i.position = "absolute", i.left = "0px", i.top = "0px", i.width = o.width + "px", i.height = o.height + "px", i.zIndex = n, "undefined" != typeof s && "" !== s && (this.div.title = s), 0 !== o.width && 0 !== o.height && (this.sized = !0), this.domElement && (this.domElement.appendChild(this.div), this.div.innerHTML = this.getHTML(o.width, o.height).replace(/&/g, "&"))
            },
            positionElement: function () {
                var e = l.getDOMObjectPosition(this.domElement), t = this.div.style;
                t.position = "absolute", t.width = e.width + "px", t.height = e.height + "px", 0 !== e.width && 0 !== e.height && (this.sized = !0, t = this.div.childNodes[0], t.width = e.width, t.height = e.height)
            },
            getHTML: function (e, t) {
                var s = "", n = "id=" + this.id + "&width=" + e + "&height=" + t;
                if (navigator.userAgent.match(/MSIE/))var o = location.href.match(/^https/i) ? "https://" : "http://", s = s + ('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="' + o + 'download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,0,0" width="' + e + '" height="' + t + '" id="' + this.movieId + '" align="middle"><param name="allowScriptAccess" value="always" /><param name="allowFullScreen" value="false" /><param name="movie" value="' + l.moviePath + '" /><param name="loop" value="false" /><param name="menu" value="false" /><param name="quality" value="best" /><param name="bgcolor" value="#ffffff" /><param name="flashvars" value="' + n + '"/><param name="wmode" value="transparent"/></object>'); else s += '<embed id="' + this.movieId + '" src="' + l.moviePath + '" loop="false" menu="false" quality="best" bgcolor="#ffffff" width="' + e + '" height="' + t + '" name="' + this.movieId + '" align="middle" allowScriptAccess="always" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="' + n + '" wmode="transparent" />';
                return s
            },
            hide: function () {
                this.div && (this.div.style.left = "-2000px")
            },
            show: function () {
                this.reposition()
            },
            destroy: function () {
                if (this.domElement && this.div) {
                    this.hide(), this.div.innerHTML = "";
                    var e = t.getElementsByTagName("body")[0];
                    try {
                        e.removeChild(this.div)
                    } catch (s) {
                    }
                    this.div = this.domElement = null
                }
            },
            reposition: function (e) {
                if (e && ((this.domElement = l.$(e)) || this.hide()), this.domElement && this.div) {
                    e = l.getDOMObjectPosition(this.domElement);
                    var t = this.div.style;
                    t.left = "" + e.left + "px", t.top = "" + e.top + "px"
                }
            },
            clearText: function () {
                this.clipText = "", this.ready && this.movie.clearText()
            },
            appendText: function (e) {
                this.clipText += e, this.ready && this.movie.appendText(e)
            },
            setText: function (e) {
                this.clipText = e, this.ready && this.movie.setText(e)
            },
            setCharSet: function (e) {
                this.charSet = e, this.ready && this.movie.setCharSet(e)
            },
            setBomInc: function (e) {
                this.incBom = e, this.ready && this.movie.setBomInc(e)
            },
            setFileName: function (e) {
                this.fileName = e, this.ready && this.movie.setFileName(e)
            },
            setAction: function (e) {
                this.action = e, this.ready && this.movie.setAction(e)
            },
            addEventListener: function (e, t) {
                e = e.toString().toLowerCase().replace(/^on/, ""), this.handlers[e] || (this.handlers[e] = []), this.handlers[e].push(t)
            },
            setHandCursor: function (e) {
                this.handCursorEnabled = e, this.ready && this.movie.setHandCursor(e)
            },
            setCSSEffects: function (e) {
                this.cssEffects = !!e
            },
            receiveEvent: function (s, n) {
                var o;
                switch (s = s.toString().toLowerCase().replace(/^on/, "")) {
                    case"load":
                        if (this.movie = t.getElementById(this.movieId), !this.movie)return o = this, void setTimeout(function () {
                            o.receiveEvent("load", null)
                        }, 1);
                        if (!this.ready && navigator.userAgent.match(/Firefox/) && navigator.userAgent.match(/Windows/))return o = this, setTimeout(function () {
                            o.receiveEvent("load", null)
                        }, 100), void(this.ready = !0);
                        this.ready = !0, this.movie.clearText(), this.movie.appendText(this.clipText), this.movie.setFileName(this.fileName), this.movie.setAction(this.action), this.movie.setCharSet(this.charSet), this.movie.setBomInc(this.incBom), this.movie.setHandCursor(this.handCursorEnabled);
                        break;
                    case"mouseover":
                        this.domElement && this.cssEffects && this.recoverActive && this.domElement.addClass("active");
                        break;
                    case"mouseout":
                        this.domElement && this.cssEffects && (this.recoverActive = !1, this.domElement.hasClass("active") && (this.domElement.removeClass("active"), this.recoverActive = !0));
                        break;
                    case"mousedown":
                        this.domElement && this.cssEffects && this.domElement.addClass("active");
                        break;
                    case"mouseup":
                        this.domElement && this.cssEffects && (this.domElement.removeClass("active"), this.recoverActive = !1)
                }
                if (this.handlers[s])for (var l = 0, i = this.handlers[s].length; i > l; l++) {
                    var a = this.handlers[s][l];
                    "function" == typeof a ? a(this, n) : "object" == typeof a && 2 == a.length ? a[0][a[1]](this, n) : "string" == typeof a && e[a](this, n)
                }
            }
        }, e.ZeroClipboard_TableTools = l, function (e, t, n) {
            TableTools = function (t, s) {
                return !this instanceof TableTools && alert("Warning: TableTools must be initialised with the keyword 'new'"), this.s = {
                    that: this,
                    dt: e.fn.dataTable.Api ? new e.fn.dataTable.Api(t).settings()[0] : t.fnSettings(),
                    print: {
                        saveStart: -1, saveLength: -1, saveScroll: -1, funcEnd: function () {
                        }
                    },
                    buttonCounter: 0,
                    select: {
                        type: "",
                        selected: [],
                        preRowSelect: null,
                        postSelected: null,
                        postDeselected: null,
                        all: !1,
                        selectedClass: ""
                    },
                    custom: {},
                    swfPath: "",
                    buttonSet: [],
                    master: !1,
                    tags: {}
                }, this.dom = {
                    container: null,
                    table: null,
                    print: {hidden: [], message: null},
                    collection: {collection: null, background: null}
                }, this.classes = e.extend(!0, {}, TableTools.classes), this.s.dt.bJUI && e.extend(!0, this.classes, TableTools.classes_themeroller), this.fnSettings = function () {
                    return this.s
                }, "undefined" == typeof s && (s = {}), TableTools._aInstances.push(this), this._fnConstruct(s), this
            }, TableTools.prototype = {
                fnGetSelected: function (e) {
                    var t, s = [], n = this.s.dt.aoData, o = this.s.dt.aiDisplay;
                    if (e)for (e = 0, t = o.length; t > e; e++)n[o[e]]._DTTT_selected && s.push(n[o[e]].nTr); else for (e = 0, t = n.length; t > e; e++)n[e]._DTTT_selected && s.push(n[e].nTr);
                    return s
                }, fnGetSelectedData: function () {
                    var e, t, s = [], n = this.s.dt.aoData;
                    for (e = 0, t = n.length; t > e; e++)n[e]._DTTT_selected && s.push(this.s.dt.oInstance.fnGetData(e));
                    return s
                }, fnGetSelectedIndexes: function (e) {
                    var t, s = [], n = this.s.dt.aoData, o = this.s.dt.aiDisplay;
                    if (e)for (e = 0, t = o.length; t > e; e++)n[o[e]]._DTTT_selected && s.push(o[e]); else for (e = 0, t = n.length; t > e; e++)n[e]._DTTT_selected && s.push(e);
                    return s
                }, fnIsSelected: function (e) {
                    return e = this.s.dt.oInstance.fnGetPosition(e), !0 === this.s.dt.aoData[e]._DTTT_selected ? !0 : !1
                }, fnSelectAll: function (e) {
                    this._fnRowSelect(e ? this.s.dt.aiDisplay : this.s.dt.aoData)
                }, fnSelectNone: function (e) {
                    this._fnRowDeselect(this.fnGetSelectedIndexes(e))
                }, fnSelect: function (e) {
                    "single" == this.s.select.type && this.fnSelectNone(), this._fnRowSelect(e)
                }, fnDeselect: function (e) {
                    this._fnRowDeselect(e)
                }, fnGetTitle: function (e) {
                    var t = "";
                    return "undefined" != typeof e.sTitle && "" !== e.sTitle ? t = e.sTitle : (e = n.getElementsByTagName("title"), 0 < e.length && (t = e[0].innerHTML)), 4 > "?".toString().length ? t.replace(/[^a-zA-Z0-9_\u00A1-\uFFFF\.,\-_ !\(\)]/g, "") : t.replace(/[^a-zA-Z0-9_\.,\-_ !\(\)]/g, "")
                }, fnCalcColRatios: function (e) {
                    var t = this.s.dt.aoColumns;
                    e = this._fnColumnTargets(e.mColumns);
                    var s, n, o = [], l = 0, i = 0;
                    for (s = 0, n = e.length; n > s; s++)e[s] && (l = t[s].nTh.offsetWidth, i += l, o.push(l));
                    for (s = 0, n = o.length; n > s; s++)o[s] /= i;
                    return o.join("	")
                }, fnGetTableData: function (e) {
                    return this.s.dt ? this._fnGetDataTablesData(e) : void 0
                }, fnSetText: function (e, t) {
                    this._fnFlashSetText(e, t)
                }, fnResizeButtons: function () {
                    for (var e in l.clients)if (e) {
                        var t = l.clients[e];
                        "undefined" != typeof t.domElement && t.domElement.parentNode && t.positionElement()
                    }
                }, fnResizeRequired: function () {
                    for (var e in l.clients)if (e) {
                        var t = l.clients[e];
                        if ("undefined" != typeof t.domElement && t.domElement.parentNode == this.dom.container && !1 === t.sized)return !0
                    }
                    return !1
                }, fnPrint: function (e, t) {
                    t === s && (t = {}), e === s || e ? this._fnPrintStart(t) : this._fnPrintEnd()
                }, fnInfo: function (t, s) {
                    var n = e("<div/>").addClass(this.classes.print.info).html(t).appendTo("body");
                    setTimeout(function () {
                        n.fadeOut("normal", function () {
                            n.remove()
                        })
                    }, s)
                }, fnContainer: function () {
                    return this.dom.container
                }, _fnConstruct: function (t) {
                    var s = this;
                    this._fnCustomiseSettings(t), this.dom.container = n.createElement(this.s.tags.container), this.dom.container.className = this.classes.container, "none" != this.s.select.type && this._fnRowSelectConfig(), this._fnButtonDefinations(this.s.buttonSet, this.dom.container), this.s.dt.aoDestroyCallback.push({
                        sName: "TableTools",
                        fn: function () {
                            e(s.s.dt.nTBody).off("click.DTTT_Select", s.s.custom.sRowSelector).off("mousedown.DTTT_Select", "tr").off("mouseup.DTTT_Select", "tr"), e(s.dom.container).empty();
                            var t = e.inArray(s, TableTools._aInstances);
                            -1 !== t && TableTools._aInstances.splice(t, 1)
                        }
                    })
                }, _fnCustomiseSettings: function (t) {
                    "undefined" == typeof this.s.dt._TableToolsInit && (this.s.master = !0, this.s.dt._TableToolsInit = !0), this.dom.table = this.s.dt.nTable, this.s.custom = e.extend({}, TableTools.DEFAULTS, t), this.s.swfPath = this.s.custom.sSwfPath, "undefined" != typeof l && (l.moviePath = this.s.swfPath), this.s.select.type = this.s.custom.sRowSelect, this.s.select.preRowSelect = this.s.custom.fnPreRowSelect, this.s.select.postSelected = this.s.custom.fnRowSelected, this.s.select.postDeselected = this.s.custom.fnRowDeselected, this.s.custom.sSelectedClass && (this.classes.select.row = this.s.custom.sSelectedClass), this.s.tags = this.s.custom.oTags, this.s.buttonSet = this.s.custom.aButtons
                }, _fnButtonDefinations: function (t, s) {
                    for (var n, o = 0, l = t.length; l > o; o++) {
                        if ("string" == typeof t[o]) {
                            if ("undefined" == typeof TableTools.BUTTONS[t[o]]) {
                                alert("TableTools: Warning - unknown button type: " + t[o]);
                                continue
                            }
                            n = e.extend({}, TableTools.BUTTONS[t[o]], !0)
                        } else {
                            if ("undefined" == typeof TableTools.BUTTONS[t[o].sExtends]) {
                                alert("TableTools: Warning - unknown button type: " + t[o].sExtends);
                                continue
                            }
                            n = e.extend({}, TableTools.BUTTONS[t[o].sExtends], !0), n = e.extend(n, t[o], !0)
                        }
                        (n = this._fnCreateButton(n, e(s).hasClass(this.classes.collection.container))) && s.appendChild(n)
                    }
                }, _fnCreateButton: function (t, s) {
                    var n = this._fnButtonBase(t, s);
                    if (t.sAction.match(/flash/)) {
                        if (!this._fnHasFlash())return !1;
                        this._fnFlashConfig(n, t)
                    } else"text" == t.sAction ? this._fnTextConfig(n, t) : "div" == t.sAction ? this._fnTextConfig(n, t) : "collection" == t.sAction && (this._fnTextConfig(n, t), this._fnCollectionConfig(n, t));
                    return -1 !== this.s.dt.iTabIndex && e(n).attr("tabindex", this.s.dt.iTabIndex).attr("aria-controls", this.s.dt.sTableId).on("keyup.DTTT", function (t) {
                        13 === t.keyCode && (t.stopPropagation(), e(this).trigger("click"))
                    }).on("mousedown.DTTT", function (e) {
                        t.sAction.match(/flash/) || e.preventDefault()
                    }), n
                }, _fnButtonBase: function (e, t) {
                    var s, o, l;
                    t ? (s = e.sTag && "default" !== e.sTag ? e.sTag : this.s.tags.collection.button, o = e.sLinerTag && "default" !== e.sLinerTag ? e.sLiner : this.s.tags.collection.liner, l = this.classes.collection.buttons.normal) : (s = e.sTag && "default" !== e.sTag ? e.sTag : this.s.tags.button, o = e.sLinerTag && "default" !== e.sLinerTag ? e.sLiner : this.s.tags.liner, l = this.classes.buttons.normal), s = n.createElement(s), o = n.createElement(o);
                    var i = this._fnGetMasterSettings();
                    return s.className = l + " " + e.sButtonClass, s.setAttribute("id", "ToolTables_" + this.s.dt.sInstance + "_" + i.buttonCounter), s.appendChild(o), o.innerHTML = e.sButtonText, i.buttonCounter++, s
                }, _fnGetMasterSettings: function () {
                    if (this.s.master)return this.s;
                    for (var e = TableTools._aInstances, t = 0, s = e.length; s > t; t++)if (this.dom.table == e[t].s.dt.nTable)return e[t].s
                }, _fnCollectionConfig: function (e, t) {
                    var s = n.createElement(this.s.tags.collection.container);
                    s.style.display = "none", s.className = this.classes.collection.container, t._collection = s, n.body.appendChild(s), this._fnButtonDefinations(t.aButtons, s)
                }, _fnCollectionShow: function (s, o) {
                    var l = this, i = e(s).offset(), a = o._collection, c = i.left, i = i.top + e(s).outerHeight(), r = e(t).height(), h = e(n).height(), f = e(t).width(), d = e(n).width();
                    a.style.position = "absolute", a.style.left = c + "px", a.style.top = i + "px", a.style.display = "block", e(a).css("opacity", 0);
                    var u = n.createElement("div");
                    u.style.position = "absolute", u.style.left = "0px", u.style.top = "0px", u.style.height = (r > h ? r : h) + "px", u.style.width = (f > d ? f : d) + "px", u.className = this.classes.collection.background, e(u).css("opacity", 0), n.body.appendChild(u), n.body.appendChild(a), r = e(a).outerWidth(), f = e(a).outerHeight(), c + r > d && (a.style.left = d - r + "px"), i + f > h && (a.style.top = i - f - e(s).outerHeight() + "px"), this.dom.collection.collection = a, this.dom.collection.background = u, setTimeout(function () {
                        e(a).animate({opacity: 1}, 500), e(u).animate({opacity: .25}, 500)
                    }, 10), this.fnResizeButtons(), e(u).click(function () {
                        l._fnCollectionHide.call(l, null, null)
                    })
                }, _fnCollectionHide: function (t, s) {
                    null !== s && "collection" == s.sExtends || null === this.dom.collection.collection || (e(this.dom.collection.collection).animate({opacity: 0}, 500, function () {
                        this.style.display = "none"
                    }), e(this.dom.collection.background).animate({opacity: 0}, 500, function () {
                        this.parentNode.removeChild(this)
                    }), this.dom.collection.collection = null, this.dom.collection.background = null)
                }, _fnRowSelectConfig: function () {
                    if (this.s.master) {
                        var t = this, s = this.s.dt;
                        e(s.nTable).addClass(this.classes.select.table), "os" === this.s.select.type && (e(s.nTBody).on("mousedown.DTTT_Select", "tr", function (t) {
                            t.shiftKey && e(s.nTBody).css("-moz-user-select", "none").one("selectstart.DTTT_Select", "tr", function () {
                                return !1
                            })
                        }), e(s.nTBody).on("mouseup.DTTT_Select", "tr", function () {
                            e(s.nTBody).css("-moz-user-select", "")
                        })), e(s.nTBody).on("click.DTTT_Select", this.s.custom.sRowSelector, function (n) {
                            var o = "tr" === this.nodeName.toLowerCase() ? this : e(this).parents("tr")[0], l = t.s.select, i = t.s.dt.oInstance.fnGetPosition(o);
                            if (o.parentNode == s.nTBody && null !== s.oInstance.fnGetData(o)) {
                                if ("os" == l.type)if (n.ctrlKey || n.metaKey)t.fnIsSelected(o) ? t._fnRowDeselect(o, n) : t._fnRowSelect(o, n); else if (n.shiftKey) {
                                    var a = t.s.dt.aiDisplay.slice(), c = e.inArray(l.lastRow, a), r = e.inArray(i, a);
                                    if (0 === t.fnGetSelected().length || -1 === c)a.splice(e.inArray(i, a) + 1, a.length); else {
                                        if (c > r)var h = r, r = c, c = h;
                                        a.splice(r + 1, a.length), a.splice(0, c)
                                    }
                                    t.fnIsSelected(o) ? (a.splice(e.inArray(i, a), 1), t._fnRowDeselect(a, n)) : t._fnRowSelect(a, n)
                                } else t.fnIsSelected(o) && 1 === t.fnGetSelected().length ? t._fnRowDeselect(o, n) : (t.fnSelectNone(), t._fnRowSelect(o, n)); else t.fnIsSelected(o) ? t._fnRowDeselect(o, n) : "single" == l.type ? (t.fnSelectNone(), t._fnRowSelect(o, n)) : "multi" == l.type && t._fnRowSelect(o, n);
                                l.lastRow = i
                            }
                        }), s.oApi._fnCallbackReg(s, "aoRowCreatedCallback", function (n, o, l) {
                            s.aoData[l]._DTTT_selected && e(n).addClass(t.classes.select.row)
                        }, "TableTools-SelectAll")
                    }
                }, _fnRowSelect: function (t, s) {
                    var n, o, l = this._fnSelectData(t), i = [];
                    for (n = 0, o = l.length; o > n; n++)l[n].nTr && i.push(l[n].nTr);
                    if (null === this.s.select.preRowSelect || this.s.select.preRowSelect.call(this, s, i, !0)) {
                        for (n = 0, o = l.length; o > n; n++)l[n]._DTTT_selected = !0, l[n].nTr && e(l[n].nTr).addClass(this.classes.select.row);
                        null !== this.s.select.postSelected && this.s.select.postSelected.call(this, i), TableTools._fnEventDispatch(this, "select", i, !0)
                    }
                }, _fnRowDeselect: function (t, s) {
                    var n, o, l = this._fnSelectData(t), i = [];
                    for (n = 0, o = l.length; o > n; n++)l[n].nTr && i.push(l[n].nTr);
                    if (null === this.s.select.preRowSelect || this.s.select.preRowSelect.call(this, s, i, !1)) {
                        for (n = 0, o = l.length; o > n; n++)l[n]._DTTT_selected = !1, l[n].nTr && e(l[n].nTr).removeClass(this.classes.select.row);
                        null !== this.s.select.postDeselected && this.s.select.postDeselected.call(this, i), TableTools._fnEventDispatch(this, "select", i, !1)
                    }
                }, _fnSelectData: function (e) {
                    var t, s, n, o = [];
                    if (e.nodeName)t = this.s.dt.oInstance.fnGetPosition(e), o.push(this.s.dt.aoData[t]); else if ("undefined" != typeof e.length)for (s = 0, n = e.length; n > s; s++)e[s].nodeName ? (t = this.s.dt.oInstance.fnGetPosition(e[s]), o.push(this.s.dt.aoData[t])) : o.push("number" == typeof e[s] ? this.s.dt.aoData[e[s]] : e[s]); else o.push("number" == typeof e ? this.s.dt.aoData[e] : e);
                    return o
                }, _fnTextConfig: function (t, s) {
                    var n = this;
                    null !== s.fnInit && s.fnInit.call(this, t, s), "" !== s.sToolTip && (t.title = s.sToolTip), e(t).hover(function () {
                        null !== s.fnMouseover && s.fnMouseover.call(this, t, s, null)
                    }, function () {
                        null !== s.fnMouseout && s.fnMouseout.call(this, t, s, null)
                    }), null !== s.fnSelect && TableTools._fnEventListen(this, "select", function (e) {
                        s.fnSelect.call(n, t, s, e)
                    }), e(t).click(function (e) {
                        null !== s.fnClick && s.fnClick.call(n, t, s, null, e), null !== s.fnComplete && s.fnComplete.call(n, t, s, null, null), n._fnCollectionHide(t, s)
                    })
                }, _fnHasFlash: function () {
                    try {
                        if (new ActiveXObject("ShockwaveFlash.ShockwaveFlash"))return !0
                    } catch (e) {
                        if (navigator.mimeTypes && navigator.mimeTypes["application/x-shockwave-flash"] !== s && navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin)return !0
                    }
                    return !1
                }, _fnFlashConfig: function (e, t) {
                    var s = this, n = new l.Client;
                    null !== t.fnInit && t.fnInit.call(this, e, t), n.setHandCursor(!0), "flash_save" == t.sAction ? (n.setAction("save"), n.setCharSet("utf16le" == t.sCharSet ? "UTF8" : "UTF8"), n.setBomInc(t.bBomInc), n.setFileName(t.sFileName.replace("*", this.fnGetTitle(t)))) : "flash_pdf" == t.sAction ? (n.setAction("pdf"), n.setFileName(t.sFileName.replace("*", this.fnGetTitle(t)))) : n.setAction("copy"), n.addEventListener("mouseOver", function () {
                        null !== t.fnMouseover && t.fnMouseover.call(s, e, t, n)
                    }), n.addEventListener("mouseOut", function () {
                        null !== t.fnMouseout && t.fnMouseout.call(s, e, t, n)
                    }), n.addEventListener("mouseDown", function () {
                        null !== t.fnClick && t.fnClick.call(s, e, t, n)
                    }), n.addEventListener("complete", function (o, l) {
                        null !== t.fnComplete && t.fnComplete.call(s, e, t, n, l), s._fnCollectionHide(e, t)
                    }), null !== t.fnSelect && TableTools._fnEventListen(this, "select", function (n) {
                        t.fnSelect.call(s, e, t, n)
                    }), this._fnFlashGlue(n, e, t.sToolTip)
                }, _fnFlashGlue: function (e, t, s) {
                    var o = this, l = t.getAttribute("id");
                    n.getElementById(l) ? e.glue(t, s) : setTimeout(function () {
                        o._fnFlashGlue(e, t, s)
                    }, 100)
                }, _fnFlashSetText: function (e, t) {
                    var s = this._fnChunkData(t, 8192);
                    e.clearText();
                    for (var n = 0, o = s.length; o > n; n++)e.appendText(s[n])
                }, _fnColumnTargets: function (t) {
                    var s, n = [], o = this.s.dt, l = o.aoColumns;
                    if (s = l.length, "function" == typeof t)for (t = t.call(this, o), o = 0; s > o; o++)n.push(-1 !== e.inArray(o, t) ? !0 : !1); else if ("object" == typeof t) {
                        for (o = 0; s > o; o++)n.push(!1);
                        for (o = 0, s = t.length; s > o; o++)n[t[o]] = !0
                    } else if ("visible" == t)for (o = 0; s > o; o++)n.push(l[o].bVisible ? !0 : !1); else if ("hidden" == t)for (o = 0; s > o; o++)n.push(l[o].bVisible ? !1 : !0); else if ("sortable" == t)for (o = 0; s > o; o++)n.push(l[o].bSortable ? !0 : !1); else for (o = 0; s > o; o++)n.push(!0);
                    return n
                }, _fnNewline: function (e) {
                    return "auto" == e.sNewLine ? navigator.userAgent.match(/Windows/) ? "\r\n" : "\n" : e.sNewLine
                }, _fnGetDataTablesData: function (t) {
                    var s, n, l, i, a, c, r = [], h = "", f = this.s.dt, d = new RegExp(t.sFieldBoundary, "g"), u = this._fnColumnTargets(t.mColumns);
                    if (l = "undefined" != typeof t.bSelectedOnly ? t.bSelectedOnly : !1, t.bHeader) {
                        for (a = [], s = 0, n = f.aoColumns.length; n > s; s++)u[s] && (h = f.aoColumns[s].sTitle.replace(/\n/g, " ").replace(/<.*?>/g, "").replace(/^\s+|\s+$/g, ""), h = this._fnHtmlDecode(h), a.push(this._fnBoundData(h, t.sFieldBoundary, d)));
                        r.push(a.join(t.sFieldSeperator))
                    }
                    l = !0;
                    var T;
                    for (i = this.fnGetSelectedIndexes(), T = (l = "none" !== this.s.select.type && l && 0 !== i.length) ? i : o.Api ? new o.Api(f).rows(t.oSelectorOpts).indexes().flatten().toArray() : f.oInstance.$("tr", t.oSelectorOpts).map(function (e, t) {
                        return f.oInstance.fnGetPosition(t)
                    }).get(), l = 0, i = T.length; i > l; l++) {
                        for (c = f.aoData[T[l]].nTr, a = [], s = 0, n = f.aoColumns.length; n > s; s++)u[s] && (h = f.oApi._fnGetCellData(f, T[l], s, "display"), t.fnCellRender ? h = t.fnCellRender(h, s, c, T[l]) + "" : "string" == typeof h ? (h = h.replace(/\n/g, " "), h = h.replace(/<img.*?\s+alt\s*=\s*(?:"([^"]+)"|'([^']+)'|([^\s>]+)).*?>/gi, "$1$2$3"), h = h.replace(/<.*?>/g, "")) : h += "", h = h.replace(/^\s+/, "").replace(/\s+$/, ""), h = this._fnHtmlDecode(h), a.push(this._fnBoundData(h, t.sFieldBoundary, d)));
                        r.push(a.join(t.sFieldSeperator)), t.bOpenRows && (s = e.grep(f.aoOpenRows, function (e) {
                            return e.nParent === c
                        }), 1 === s.length && (h = this._fnBoundData(e("td", s[0].nTr).html(), t.sFieldBoundary, d), r.push(h)))
                    }
                    if (t.bFooter && null !== f.nTFoot) {
                        for (a = [], s = 0, n = f.aoColumns.length; n > s; s++)u[s] && null !== f.aoColumns[s].nTf && (h = f.aoColumns[s].nTf.innerHTML.replace(/\n/g, " ").replace(/<.*?>/g, ""), h = this._fnHtmlDecode(h), a.push(this._fnBoundData(h, t.sFieldBoundary, d)));
                        r.push(a.join(t.sFieldSeperator))
                    }
                    return r.join(this._fnNewline(t))
                }, _fnBoundData: function (e, t, s) {
                    return "" === t ? e : t + e.replace(s, t + t) + t
                }, _fnChunkData: function (e, t) {
                    for (var s = [], n = e.length, o = 0; n > o; o += t)s.push(n > o + t ? e.substring(o, o + t) : e.substring(o, n));
                    return s
                }, _fnHtmlDecode: function (e) {
                    if (-1 === e.indexOf("&"))return e;
                    var t = n.createElement("div");
                    return e.replace(/&([^\s]*?);/g, function (e, s) {
                        return "#" === e.substr(1, 1) ? String.fromCharCode(Number(s.substr(1))) : (t.innerHTML = e, t.childNodes[0].nodeValue)
                    })
                }, _fnPrintStart: function (s) {
                    var o = this, l = this.s.dt;
                    this._fnPrintHideNodes(l.nTable), this.s.print.saveStart = l._iDisplayStart, this.s.print.saveLength = l._iDisplayLength, s.bShowAll && (l._iDisplayStart = 0, l._iDisplayLength = -1, l.oApi._fnCalculateEnd && l.oApi._fnCalculateEnd(l), l.oApi._fnDraw(l)), ("" !== l.oScroll.sX || "" !== l.oScroll.sY) && (this._fnPrintScrollStart(l), e(this.s.dt.nTable).bind("draw.DTTT_Print", function () {
                        o._fnPrintScrollStart(l)
                    }));
                    var i, a = l.aanFeatures;
                    for (i in a)if ("i" != i && "t" != i && 1 == i.length)for (var c = 0, r = a[i].length; r > c; c++)this.dom.print.hidden.push({
                        node: a[i][c],
                        display: "block"
                    }), a[i][c].style.display = "none";
                    e(n.body).addClass(this.classes.print.body), "" !== s.sInfo && this.fnInfo(s.sInfo, 3e3), s.sMessage && e("<div/>").addClass(this.classes.print.message).html(s.sMessage).prependTo("body"), this.s.print.saveScroll = e(t).scrollTop(), t.scrollTo(0, 0), e(n).bind("keydown.DTTT", function (e) {
                        27 == e.keyCode && (e.preventDefault(), o._fnPrintEnd.call(o, e))
                    })
                }, _fnPrintEnd: function (s) {
                    s = this.s.dt;
                    var o = this.s.print;
                    this._fnPrintShowNodes(), ("" !== s.oScroll.sX || "" !== s.oScroll.sY) && (e(this.s.dt.nTable).unbind("draw.DTTT_Print"), this._fnPrintScrollEnd()), t.scrollTo(0, o.saveScroll), e("div." + this.classes.print.message).remove(), e(n.body).removeClass("DTTT_Print"), s._iDisplayStart = o.saveStart, s._iDisplayLength = o.saveLength, s.oApi._fnCalculateEnd && s.oApi._fnCalculateEnd(s), s.oApi._fnDraw(s), e(n).unbind("keydown.DTTT")
                }, _fnPrintScrollStart: function () {
                    var t = this.s.dt;
                    t.nScrollHead.getElementsByTagName("div")[0].getElementsByTagName("table");
                    var s, n = t.nTable.parentNode;
                    s = t.nTable.getElementsByTagName("thead"), 0 < s.length && t.nTable.removeChild(s[0]), null !== t.nTFoot && (s = t.nTable.getElementsByTagName("tfoot"), 0 < s.length && t.nTable.removeChild(s[0])), s = t.nTHead.cloneNode(!0), t.nTable.insertBefore(s, t.nTable.childNodes[0]), null !== t.nTFoot && (s = t.nTFoot.cloneNode(!0), t.nTable.insertBefore(s, t.nTable.childNodes[1])), "" !== t.oScroll.sX && (t.nTable.style.width = e(t.nTable).outerWidth() + "px", n.style.width = e(t.nTable).outerWidth() + "px", n.style.overflow = "visible"), "" !== t.oScroll.sY && (n.style.height = e(t.nTable).outerHeight() + "px", n.style.overflow = "visible")
                }, _fnPrintScrollEnd: function () {
                    var e = this.s.dt, t = e.nTable.parentNode;
                    "" !== e.oScroll.sX && (t.style.width = e.oApi._fnStringToCss(e.oScroll.sX), t.style.overflow = "auto"), "" !== e.oScroll.sY && (t.style.height = e.oApi._fnStringToCss(e.oScroll.sY), t.style.overflow = "auto")
                }, _fnPrintShowNodes: function () {
                    for (var e = this.dom.print.hidden, t = 0, s = e.length; s > t; t++)e[t].node.style.display = e[t].display;
                    e.splice(0, e.length)
                }, _fnPrintHideNodes: function (t) {
                    for (var s = this.dom.print.hidden, n = t.parentNode, o = n.childNodes, l = 0, i = o.length; i > l; l++)if (o[l] != t && 1 == o[l].nodeType) {
                        var a = e(o[l]).css("display");
                        "none" != a && (s.push({node: o[l], display: a}), o[l].style.display = "none")
                    }
                    "BODY" != n.nodeName.toUpperCase() && this._fnPrintHideNodes(n)
                }
            }, TableTools._aInstances = [], TableTools._aListeners = [], TableTools.fnGetMasters = function () {
                for (var e = [], t = 0, s = TableTools._aInstances.length; s > t; t++)TableTools._aInstances[t].s.master && e.push(TableTools._aInstances[t]);
                return e
            }, TableTools.fnGetInstance = function (e) {
                "object" != typeof e && (e = n.getElementById(e));
                for (var t = 0, s = TableTools._aInstances.length; s > t; t++)if (TableTools._aInstances[t].s.master && TableTools._aInstances[t].dom.table == e)return TableTools._aInstances[t];
                return null
            }, TableTools._fnEventListen = function (e, t, s) {
                TableTools._aListeners.push({that: e, type: t, fn: s})
            }, TableTools._fnEventDispatch = function (e, t, s, n) {
                for (var o = TableTools._aListeners, l = 0, i = o.length; i > l; l++)e.dom.table == o[l].that.dom.table && o[l].type == t && o[l].fn(s, n)
            }, TableTools.buttonBase = {
                sAction: "text",
                sTag: "default",
                sLinerTag: "default",
                sButtonClass: "DTTT_button_text",
                sButtonText: "Button text",
                sTitle: "",
                sToolTip: "",
                sCharSet: "utf8",
                bBomInc: !1,
                sFileName: "*.csv",
                sFieldBoundary: "",
                sFieldSeperator: "	",
                sNewLine: "auto",
                mColumns: "all",
                bHeader: !0,
                bFooter: !0,
                bOpenRows: !1,
                bSelectedOnly: !1,
                oSelectorOpts: s,
                fnMouseover: null,
                fnMouseout: null,
                fnClick: null,
                fnSelect: null,
                fnComplete: null,
                fnInit: null,
                fnCellRender: null
            }, TableTools.BUTTONS = {
                csv: e.extend({}, TableTools.buttonBase, {
                    sAction: "flash_save",
                    sButtonClass: "DTTT_button_csv",
                    sButtonText: "CSV",
                    sFieldBoundary: '"',
                    sFieldSeperator: ",",
                    fnClick: function (e, t, s) {
                        this.fnSetText(s, this.fnGetTableData(t))
                    }
                }),
                xls: e.extend({}, TableTools.buttonBase, {
                    sAction: "flash_save",
                    sCharSet: "utf8",
                    bBomInc: !0,
                    sButtonClass: "DTTT_button_xls",
                    sButtonText: "Excel",
                    fnClick: function (e, t, s) {
                        this.fnSetText(s, this.fnGetTableData(t))
                    }
                }),
                copy: e.extend({}, TableTools.buttonBase, {
                    sAction: "flash_copy",
                    sButtonClass: "DTTT_button_copy",
                    sButtonText: "نسخ",
                    fnClick: function (e, t, s) {
                        this.fnSetText(s, this.fnGetTableData(t))
                    },
                    fnComplete: function (e, t, s, n) {
                        e = n.split("\n").length, t.bHeader && e--, null !== this.s.dt.nTFoot && t.bFooter && e--, this.fnInfo("<h6>Table copied</h6><p>Copied " + e + " row" + (1 == e ? "" : "s") + " to the clipboard.</p>", 1500)
                    }
                }),
                pdf: e.extend({}, TableTools.buttonBase, {
                    sAction: "flash_pdf",
                    sNewLine: "\n",
                    sFileName: "*.pdf",
                    sButtonClass: "DTTT_button_pdf",
                    sButtonText: "PDF",
                    sPdfOrientation: "portrait",
                    sPdfSize: "A4",
                    sPdfMessage: "",
                    fnClick: function (e, t, s) {
                        this.fnSetText(s, "title:" + this.fnGetTitle(t) + "\nmessage:" + t.sPdfMessage + "\ncolWidth:" + this.fnCalcColRatios(t) + "\norientation:" + t.sPdfOrientation + "\nsize:" + t.sPdfSize + "\n--/TableToolsOpts--\n" + this.fnGetTableData(t))
                    }
                }),
                print: e.extend({}, TableTools.buttonBase, {
                    sInfo: "<h6>Print view</h6><p>Please use your browser's print function to print this table. Press escape when finished.</p>",
                    sMessage: null,
                    bShowAll: !0,
                    sToolTip: "View print view",
                    sButtonClass: "DTTT_button_print",
                    sButtonText: "طباعة",
                    fnClick: function (e, t) {
                        this.fnPrint(!0, t)
                    }
                }),
                text: e.extend({}, TableTools.buttonBase),
                select: e.extend({}, TableTools.buttonBase, {
                    sButtonText: "Select button", fnSelect: function (t) {
                        0 !== this.fnGetSelected().length ? e(t).removeClass(this.classes.buttons.disabled) : e(t).addClass(this.classes.buttons.disabled)
                    }, fnInit: function (t) {
                        e(t).addClass(this.classes.buttons.disabled)
                    }
                }),
                select_single: e.extend({}, TableTools.buttonBase, {
                    sButtonText: "Select button",
                    fnSelect: function (t) {
                        1 == this.fnGetSelected().length ? e(t).removeClass(this.classes.buttons.disabled) : e(t).addClass(this.classes.buttons.disabled)
                    },
                    fnInit: function (t) {
                        e(t).addClass(this.classes.buttons.disabled)
                    }
                }),
                select_all: e.extend({}, TableTools.buttonBase, {
                    sButtonText: "Select all", fnClick: function () {
                        this.fnSelectAll()
                    }, fnSelect: function (t) {
                        this.fnGetSelected().length == this.s.dt.fnRecordsDisplay() ? e(t).addClass(this.classes.buttons.disabled) : e(t).removeClass(this.classes.buttons.disabled)
                    }
                }),
                select_none: e.extend({}, TableTools.buttonBase, {
                    sButtonText: "Deselect all", fnClick: function () {
                        this.fnSelectNone()
                    }, fnSelect: function (t) {
                        0 !== this.fnGetSelected().length ? e(t).removeClass(this.classes.buttons.disabled) : e(t).addClass(this.classes.buttons.disabled)
                    }, fnInit: function (t) {
                        e(t).addClass(this.classes.buttons.disabled)
                    }
                }),
                ajax: e.extend({}, TableTools.buttonBase, {
                    sAjaxUrl: "/xhr.php",
                    sButtonText: "Ajax button",
                    fnClick: function (t, s) {
                        var n = this.fnGetTableData(s);
                        e.ajax({
                            url: s.sAjaxUrl,
                            data: [{name: "tableData", value: n}],
                            success: s.fnAjaxComplete,
                            dataType: "json",
                            type: "POST",
                            cache: !1,
                            error: function () {
                                alert("Error detected when sending table data to server")
                            }
                        })
                    },
                    fnAjaxComplete: function () {
                        alert("Ajax complete")
                    }
                }),
                div: e.extend({}, TableTools.buttonBase, {
                    sAction: "div",
                    sTag: "div",
                    sButtonClass: "DTTT_nonbutton",
                    sButtonText: "Text button"
                }),
                collection: e.extend({}, TableTools.buttonBase, {
                    sAction: "collection",
                    sButtonClass: "DTTT_button_collection",
                    sButtonText: "Collection",
                    fnClick: function (e, t) {
                        this._fnCollectionShow(e, t)
                    }
                })
            }, TableTools.buttons = TableTools.BUTTONS, TableTools.classes = {
                container: "DTTT_container",
                buttons: {normal: "DTTT_button", disabled: "DTTT_disabled"},
                collection: {
                    container: "DTTT_collection",
                    background: "DTTT_collection_background",
                    buttons: {normal: "DTTT_button", disabled: "DTTT_disabled"}
                },
                select: {table: "DTTT_selectable", row: "DTTT_selected selected"},
                print: {body: "DTTT_Print", info: "DTTT_print_info", message: "DTTT_PrintMessage"}
            }, TableTools.classes_themeroller = {
                container: "DTTT_container ui-buttonset ui-buttonset-multi",
                buttons: {normal: "DTTT_button ui-button ui-state-default"},
                collection: {container: "DTTT_collection ui-buttonset ui-buttonset-multi"}
            }, TableTools.DEFAULTS = {
                sSwfPath: "../swf/copy_csv_xls_pdf.swf",
                sRowSelect: "none",
                sRowSelector: "tr",
                sSelectedClass: null,
                fnPreRowSelect: null,
                fnRowSelected: null,
                fnRowDeselected: null,
                aButtons: ["copy", "csv", "xls", "pdf", "print"],
                oTags: {
                    container: "div",
                    button: "a",
                    liner: "span",
                    collection: {container: "div", button: "a", liner: "span"}
                }
            }, TableTools.defaults = TableTools.DEFAULTS, TableTools.prototype.CLASS = "TableTools", TableTools.version = "2.2.4", e.fn.dataTable.Api && e.fn.dataTable.Api.register("tabletools()", function () {
                var e = null;
                return 0 < this.context.length && (e = TableTools.fnGetInstance(this.context[0].nTable)), e
            }), "function" == typeof e.fn.dataTable && "function" == typeof e.fn.dataTableExt.fnVersionCheck && e.fn.dataTableExt.fnVersionCheck("1.9.0") ? e.fn.dataTableExt.aoFeatures.push({
                fnInit: function (e) {
                    var t = e.oInit;
                    return new TableTools(e.oInstance, t ? t.tableTools || t.oTableTools || {} : {}).dom.container
                }, cFeature: "T", sFeature: "TableTools"
            }) : alert("Warning: TableTools requires DataTables 1.9.0 or newer - www.datatables.net/download"), e.fn.DataTable.TableTools = TableTools
        }(jQuery, e, t), "function" == typeof n.fn.dataTable && "function" == typeof n.fn.dataTableExt.fnVersionCheck && n.fn.dataTableExt.fnVersionCheck("1.9.0") ? n.fn.dataTableExt.aoFeatures.push({
            fnInit: function (e) {
                return e = new TableTools(e.oInstance, "undefined" != typeof e.oInit.oTableTools ? e.oInit.oTableTools : {}), TableTools._aInstances.push(e), e.dom.container
            }, cFeature: "T", sFeature: "TableTools"
        }) : alert("Warning: TableTools 2 requires DataTables 1.9.0 or newer - www.datatables.net/download"), n.fn.dataTable.TableTools = TableTools, n.fn.DataTable.TableTools = TableTools
    };
    "function" == typeof define && define.amd ? define(["jquery", "datatables"], n) : "object" == typeof exports ? n(require("jquery"), require("datatables")) : jQuery && !jQuery.fn.dataTable.TableTools && n(jQuery, jQuery.fn.dataTable)
}(window, document);