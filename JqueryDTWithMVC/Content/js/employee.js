/**
 * Created by Khalid Bj on 10/11/2015.
 */
$(document).ready(function () {
    var _body = $('body'),
        _html = $('html'),
        _container = $('#container');
    var ListEmp, ListAllCheckPoints, ListTimes, ListAllReports, ListManagers, ListBranches;
    var regIsEmail = new RegExp("^[0-9a-zA-Z._]+@[0-9a-zA-Z-]+[\.]{1}[0-9a-zA-Z]+[\.]?[0-9a-zA-Z]+$");

    $('#ViewPoints').click(function () {
        $(".navbar-collapse").collapse('hide');
        _container.html('<section id="MainSection">  ' +
            '<div><h1>دخول و خروج</h1></div> ' +
            '<div class="btn_c well">  ' +
            '<div class="row">  ' +
            '<div class="col-sm-5">  ' +
            '<button class="btn btn-default" id="RefreshEmp"><span class="icon-spin3"></span></button>  ' +
            '</div>  ' +
            '<div class="col-sm-3">  ' +
            '<input type="text" class="form-control" name="fromdate" id="fromdate" />' +
            '<input type="text" class="form-control" name="todate" id="todate" />' +
            '<button class="btn btn-info" id="LoadPointBtn">عرض</button>' +
            '</div>  ' +

            '<div class="col-md-3">  ' +
            '<div><span>تأريخ  : </span><span id="reportDate"></span></div>' +
            '<div><span>عدد الايام  : </span><span id="dateCount"></span></div>' +
            '</div>  ' +
            '</div>  ' +

            '</div>  ' +
            '<table id="AllCheckPoints" class="table table-striped table-bordered">  ' +
            '<thead> ' +
            '<tr> ' +
            '<th>الموظف</th> ' +
            '<th>التأريخ</th> ' +
            '<th>اليوم</th> ' +
            '<th>نوعها</th> ' +
            '<th>الدخول</th> ' +
            '<th>الخروج</th> ' +
            '<th>التأخير</th> ' +
            '</tr> ' +
            '</thead> ' +
            '<tfoot> ' +
            '<tr> ' +
            '<th></th> ' +
            '<th></th> ' +
            '<th></th> ' +
            '<th></th> ' +
            '<th></th> ' +
            '<th></th> ' +
            '<th></th> ' +
            '</tr> ' +
            '</tfoot> ' +
            '</table></section>');

        var RefreshEmp = $('#RefreshEmp'),
            LoadPointBtn = $('#LoadPointBtn'),
            fromdate = $('#fromdate'),
            todate = $('#todate'),
            reportDate = $('#reportDate'),
            dateCount = $('#dateCount');

        fromdate.datetimepicker({
            pickTime: false,
            format: 'YYYY-MM-DD'
        });
        todate.datetimepicker({
            pickTime: false,
            format: 'YYYY-MM-DD'
        });

        ListAllCheckPoints = $("#AllCheckPoints").dataTable({
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var TotalCost = 0;
                for (var i = iStart; i < iEnd; i++) {
                    var aDate = aaData[aiDisplay[i]]['CheckRLength'];
                    if ($.isNumeric(aDate))
                        TotalCost += parseFloat(aDate);
                }
                /* Modify the footer row to match what we want */
                var nCells = nRow.getElementsByTagName('th');
                nCells[6].innerHTML = TotalCost.toFixed(2);
            },
            "rowCallback": function (row, data) {

            },
            "aoColumnDefs": [{
                "aTargets": [3],
                "mRender": function (data, type, full) {
                    switch (full.CheckType) {

                            case '0':

                                return '<span class="label label-primary">' + data + '</span>';

                            case '1':

                                return '<span class="label label-info">' + data + '</span>';

                                break;

                            case '2':

                                return '<span class="label label-success">' + data + '</span>';

                                break;

                            case '3':

                                return '<span class="label label-warning">' + data + '</span>';

                                break;

                            case '4':

                                return '<span class="label label-default">' + data + '</span>';

                                break;

                            case '5':

                                return '<span class="label label-danger">' + data + '</span>';

                                break;

                            case '6':

                                return '<span class="label label-default">' + data + '</span>';

                                break;

                            case '7':

                                return '<span class="label label-default">' + data + '</span>';

                                break;

                            case '8':

                                return '<span class="label label-info">' + data + '</span>';

                                break;
                    }
                }
            }, {
                "aTargets": [6],
                "mRender": function (data, type, full) {
                    if (data > 0) {
                        return data + ' دقيقة ';
                    } else {
                        return '';
                    }
                }
            }],
            "columns": [
                {"data": "EmployeeAName"},
                {"data": "CheckDate"},
                {"data": "CheckDay"},
                {"data": "CheckName"},
                {"data": "CheckTimeIn"},
                {"data": "CheckTimeOut"},
                {"data": "CheckRLength"}
            ],
            "aaSorting": [[1, 'asc']],
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            aLengthMenu: [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": true
        });
        LoadPointBtn.click(function () {
            if (fromdate.val().length <= 0) {
                generate('warning', 'يجب أختيار الفترات !', 2000);
                fromdate.focus();
                return;
            }
            if (todate.val().length <= 0) {
                generate('warning', 'يجب أختيار الفترات !', 2000);
                todate.focus();
                return;
            }

            $.ajax({
                url: "options.html",
                type: "post",
                data: {
                    action: 'AllCheckPoints',
                    TimeDateFrom: fromdate.val(),
                    TimeDateTo: todate.val()
                }
            })
                .done(function (result) {
                    ListAllCheckPoints.api().clear().draw();
                    ListAllCheckPoints.api().rows.add(result.data).draw();
                    reportDate.text(result.date);
                    var _dates = result.date.split('الى');
                    var _date1 = _dates[0].split('-');
                    var _date2 = _dates[1].split('-');

                    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                    var firstDate = new Date(_date1[0], _date1[1], _date1[2]);
                    var secondDate = new Date(_date2[0], _date2[1], _date2[2]);

                    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
                    dateCount.text(diffDays);
                    console.clear();
                });
        });
        console.clear();
        RefreshEmp.click(function () {
            LoadPointBtn.click();
            console.clear();
        });

    });
    $('#ViewTimes').click(function () {
        $(".navbar-collapse").collapse('hide');
        _container.html('<section id="MainSection">  ' +
            '<div><h1>معدل الاوقات</h1></div> ' +
            '<div class="btn_c well">  ' +

            '<div class="row">  ' +
            '<div class="col-sm-4">  ' +
            '<button class="btn btn-default" id="RefreshEmp"><span class="icon-spin3"></span></button>  ' +
            '</div>  ' +
            '<div class="col-sm-3">  ' +
            '<input type="text" class="form-control" name="fromdate" id="fromdate" />' +
            '<input type="text" class="form-control" name="todate" id="todate" />' +
            '<button class="btn btn-info" id="LoadPointBtn">عرض</button>' +
            '</div>  ' +

            '<div class="col-md-5">  ' +
            '<div><span>تأريخ  : </span><span id="reportDate"></span></div>' +
            '<div><span>عدد الايام  : </span><span id="dateCount"></span></div>' +
            '</div>  ' +
            '</div>  ' +

            '</div>  ' +
            '</div>  ' +
            '<table id="AllTimesResult" class="table table-striped table-bordered">  ' +
            '<thead> ' +
            '<tr> ' +
            '<th>الموظف</th> ' +
            '<th>نوعها</th> ' +
            '<th>المدة</th> ' +
            '</tr> ' +
            '</thead> ' +
            '</table></section>');

        var RefreshEmp = $('#RefreshEmp'),
            LoadPointBtn = $('#LoadPointBtn'),
            fromdate = $('#fromdate'),
            todate = $('#todate'),
            dateCount = $('#dateCount'),
            reportDate = $('#reportDate');
        fromdate.datetimepicker({
            pickTime: false,
            format: 'YYYY-MM-DD'
        });
        todate.datetimepicker({
            pickTime: false,
            format: 'YYYY-MM-DD'
        });
        ListTimes = $("#AllTimesResult").dataTable({
            "rowCallback": function (row, data) {

            }, "aoColumnDefs": [{
                "aTargets": [1],
                "mRender": function (data, type, full) {
                    switch (full.CheckRType) {

                            case '0':

                                return '<span class="label label-primary">' + data + '</span>';

                            case '1':

                                return '<span class="label label-info">' + data + '</span>';

                                break;

                            case '2':

                                return '<span class="label label-success">' + data + '</span>';

                                break;

                            case '3':

                                return '<span class="label label-warning">' + data + '</span>';

                                break;

                            case '4':

                                return '<span class="label label-default">' + data + '</span>';

                                break;

                            case '5':

                                return '<span class="label label-danger">' + data + '</span>';

                                break;

                            case '6':

                                return '<span class="label label-default">' + data + '</span>';

                                break;

                            case '7':

                                return '<span class="label label-default">' + data + '</span>';

                                break;

                            case '8':

                                return '<span class="label label-info">' + data + '</span>';

                                break;
                    }
                }
            }, {
                "aTargets": [2],
                "mRender": function (data, type, full) {
                    if (data.length > 0) {
                        return data + ' دقيقة ';
                    } else {
                        return '';
                    }
                }
            }],
            "columns": [
                {"data": "EmployeeAName"},
                {"data": "CheckName"},
                {"data": "CheckRLength"}
            ],
            "aaSorting": [[1, 'asc']],
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            aLengthMenu: [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": true
        });
        LoadPointBtn.click(function () {
            if (fromdate.val().length <= 0) {
                generate('warning', 'يجب أختيار التأريخ !', 2000);
                fromdate.focus();
                return;
            }
            if (todate.val().length <= 0) {
                generate('warning', 'يجب أختيار التأريخ !', 2000);
                todate.focus();
                return;
            }

            $.ajax({
                url: "options.html",
                type: "post",
                data: {
                    action: 'AllTimes',
                    TimeDateFrom: fromdate.val(),
                    TimeDateTo: todate.val()
                }
            })
                .done(function (result) {
                    ListTimes.api().clear().draw();
                    reportDate.text(result.date);
                    ListTimes.api().rows.add(result.data).draw();
                    var _dates = result.date.split('الى');
                    var _date1 = _dates[0].split('-');
                    var _date2 = _dates[1].split('-');

                    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                    var firstDate = new Date(_date1[0], _date1[1], _date1[2]);
                    var secondDate = new Date(_date2[0], _date2[1], _date2[2]);

                    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
                    dateCount.text(diffDays);
                    console.clear();
                });
        });
        RefreshEmp.click(function () {
            LoadPointBtn.click();
            console.clear();
        });

    });
    $('#ViewReports').click(function () {
        $(".navbar-collapse").collapse('hide');
        _container.html('<section id="MainSection">  ' +
            '<div><h1>التقارير</h1></div> ' +
            '<div class="btn_c well">  ' +
            '<div class="row">  ' +
            '<div class="col-sm-4">  ' +
            '<button class="btn btn-default" id="RefreshEmp"><span class="icon-spin3"></span></button>  ' +
            '</div>  ' +
            '<div class="col-sm-3">  ' +
            '<input type="text" class="form-control" name="fromdate" id="fromdate" />' +
            '<input type="text" class="form-control" name="todate" id="todate" />' +
            '<button class="btn btn-info" id="LoadPointBtn">عرض</button>' +
            '</div>  ' +
            '<div class="col-md-5">  ' +
            '<div id="thisReport"><span>تأريخ التقرير :</span><span id="reportDate"></span></div>' +
            '<div><span>عدد الايام  : </span><span id="dateCount"></span></div>' +
            '</div>  ' +
            '</div>  ' +
            '</div>  ' +

            '</div>  ' +
            '<table id="AllReports" class="table table-striped table-bordered">  ' +
            '<thead> ' +
            '<tr> ' +
            '<th>الموظف</th> ' +
            '<th>الايام المطلوبة</th> ' +
            '<th>الدقائق المطلوبة</th> ' +
            '<th>دقائق الحضور</th> ' +
            '<th>دقائق الاجازات</th> ' +
            '<th>دقائق الغيابات</th> ' +
            '<th>ساعات الغيابات</th> ' +
            '<th>عدد الاجازات</th> ' +
            '<th>عدد الغيابات</th> ' +
            '</tr> ' +
            '</thead> ' +
            '</table></section>');

        var RefreshEmp = $('#RefreshEmp'),
            LoadPointBtn = $('#LoadPointBtn'),
            fromdate = $('#fromdate'),
            todate = $('#todate'),
            dateCount = $('#dateCount'),
            reportDate = $('#reportDate');

        fromdate.datetimepicker({
            pickTime: false,
            format: 'YYYY-MM-DD'
        });
        todate.datetimepicker({
            pickTime: false,
            format: 'YYYY-MM-DD'
        });
        ListAllReports = $("#AllReports").dataTable({
            dom: 'Bfrtip',
            buttons: [
                'print'
            ],
            "rowCallback": function (row, data) {

            }, "aoColumnDefs": [{
                "aTargets": [1],
                "mRender": function (data, type, full) {
                    return data;
                }
            }, {
                "aTargets": [2],
                "mRender": function (data, type, full) {
                    return data;
                }
            }, {
                "aTargets": [6],
                "mRender": function (data, type, full) {
                    return (data > 0) ? (data / 60).toFixed(2) : data;
                }
            }],
            "columns": [
                {"data": "EmployeeAName"},
                {"data": "CountDays"},
                {"data": "WorkTime"},
                {"data": "WorkedTime"},
                {"data": "Rest"},
                {"data": "Absence"},
                {"data": "Absence"},
                {"data": "RestCount"},
                {"data": "AbsenceCount"}
            ],
            "aaSorting": [[0, 'asc']],
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "aLengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": true
        });
        LoadPointBtn.click(function () {
            if (fromdate.val().length <= 0) {
                generate('warning', 'يجب أختيار الفترات !', 2000);
                fromdate.focus();
                return;
            }
            if (todate.val().length <= 0) {
                generate('warning', 'يجب أختيار الفترات !', 2000);
                todate.focus();
                return;
            }
            $.ajax({
                url: "options.html",
                type: "post",
                data: {
                    action: 'ReportAllEmployee',
                    TimeDateFrom: fromdate.val(),
                    TimeDateTo: todate.val()
                }
            })
                .done(function (result) {
                    ListAllReports.api().clear().draw();
                    ListAllReports.api().rows.add(result.data).draw();
                    reportDate.text(result.date);

                    var _dates = result.date.split('الى');
                    var _date1 = _dates[0].split('-');
                    var _date2 = _dates[1].split('-');

                    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                    var firstDate = new Date(_date1[0], _date1[1], _date1[2]);
                    var secondDate = new Date(_date2[0], _date2[1], _date2[2]);

                    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
                    dateCount.text(diffDays);
                    console.clear();
                });
        });
        RefreshEmp.click(function () {
            LoadPointBtn.click();
            console.clear();
        });
    });
    $('#ChangePassword').click(function () {
        _body.append('<div class="modal fade" id="PasswordModel" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="PasswordModelLabel" aria-hidden="true" xmlns="http://www.w3.org/1999/html">' +
            '<div class="modal-dialog modal-md"> ' +
            '<div class="modal-content"> ' +
            '<div class="modal-header"> ' +
            '<h4 class="modal-title">تغير رمز الدخول</h4>' +
            '</div>' +
            '<div class="modal-body">' +
            '<form class="Main_frm">  ' +
            '<table> ' +
            '<tbody> ' +
            '<tr> ' +
            '<td>الحالي :</td> ' +
            '<td><div class="input-group">' +
            '<span class="input-group-addon" id="PasswordOld-icon"><span class="icon-lock-1"></span></span> ' +
            '<input placeholder="الرمز الحالي" dir="auto" class="form-control" type="text" name="PasswordOld" id="PasswordOld" aria-describedby="PasswordOld-icon"/>' +
            '</div></td> ' +
            '</tr> ' +
            '<tr> ' +
            '<td>الجديد :</td> ' +
            '<td><div class="input-group"> ' +
            '<span class="input-group-addon" id="PasswordNew-icon"><span class="icon-lock-2"></span></span> ' +
            '<input  placeholder="الرمز الجديد" dir="auto" class="form-control" type="text" name="PasswordNew" id="PasswordNew" aria-describedby="PasswordNew-icon"/>' +
            '</div></td> ' +
            '</tr> ' +
            '</tbody> ' +
            '</table> ' +
            '</form> ' +
            '</div>' +
            '<div class="modal-footer"> ' +
            '<button id="ConfirmChange" type="button" class="btn btn-info">تغير</button> ' +
            '<button type="button" class="btn btn-default" data-dismiss="modal">أغلاق</button> ' +
            '</div> ' +
            '</div> ' +
            '</div> ' +
            '</div>');

        $('#PasswordModel').on('show.bs.modal', function (e) {
            var ConfirmChange = $('#ConfirmChange');
            var PasswordOld = $('#PasswordOld');
            var PasswordNew = $('#PasswordNew');
            $(PasswordNew).keydown(function (ev) {
                if (ev.keyCode == 13) {
                    ConfirmChange.click();
                }
            });

            $(PasswordOld).keydown(function (ev) {
                if (ev.keyCode == 13) {
                    ConfirmChange.click();
                }
            });

            ConfirmChange.click(function () {
                if (PasswordOld.val().length <= 0) {
                    generate('warning', 'يجب أدخال الرمز القديم !', 2000);
                    PasswordOld.focus();
                    return false;
                }
                if (PasswordNew.val().length <= 0) {
                    generate('warning', 'يجب أدخال الرمز القديم !', 2000);
                    PasswordNew.focus();
                    return false;
                } else {
                    if (PasswordNew.val().length <= 7) {
                        generate('warning', 'يجب أن يكون الرمز أطول من 8 حروف و أرقام !', 2000);
                        PasswordNew.focus();
                        return false;
                    }
                }

                $.ajax({
                    method: "POST",
                    url: "options.html",
                    data: {
                        action: 'changePasswordEm',
                        PasswordOld: PasswordOld.val(),
                        PasswordNew: PasswordNew.val()
                    }
                })
                    .done(function (ret) {
                        switch (ret) {
                            case 'old':
                                generate('error', 'الرمز القديم غير مطابق !', 2000);
                                PasswordOld.focus();
                                break;
                            case 'done':
                                $('#PasswordModel').modal('hide');
                                generate('success', 'تم التغير بنجاح !', 2000);
                                break;
                            default:
                                generate('error', 'صل خطأ ما يرجى اعادة تحميل الصفحة !', 2000);
                                break;
                        }
                        console.clear();
                    });
            });
        }).on('hidden.bs.modal', function (e) {
            $(this).remove();
        }).modal('show');


    });
});