/**
 * Created by Khalid Bj on 10/11/2015.
 */
$(document).ready(function(){
    var _body = $('body'),
        _html = $('html'),
        _container = $('#container');
    var ListEmp, ListAllCheckPoints, ListTimes, ListAllReports, ListManagers, ListBranches;
    var regIsEmail = new RegExp("^[0-9a-zA-Z._]+@[0-9a-zA-Z-]+[\.]{1}[0-9a-zA-Z]+[\.]?[0-9a-zA-Z]+$");

    $('#login_frm').submit(function (ev) {
        ev.preventDefault();
        var userEmail = $('#Email');
        var userPassword = $('#Password');
        var _this = $(this);

        if (userEmail.val().trim().length <= 0 || !regIsEmail.test(userEmail.val())) {
            userEmail.focus();
            generate('warning', 'يرجى كتابة البريد بشكل صحيح !', 2000);
            return;
        }
        if (userPassword.val().length <= 0) {
            userPassword.focus();
            generate('warning', 'يرجى كتابة رمز المرور بشكل صحيح !', 2000);
            return;
        }
        $.ajax({
            method: "POST",
            url: "",
            data: {
                action: "getLogin",
                Email: userEmail.val(),
                Password: userPassword.val()
            }
        }).done(function (msg) {
            switch (msg) {
                case 'locked':
                    generate('warning', 'تم تعطيل هذا الحساب !', 2000);
                    break;
                case 'login':
                    window.location = 'index.html';
                    break;
                case 'no':
                    userEmail.focus();
                    generate('warning', 'يرجى التأكد من معلومات المستخدم !', 2000);
                    break;
            }
            console.clear();
        });
    });
    $('#getLogout').click(function () {
        $.ajax({
            method: "POST",
            url: "options.html",
            data: {
                action: "getLogout"
            }
        }).done(function (msg) {
            window.location = 'login.html';
            console.clear();
        });
    });

});